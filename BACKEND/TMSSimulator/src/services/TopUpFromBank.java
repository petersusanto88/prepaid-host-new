package services;

import java.io.IOException;
import java.net.Socket;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;

import utility.Utility;

public class TopUpFromBank implements Constants {

	private Socket socket;
	private MessageFactory mfact;
	private Utility util = new Utility();
	
	public TopUpFromBank( Socket socket, MessageFactory messagefactory ){
		
		this.socket = socket;
		this.mfact  = messagefactory;
		
	}
	
	private String generateField48( String fromAccountNumber,
									String toAccountNumber,
									String topupAmount,
									String lastBalance,
									String topUpPendingBalance,
									String cardBalance){
		
		String result			= "";
		String refNumber 		= multiSocket.util.randomAlphaNumeric(18);
		
		for( int i = fromAccountNumber.length() + 1; i <= 18; i++ ){
			fromAccountNumber 			= ( new StringBuilder() ).append(fromAccountNumber).append(" ").toString();
		}
		
		for( int i = toAccountNumber.length() + 1; i <= 18; i++ ){
			toAccountNumber 			= ( new StringBuilder() ).append(toAccountNumber).append(" ").toString();
		}
		
		for( int i = topupAmount.length() + 1; i <= 12; i++ ){
			topupAmount 			= ( new StringBuilder() ).append("0").append(topupAmount).toString();
		}
		
		for( int i = lastBalance.length() + 1; i <= 12; i++ ){
			lastBalance 			= ( new StringBuilder() ).append("0").append(lastBalance).toString();
		}
		
		for( int i = topUpPendingBalance.length() + 1; i <= 12; i++ ){
			topUpPendingBalance 			= ( new StringBuilder() ).append("0").append(topUpPendingBalance).toString();
		}
		
		for( int i = cardBalance.length() + 1; i <= 12; i++ ){
			cardBalance 			= ( new StringBuilder() ).append("0").append(cardBalance).toString();
		}
		
		result 						= ( new StringBuilder() ).append(refNumber)
											.append(fromAccountNumber)
											.append(toAccountNumber)
											.append(topupAmount)
											.append(lastBalance)
											.append(topUpPendingBalance)
											.append(cardBalance).toString();
		
		return result;
		
	}
	
	public String generateField57Topup( String amount ){
    	
		//Format : 
		//TXN_ID;TXN_TOKEN;TXN_TYPE;TXN_CHANNEL;TID;MID;CARD_ID;KEY_INDEX;CARD_TYPE;AMOUNT TRANSACTION;CARD_BALANCE
		
		String field57Topup	= "";		
		
		field57Topup 		= "100317113047012345671122334401;" + //TXN_ID
							  "064500C30300409AC2667171FAFA39FA39921E9087A910208828903A1165A8AED5E132229342D51E2137BEF179EF89F5A7BBD78DB9B430BC3C6E36E8FCB80834B21958C7D4D0599000044132125D478000;" + //TXN_TOKEN
							  "TCSH;" + //TXN_TYPE
							  "EDC0;" + // TXN_CHANNEL
							  "01111111;" + // TERMINAL ID
							  "11223344;" + // MERCHANT ID
							  "0014030000000019;" +  // CARD ID
							  "01;" + // CARD TYPE
							  "50000;" + // AMOUNT
							  "10000"; // LAST BALANCE
    			
    	return field57Topup;
    	
    }
	
	public String generateField57TopupCash( String amount, String currentBalance, String cardUID, String cardType, String cardId ){
    	
		//Format : 
		//TXN_ID;TXN_TOKEN;TXN_TYPE;TXN_CHANNEL;TID;MID;CARD_ID;KEY_INDEX;CARD_TYPE;AMOUNT TRANSACTION;CARD_BALANCE;CARD UID
		
		String field57Topup	= "";		
		
		field57Topup 		= "100317113047012345671122334401;" + //TXN_ID
							  "064500C30300409AC2667171FAFA39FA39921E9087A910208828903A1165A8AED5E132229342D51E2137BEF179EF89F5A7BBD78DB9B430BC3C6E36E8FCB80834B21958C7D4D0599000" + cardUID + ";" +  //TXN_TOKEN
							  "TCSH;" + //TXN_TYPE
							  "EDC0;" + // TXN_CHANNEL
							  "01111111;" + // TERMINAL ID
							  "11223344;" + // MERCHANT ID
							  cardId + ";" +  // CARD ID
							  cardType + ";" + // CARD TYPE
							  amount + ";" + // AMOUNT
							  currentBalance; // CARD BALANCE
    			
    	return field57Topup;
    	
    }
	
	public void send200TopupPendingFromBank( String cardNumber,
											 String amount,
											 String channelType ){
		
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48(FROM_ACCOUNT_NUMBER, 
													  TO_ACCOUNT_NUMBER, 
													  amount, 
													  "70000", 
													  "", 
													  "");
		
		String f57 					= generateField57Topup( amount );
		
		msg.setValue(2, PAN, IsoType.LLVAR, 0);
		msg.setValue(3, "850000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, channelType, IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "00192283", IsoType.ALPHA, 8);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(48, f48, IsoType.LLLVAR, 0);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}	
	
	public void send200TopupFromEDCBank( String amount ){
								
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48(FROM_ACCOUNT_NUMBER, 
													  TO_ACCOUNT_NUMBER, 
													  amount, 
													  "70000", 
													  "", 
													  "");
		
		String f57 					= generateField57Topup( amount );
		
		msg.setValue(2, PAN, IsoType.LLVAR, 0);
		msg.setValue(3, "850000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, "6012", IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "00192283", IsoType.ALPHA, 8);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(48, f48, IsoType.LLLVAR, 0);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}
	
	public void send200TopupCash( String cardNumber,
								  String amount, 
								  String currentBalance,
								  String cardUID,
								  String cardType){
		
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48("", 		// From Account Number
													  "", 		// To Account Number
													  amount,   // Amount
													  "100000",  // Last Balance
													  "", 		// Topoup Pending Balance
													  "");		// Current Balance
		
		String f57 					= generateField57TopupCash( amount, currentBalance, cardUID, cardType, cardNumber );
		
		msg.setValue(2, cardNumber, IsoType.LLVAR, 0);
		msg.setValue(3, "850000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, "6010", IsoType.NUMERIC, 4);		
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "00000004", IsoType.ALPHA, 8);
		msg.setValue(42, "0000000000000M1", IsoType.ALPHA, 15);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}
	
	public void send9200Acknowledge( IsoMessage isoMsg,
									 String f4,
									 String f11,
									 String f37,
									 String f39) throws IOException{

		IsoMessage msg				= mfact.newMessage(37376);
		String s9 					= multiSocket.util.localTransactionTime();
		
		msg.setField(2, isoMsg.getField(2));
		msg.setField(3, isoMsg.getField(3));
		//msg.setField(4, isoMsg.getField(4));
		msg.setValue(4, f4, IsoType.NUMERIC, 12);
		msg.setField(7, isoMsg.getField(7));
		msg.setValue(11, f11, IsoType.NUMERIC, 6);
		msg.setField(12, isoMsg.getField(12));
		msg.setField(13, isoMsg.getField(13));
		msg.setField(18, isoMsg.getField(18));
		msg.setField(22, isoMsg.getField(22));
		msg.setField(32, isoMsg.getField(32));
		msg.setField(33, isoMsg.getField(33));
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(39, f39, IsoType.ALPHA, 2);
		msg.setField(41, isoMsg.getField(41));
		msg.setField(42, isoMsg.getField(42));
		msg.setField(43, isoMsg.getField(43));
		msg.setField(57, isoMsg.getField(57));
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
		multiSocket.mainMenu();
		
	}
	
}
