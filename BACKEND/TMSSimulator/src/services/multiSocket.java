package services;

import java.net.*;
import java.util.*;
import java.io.*;

import utility.F;
import utility.JSON;
import utility.RC;
import utility.Utility;


import com.solab.iso8583.*;
import com.solab.iso8583.impl.SimpleTraceGenerator;
import com.solab.iso8583.parse.ConfigParser;

import java.io.*;
import java.net.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;

import org.json.simple.*;
import org.json.simple.parser.*;

import manager.HSMUtility;



public class multiSocket
{
	private BufferedReader in;
	public static PrintWriter keluar;
    ServerSocket sSocket;
	
	private static FileOutputStream fos = null;
	private static boolean debug = false;
	private static String fileDate = "";
	public static Utility util = new Utility();
	private static String debugFilename = "";
	private static Properties props = null;
	private static String xmlconfig = "";
	
	private static boolean hasKWP = false;
	
	public static Socket socketClient	 = null;
    private static int CLIENTPort = 0;
    private static ListenerASIPCLIENT li = null;
	
	private static String sql1;
    private static String sql2;
    private static String sql3;
    private static String sql4;
    private static String sql5;
    private static String sql6;
    private static String sql7;
    private static String sql8;
    private static String sql9;
    private static String sql10;
    private static String sql11;
    public static int maxRow = 10;
    private static MessageFactory mfact;
    private static SimpleTraceGenerator tracer = null;
    public static OutputStream out;
    public static boolean ReceiveResponse = true;
    private static Socket cn = null;
    private static int interval = 1;
    private static ResponseThread it = null;
    
    public static String nama_server;
    public static String nama_client;
    
    private static String config_file = "config_service_tms_simulator.ini";
    
    /*START: Variable for Key*/
	private static String ZMKLMK = "";
    private static String clearZMK = "";
    private static String keyLength = "";
    private static String hsmHost = "";
    private static String hsmPort = "";
	/*START: Variable for key*/
    
    class ListenerASIPCLIENT extends Thread
    {

        public void run()
        {
            do
            {
                if(s == null || s.isClosed())
                    try
                    {
                    	writeDebug("== Waiting Connection From ASIP ==");	
                        s = ss.accept();
                        socketClient=s;
                        multiSocket.out = s.getOutputStream();
                        ResponseThread rT = new ResponseThread(s,mfact);
        	            new Thread(rT).start();
        	            
                    }
                    catch(Exception exception)
                    {
                    	multiSocket.out = null;
                        try
                        {
                            if(!s.isClosed())
                                s.close();
                        }
                        catch(Exception exception2) { }
                    }
                try
                {
                	ListenerASIPCLIENT _tmp = this;
                    sleep(1000L);
                }
                catch(Exception exception1)
                {
                	multiSocket.out = null;
                }
            } while(true);
        }

        private ServerSocket ss;
        private Socket s;
        //private CommandLineTOASIPMASTER cln;
        final multiSocket this$0;

        public ListenerASIPCLIENT()
        {
        	super();
            this$0 = multiSocket.this;
            
            try
            {
            	//=== listen request connection from ASIP CLIENT == 
                ss = new ServerSocket(multiSocket.CLIENTPort);
                s = null;
                
            }
            catch(Exception exception) { }
        }
    }
    class ResponseThread extends Thread
    {
        private class Processor
            implements Runnable
        {

            public void run()
            {
                try
                {
                	if(multiSocket.hasKWP){
                		multiSocket.writeDebug((new StringBuilder()).append("Status KWP: OK").toString());
                	}
                	
                	multiSocket.writeDebug((new StringBuilder()).append("ISOMessage incoming : '").append(new String(msg)).append("'").toString());
                    IsoMessage isomessage = mfact.parseMessage(msg, 0);
                    String s1=Integer.toHexString(isomessage.getType()).toUpperCase();
                    
                    //Integer.getInteger("0500");
                    multiSocket.writeDebug((new StringBuilder()).append("s1: '").append(new String(s1)).append("'").toString());
                    multiSocket.writeDebug("");
                    
                    
                    if(s1.compareTo("200")==0){
                 	   
                          try{
                    		_200( isomessage );
                    	   }catch (Exception e) { multiSocket.writeDebug( "[Error] response200() : "+e.getMessage() ); }
                        
                    }else if(s1.compareTo("210")==0){
                  	   
                        try{
                        	this.parsingField(isomessage);
                        	responseAcknowledge(isomessage);
                  	   }catch (Exception e) { multiSocket.writeDebug( "[Error] response210() : "+e.getMessage() ); }
                      
                    }else if(s1.compareTo("800") == 0)
                    {
                        
                    	  try{
                     		_800( isomessage );
                     	   }catch (Exception e) { multiSocket.writeDebug( "[Error] request800() : "+e.getMessage() ); }
                    	 
                    }else if(s1.compareTo("810") == 0)
                    {
                    
                    	 try{
                      		_810( isomessage );
                      	   }catch (Exception e) { multiSocket.writeDebug( "[Error] response810() : "+e.getMessage() ); }
                    	 
                    }else if(s1.compareTo("420") == 0)
                    {
                        
                  	  try{
                   		_420( isomessage );
                   	   }catch (Exception e) { multiSocket.writeDebug( "[Error] request420() : "+e.getMessage() ); }
                  	 
                   }else if(s1.compareTo("430") == 0)
                    {
                 	   multiSocket.writeDebug("(R) Reversal Message Response DARI " + nama_server);
                        
                 	   for(int j1 = 0; j1 <= 104; j1++)
                            if(isomessage.hasField(j1))
                         	   multiSocket.writeDebug((new StringBuilder()).append("Field ").append(j1).append(" : ").append(isomessage.getField(j1).toString()).toString());
                        
                    }
                    else
                    {
                 	   multiSocket.writeDebug("Unspecified Request");
                    }
                    
                    multiSocket.writeDebug("\n");
                }
                catch(ParseException parseexception)
                {
             	   multiSocket.writeDebug((new StringBuilder()).append("Parsing incoming message ").append(parseexception.toString()).toString());
                }
                
            }
            public void parsingField( IsoMessage isomessage )
            {
            	
                for(int j1 = 0; j1 <= 104; j1++)
                    if(isomessage.hasField(j1))
                    	multiSocket.writeDebug((new StringBuilder()).append("Field ").append(j1).append(" : ").append(isomessage.getField(j1).toString()).toString());
            }
            
            private void _800( IsoMessage isomessage ) throws Exception
            {
            	
            	/*String kwp					= "1170PK0000";
            	String ZPKZMK 				= "";
            	HSMUtility hsmUtil 			= new HSMUtility(ZMKLMK, keyLength, hsmHost, Integer.parseInt( hsmPort ));
*/            	
            	multiSocket.writeDebug("");
            	multiSocket.writeDebug((new StringBuilder()).append("(R) Network Management Request 800 FROM "+multiSocket.nama_client+": ").append(new String(isomessage.writeData())).toString());
                
                this.parsingField( isomessage );
                
                String traceid 				= F.getTraceID( isomessage );

                multiSocket.ReceiveResponse = true;
       
                String status_code="00";
                
                IsoMessage isomessage1 = mfact.createResponse(isomessage);
                isomessage1.setField(11, isomessage.getField(11));
                isomessage1.setField(33, isomessage.getField(33));
                isomessage1.setValue(39, status_code, IsoType.ALPHA, 2);
                isomessage1.setField(70, isomessage.getField(70));                
                
                
                multiSocket.ReceiveResponse = true;
                multiSocket.writeDebug("");
                multiSocket.writeDebug((new StringBuilder()).append("(S) Network Management Response 810 TO "+multiSocket.nama_client+": ").append(new String(isomessage1.writeData())).toString());
                sendISO( s, isomessage1 );
                
                /*Statement for Key Exchange*/
                /*Thread.sleep(3000L);                
                ZPKZMK						= hsmUtil.generateZPKZMK();
                kwp 						= kwp + ZPKZMK;
                
                multiSocket.writeDebug("");
                multiSocket.writeDebug("SEND KWP TO CLIENT :" + multiSocket.nama_client);
                
                multiSocket.writeDebug((new StringBuilder()).append("(S) Network Management Request 800 TO "+multiSocket.nama_client+" PORT = "+sock.getPort()+" : ").append(new String(isomessage.writeData())).toString());
                
                String s8 = multiSocket.util.localTransactionTime();
                IsoMessage isomessage2 = mfact.newMessage(2048);
                isomessage2.setField(2, isomessage.getField(2));
                isomessage2.setValue(7, s8, IsoType.DATE10, 0);
                isomessage2.setField(33, isomessage.getField(33));
                isomessage2.setValue(48, kwp ,IsoType.LLLVAR, 0);
                
                multiSocket.writeDebug("");
                sendISO( s, isomessage2 );*/      
                
                multiSocket.mainMenu();
               
            }
            
            private void _810( IsoMessage isomessage ) throws Exception
            {
            	multiSocket.writeDebug("");
            	multiSocket.writeDebug((new StringBuilder()).append("(R) Network Management Response 810 FROM "+multiSocket.nama_client+" PORT = "+sock.getPort()+" : ").append(new String(isomessage.writeData())).toString());
                multiSocket.ReceiveResponse = true;
                
         	    this.parsingField(isomessage);
                	
            }
            
            private void _200( IsoMessage isomessage ) throws Exception
            {
            	
            	String f39				= "";
            	String f48				= "";
            	String processingCode 	= isomessage.getField(3).toString().substring(0, 2);
            	String f104 			= isomessage.getField(104).toString().substring(6);
            	
            	if( processingCode.equals("34") ){
            		
            		if( f104.equals("101010") ){ // Benef Normal
            			
            			f48				= "                                                PETER SUSANTO";
            			f39 			= "00";
            			
            		}else if( f104.equals("191919") ){ // AFEP Timeout
            			
            			f48				= "                                                PETER SUSANTO";
            			f39 			= "00";
            			
            		}else if( f104.equals("131313") ){ // Benef inactive/suspend
            			
            			f48				= "";
            			f39 			= "30";
            			
            		}
            		
            	}else{
            		
            		f48				= "                                                PETER SUSANTO";
            		f39 			= "00";
            		
            	}
            	
            	IsoMessage isomessage3 	= mfact.newMessage(528);
		        
	   	        isomessage3.setField(2, isomessage.getField(2)); // PAN
	   	        isomessage3.setField(3, isomessage.getField(3)); // PROCESSING CODE
	   	        isomessage3.setField(4, isomessage.getField(4)); // AMOUNT
	   	        isomessage3.setField(7, isomessage.getField(7)); // DATE TRANSMISSION
	   	        isomessage3.setField(11, isomessage.getField(11)); // TRACE AUDIT NUMBER
	   	        isomessage3.setField(12, isomessage.getField(12)); // 
	   	        isomessage3.setField(13, isomessage.getField(13)); // 
	   	        isomessage3.setField(15, isomessage.getField(15));
	   	        isomessage3.setField(29, isomessage.getField(29));
	   	        //isomessage3.setField(32, isomessage.getField(32)); // ACQUIRING INST CODE
	   			isomessage3.setField(33, isomessage.getField(33)); // FORWARD INST CODE
	   			//isomessage3.setField(35, isomessage.getField(35));
	   			isomessage3.setField(37, isomessage.getField(37)); // UNIQUE NUMBER 12DIGIT
	   			isomessage3.setValue(39, f39, IsoType.ALPHA, 2);
	   			isomessage3.setField(41, isomessage.getField(41));
//	   			isomessage3.setField(43, isomessage.getField(43));
	   			
	   			if( f39.equals("00") ){
	   				isomessage3.setValue(48, f48, IsoType.LLLVAR, 0);
	   			}	   			
	   			
	   			isomessage3.setField(49, isomessage.getField(49)); // Currency Code, Transaction
	   			isomessage3.setField(50, isomessage.getField(50)); // Currency Code, Transaction
	   			//isomessage3.setField(52, isomessage.getField(52));
	   			//isomessage3.setValue(54, bit54, IsoType.LLLVAR, 0);
	   			//isomessage3.setField(61, isomessage.getField(61));
	   			isomessage3.setField(63, isomessage.getField(63));
	   			
	   			/*if( jenis_transaksi.equals("41") ){
	   				isomessage3.setValue(102, isomessage.getField(102), IsoType.LLVAR, 0);
	   			}else{
	   				isomessage3.setField(102, isomessage.getField(102));
	   			}*/			
	   			
	   			isomessage3.setField(104, isomessage.getField(104));
   			
	   			if( !f104.equals("191919") ){
	   				sendISO(s,isomessage3);
	   				this.parsingField(isomessage3);
	   			}else{
	   				
	   				if( processingCode.equals("34") ){
	   					sendISO(s,isomessage3);
		   				this.parsingField(isomessage3);
	   				}
	   				
	   			}
	   			
	   			
            	
            }
            
            private void _200_old( IsoMessage isomessage ) throws Exception
            {
         	   multiSocket.writeDebug("");
                
         	   /** GET TRACE ID */
                String traceid = F.getTraceID( isomessage );
                
 	           	String jenis_transaksi="";
 	           	String type_account="";
 	           	
 	           	int stat_jenis_transaksi=1; 
 	           	int stat_type_account=1; 
 	           	
 	           	String st_validasi_kartu="0";
 	           	String account_no="";
 	           	
 	           	String card_id="";
 	            String bit48="";
	           	String bit102="";
	           	String bit54="";
            	
 	           	multiSocket.writeDebug("(R) Financial Message Request from SCAFEP "+sock.getInetAddress() + ":" + sock.getPort()+"");
 	           	this.parsingField(isomessage);
           
            	
                 jenis_transaksi=isomessage.getField(3).toString().substring(0, 2);
                 type_account=isomessage.getField(3).toString().substring(2, 2);
                
                 if(jenis_transaksi.compareTo("30")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Cek Saldo " );
 					bit54="1002360C000060000000";
 					bit102=isomessage.getField(102).toString();
 				}else if(jenis_transaksi.compareTo("01")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Tarik Tunai " );
 					bit54="1002360C000060000000";
 					bit102=isomessage.getField(102).toString();
 				}else if(jenis_transaksi.compareTo("34")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Inquiry Transfer Request " );
 					bit48="                                                PETER SUSANTO"; // semetara di hard code 					
 					//bit102=isomessage.getField(102).toString();
 				}else if(jenis_transaksi.compareTo("41")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Transfer Request" );
 					bit48="                                                PETER SUSANTO"; // semetara di hard code
 					//bit102=isomessage.getField(102).toString();
 				}else if(jenis_transaksi.compareTo("50")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Biller  Payment Request " );
 				}else if(jenis_transaksi.compareTo("80")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Inquiry Purchase " );
 					bit48="                                                PETER SUSANTO"; // semetara di hard code
 					bit102="00101757131";
 				}else if(jenis_transaksi.compareTo("86")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Payment Purchase" );
 					bit48="                  PITER SUSANTO KERE            PETER SUSANTO";
 				}else{
 				   	stat_jenis_transaksi=0; 
 				}
                 
 				
               
 	       		if(stat_jenis_transaksi==1){
 	       			
	 	       		String s30="00";
	        		
	       			multiSocket.writeDebug("Jenis Transaksi Valid");
	       			
	       			IsoMessage isomessage3 = mfact.newMessage(528);
			        
		   	        isomessage3.setField(2, isomessage.getField(2)); // PAN
		   	        isomessage3.setField(3, isomessage.getField(3)); // PROCESSING CODE
		   	        isomessage3.setField(4, isomessage.getField(4)); // AMOUNT
		   	        isomessage3.setField(7, isomessage.getField(7)); // DATE TRANSMISSION
		   	        isomessage3.setField(11, isomessage.getField(11)); // TRACE AUDIT NUMBER
		   	        isomessage3.setField(12, isomessage.getField(12)); // 
		   	        isomessage3.setField(13, isomessage.getField(13)); // 
		   	        isomessage3.setField(15, isomessage.getField(15));
		   	        isomessage3.setField(29, isomessage.getField(29));
		   	        //isomessage3.setField(32, isomessage.getField(32)); // ACQUIRING INST CODE
		   			isomessage3.setField(33, isomessage.getField(33)); // FORWARD INST CODE
		   			//isomessage3.setField(35, isomessage.getField(35));
		   			isomessage3.setField(37, isomessage.getField(37)); // UNIQUE NUMBER 12DIGIT
		   			isomessage3.setValue(39, s30, IsoType.ALPHA, 2);
		   			isomessage3.setField(41, isomessage.getField(41));
//		   			isomessage3.setField(43, isomessage.getField(43));
		   			isomessage3.setValue(48, bit48, IsoType.LLLVAR, 0);
		   			isomessage3.setField(49, isomessage.getField(49)); // Currency Code, Transaction
		   			isomessage3.setField(50, isomessage.getField(50)); // Currency Code, Transaction
		   			//isomessage3.setField(52, isomessage.getField(52));
		   			//isomessage3.setValue(54, bit54, IsoType.LLLVAR, 0);
		   			//isomessage3.setField(61, isomessage.getField(61));
		   			isomessage3.setField(63, isomessage.getField(63));
		   			
		   			/*if( jenis_transaksi.equals("41") ){
		   				isomessage3.setValue(102, isomessage.getField(102), IsoType.LLVAR, 0);
		   			}else{
		   				isomessage3.setField(102, isomessage.getField(102));
		   			}*/			
		   			
		   			isomessage3.setField(104, isomessage.getField(104));
	   			
		   			sendISO(s,isomessage3);
	   				this.parsingField(isomessage3);
		   			
	 	       	
 	       		}else{ // invalid transaction
 	       			try{
 	       				msgReject(new String(isomessage.writeData()),"12");
 	       			}
 	       			catch(Exception exception){
 	       				multiSocket.writeDebug((new StringBuilder()).append("Send Message Reject [Exception] : ").append(exception.toString()).toString());
 	                   }
 	       		}
                
            }
            
            private void _420( IsoMessage isomessage ) throws Exception
            {
            	multiSocket.writeDebug("(R) Financial Message Request from SCAFEP "+sock.getInetAddress() + ":" + sock.getPort()+"");
 	           	this.parsingField(isomessage);
 	           	
 	           	String jenis_transaksi="";
	           	String type_account="";
	        	int stat_jenis_transaksi=1;
	        	
	            jenis_transaksi=isomessage.getField(3).toString().substring(0, 2);
                type_account=isomessage.getField(3).toString().substring(2, 2);
	           	
                if(jenis_transaksi.compareTo("01")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Reversal Tarik Tunai " );
 					
 				}else if(jenis_transaksi.compareTo("34")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Reversal Inquiry Transfer" );
 				}else if(jenis_transaksi.compareTo("41")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Reversal Transfer" );
 				}else if(jenis_transaksi.compareTo("30")==0){
 					multiSocket.writeDebug("Jenis Transaksi Request : Reversal Inquiry Saldo" );
 				}else{
 				   	stat_jenis_transaksi=0; 
 				}
                
                if(stat_jenis_transaksi==1){
                	String s30="00";
	        		
	       			multiSocket.writeDebug("Jenis Transaksi Valid");
	       			
	       			char c1 = '\u0430';
	                IsoMessage isomessage3 = mfact.newMessage(c1);
			        
		   	        isomessage3.setField(2, isomessage.getField(2)); // PAN
		   	        isomessage3.setField(3, isomessage.getField(3)); // PROCESSING CODE
		   	        isomessage3.setField(4, isomessage.getField(4)); // AMOUNT
		   	        isomessage3.setField(7, isomessage.getField(7)); // DATE TRANSMISSION
		   	        isomessage3.setField(11, isomessage.getField(11)); // TRACE AUDIT NUMBER
		   	        isomessage3.setField(12, isomessage.getField(12)); // 
		   	        isomessage3.setField(13, isomessage.getField(13)); // 
		   	        isomessage3.setField(32, isomessage.getField(32)); // ACQUIRING INST CODE
		   			isomessage3.setField(33, isomessage.getField(33)); // FORWARD INST CODE
		   			isomessage3.setField(35, isomessage.getField(35));
		   			isomessage3.setField(37, isomessage.getField(37)); // UNIQUE NUMBER 12DIGIT
		   			isomessage3.setValue(39, s30, IsoType.ALPHA, 2);
		   			isomessage3.setField(41, isomessage.getField(41));
		   			isomessage3.setField(43, isomessage.getField(43));
		   			isomessage3.setField(48, isomessage.getField(48));
		   			isomessage3.setField(49, isomessage.getField(49)); // Currency Code, Transaction
		   			isomessage3.setField(50, isomessage.getField(50)); // Currency Code, Transaction
		   			isomessage3.setField(52, isomessage.getField(52));
		   			isomessage3.setField(63, isomessage.getField(63));
		   			isomessage3.setField(95, isomessage.getField(95));
		   			
		   			
		   			sendISOToASIPCLIENT(s,isomessage3);
		   			
                }else{ // invalid transaction
 	       			try{
 	       				msgReject(new String(isomessage.writeData()),"12");
 	       			}
 	       			catch(Exception exception){
 	       				multiSocket.writeDebug((new StringBuilder()).append("Send Message Reject [Exception] : ").append(exception.toString()).toString());
 	                   }
 	       		}
            }
            
            private boolean msgReject(String msg,String RC) throws Exception{
            	IsoMessage isomessage4 = mfact.parseMessage(msg.getBytes(), 0);
            	
 				writeDebug("(R) Financial Message Response Autoreject : ");
 				IsoMessage isomessage2 = mfact.newMessage(528);
 				isomessage2.setField(2, isomessage4.getField(2)); // PAN
 				isomessage2.setValue(3, isomessage4.getField(3), IsoType.NUMERIC, 6); // PROCESSING CODE
 				isomessage2.setField(4, isomessage4.getField(4)); // AMOUNT
 				isomessage2.setField(7, isomessage4.getField(7)); // DATE TRANSMISSION
 				isomessage2.setField(11, isomessage4.getField(11)); // TRACE AUDIT NUMBER
 				isomessage2.setField(12, isomessage4.getField(12)); // TIME LOCAL TRANSAKSI
 				isomessage2.setField(13, isomessage4.getField(13)); // DATE LOCAL TRANSAKSI
 				isomessage2.setField(15, isomessage4.getField(15)); // DATE, SETTLEMENT
 				isomessage2.setField(29, isomessage4.getField(29)); // SETTLEMENT FEE
 				isomessage2.setField(31, isomessage4.getField(31)); // PROCESSING FEE
 				isomessage2.setField(32, isomessage4.getField(32)); // ACQUIRING INST CODE
 				isomessage2.setField(33, isomessage4.getField(33)); // FORWARD INST CODE
 				isomessage2.setField(37, isomessage4.getField(37)); // UNIQUE NUMBER 12DIGIT
 				isomessage2.setValue(39, RC, IsoType.ALPHA, 2); // RESPONSE CODE
 				isomessage2.setField(41, isomessage4.getField(41)); // Card acceptor terminal
 				isomessage2.setField(48, isomessage4.getField(48));  // Additional Data Private
 				isomessage2.setField(49, isomessage4.getField(49));
 				isomessage2.setField(50, isomessage4.getField(50));
 				isomessage2.setField(61, isomessage4.getField(61));
 				isomessage2.setField(63, isomessage4.getField(63));
 				isomessage2.setField(102, isomessage4.getField(102));
 				isomessage2.setField(104, isomessage4.getField(104));
 				
 				for(int k = 0; k < 104; k++)
 				    if(isomessage2.hasField(k))
 				    	writeDebug((new StringBuilder()).append("Field ").append(k).append(" : ").append(isomessage2.getField(k).toString()).toString());
 				   
 				      //======== Send Request To Postilion ========== 
 				  writeDebug("(R) Financial Message Response To  : " + nama_client);
 				  //isomessage2.write(s.getOutputStream(), 2);
 				 sendISOToASIPCLIENT(s,isomessage2);
 				  //======== End Send Request To Host Finnet ======
           
              
            	return true;
            }

            private byte msg[];
            private Socket sock;
            final ResponseThread this$1;

            Processor(byte abyte0[], Socket socket)
            {
            	super();
                this$1 = ResponseThread.this;
                
                msg = abyte0;
                sock = socket;
            }
        }


        public void run()
        {
            int i = 0;
            byte abyte0[] = new byte[2];
            try
            {
                do
                {
                    if(stop)
                        break;
                    do
                    {
                        if(s == null || !s.isConnected())
                            break;
                        if(s.getInputStream().read(abyte0) == 2)
                        {
                            int j = (abyte0[0] & 0xff) << 8 | abyte0[1] & 0xff;
                            multiSocket.writeDebug("");
                            System.out.printf("Read %d bytes (%02x%02x)%n", new Object[] {
                                Integer.valueOf(j), Byte.valueOf(abyte0[0]), Byte.valueOf(abyte0[1])
                            });
                            byte abyte1[] = new byte[j];
                            s.getInputStream().read(abyte1);
                            i++;
                            Processor processor = new Processor(abyte1, s);
                            (new Thread(processor)).start();
                        }
                    } while(true);
                    try
                    {
                        if(!s.isClosed())
                            s.close();
                    }
                    catch(IOException ioexception) { }
                } while(true);
            }
            catch(IOException ioexception1)
            {
         	   multiSocket.writeDebug((new StringBuilder()).append("ResponseThread Exception occurred... ").append(ioexception1.toString()).toString());
                try
                {
                    if(!s.isClosed())
                        s.close();
                }
                catch(IOException ioexception2) { }
            }
            multiSocket.writeDebug((new StringBuilder()).append("ResponseThread Exiting after reading ").append(i).append(" requests").toString());
        }

        private Socket s;
        
        private InputStream is;
        private MessageFactory mfact;
        volatile boolean stop;
        final multiSocket this$0;



        public ResponseThread(Socket socket, MessageFactory messagefactory)
        {
        	super();
            this$0 = multiSocket.this;
            stop = false;
            try
            {
                s = socket;
                
                is = s.getInputStream();
                mfact = messagefactory;
            }
            catch(Exception exception)
            {
                try
                {
                    if(!s.isClosed())
                        s.close();
                    multiSocket.writeDebug("Connection Closed", 1);
                }
                catch(Exception exception1) { }
            }
        }
    }
  
    
    
   
    public static void main(String[] args) throws IOException {
        
        multiSocket socketServer = new multiSocket(args);
    }
    
    public static void responseAcknowledge( IsoMessage isoMsg ) throws IOException{
    	
    	Scanner input 					= new Scanner(System.in); 
    	System.out.println("\n");
		System.out.println("=================================================");
		System.out.println("************TMS Simulator Acknowledge************");
		System.out.println("=================================================");		
		System.out.println("1> Response Acknowledge Success Write to Card");
		System.out.println("2> Response Acknowledge Failed Write to Card");
		
		int menuId 						= input.nextInt();
		
		if( menuId == 1 ){
			
			TopUpFromBank topup 		= new TopUpFromBank(multiSocket.socketClient, multiSocket.mfact);
			topup.send9200Acknowledge(isoMsg, 
									  isoMsg.getField(4).toString(), 
									  isoMsg.getField(11).toString(), 
									  isoMsg.getField(37).toString(), 
									  "00");		
			
		}else if( menuId == 2 ){
			
			TopUpFromBank topup 		= new TopUpFromBank(multiSocket.socketClient, multiSocket.mfact);
			topup.send9200Acknowledge(isoMsg, 
									  isoMsg.getField(4).toString(), 
									  isoMsg.getField(11).toString(), 
									  isoMsg.getField(37).toString(), 
									  "29");	
			
		}
    	
    }
    
    public static void mainMenu() throws IOException{
    	
    	Scanner input 					= new Scanner(System.in); 
    	System.out.println("\n");
		System.out.println("=================================================");
		System.out.println("*******************TMS Simulator*****************");
		System.out.println("=================================================");
		
		System.out.println("1> Topup From EDC");
		System.out.println("2> Topup Cash");
		System.out.println("Silahkan input menu : ");
		
		int menuId 						= input.nextInt();
		
		if( menuId == 1 ){
			
			System.out.println("-> Insert Debit Card");
			System.out.println("-> Choosing TOP UP Menu");
			System.out.println("-> Input Amount");
			String amount 				= input.next();
			System.out.println("-> Tapping Mezzo Card");
			System.out.println("-> Entry PIN");
			System.out.println("-> Submit (OK)");
			
			TopUpFromBank topup 		= new TopUpFromBank(multiSocket.socketClient, multiSocket.mfact);
			topup.send200TopupFromEDCBank(amount);		
			
		}else if( menuId == 2 ){
			
			System.out.println("Card Type : ");
			String cardType 			= input.next();
			
			System.out.println("Card Number : ");
			String cardNumber 			= input.next();
			
			System.out.println("Card UID : ");
			String cardUID 				= input.next();
			
			System.out.println("Amount topup cash : ");
			String amount 				= input.next();
			
			System.out.println("Current Balance : ");
			String currentBalance 		= input.next();
			
			TopUpFromBank topup 		= new TopUpFromBank(multiSocket.socketClient, multiSocket.mfact);
			topup.send200TopupCash(cardNumber, amount, currentBalance, cardUID, cardType);
			
		}
    	
    }
    
    
    public void close() throws IOException { sSocket.close(); }
    
    public multiSocket(String as[])
    {
        //We need a try-catch because lots of errors can be thrown
    	
    	in = null;
    	keluar = null;
		sSocket = null;
        try {
        	
        	//getConfiguration(as[0]);
        	getConfiguration( config_file );
            fileDate = util.currentDate();
     
            debugFilename = props.getProperty("debugFilename");
        	if(props.getProperty("debugToFile").compareTo("1") == 0)
            {
                debug = true;
                openFileDebug();
            }
        	
        	xmlconfig = props.getProperty("xmlconfig");
        	interval = Integer.parseInt(props.getProperty("interval"));
            CLIENTPort = Integer.parseInt(props.getProperty("CLIENTPort"));
            nama_server = props.getProperty("nama_server");
            nama_client = props.getProperty("nama_client");
            
        	writeDebug("----------------------------------------------------------------");
            writeDebug((new StringBuilder()).append(nama_server+" , started at : ").append(util.currentDateTime()).toString());
            writeDebug("With Port : " + CLIENTPort);
            writeDebug("----------------------------------------------------------------");
            mfact = ConfigParser.createFromClasspathConfig(xmlconfig);
            mfact.setAssignDate(false);
            tracer = new SimpleTraceGenerator((int)(System.currentTimeMillis() % 10000L));
            mfact.setTraceNumberGenerator(tracer);
            writeDebug("Message Factory's configured");//================================= LISTENING REQUEST =========================
            sSocket = new ServerSocket(CLIENTPort);
            
            //Loop that runs server functions
            while(true) {
                //Wait for a client to connect
            	writeDebug("Waiting for connection from client...");
                Socket socket = sSocket.accept();
                socketClient=socket;
                writeDebug("== WAITING MEZZO Service ==!");
                writeDebug("Connection From : " + socket.getInetAddress() + ":" + socket.getPort());        	    
                
                ResponseThread rT = new ResponseThread(socketClient,mfact);
	            new Thread(rT).start();
            }
            
    
        } catch(IOException exception) {
        	writeDebug("Error: " + exception);
        	
        } catch(Exception exception){
        	 writeDebug((new StringBuilder()).append("Error while processing : ").append(exception.toString()).toString());
        }
    }
     
    
    
    private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                System.exit(1);
            }
        }
    }
    
    private static String getLastRandom()
	 {
	     String[] digit = {"1","2","3","4"};
	 
		 for( int v=0; v<digit.length; v++ ) digit[v] = randInt(0,99)+"";
		     String lastdigit = digit[0] + digit[1] + digit[2] + digit[3];
		  return lastdigit;
		 }
	 
	 private static int randInt(int min, int max) 
	 {
	        Random rand = new Random();
	        int randomNum = rand.nextInt((max - min) + 1) + min;
	        return randomNum;
	 }
	 
    private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
    
    public static void writeDebug(String s)
    {
        try
        {
            System.out.println(s);
            /*
            if(keluar != null)
            	keluar.println((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
            */
            
            if(debug)
            {
                if(fileDate.compareTo(util.currentDate()) != 0)
                {
                    try
                    {
                        if(fos != null)
                            fos.close();
                    }
                    catch(Exception exception)
                    {
                        System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
                    }
                    fileDate = util.currentDate();
                    openFileDebug();
                }
                s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
                try
                {
                    fos.write(s.getBytes());
                }
                catch(IOException ioexception)
                {
                    System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
                }
            }
        }
        catch(Exception exception1) { }
    }
    
    public static byte[] getByteHeader( int length )
	{
		try{
	    	int i1 = length/256;
	    	int i2 = length-(256*i1);

			byte[] result = new byte[2];
			result[0] = (byte) (i2);
			result[1] = (byte) (i1);
			return result;
		}catch (Exception e) {
			System.out.println( "<error> intToByte "+e.getMessage() );
		}
		return null;
	}
	
	public static byte[] addBytes( byte[] bytes1, byte[] bytes2 )
	{
		byte[] bb = new byte[ bytes1.length + bytes2.length ];
		int count = 0;
		for( int v=0; v<bytes1.length; v++ )
		{
			bb[count] = bytes1[v];
			count++;
		}
		for( int v=0; v<bytes2.length; v++ )
		{
			bb[count] = bytes2[v];
			count++;
		}
		return bb;
	}
	
    public String sendISOToASIPCLIENT( Socket socket, IsoMessage isomessage )
	{
		String msg = "ERROR";
		try{
			
//			isomessage.write(socket.getOutputStream(), 2);
			
	        String result = new String( isomessage.writeData() );
			byte[] header = getByteHeader( isomessage.writeData().length );
			byte[] isomsg = isomessage.writeData();
			byte[] pack = addBytes( header, isomsg );
			multiSocket.writeDebug( "SEND TO " + multiSocket.nama_client +""+" IP:PORT = "+socket.getInetAddress()+":"+socket.getPort());
   			multiSocket.writeDebug("ISOMessage Sending : '" +new String( pack)+"'" );
   			
			socket.getOutputStream().write( pack );
	        
			return "";
		}catch (Exception e) { msg = e.getMessage(); }
		return msg;
	}
    
    
    
    public static void sendISO( Socket socket, IsoMessage isomessage )
	{
		try{
			
			sendISONew( new String( isomessage.writeData() ), socket );	
			
		}catch (Exception e) {  }
	}
	
	
	public static String sendISONew( String message,Socket s ){
	     String responseServer="";
	     try
	           {
	      
	      String mti=message.substring(0,4);
	       
	      OutputStream outToServer = s.getOutputStream();
	      DataOutputStream out = new DataOutputStream(outToServer);
	      
	      writeDebug("Send Message ["+mti+"] : " + message);
	      out.writeUTF(message);
	          }catch(IOException e)
	          {
	           writeDebug( "IOException.services.send() "+e.getMessage() );
	          }
	          
	     return responseServer;
	 }
    

    public static void writeDebug(String s, int i)
    {
        try
        {
            System.out.println(s);
            if(i == 0 && keluar != null)
            	keluar.println((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
    
    
    public static void parsingField( IsoMessage isomessage )
    {
    	
        for(int j1 = 0; j1 <= 104; j1++)
            if(isomessage.hasField(j1))
            	multiSocket.writeDebug((new StringBuilder()).append("Field ").append(j1).append(" : ").append(isomessage.getField(j1).toString()).toString());
    }
    
    

}