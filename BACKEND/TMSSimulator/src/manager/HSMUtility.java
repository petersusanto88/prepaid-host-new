package manager;

import org.hsm.thales.HSM;

public class HSMUtility {
	
	private static String ZMKLMK = "";
	private static String keyLength = "";
	private static String host = "";
	private static int port;
	private static HSM hsm;
	
	public HSMUtility( String ZMKLMK,
					   String keyLength,
					   String host,
					   int port){
		
		this.ZMKLMK = ZMKLMK;
		this.keyLength = keyLength;
		this.host = host;
		this.port = port;
		
		hsm = new HSM( ZMKLMK, keyLength, host, port );
		
	}

	public String generateZPKZMK(){
		
		String ZPKZMK = "";		
		ZPKZMK 		  = hsm.generateKeyExchange();		
		return ZPKZMK;
		
	}
	
}
