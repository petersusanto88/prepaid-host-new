package utility;

public class RC 
{
	public static byte APPROVED 				= 0;
	public static byte ERROR 					= 6;

	
	public static byte INVALID_TRANSACTION		= 12; //Invalid transaction
	public static byte INVALID_AMOUNT			= 13; //Invalid amount
	public static byte INVALID_CARD_NUMBER		= 14; //Invalid card number (no such number)
	
	public static byte MESSAGE_FORMAT_ERROR		= 30;
//	34 xSuspected fraud, diduga penipuan
//	36 Restricted card, prohibit all - kartu dibatasi, melarang semua 
	public static byte LIMIT_PIN_RETRIES		= 38; //Allowable pin retries exceeded, max percobaan pin
//	39 No Credit Account
	
	public static byte LOST_CARD				= 41;
	public static byte STOLEN_CARD				= 43;
	
	public static byte NO_CHECKING_ACCOUNT_DECLINE= 52;//No Checking Account Decline - tidak ada link account
	public static byte NO_SAVING_ACCOUNT		= 53;
	public static byte EXPIRED_CARD 			= 54;
	public static byte INCORRECT_PIN 			= 55;
	public static byte NO_CARD_RECORD			= 56;
	
	public static byte LIMIT_AMOUNT_WIDTHRAWAL	= 61;//Exceeds withdrawal amount limit, melebihi batas jumlah penarikan
	public static byte CARD_RESTRICTED			= 62;//Restricted card, kartu dibatasi
//	63 Security violation, pelanggaran keamanan
//	64 Original amount incorrect
//	65 Exceeds withdrawal frequency limit, Melebihi batas frekuensi penarikan
	public static byte RESPONSE_RECEIVED_TOO_LATE= 68;//Response received too late
	
	public static byte LIMIT_NUMBER_PIN_RETRIES	= 75; // Allowable number of PIN tries exceeded - Jumlah diijinkan PIN mencoba melebihi
	
//	81 Institution not supported
	
//	93 Transaction cannot be completed. Violation of law
//	96 Invalid pin translation, system malfunction
	
	public static String get( byte code )
	{
		String resp = code+"";
		if( resp.length() == 1 ) resp = "0"+resp;
		return resp;
	}
	
}