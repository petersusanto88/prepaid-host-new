package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan kartu.
 */
public interface CardService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini dipakai untuk operasi validasi kartu.
	 * @param cardNumber
	 * @param cardUID
	 * @param cardType
	 * @param dbAESKey
	 * @return
	 */
	public JSONObject validateCard( String cardNumber,
									String cardUID,
									String cardType,
									String dbAESKey);
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakkan untuk mengambil cardId dari database berdasarkan cardNumber
	 * @param cardNumber
	 * @return
	 */
	public long getCardIdByCardNumber( String cardNumber );
	
}
