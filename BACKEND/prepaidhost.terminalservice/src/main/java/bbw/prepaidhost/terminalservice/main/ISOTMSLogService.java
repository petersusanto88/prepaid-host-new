package bbw.prepaidhost.terminalservice.main;

import com.solab.iso8583.IsoMessage;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan log TMS.
 */
public interface ISOTMSLogService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan sebagai operasi penyimpanan data log dari TMS ke database.
	 * @param isoMsg
	 * @param logType
	 * @throws Exception
	 */
	public void saveMessageTMS( IsoMessage isoMsg, short logType ) throws Exception;
	
}
