package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

import bbw.prepaidhost.terminalservice.model.Merchant;

public class MerchantServiceImpl implements MerchantService{
	
	private MerchantRepository merchantRepo = new MerchantRepository();
	
	public MerchantServiceImpl(){}
	
	public JSONObject getMerchantInfoDetail( String merchantCode ){
		
		JSONObject joResult 	= new JSONObject();
		
		Merchant m 				= merchantRepo.getMerchantInfoByMerchantCode(merchantCode);
		
		if( m != null ){
			
			joResult.put("errCode", "00");
			joResult.put("errMsg", "OK");
			joResult.put("merchantId", m.getMerchantId());
			joResult.put("acquireId", m.getAcquireId());
			
		}else{
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Merchant not found");
			
		}
		
		return joResult;
		
	}
	
}
