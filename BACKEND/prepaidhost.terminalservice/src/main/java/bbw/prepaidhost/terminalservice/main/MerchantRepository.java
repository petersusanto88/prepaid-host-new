package bbw.prepaidhost.terminalservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.model.Merchant;
import bbw.prepaidhost.terminalservice.model.MezzoResponseCode;
import bbw.prepaidhost.terminalservice.util.HibernateUtil;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan data merchant
 *
 */
public class MerchantRepository {

	public MerchantRepository(){}
	
	public Merchant getMerchantInfoByMerchantCode( String merchantCode ){
		
		JSONObject joResult 				= new JSONObject();			
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		Merchant m 							= null;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			tx								= session.beginTransaction();
			
			Query q							= session.createQuery( "from Merchant where merchantCode = :merchantCode " )
												.setString("merchantCode", merchantCode.trim());
			
			m								= (Merchant)q.uniqueResult();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TerminalRepository] Exception<getMerchantInfoByMerchantCode> : " + ex.getMessage());
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return m;	
		
	}
	
}
