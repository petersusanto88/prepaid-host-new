package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan transaksi kartu.
 *
 */
public interface TransactionService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menghandle transaksi topup ke kartu melalui terminal (Topup Cash).
	 * @param cardNumber
	 * @param cardBalance
	 * @param amountTopupCash
	 * @param vAESKey
	 * @param merchantCode
	 * @param terminalCode
	 * @param samId
	 * @param counterSAM
	 * @param counterCard
	 * @param traceNumber
	 * @param transactionFrom
	 * @return
	 */
	public JSONObject topupCash( String cardNumber,
							     double cardBalance,
							     double amountTopupCash,
							     String vAESKey,
							     //long issuerId,
							     String merchantCode,
							     String terminalCode,
							     long samId,
							     String counterSAM,
							     String counterCard,
							     //String keyVersion,
							     String traceNumber,
							     short transactionFrom);
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk melakukan void topup cash yang gagal dikarenakan write ke kartu gagal atau koneksi terputus
	 * sehingga di sisi database perlu penanda transaksi gagal atau sukses.
	 * @param isoTraceNumber
	 * @param amount
	 * @return
	 */
	public JSONObject voidTopupCash( String isoTraceNumber, long amount );
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan sebagai operasi update status transaksi. 
	 * @param status
	 * @param isoTraceNumber
	 * @return
	 */
	public JSONObject updateStatusTransaction( short status, String isoTraceNumber );
	
}
