package bbw.prepaidhost.terminalservice;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.impl.SimpleTraceGenerator;
import com.solab.iso8583.parse.ConfigParser;

import bbw.prepaidhost.terminalservice.util.MezzoUtil;

/**
 * @author PETER SUSANTO
 * @version 1.0
 * Class ini merupakan main class. Saat aplikasi dijalankan, class ini yang akan dipanggil terlebih dahulu.
 */
public class Service {

	private static String xmlconfig 			= "";
	private static Properties props 			= null;
	public static int timeout 					= 0;
    public static int interval 					= 1;
    private static boolean debug 				= false;
    private static FileOutputStream fos 		= null;
    private static String fileDate 				= "";
    private static String debugFilename 		= "";
    public static OutputStream out;
    public static boolean ReceiveResponse 		= true;
    public static MessageFactory mfact;
    private static boolean hasKWP 				= false;
    private static SimpleTraceGenerator tracer 	= null;
    public static MezzoUtil util 				= new MezzoUtil();
    
    //Host yang digunakan untuk connect ke TMS
    public static String hostTMS;
    //Port yang digunakan untuk connect ke TMS
    public static int portTMS;
    //Socket yang digunakan connect ke TMS
    public static Socket sockTMS;
    
    public static int listeningPort;
    public static String cbcMezzo;
    
    public static String aesKey 				= "";
    
    String config 								= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_tms.ini";
    
    public static String hostHSM 				= "";
    public static int portHSM 					= 0;
    
    private static ServiceMezzoTMSResponseThread mezzoTMSResponseThread;
    
    public static void main(String args[]) throws IOException, InterruptedException
    {
        /*if(args.length > 0 && (args[0].equals("-h") || args[0].equals("-help")) || args.length == 0)
        {
            System.out.println("usage: java -jar MezzoCMSService.jar");
            System.exit(1);
        }*/
        
        Service iso8583 = new Service(args);
    }
    
    public Service(String as[]) throws IOException, InterruptedException{
    	
    	getConfiguration(config);
    	fileDate 					= util.currentDate();
		debugFilename 				= props.getProperty("debugFilename");
		listeningPort 				= Integer.parseInt( props.getProperty("listeningPort") );
		
		cbcMezzo 					= props.getProperty("cbcMezzo");
		
		hostTMS 					= props.getProperty("hostTMS");
		portTMS 					= Integer.parseInt( props.getProperty("portTMS") );
		
		hostHSM 					= props.getProperty("hostHSM");
		portHSM 					= Integer.parseInt( props.getProperty("portHSM") );
		
		aesKey 						= props.getProperty("dbAESKey");
		
		if( props.getProperty("debugToFile").compareTo("1") == 0 ){
			debug					= true;
			openFileDebug();
		}
		
		xmlconfig 					= props.getProperty("xmlconfig");
		mfact 						= ConfigParser.createFromClasspathConfig(xmlconfig);
		mfact.setAssignDate(false);
		
		tracer = new SimpleTraceGenerator((int)(System.currentTimeMillis() % 10000L));
        mfact.setTraceNumberGenerator(tracer);
		
		writeDebug("----------------------------------------------------------------");
        writeDebug((new StringBuilder()).append("MEZZO CMS Service, started at : ").append(util.currentDateTime()).toString());
        writeDebug("----------------------------------------------------------------");
    	
        /*======================== OPEN Socket for Connect to AFEP ========================*/
		sockTMS 					= null;
		sockTMS						= new Socket( hostTMS, portTMS );
		sockTMS.setKeepAlive(true);
		/*======================== Start Connection to AFEP ========================*/
		ServiceMezzoTMSRequestThread srvMezzoTMSReq = new ServiceMezzoTMSRequestThread( sockTMS );
		/*======================== Listening Request / Response From ALTO ========================*/
		mezzoTMSResponseThread		= new ServiceMezzoTMSResponseThread(sockTMS, mfact);
		mezzoTMSResponseThread.start();
        
    }
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini digunakan untuk open file untuk debug.
     */
    private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini digunakan untuk menulis debug ke file.
     * @param s
     * @param i
     */
    public static void writeDebug(String s, int i)
    {
        try
        {
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini digunakan untuk menulis debug ke file.
     * @param s
     */
    public static void writeDebug(String s){
		try{
			
			System.out.println(s);
			if(out != null)
				out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
		
		
			if(fileDate.compareTo(util.currentDate()) != 0){
				try{
					if(fos != null)
					  fos.close();
				}catch(Exception exception){
					System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
				}
				fileDate = util.currentDate();
				openFileDebug();
			}
			s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
			try{
				fos.write(s.getBytes());
			}
			catch(IOException ioexception){
				System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
			}
		
		
		}catch(Exception exception1) { }
	}
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini digunakan untuk mengambil configuration dari sebuah file.
     * @param s
     */
    private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                System.exit(1);
            }
        }
    }
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini digunakan untuk send iso message ke service client
     * @param socket
     * @param isomessage
     * @param section
     */
    public static void sendISO( Socket socket, IsoMessage isomessage, String section )
	{
		try{
			
			sendISONew( new String( isomessage.writeData() ), socket, section );	
			
		}catch (Exception e) {  }
	}
	
	
	 public static String sendISONew( String message,Socket s, String section ){
	     String responseServer="";
	     
	     OutputStream outToServer = null;
	     DataOutputStream out = null;
	     
	     try
           {
      
		      String mti=message.substring(0,4);
		       
		      outToServer = s.getOutputStream();
		      out = new DataOutputStream(outToServer);
		      
		      writeDebug("[" + section + "] Send ("+mti+") : " + message);
		      out.writeUTF(message);
          }catch(IOException e)
          {
           writeDebug( "IOException.services.send() "+e.getMessage() );
          }finally {
			
          }
	          
	     return responseServer;
	 }
	 
	 /**
	  * <strong>Description</strong>:<br>
	  * Method ini digunakan untuk parsing field ISO8583 berdasarkan field-field di dalamnya.
	  * @param isomessage
	  * @param section
	  */
	 public static void parsingField( IsoMessage isomessage, String section ){
    	
        for(int j1 = 0; j1 <= 104; j1++)
            if(isomessage.hasField(j1))
            	Service.writeDebug((new StringBuilder()).append("[" + section + "] Field ").append(j1).append(" : ").append(isomessage.getField(j1).toString()).toString());
     }
    
}
