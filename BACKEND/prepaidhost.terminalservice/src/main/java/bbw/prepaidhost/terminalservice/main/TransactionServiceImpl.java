package bbw.prepaidhost.terminalservice.main;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.util.HibernateUtil;

public class TransactionServiceImpl implements TransactionService {

	public TransactionServiceImpl(){}
	private TransactionServiceRepository transactionRepo = new TransactionServiceRepository();
	
	public JSONObject topupCash( String cardNumber,
							     double cardBalance,
							     double amountTopupCash,
							     String vAESKey,
							     //long issuerId,
							     String merchantCode,
							     String terminalCode,
							     long samId,
							     String counterSAM,
							     String counterCard,
							     //String keyVersion,
							     String traceNumber,
							     short transactionFrom){
		
		JSONObject joResult			= new JSONObject();
		
		String spResult				= transactionRepo.callSPTopupCash(cardNumber, 
														   cardBalance, 
														   amountTopupCash, 
														   vAESKey, 
														   //issuerId, 
														   merchantCode, 
														   terminalCode, 
														   samId, 
														   counterSAM, 
														   counterCard, 
														   //keyVersion, 
														   traceNumber,
														   transactionFrom);
				
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Topup cash failed.");		
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMsg", arrSPResult[3]);
			joResult.put("flagPendingBalance", arrSPResult[4]);
			joResult.put("lastPendingBalance", arrSPResult[5]);
			joResult.put("amountTopup", arrSPResult[6]);
			joResult.put("lastCardBalance", arrSPResult[7]);
			joResult.put("currentBalance", arrSPResult[8]);
			
		}
		
		return joResult;
		
	}
	
	
	public JSONObject voidTopupCash( String isoTraceNumber, long amount ){
		

		
		JSONObject joResult			= new JSONObject();
		
		String spResult				= transactionRepo.callSPVoidTopupCash(isoTraceNumber, amount);
				
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Void Topup cash failed.");		
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			
		}
		
		return joResult;
		
	
		
	}
	
	
	public JSONObject updateStatusTransaction( short status, String isoTraceNumber ){
		
		return transactionRepo.repo_UpdateCardTransactionStatus(status, isoTraceNumber);
		
	}
	
}
