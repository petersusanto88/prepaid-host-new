package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

public class MezzoResponseCodeServiceImpl implements MezzoResponseCodeService  {

	public MezzoResponseCodeServiceImpl(){}
	private MezzoResponseCodeServiceRepository responseCodeRepo = new MezzoResponseCodeServiceRepository();
	
	public String getServiceALTOResponseCode( String mezzoResponseCode ){
		
		JSONObject joResult		= new JSONObject();
		
		String altoResponseCode = responseCodeRepo.getALTOResponseCode(mezzoResponseCode);
		
		return altoResponseCode;
		
	}
	
}
