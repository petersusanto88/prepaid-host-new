package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

import com.solab.iso8583.IsoMessage;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.model.TMSISOLog;
import bbw.prepaidhost.terminalservice.util.AES;
import bbw.prepaidhost.terminalservice.util.JSON;

public class ISOTMSLogServiceImpl implements ISOTMSLogService{
	
	public ISOTMSLogServiceImpl(){}
	
	
	private MerchantServiceImpl merchantService		= new MerchantServiceImpl();
	private TerminalServiceImpl terminalService 	= new TerminalServiceImpl();
	private CardService cardService 				= new CardServiceImpl();
	private AES aes 								= new AES();
	private ISOTMSLogRepository afepRepo 			= new ISOTMSLogRepository();
	
	public void saveMessageTMS( IsoMessage isoMsg, short logType ) throws Exception{
		
		long logId 					 = 0;
		
		String merchantCode 		 = ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" );		
		String terminalCode 		 = ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" );		
		String cardNumber 		 	 = ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
		String f11 		 	 		 = ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
		String f37 		 	 		 = ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
		String traceNumber 			 = f11 + f37;
		
		JSONObject joMerchantInfo	 = merchantService.getMerchantInfoDetail(merchantCode);
		long terminalId 			 = terminalService.getTerminalIdByTerminalCode(terminalCode);
		long cardId 				 = cardService.getCardIdByCardNumber( aes.encrypt( cardNumber, Service.aesKey) );

		System.out.println("### Merchant Code  :" + merchantCode);
		System.out.println("### joMerchantInfo : " + joMerchantInfo);
		
		TMSISOLog isoLog 			 = new TMSISOLog();
		isoLog.setAcquirerId( JSON.getInt(joMerchantInfo, "acquireId") );
		isoLog.setMerchantId( Long.parseLong( JSON.get(joMerchantInfo, "merchantId") )  );
		isoLog.setTerminalId(terminalId);
		isoLog.setCardId(cardId);
		isoLog.setCardNumber(cardNumber);
		isoLog.setTraceNumber(traceNumber);
		isoLog.setIsoData( new String( isoMsg.writeData() ) );
		isoLog.setMti( Integer.toHexString( isoMsg.getType() ).toUpperCase() );
		isoLog.setF2( cardNumber );
		isoLog.setF3( ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" ) );
		isoLog.setF4( ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" ) );
		isoLog.setF7( ( isoMsg.hasField(7) ? isoMsg.getField(7).toString() : "" ) );
		isoLog.setF11( ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" ) );
		isoLog.setF12( ( isoMsg.hasField(12) ? isoMsg.getField(12).toString() : "" ) );
		isoLog.setF13( ( isoMsg.hasField(13) ? isoMsg.getField(13).toString() : "" ) );
		isoLog.setF15( ( isoMsg.hasField(15) ? isoMsg.getField(15).toString() : "" ) );
		isoLog.setF18( ( isoMsg.hasField(18) ? isoMsg.getField(18).toString() : "" ) );
		isoLog.setF22( ( isoMsg.hasField(22) ? isoMsg.getField(22).toString() : "" ) );
		isoLog.setF26( ( isoMsg.hasField(26) ? isoMsg.getField(26).toString() : "" ) );
		isoLog.setF31( ( isoMsg.hasField(31) ? isoMsg.getField(31).toString() : "" ) );
		isoLog.setF32( ( isoMsg.hasField(32) ? isoMsg.getField(32).toString() : "" ) );
		isoLog.setF33( ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" ) );
		isoLog.setF37( ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" ) );
		isoLog.setF38( ( isoMsg.hasField(38) ? isoMsg.getField(38).toString() : "" ) );
		isoLog.setF39( ( isoMsg.hasField(39) ? isoMsg.getField(39).toString() : "" ) );
		isoLog.setF41( ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" ) );
		isoLog.setF42( ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" ) );
		isoLog.setF43( ( isoMsg.hasField(43) ? isoMsg.getField(43).toString() : "" ) );
		isoLog.setF48( ( isoMsg.hasField(48) ? isoMsg.getField(48).toString() : "" ) );
		isoLog.setF49( ( isoMsg.hasField(49) ? isoMsg.getField(49).toString() : "" ) );
		isoLog.setF61( ( isoMsg.hasField(61) ? isoMsg.getField(61).toString() : "" ) );
		isoLog.setF63( ( isoMsg.hasField(63) ? isoMsg.getField(63).toString() : "" ) );
		isoLog.setF102( ( isoMsg.hasField(102) ? isoMsg.getField(102).toString() : "" ) );
		isoLog.setF104( ( isoMsg.hasField(104) ? isoMsg.getField(104).toString() : "" ) );
		isoLog.setLogDate( Service.util.getDateTime() );
		isoLog.setLogType(logType);
		
		logId 						= afepRepo.saveTMSLog(isoLog);
		
		Service.writeDebug("[ServiceMezzoAFEPResponseThread] Save AFEP Log Successfully. logId = " + logId);
				
	}
	
	
	
}
