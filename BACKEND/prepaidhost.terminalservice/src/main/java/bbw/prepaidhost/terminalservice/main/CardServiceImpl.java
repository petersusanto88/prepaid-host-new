package bbw.prepaidhost.terminalservice.main;

import java.util.List;

import org.json.simple.JSONObject;

public class CardServiceImpl implements CardService {

	public CardServiceImpl(){}
	private CardServiceRepository cardRepo 			= new CardServiceRepository();
	
	public JSONObject validateCard( String cardNumber,
									String cardUID,
									String cardType,
									String dbAESKey){
		
		JSONObject joResult			= new JSONObject();
		
		
		String spResult				= cardRepo.callSPValidateCard(cardNumber, 
																  cardUID, 
																  cardType, 
																  dbAESKey);
		
		System.out.println("spResult : " + spResult);
		
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Validate card failed.");		
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			
		}
		
		return joResult;
		
	}
	

	public long getCardIdByCardNumber( String cardNumber ) {
		return cardRepo.getCardIdByCardNumber(cardNumber);
	}
	
}
