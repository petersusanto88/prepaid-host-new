package bbw.prepaidhost.terminalservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.text.ParseException;

import org.json.simple.JSONObject;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;

import bbw.prepaidhost.terminalservice.main.CardService;
import bbw.prepaidhost.terminalservice.main.CardServiceImpl;
import bbw.prepaidhost.terminalservice.main.ISOTMSLogService;
import bbw.prepaidhost.terminalservice.main.ISOTMSLogServiceImpl;
import bbw.prepaidhost.terminalservice.main.MezzoResponseCodeService;
import bbw.prepaidhost.terminalservice.main.MezzoResponseCodeServiceImpl;
import bbw.prepaidhost.terminalservice.main.TransactionService;
import bbw.prepaidhost.terminalservice.main.TransactionServiceImpl;
import bbw.prepaidhost.terminalservice.util.JSON;
import bbw.prepaidhost.terminalservice.util.KMSUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ServiceMezzoTMSResponseThread digunakan sebagai thread yang menghandle message yang masuk dari TMS.
 */
public class ServiceMezzoTMSResponseThread extends Thread {
	
	private static CardService cardService 					= new CardServiceImpl();
	private static TransactionService transService			= new TransactionServiceImpl();
	private static MezzoResponseCodeService mezzoRC			= new MezzoResponseCodeServiceImpl();
	private static ISOTMSLogService isoLog					= new ISOTMSLogServiceImpl();
	
	
	private class Processor implements Runnable{
		
		public void run(){
			
			try{
				
				Service.writeDebug("=================================================");
				Service.writeDebug("TMS Response Thread] Parsing incoming from AFEP");
				Service.writeDebug("=================================================");	
				
				IsoMessage isoMessage		= mfact.parseMessage(msg, 0);
				String mti 					= Integer.toHexString( isoMessage.getType() ).toUpperCase();	
				Service.writeDebug("[TMS Response Thread] MTI:" + mti );
				Service.writeDebug("[TMS Response Thread] DATA:" +  new String( isoMessage.writeData() ));
				
				if( mti.compareTo("810") == 0 ){
					
					try{
						
						Service.parsingField(isoMessage, "TMS Response Thread");		
						
					}catch( Exception ex ){
						Service.writeDebug("[TMS Response Thread] Exception Response 0810 : " + ex.getMessage() );
					}
					
				}else if( mti.compareTo("200") ==  0 ){
					
					try{
						
						Service.parsingField(isoMessage, "TMS Response Thread");
						_200( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[TMS Response Thread] Exception 0200 : " + ex.getMessage());
					}
					
				}else if( mti.compareTo("9200") ==  0 ){
					
					try{
						
						Service.parsingField(isoMessage, "TMS Response Thread");
						_9200( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[TMS Response Thread] Exception 09200 : " + ex.getMessage());
					}
					
				}
				
			}catch( ParseException e ){
				Service.writeDebug("[TMS Response Thread] ParseException : " + e.getMessage());
			} catch (UnsupportedEncodingException e) {
				Service.writeDebug("[TMS Response Thread] UnsupportedEncodingException : " + e.getMessage());
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan sebagai format ACK (Acknowledge), dimana terminal yang berhasil update balance di kartu harus
		 * menginformasikan ke prepaid host sehingga di sisi prepaid host status transaksi adalah berhasil. 
		 * @param isoMsg
		 */
		private void _9200( IsoMessage isoMsg ){
			
			String f3						= ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" );
			String f4 						= ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" );
			String f11 						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f37 						= ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
			String f39 						= ( isoMsg.hasField(39) ? isoMsg.getField(39).toString() : "" );
			String f57 						= ( isoMsg.hasField(57) ? isoMsg.getField(57).toString() : "" );
			
			JSONObject joCardInfo 			= Service.util.parseField57(f57);
			
			String isoTraceNumber 			= (new StringBuilder()).append(f11).append(f37).toString();
			
			JSONObject joVoid				= null;
			
			// Jika processing code = Topup Online
			if( f3.compareTo("850000") == 0 ){
				
				/*Note: untuk asumsi, apabila write to card gagal, RC Code = 29*/
				if( f39.compareTo("29") == 0 ){
					
					/*Update to transaction record*/
					JSONObject joUpdateTransaction			= transService.updateStatusTransaction((short)-1, 
																								   isoTraceNumber);
					
					/*Do Void transaction*/
					if( JSON.get(joCardInfo, "TXN_TYPE").compareTo("TCSH") == 0 ){
						
						joVoid 								= transService.voidTopupCash(isoTraceNumber, Long.parseLong( f4 ));
						
					}
					
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Update Card Transaction after write to card : ");
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Status Ack : 29");		
					Service.writeDebug("[TMS Response Thread] Response Void Topup Cash : " + joVoid.toString());
					
				}else if( f39.compareTo("00") == 0 ){
					
					/*Update to transaction record*/
					JSONObject joUpdateTransaction			= transService.updateStatusTransaction((short)2, 
																								   isoTraceNumber);
					
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Update Card Transaction after write to card : ");
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Status Ack : 00");		
					Service.writeDebug("[TMS Response Thread] Response Update to Database : " + joUpdateTransaction.toString());
				
				}
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk handle message format 0200 dari TMS.
		 * Message 0200 ini untuk Topup Cash
		 * </ul> 
		 * @param isoMsg
		 * @throws Exception
		 */
		private void _200( IsoMessage isoMsg ) throws Exception{
		
			String f3						= ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" );
			String f2 						= ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
			String f4 						= ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" );
			String f11 						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f18 						= ( isoMsg.hasField(18) ? isoMsg.getField(18).toString() : "" );
			String f37 						= ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
			String f41 						= ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" );
			String f42 						= ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" );
			String f57						= ( isoMsg.hasField(57) ? isoMsg.getField(57).toString() : "" );
			String f39 						= "";
			String f48						= "";
			
			String spResult 				= "";			
			String[] arrSPResult;
			
			JSONObject joCardInfo 			= Service.util.parseField57(f57);			
			boolean cardVerifyLib			= false; 
			
			String traceNumber  			= f11 + f37;
			String transType 				= "";
			String f57Response				= "";
			String f48Response 				= "";
			
			String amountTopup				= "";
			String lastCardBalance 			= "";
			String currentBalance 			= "";
			String lastPendingBalance 		= "";
			
			short transactionFrom 			= 0;
			
			JSONObject joResultSP 			= null;
			JSONObject joCardVerifyLib 		= new JSONObject();
			
			// Save iso message log from TMS to database
			//isoLog.saveMessageTMS(isoMsg, (short)1);
			
			// Jika processing code = Topup Online
			if( f3.compareTo("850000") == 0 ){
				
				/*1. Validasi kartu*/
				JSONObject joValidateCard	= new JSONObject();			
				
				Service.writeDebug("[ServiceMezzoTMSResponseThread] Start Validate Card");
				
				joValidateCard 				= cardService.validateCard(f2, 
																	   JSON.get(joCardInfo, "CARD_UID"), 
																	   JSON.get(joCardInfo, "CARD_TYPE"), 
																	   Service.aesKey);				
				
				
				if( JSON.get(joValidateCard, "errCode").compareTo("00") == 0 ){
					
					Service.writeDebug("[ServiceMezzoTMSResponseThread] 1) Verify Card into Database : Valid");
					
					/*2. Verifikasi kartu dengan menggunakan lib*/
					//Sementara di hardcode = true, karena lib blm tersedia
					joCardVerifyLib				= KMSUtil.KMS_unwrapTransaction( Service.hostHSM,
																		         Service.portHSM,
																		         JSON.get(joCardInfo, "TXN_TOKEN"),
																		         JSON.getInt(joCardInfo, "AMOUNT"),
																		         joCardInfo);
					
					if( true ) {//JSON.get(joCardVerifyLib, "errCode").compareTo("00") == 0 ){
						
						Service.writeDebug("[ServiceMezzoTMSResponseThread] 1) Verify Card into KMS : Valid");
						
						/*Convert field18 into transactionFrom*/
						transactionFrom 		= Service.util.convertField18IntoMezzoSpec(f18);
						
						/*3. Process depends on transaction type*/
						//3.1 Topup Cash
							if( JSON.get(joCardInfo, "TXN_TYPE").compareTo("TCSH") == 0 ){
								
								Service.writeDebug("[ServiceMezzoTMSResponseThread] Processing Cash Topup..." + JSON.get(joCardInfo, "CARD_ID"));
								
								joResultSP			= transService.topupCash(JSON.get(joCardInfo, "CARD_ID"), 
																		     Double.parseDouble( JSON.get(joCardInfo, "CARD_BALANCE") ), 
																		     Double.parseDouble( f4 ), 
																		     Service.aesKey, 
																		     //Long.parseLong( JSON.get(joCardInfo, "KEY_INDEX") ), 
																		     f42, 
																		     f41, 
																		     (long)0, 
																		     "", 
																		     "", 
																		     //JSON.get(joCardInfo, "KEY_INDEX"), 
																		     traceNumber,
																		     transactionFrom);
								
								if( JSON.get(joResultSP, "errCode").compareTo("00")  == 0 ){
									
									f39					= "00";
									
									lastPendingBalance 	= JSON.get(joResultSP, "lastPendingBalance");
									amountTopup			= JSON.get(joResultSP, "amountTopup");
									lastCardBalance		= JSON.get(joResultSP, "lastCardBalance");
									currentBalance		= JSON.get(joResultSP, "currentBalance");
									
									/*Define Trans Type for response to TMS*/
									if( JSON.get(joResultSP, "flagPendingBalance") != null && JSON.get(joResultSP, "flagPendingBalance").compareTo("t") == 0 ){
										transType 		= "TUBL";
									}else{
										transType		= "UPBL";
									}
									
									/*Construct field 48*/
									f48Response 		= Service.util.constructField48(traceNumber, 
																					    "", 
																					    f2, 
																					    f4, 
																					    lastCardBalance, 
																					    lastPendingBalance, 
																					    currentBalance, 
																					    "");
									
									/*Construct field 57*/
									f57Response 		= Service.util.constructField57(JSON.get(joCardInfo, "TXN_ID"), 
																						JSON.get(joCardInfo, "TXN_TOKEN"), 
																						JSON.get(joCardInfo, "TXN_TYPE"), 
																						JSON.get(joCardInfo, "TXN_CHANNEL"), 
																						JSON.get(joCardInfo, "TID"), 
																						JSON.get(joCardInfo, "MID"), 
																						JSON.get(joCardInfo, "CARD_ID"), 
																						JSON.get(joCardInfo, "CARD_TYPE"), 
																						JSON.get(joCardInfo, "AMOUNT"), 
																						JSON.get(joCardInfo, "CARD_BALANCE"));
									
									this.response210(isoMsg, f39, f48Response, f57Response);
								
								}else{
									
									f39 			= mezzoRC.getServiceALTOResponseCode(JSON.get(joResultSP, "errCode"));
									this.response210(isoMsg, f39, f48, "");
									
								}
								
							}
						
					}else{
						
						Service.writeDebug("[ServiceMezzoTMSResponseThread] 1) Verify Card into KMS : Invalid");
						this.response210(isoMsg, "57", f48, "");
						
					}
					
						
				}else{
					
					System.out.println("RC Validate Card : " + JSON.get(joValidateCard, "errCode"));
					
					f39		= mezzoRC.getServiceALTOResponseCode(JSON.get(joValidateCard, "errCode"));
					this.response210(isoMsg, f39, f48, "");
					
				}
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk membuat response 210 sebagai balasan dari PrepaidHost ke TMS
		 * @param reqMsg
		 * @param f39
		 * @param f48
		 * @param f57
		 * @throws Exception
		 */
		private void response210( IsoMessage reqMsg,
								  String f39,
								  String f48,
								  String f57) throws Exception{
			
			IsoMessage msg		= mfact.newMessage(528);
			
			msg.setField(2, reqMsg.getField(2));
			msg.setField(3, reqMsg.getField(3));
			msg.setField(4, reqMsg.getField(4));
			msg.setField(7, reqMsg.getField(7));
			msg.setField(11, reqMsg.getField(11));
			msg.setField(15, reqMsg.getField(15));
			msg.setField(18, reqMsg.getField(18));
			msg.setField(32, reqMsg.getField(32));
			msg.setField(33, reqMsg.getField(33));
			msg.setField(37, reqMsg.getField(37));
			msg.setValue(39, f39, IsoType.ALPHA, 2);
			msg.setField(41, reqMsg.getField(41));
			msg.setField(42, reqMsg.getField(42));
			msg.setField(43, reqMsg.getField(43));
			
			if( f39.compareTo("00") == 0 ){
				msg.setValue(57, f57, IsoType.LLLVAR, 0);
				msg.setValue(48, f48, IsoType.LLLVAR, 0);
			}else{
				
				
				
			}
			
			Service.writeDebug("[TMS Response Thread] Response 0210 to TMS : ");
			Service.parsingField(msg, "TMS Response Thread");
			Service.sendISO(s, msg, "TMS Response Thread");
			
			// Save iso message log to TMS to database
			//isoLog.saveMessageTMS(msg, (short)2);
			
		}
		
		private byte msg[];
        private Socket sock;
        final ServiceMezzoTMSResponseThread this$1;

        Processor(byte abyte0[], Socket socket)
        {
        	super();
            this$1 = ServiceMezzoTMSResponseThread.this;
            
            msg = abyte0;
            sock = socket;
        }
		
	}
	
	public void run()
    {
        int i = 0;
        byte abyte0[] = new byte[2];
        try
        {
            do
            {
                if(stop)
                    break;
                do
                {
                    if(s == null || !s.isConnected())
                        break;
                    if(s.getInputStream().read(abyte0) == 2)
                    {
                        int j = (abyte0[0] & 0xff) << 8 | abyte0[1] & 0xff;
                        System.out.printf("TMS Response Thread] Read %d bytes (%02x%02x)%n", new Object[] {
                            Integer.valueOf(j), Byte.valueOf(abyte0[0]), Byte.valueOf(abyte0[1])
                        });
                        byte abyte1[] = new byte[j];
                        s.getInputStream().read(abyte1);
                        Service.writeDebug("TMS Response Thread] LENGTH J : " + j);
                        i++;
                        Processor processor = new Processor(abyte1, s);
                        (new Thread(processor)).start();
                    }
                } while(true);
                try
                {
                    if(!s.isClosed())
                        s.close();
                }
                catch(IOException ioexception) { }
            } while(true);
        }
        catch(IOException ioexception1)
        {
        	Service.writeDebug((new StringBuilder()).append("TMS Response Thread] ResponseThread Exception occurred... ").append(ioexception1.toString()).toString());
            try
            {
                if(!s.isClosed())
                    s.close();
            }
            catch(IOException ioexception2) { }
        }
        Service.writeDebug((new StringBuilder()).append("TMS Response Thread] ResponseThread Exiting after reading ").append(i).append(" requests").toString());
    }
	
	private Socket s;
    private InputStream is;
    private MessageFactory mfact;
    volatile boolean stop;
    
    public ServiceMezzoTMSResponseThread(Socket socket, MessageFactory messagefactory)
    {
    	super();
        //this$0 = ISO8583.this;
        stop = false;
        try
        {
            s = socket;
            is = s.getInputStream();
            mfact = messagefactory;
        }
        catch(Exception exception)
        {
            try
            {
                if(!s.isClosed())
                    s.close();
                Service.writeDebug("Connection Closed", 1);
            }
            catch(Exception exception1) { }
        }
    }
	
}
