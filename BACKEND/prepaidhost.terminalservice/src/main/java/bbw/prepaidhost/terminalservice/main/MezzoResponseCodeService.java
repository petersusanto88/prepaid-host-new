package bbw.prepaidhost.terminalservice.main;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan response code ALTO
 *
 */
public interface MezzoResponseCodeService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengambil data response code ALTO dari database.
	 * @param mezzoResponseCode
	 * @return
	 */
	public String getServiceALTOResponseCode( String mezzoResponseCode );
	
}
