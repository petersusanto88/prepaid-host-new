package bbw.prepaidhost.terminalservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSMezzoResponseCodes\"")
public class MezzoResponseCode implements Serializable {

	@Id
	@Column(name="\"responseCode\"")
	private String responseCode;
	
	@Column(name="\"responseMsg\"")
	private String responseMsg;
	
	private String description;
	
	@Column(name="\"altoResponseCode\"")	
	private String altoResponseCode;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAltoResponseCode() {
		return altoResponseCode;
	}
	public void setAltoResponseCode(String altoResponseCode) {
		this.altoResponseCode = altoResponseCode;
	}
	
}
