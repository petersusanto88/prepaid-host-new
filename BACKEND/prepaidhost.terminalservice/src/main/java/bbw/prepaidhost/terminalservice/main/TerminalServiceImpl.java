package bbw.prepaidhost.terminalservice.main;

import org.json.simple.JSONObject;

import bbw.prepaidhost.terminalservice.model.Merchant;
import bbw.prepaidhost.terminalservice.model.Terminal;

public class TerminalServiceImpl implements TerminalService {

	public TerminalServiceImpl(){}
	
	private TerminalRepository terminalRepo = new TerminalRepository();
	
	public long getTerminalIdByTerminalCode( String terminalCode ){
		
		JSONObject joResult 	= new JSONObject();		
		long terminalId 		= 0;		
		Terminal t 				= terminalRepo.getTerminalIdByTerminalCode(terminalCode);
		
		if( t != null ){
			
			terminalId			= t.getTerminalId();  
			
		}
		
		return terminalId;
		
	}
	
}
