package bbw.prepaidhost.terminalservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread.AutomaticPing;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread.CommandLine;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread.InputThreadServer;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread.Listener;
import bbw.prepaidhost.terminalservice.ServiceMezzoTMSRequestThread.Reconnect;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini bertugas sebagai thread yang melakukan request ke TMS. Aktifitas yang dilakukan antara lain : <br>
 * <ul>
 * 	<li>Send Echo Test</li>
 *  <li>Auto reconnect </li>
 *  <li>Auto Send echo test</li>
 * </ul>
 */
public class ServiceMezzoTMSRequestThread {

	private static CommandLine cl = null;
    private static Listener l = null;
    private static AutomaticPing ap = null;
    private static boolean ReceiveResponse = true;
    
    class InputThreadServer extends Thread{

        public void run()
        {
            String s1 = "";
            try
            {
                BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(s.getInputStream()));
                do
                {
                    if(s.isClosed())
                        break;
                    String s2 = bufferedreader.readLine();
                    if(s2 == null || s2.equalsIgnoreCase("quit"))
                        break;
                    cln.setCommand(s2);
                } while(true);
                Service.out = null;
                if(!s.isClosed())
                    s.close();
            }
            catch(Exception exception)
            {
            	Service.out = null;
                try
                {
                    if(!s.isClosed())
                        s.close();
                }
                catch(Exception exception1) { }
            }
            
            if( Thread.currentThread() != null ){
				Thread.currentThread().interrupt();
			}
        }

        Socket s;
        CommandLine cln;
        final ServiceMezzoTMSRequestThread this$0;

        public InputThreadServer(Socket socket, CommandLine commandline)
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;   
            s = socket;
            cln = commandline;
        }
    
		
	}
    
    /**
     * 
     * @author PETER SUSANTO
     * <br><br>
     * Class Reconnect() digunakan untuk melakukan koneksi ulang ke TMS yang terputus.
     *
     */
    class Reconnect extends Thread{


        public void run()
        {
            String as[] = new String[0];
            for(boolean flag = true; flag;)
            {
                try
                {
                    Service.writeDebug((new StringBuilder()).append("\n").append(Service.util.currentDateTime()).append(" Unable to connect, retry in ").append(Service.interval).append(" seconds").toString(), 1);
                    Reconnect _tmp = this;
                    sleep(Service.interval * 1000);
                }
                catch(Exception exception) { }
                try
                {
                	Service.sockTMS.close();
                	Service.writeDebug((new StringBuilder()).append("\nRe-Connecting to ").append(Service.hostTMS).append(":").append(Service.portTMS).toString(), 1);
                	Service.sockTMS = new Socket(Service.hostTMS, Service.portTMS);
                	Service.writeDebug("Connected\n", 1);
                    
                    if(ap != null)
                    {
                    	ap.stop = true;
                    	ap.interrupt();
                        System.out.println("ap.interrupt()");
                    }
                    Thread.sleep(3000L);
                    ap = new AutomaticPing(Service.sockTMS, cl);
                    ap.start();
                    Service.writeDebug("Type \"help\" to see list of commands", 0);
                    flag = false;
                }
                catch(Exception exception1)
                {
//                    ISO8583.writeDebug((new StringBuilder()).append("Re-connect Exception : ").append(exception1.toString()).append("\n").toString());
                }
            }

        }

        final ServiceMezzoTMSRequestThread this$0;

        public Reconnect()
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;
            
        }
    
		
	}
    
    /**
     * <strong>Description</strong>:<br>
     * Class ini berfungsi sebagai thread yang berfungsi sebagai thread yang bertugas melakukan konversi message dari
     * byte stream menjadi character stream yang diterima dari TMS.
     * @author PETER SUSANTO     
     */
    class Listener extends Thread{	

        public void run()
        {
            do
            {
                if(s == null || s.isClosed())
                    try
                    {
                        s = ss.accept();
                        Service.out = s.getOutputStream();
                        InputThreadServer inputthreadserver = new InputThreadServer(s, cln);
                        inputthreadserver.start();
                    }
                    catch(Exception exception)
                    {
                    	Service.out = null;
                        try
                        {
                            if(!s.isClosed())
                                s.close();
                        }
                        catch(Exception exception2) { }
                    }
                try
                {
                    Listener _tmp = this;
                    sleep(1000L);
                }
                catch(Exception exception1)
                {
                	Service.out = null;
                }
            } while(true);
        }

        private ServerSocket ss;
        private Socket s;
        private CommandLine cln;
        final ServiceMezzoTMSRequestThread this$0;

        public Listener(CommandLine commandline)
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;
            
            try
            {
                ss = new ServerSocket(Service.listeningPort);
                s = null;
                cln = commandline;
            }
            catch(Exception exception) { }
        }    
		
	}
    
    /**
     * <strong>Description</strong>:<br>
     * Class thread ini dipakai untuk melakukan otomatisasi echo test ke TMS setiap 60 detik sekali.
     * Hal ini dilakukan untuk melakukan pengecekan koneksi ke TMS dan memastikan TMS mengirim response dari echo test.
     * @author PETER SUSANTO     
     */
    class AutomaticPing extends Thread{

        public void run()
        {
            do
            {
                if(stop)
                    break;
                do
                {
                    if(s.isClosed())
                        break;
                    
                    // Marked At : 07-06-2016
                    // Desc 	 : Make blowup used memory... wow!!! 
                    //ISO8583.ReceiveResponse = false;
                    
                    cln.setCommand("ping");
                    try
                    {
                        AutomaticPing _tmp = this;
                        sleep(60000L);
                        //sleep(5000L);
                    }
                    catch(Exception exception) { }
                    
                    if(!Service.ReceiveResponse)
                    {
                    	Service.writeDebug("TMS Request Thread] receive response is false", 0);
                        try
                        {
                            if(!s.isClosed())
                                s.close();
                            hasReconnect = true;
                            Reconnect reconnect = new Reconnect();
                            reconnect.start();
                        }
                        catch(Exception exception1) { }
                        break;
                    }
                    try
                    {
                        AutomaticPing _tmp1 = this;
                        sleep(0x3a980L);
//                        sleep(5000L);
                    }
                    catch(Exception exception2) { }
                } while(true);
            } while(true);
        }

        private Socket s;
        private CommandLine cln;
        volatile boolean stop;
        private boolean hasReconnect;
        final ServiceMezzoTMSRequestThread this$0;

        public AutomaticPing(Socket socket, CommandLine commandline)
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;
            
            stop = false;
            hasReconnect = false;
            s = socket;
            cln = commandline;
        }
    }
    
    /**
     * <strong>Description</strong>:<br>
     * Class CommandLine digunakan untuk mengirim echo test ke TMS 
     * @author PETER SUSANTO     
     */
    class CommandLine extends Thread{	


        public void setSocket(Socket socket)
        {
            s = socket;
        }

        public void setCommand(String s1)
        {
            try
            {
                if(s1.equalsIgnoreCase("help"))
                {
                    Service.writeDebug("List of commands :", 0);
                    Service.writeDebug("help\t     Display this commands", 0);
                    Service.writeDebug("ping\t     Send Network Management Request to Server", 0);
                    Service.writeDebug("quit\t     Disconnect this connection", 0);
                    Service.writeDebug("exit\t     Exit from this program\n", 0);
                } else
                if(s1.equalsIgnoreCase("ping") && s != null)
                {
                	Service.writeDebug((new StringBuilder()).append("Pinging ").append(s.getInetAddress().getHostAddress()).append(" With Network Management Request").toString(), 0);
                    if(s.isClosed())
                    {
                    	Service.writeDebug("TMS Request Thread] Connection already disconnected", 0);
                    } else
                    {
                        String s2 = Service.util.localTransactionTime();
                        IsoMessage isomessage = Service.mfact.newMessage(2048);
                        isomessage.setValue(7, s2, IsoType.DATE10, 0);
    	                isomessage.setValue(33, Service.cbcMezzo, IsoType.LLVAR, 0);
    	                isomessage.setValue(70, "301", IsoType.NUMERIC, 3);
    	                
                        Service.sendISO(s, isomessage, "AFEP Request Thread");
                        
            			String pesan = new String( isomessage.writeData() );
            			Service.writeDebug("TMS Request Thread] '"+pesan+"'" );
                        
                    	for(int i = 0; i < 104; i++)
                            if(isomessage.hasField(i))
                            	Service.writeDebug((new StringBuilder()).append("TMS Request Thread] Field ").append(i).append(" : ").append(isomessage.getField(i).toString()).toString());                  
                    }
                } else
                if(s1.equalsIgnoreCase("exit"))
                {
                	Service.writeDebug("Disconnecting ...", 0);
                    try
                    {
                        if(!s.isClosed())
                            s.close();
                    }
                    catch(Exception exception) { }
                    System.exit(0);
                } else
                if(!s1.equalsIgnoreCase(""))
                	Service.writeDebug("Bad Command", 0);
            }
            catch(Exception exception1) { }
        }

        public void run()
        {
            String s1 = "";
            try
            {
                BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
                do
                {
                    String s2 = bufferedreader.readLine();
                    if(s2.equalsIgnoreCase("help"))
                    {
                    	Service.writeDebug("List of commands :", 0);
                    	Service.writeDebug("help\t     Display this commands", 0);
                    	Service.writeDebug("ping\t     Send Network Management Request to Server", 0);
                    	Service.writeDebug("quit\t     Disconnect this connection", 0);
                    	Service.writeDebug("exit\t     Exit from this program\n", 0);
                    } else
                    if(s2.equalsIgnoreCase("ping") && s != null)
                    {
                    	Service.writeDebug((new StringBuilder()).append("TMS Request Thread] Pinging ").append(s.getInetAddress().getHostAddress()).append(" With Network Management Request").toString(), 0);
                        
                                               
                        if(s.isClosed())
                        {
                        	Service.writeDebug("TMS Request Thread] Connection already disconnected", 0);
                        } else
                        {
                            String s3 = Service.util.localTransactionTime();
                            IsoMessage isomessage = Service.mfact.newMessage(2048);
                            isomessage.setValue(7, s2, IsoType.DATE10, 0);
        	                isomessage.setValue(33, Service.cbcMezzo, IsoType.LLVAR, 0);
        	                isomessage.setValue(70, "601", IsoType.NUMERIC, 3);
        	                Service.sendISO(s, isomessage, "AFEP Request Thread");
                        }
                    } else
                    if(s2.equalsIgnoreCase("exit"))
                    {
                    	Service.writeDebug("Disconnecting ...", 0);
                        try
                        {
                            if(!s.isClosed())
                                s.close();
                        }
                        catch(Exception exception1) { }
                        System.exit(0);
                    } else
                    if(!s2.equalsIgnoreCase(""))
                    	Service.writeDebug("Bad Command", 0);
                } while(true);
            }
            catch(Exception exception)
            {
            	Service.writeDebug(exception.getMessage(), 0);
            }
        }

        private Socket s;
        final ServiceMezzoTMSRequestThread this$0;

        public CommandLine(Socket socket)
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;
            
            s = socket;
        }

        public CommandLine()
        {
        	super();
            this$0 = ServiceMezzoTMSRequestThread.this;
            
            s = null;
        }      
    
		
	}
    
    /**
     * <strong>Description</strong>:<br>
     * Method ini merupakan constructor dari class ServiceMezzoTMSRequestThread yang menjalan 3 thread yaitu : <br>
     * <ul>
     * 	<li>CommandLine</li>
     *  <li>AutomaticPing</li>
     *  <li>Listener</li>
     * </ul>  
     * @param sockTMS
     */
    public ServiceMezzoTMSRequestThread( Socket sockTMS ){
    	
    	cl 							= new CommandLine(sockTMS);
		ap 							= new AutomaticPing(sockTMS, cl);
		l 							= new Listener(cl);  
		
		cl.start();
        l.start();
        ap.start();
    	
    }
	
}
