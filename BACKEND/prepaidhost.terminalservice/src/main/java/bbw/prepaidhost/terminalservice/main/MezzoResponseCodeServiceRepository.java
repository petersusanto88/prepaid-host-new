package bbw.prepaidhost.terminalservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.terminalservice.model.MezzoResponseCode;
import bbw.prepaidhost.terminalservice.util.HibernateUtil;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan untuk operasi database yang berhubungan dengan konversi response code dari PrepaidHost ke response code TMS.
 *
 */
public class MezzoResponseCodeServiceRepository {

	public MezzoResponseCodeServiceRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method untuk mengambil ALTO Response Code.
	 * @param mezzoResponseCode
	 * @return
	 */
	public String getALTOResponseCode( String mezzoResponseCode ){
		
		String altoRC 					= "";
		
		SessionFactory sf				= HibernateUtil.getSessionFactory();
		Session session 				= sf.getCurrentSession();
		
		Transaction tx					= session.beginTransaction();
		Query q							= session.createQuery( "from MezzoResponseCode where responseCode = :rc " )
											.setString("rc", mezzoResponseCode);
		
		List<MezzoResponseCode> lRC		= q.list();
		for( MezzoResponseCode mrc : lRC ){
			altoRC						= mrc.getAltoResponseCode();
		}
		
		return altoRC;
		
	}
	
}
