package bbw.prepaidhost.terminalservice.main;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.util.HibernateUtil;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini dipakai sebagai operasi database yang berhubungan dengan transaksi.
 *
 */
public class TransactionServiceRepository {

	public TransactionServiceRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method yang digunakan untuk memanggil FUNCTION Postgresql terkait transaksi topup (Topup Cash -> Online)
	 * @param cardNumber
	 * @param cardBalance
	 * @param amountTopupCash
	 * @param vAESKey
	 * @param merchantCode
	 * @param terminalCode
	 * @param samId
	 * @param counterSAM
	 * @param counterCard
	 * @param traceNumber
	 * @param transactionFrom
	 * @return
	 */
	public String callSPTopupCash( String cardNumber,
							       double cardBalance,
							       double amountTopupCash,
							       String vAESKey,
							       //long issuerId,
							       String merchantCode,
							       String terminalCode,
							       long samId,
							       String counterSAM,
							       String counterCard,
							       //String keyVersion,
							       String traceNumber,
							       short transactionFrom){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sqlQuery 				= " SELECT  sp_topup_cash(:vCardNumber,"
															   + ":vCardBalance,"
															   + ":vAmount,"
															   + ":vAESKey,"
															   + ":vMerchantCode,"
															   + ":vTerminalCode,"
															   + ":vSAMId,"
															   + ":vCounterSAM,"
															   + ":vCounterCard,"
															   + ":vTraceNumber,"
															   + ":vTransactionFrom)";
		String spResult 				= "";
		
		try{
			
			SQLQuery q						= (SQLQuery)session.createSQLQuery(sqlQuery)
												.setParameter("vCardNumber", cardNumber)
												.setParameter("vCardBalance", cardBalance)
												.setParameter("vAmount", amountTopupCash)
												.setParameter("vAESKey", vAESKey)
												.setParameter("vMerchantCode", merchantCode)
												.setParameter("vTerminalCode", terminalCode)
												.setParameter("vSAMId", samId)
												.setParameter("vCounterSAM", counterSAM)
												.setParameter("vCounterCard", counterCard)
												.setParameter("vTraceNumber", traceNumber)
												.setParameter("vTransactionFrom", transactionFrom);
			
			
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TransactionServiceRepository] Exception<callSPTopupCash> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		Service.writeDebug("[TMS Response Thread] <callSPTopupCash>" + spResult + " ###");
		
		return spResult;
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method yang digunakan untuk memanggil FUNCTION postgresql terkait transaksi reversal.
	 * @param isoTraceNumber
	 * @param amount
	 * @return
	 */
	public String callSPVoidTopupCash( String isoTraceNumber, long amount ){
		

		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sqlQuery 				= " SELECT  sp_void_topup_cash(:vIsoTraceNumber,"
															   		+ ":vAmount,"
															   		+ ":vAESKey)";
		String spResult 				= "";
		
		try{
		
			SQLQuery q						= (SQLQuery)session.createSQLQuery(sqlQuery)
												.setParameter("vIsoTraceNumber", isoTraceNumber)
												.setParameter("vAmount", amount)
												.setParameter("vAESKey", Service.aesKey);
			
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TransactionServiceRepository] Exception<callSPVoidTopupCash> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		Service.writeDebug("[TMS Response Thread] <callSPVoidTopupCash>" + spResult + " ###");
		
		return spResult;	
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini dipanggil pada saat acknowledge transaksi topup dan melakukan update ke record transaksi di database.
	 * @param status
	 * @param isoTraceNumber
	 * @return
	 */
	public JSONObject repo_UpdateCardTransactionStatus( short status, String isoTraceNumber){
		
		int resultUpdate 				= 0;
		JSONObject joResult 			= new JSONObject();
		
		SessionFactory sf				= HibernateUtil.getSessionFactory();
		Session session 				= sf.getCurrentSession();
		
		Transaction tx					= session.beginTransaction();
		Query q 						= session.createQuery("update CardTransaction set status = :status where isoTraceNumber = :isoTraceNumber");
		
		try{		
			
			q.setShort("status", status);
			q.setString("isoTraceNumber", isoTraceNumber);
			resultUpdate				= q.executeUpdate();
			tx.commit();
			
			if( resultUpdate == 1 ){
				
				joResult.put("errCode", "00");
				joResult.put("errMsg", "Update transaction status success");
				
			}else{
				
				joResult.put("errCode", "109");
				joResult.put("errMsg", "Update transaction status failed");
				
			}
			
		}catch( Exception ex ){
			joResult.put("errCode", "109");
			joResult.put("errMsg", "Update transaction status failed. Error : " + ex.getMessage());
			Service.writeDebug("Exception <updateStatusTransaction> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return joResult;
		
	}
	
}
