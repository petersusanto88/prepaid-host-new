package bbw.prepaidhost.terminalservice.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.terminalservice.Service;
import bbw.prepaidhost.terminalservice.model.TMSISOLog;
import bbw.prepaidhost.terminalservice.util.HibernateUtil;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan log message dari TMS
 *
 */
public class ISOTMSLogRepository {
	
	public ISOTMSLogRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menyimpan data log dari TMS ke database.
	 * @param data
	 * @return
	 */
	public long saveTMSLog( TMSISOLog data ){
		
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		long logId 							= 0;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			session.beginTransaction();
			
			session.save( data );	
			
			session.getTransaction().commit();
			
			logId 							= data.getId();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[ISOAFEPLogRepository] Exception<saveAFEPLog> : " + ex.getMessage());
			session.getTransaction().rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
		}
		
		return logId;
		
	}

}
