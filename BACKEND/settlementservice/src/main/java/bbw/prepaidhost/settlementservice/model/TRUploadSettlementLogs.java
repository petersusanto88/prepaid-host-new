package bbw.prepaidhost.settlementservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRUploadSettlementLogs\"")
public class TRUploadSettlementLogs {

	@Id
	@Column(name="\"logId\"")
	private long logId;
	
	@Column(name="\"logDate\"")
	private Date logDate;
	
	@Column(name="\"acquirerId\"")
	private int acquirerId;
	
	@Column(name="\"fileNameData\"")
	private String fileNameData;
	
	@Column(name="\"fileNameAck\"")
	private String fileNameAck;
	
	@Column(name="\"fileNameRecon\"")
	private String fileNameRecon;
	
	private short status;
	
	@Column(name="\"startProcess\"")
	private Date startProcess;
	
	@Column(name="\"endProcess\"")
	private Date endProcess;
	
	

	public long getLogId() {
		return logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public int getAcquirerId() {
		return acquirerId;
	}

	public void setAcquirerId(int acquirerId) {
		this.acquirerId = acquirerId;
	}

	public String getFileNameData() {
		return fileNameData;
	}

	public void setFileNameData(String fileNameData) {
		this.fileNameData = fileNameData;
	}
	
	public String getfileNameAck() {
		return fileNameAck;
	}

	public void setFileNameAck(String fileNameAck) {
		this.fileNameAck = fileNameAck;
	}
	
	public String getFileNameRecon() {
		return fileNameRecon;
	}

	public void setFileNameRecon(String fileNameRecon) {
		this.fileNameRecon = fileNameRecon;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public Date getStartProcess() {
		return startProcess;
	}

	public void setStartProcess(Date startProcess) {
		this.startProcess = startProcess;
	}

	public Date getEndProcess() {
		return endProcess;
	}

	public void setEndProcess(Date endProcess) {
		this.endProcess = endProcess;
	}	
}
