package bbw.prepaidhost.settlementservice.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import bbw.prepaidhost.settlementservice.main.Main;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

public class GlobalUtil {

	public String deleteWhiteSpace(String s)
    {
        s = s.replaceAll("\r", "");
        s = s.replaceAll("\n", "");
        s = s.replaceAll("\r\n", "");
        s = s.replaceAll("\t", "");
        s = s.trim();
        return s;
    }

    public boolean isTime(String s)
    {
        String as[] = s.split("\\:");
        Calendar calendar = Calendar.getInstance();
        calendar.set(9, 0);
        if(as[0] != null)
            calendar.set(10, Integer.parseInt(as[0]));
        if(as[1] != null)
            calendar.set(12, Integer.parseInt(as[1]));
        if(as.length > 2 && as[2] != null)
            calendar.set(13, Integer.parseInt(as[2]));
        Calendar calendar1 = Calendar.getInstance();
        return calendar.before(calendar1);
    }

    public String nextTimeInterval(int i)
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        long l = date.getTime();
        l += i * 1000;
        date.setTime(l);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm");
        return simpledateformat.format(date);
    }

    public String currentDateTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
        return simpledateformat.format(date);
    }

    public String currentDate()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
        return simpledateformat.format(date);
    }

    public String localTransactionTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MMddkkmmss");
        return simpledateformat.format(date);
    }
    
    public String localProsesTime()
    {
		Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpledateformat.format(date);
    }
    public String localProses()
    {
		Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("ddMMyyHHmmss");
        
        SimpleDateFormat simpledateformat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("Date Convert    : "+simpledateformat2.format(date));
        return simpledateformat.format(date)+"||"+simpledateformat2.format(date);
    }

    public String stripMSISDN(String s)
    {
        String s1 = "";
        if(s.substring(0, 1).compareTo("+") == 0)
            s1 = s.substring(3);
        else
        if(s.substring(0, 1).compareTo("0") == 0)
            s1 = s.substring(1);
        else
        if(s.substring(0, 2).compareTo("62") == 0)
            s1 = s.substring(2);
        else
            s1 = s;
        return s1;
    }

    public String formatMSISDN(String s)
    {
        String s1 = "";
        if(s.substring(0, 1).compareTo("+") == 0)
            s1 = (new StringBuilder()).append("0").append(s.substring(3)).toString();
        else
        if(s.substring(0, 2).compareTo("62") == 0)
            s1 = (new StringBuilder()).append("0").append(s.substring(2)).toString();
        else
            s1 = s;
        return s1;
    }
    
    
    public String printDataBinding( int iterate ){
    	
    	String tanda = "?";
      	for( int v=1; v<iterate; v++ ) tanda += ",?";
      	return tanda;	
    	
    }
    
    public static byte[] hexToByte(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
    
    public String batchHeaderDateTime()
    {
		Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("ddMMyyyyHHmmss");
        return simpledateformat.format(date);
    }
    
//    public static String sendRequest( String param )
//    {
//    	String msg	= "";
//    	String s3	= "";
//		
//		String request = Main.ipPPOBBackend;
//		
//		Main.writeDebug("[PPOB Batch Main] URL : " + request);
//		Main.writeDebug("[PPOB Batch Main] Param : " + param);
//		
//		try
//        {
//			URL url = new URL(request);
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
//			connection.setDoOutput(true);
//			connection.setDoInput(true);
//			connection.setInstanceFollowRedirects(false); 
//			connection.setRequestMethod("POST"); 
//			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
//			connection.setRequestProperty("charset", "utf-8");
//			connection.setRequestProperty("Content-Length", "" + Integer.toString(param.getBytes().length));
//			connection.setUseCaches (false);
//			OutputStream outputstream = connection.getOutputStream();
//            outputstream.write(param.getBytes());
//            outputstream.close();
//            int i = connection.getResponseCode();
//            if(i == 200)
//            {
//                InputStream inputstream = connection.getInputStream();
//                int j = connection.getContentLength();
//                if(j > 0)
//                {
//                    byte abyte0[] = new byte[j];
//                    int k;
//                    for(int j1 = 0; (k = inputstream.read()) != -1; j1++)
//                        abyte0[j1] = (byte)k;
//
//                    s3 = new String(abyte0);
//                } else
//                {
//                    char c = '\u0800';
//                    byte abyte1[] = new byte[c];
//                    int k1 = 0;
//                    do
//                    {
//                        int i1;
//                        if((i1 = inputstream.read()) == -1)
//                            break;
//                        abyte1[k1] = (byte)i1;
//                        if(++k1 == c)
//                        {
//                            s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
//                            abyte1 = new byte[c];
//                            k1 = 0;
//                        }
//                    } while(true);
//                    if(k1 < c)
//                        s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
//                }
//            }
//            s3 = s3.trim();
//            msg=s3;
//            //billing.writeDebug((new StringBuilder()).append("Request response : ").append(s3).toString());
//            
//            
//        }catch(Exception ex){
//        	Main.writeDebug( "[PPOB Batch Main] sendRequest() "+param  );
//        }
//		/**TODO 2014/01/13, response request printout to file debug**/
//		//Main.writeDebug( "[PPOB Batch Main] Response:> "+msg );
//		return msg;
//	}
    
    private static void trustAllCert() throws NoSuchAlgorithmException, KeyManagementException{
    	
    	// Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };
 
        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
 
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
 
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    	
    }
    
    public String generateRandom( int len ){
		
 	   String AB = "0123456789";
 	   SecureRandom rnd = new SecureRandom();
 		
 	   StringBuilder sb = new StringBuilder( len );
 	   for( int i = 0; i < len; i++ ) 
 	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
 	   return sb.toString();
 		
 	}
    
//    public static String sendRequestHttps( String param )
//    {
//    	String msg	= "";
//    	String s3	= "";
//		
//		String request = Main.ipPPOBBackend;
//		
//		Main.writeDebug("[PPOB Batch Main] URL : " + request);
//		Main.writeDebug("[PPOB Batch Main] Param : " + param);
//		
//		try
//        {
//			trustAllCert();
//			
//			URL url = new URL(request);
//			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();           
//			connection.setDoOutput(true);
//			connection.setDoInput(true);
//			connection.setInstanceFollowRedirects(false); 
//			connection.setRequestMethod("POST"); 
//			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
//			connection.setRequestProperty("charset", "utf-8");
//			connection.setRequestProperty("Content-Length", "" + Integer.toString(param.getBytes().length));
//			connection.setUseCaches (false);
//			OutputStream outputstream = connection.getOutputStream();
//            outputstream.write(param.getBytes());
//            outputstream.close();
//            int i = connection.getResponseCode();
//            if(i == 200)
//            {
//                InputStream inputstream = connection.getInputStream();
//                int j = connection.getContentLength();
//                if(j > 0)
//                {
//                    byte abyte0[] = new byte[j];
//                    int k;
//                    for(int j1 = 0; (k = inputstream.read()) != -1; j1++)
//                        abyte0[j1] = (byte)k;
//
//                    s3 = new String(abyte0);
//                } else
//                {
//                    char c = '\u0800';
//                    byte abyte1[] = new byte[c];
//                    int k1 = 0;
//                    do
//                    {
//                        int i1;
//                        if((i1 = inputstream.read()) == -1)
//                            break;
//                        abyte1[k1] = (byte)i1;
//                        if(++k1 == c)
//                        {
//                            s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
//                            abyte1 = new byte[c];
//                            k1 = 0;
//                        }
//                    } while(true);
//                    if(k1 < c)
//                        s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
//                }
//            }
//            s3 = s3.trim();
//            msg=s3;
//            //billing.writeDebug((new StringBuilder()).append("Request response : ").append(s3).toString());
//            
//            
//        }catch(Exception ex){
//        	Main.writeDebug( "[PPOB Batch Main] Exception sendRequestHttps() " + ex.getMessage()  );
//        }
//		/**TODO 2014/01/13, response request printout to file debug**/
//		//Main.writeDebug( "[PPOB Batch Main] Response:> "+msg );
//		return msg;
//	}
    
    public Date getDateTime() throws ParseException{
		
		Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        return simpledateformat.parse(simpledateformat.format(date));
				
	}
    
    public static String padRight(String s, int n) {
		return String.format("%-" + n + "s", s).replace(' ', '0'); 
	}
	
	public static String padLeft(String s, int n) {
		return s.format("%" + n + "s", s).replace(' ', '0');
	}
	
}
