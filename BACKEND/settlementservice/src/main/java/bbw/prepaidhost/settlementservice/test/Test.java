package bbw.prepaidhost.settlementservice.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import bbw.prepaidhost.settlementservice.util.GlobalUtil;
import bbw.prepaidhost.settlementservice.util.HexaToDouble;

public class Test {
	
//	1=>Normal, 2=>Fraud
	private static int scenario 		  = 2;
	
	// >>> NORMAL CASE
	private static String[] arrCardNumber = {"3007880140000029","3007880140000011","3007880140000003"/*,"2805160140000006","2805160140000048",
											 "2805160140000055","2805160140000063","2805160140000071","2805160140000089","2805160140000097",
											 "2805160140000105","2805160140000105","2805160140000113","2805160140000121","2805160140000147",
											 "2805160140000154","2805160140000162","2805160140000170","2805160140000014","2805160140000030"*/};

	
	private static String[] arrUID 		  = { "000430375AFF2B61","000430375AFF2B97","000430375AFF2B96"/*,"000430375AFF2B53","000430375AFF2B04",
											  "000430375AFF2B05","000430375AFF2B06","000430375AFF2B07","000430375AFF2B08","000430375AFF2B09",
											  "000430375AFF2B10","000430375AFF2B10","000430375AFF2B11","000430375AFF2B12","000430375AFF2B14",
											  "000430375AFF2B15","000430375AFF2B16","000430375AFF2B17","000430375AFF2B01","000430375AFF2B03"*/};
	
								  
	private static String[] arrCardType   = { "63","63","63"/*,"62","62",
											  "62","62","62","62","62",
											  "62","62","62","62","62",
											  "62","62","62","62","62"*/};	

	private static int[] arrLastBalance   = { 500000, 500000, 500000/*, 472000, 472000,
											  472000, 472000, 493000, 496500, 500000,
											  500000, 500000, 500000, 500000, 500000,
											  500000, 500000, 500000, 500000, 500000*/};
	
	private static String[] arrTransDateTime = { "19102017150001", "19102017150002", "19102017150003"/*, "31082017151004", "31082017151005",
												 "31082017151006", "31082017151007", "31082017220008", "31082017210008", "31082017140010",
												 "31082017140011", "31082017140012", "31082017140013", "31082017140014", "31082017140015",
												 "31082017140016", "31082017140017", "31082017140018", "31082017140019", "31082017140020"*/};

	
//  >>> FRAUD SCENARIO 1
	private static String[] _FRAUD_arrCardNumber = {"3007880140000029","3007880140000029","3007880140000029","3007880140000029","3007880140000029",
											 		"3007880140000029","3007880140000029","3007880140000011","3007880140000003"};
								
								
	private static String[] _FRAUD_arrUID 		  = { "000430375AFF2B61","000430375AFF2B61","000430375AFF2B61","000430375AFF2B61","000430375AFF2B61",
											  	      "000430375AFF2B61","000430375AFF2B61","000430375AFF2B97","000430375AFF2B96"};
	
	private static String[] _FRAUD_arrCardType   = { "63","63","63","63","63",
											  		 "63","63","63","63"};
	
	private static int[] _FRAUD_arrLastBalance   = { 496500, 493000, 489500, 486000, 482500,
													 479000, 475500, 496500, 496500/*, 500000,
													 500000, 500000, 500000, 500000, 500000,
													 500000, 500000, 500000, 500000, 500000*/};
	
	private static String[] _FRAUD_arrTransDateTime = { "19102017150004", "19102017150006", "19102017150010", "19102017150012", "19102017150015",
			 									 		"19102017150017", "19102017150020", "19102017160002", "19102017170003"};
	
	private static int amount 			  = 3500;
	
	private static String SAMID 		  = "201701240000000F";
	
	private static String merchantId 	  = "11223344";
	private static String terminalId 	  = "01111111";
	private static String issuerId 		  = "0041";
	
	private static GlobalUtil util 		 = new GlobalUtil();
	private static HexaToDouble hexUtil	 = new HexaToDouble();
	
	private static int lineNumber		 = 0;
	
	private static void generateSettlementFile() throws IOException {
		
		String fileName    = (new StringBuilder())
								.append(merchantId)
								.append(util.batchHeaderDateTime())
								.append("001")
								.append(".DATA")
								.toString();
		
		PrintWriter writer = new PrintWriter("D:\\BBW\\AG Prepaid Host\\Settlement Offline\\DATA\\" + fileName, "UTF-8");
		//PrintWriter writerCRC32 = new PrintWriter("D:\\BBW\\AG Prepaid Host\\Settlement Offline\\DATA\\fileCRC32.DATA", "UTF-8");
		
		
		if( scenario == 1 ) {
		
			/*Print HEADER*/
			String header 		= (new StringBuilder())
									.append("H")
									.append( util.padLeft( Integer.toString(arrCardNumber.length), 8) )
									.append( util.padLeft( Long.toString(((amount) * arrCardNumber.length)),12) )
									.toString();
			
			writer.println(header);
			
			/*Print Content*/
			lineNumber 		= 0;
			for( int i = 0; i < arrCardNumber.length; i++ ) {
				
				String content 	= ( new StringBuilder() )
									.append( arrCardNumber[i] ) 			//	Card Number
									.append( "0000000000000000" ) 			//  SAM ID
									.append( terminalId )           		//	Terminal ID
									.append( merchantId )           		// 	Merchant ID
									.append( "11" )							//	Trans Code
									.append( util.padLeft(Integer.toString(amount), 8) ) 	// 	Amount
									.append( util.padLeft(Integer.toString(arrLastBalance[i] - amount), 8) )	// 	Last Balance
									.append( arrTransDateTime[i] )//util.batchHeaderDateTime() )	// Date Time
									.append( "2F000000" )					// Counter SAM
									.append( "210000" )						// Counter Card
									.append( issuerId )						// Index Issuer
									.append( arrCardType[i] )				// Card Type
									.append( "0000000000000000" )			// RFU
									.append( "000000" )						// MAC Issuer
									.append( "000000" )						// MAC Mezzo
									.append( "9000" )						// Smartcard Status
									.append( util.padLeft( Integer.toString((i+1)), 6) )	// Sequence
									.append( util.padLeft(Integer.toString(amount), 8) )	// Normal Amount
									.append( arrUID[i] )						// UID
									.append( util.generateRandom(10) )		// Transaction ID
									.toString();
				
				writer.println(content);
				lineNumber++;
				
			}
			
		}else {
			
			/*Print HEADER*/
			String header 		= (new StringBuilder())
									.append("H")
									.append( util.padLeft( Integer.toString(_FRAUD_arrCardNumber.length), 8) )
									.append( util.padLeft( Long.toString(((amount) * _FRAUD_arrCardNumber.length)),12) )
									.toString();
			
			writer.println(header);
			
			/*Print Content*/
			lineNumber 		= 0;
			for( int i = 0; i < _FRAUD_arrCardNumber.length; i++ ) {
				
				String content 	= ( new StringBuilder() )
									.append( _FRAUD_arrCardNumber[i] ) 			//	Card Number
									.append( "0000000000000000" ) 			//  SAM ID
									.append( terminalId )           		//	Terminal ID
									.append( merchantId )           		// 	Merchant ID
									.append( "11" )							//	Trans Code
									.append( util.padLeft(Integer.toString(amount), 8) ) 	// 	Amount
									.append( util.padLeft(Integer.toString(_FRAUD_arrLastBalance[i] - amount), 8) )	// 	Last Balance
									.append( _FRAUD_arrTransDateTime[i] )//util.batchHeaderDateTime() )	// Date Time
									.append( "2F000000" )					// Counter SAM
									.append( "210000" )						// Counter Card
									.append( issuerId )						// Index Issuer
									.append( _FRAUD_arrCardType[i] )				// Card Type
									.append( "0000000000000000" )			// RFU
									.append( "000000" )						// MAC Issuer
									.append( "000000" )						// MAC Mezzo
									.append( "9000" )						// Smartcard Status
									.append( util.padLeft( Integer.toString((i+1)), 6) )	// Sequence
									.append( util.padLeft(Integer.toString(amount), 8) )	// Normal Amount
									.append( _FRAUD_arrUID[i] )						// UID
									.append( util.generateRandom(10) )		// Transaction ID
									.toString();
				
				writer.println(content);
				lineNumber++;
				
			}
			
		}
		
		/*Get CRC32*/
		writer.close();
		CheckedInputStream cis = null;
		try {
			cis = new CheckedInputStream( new FileInputStream( new File( "D:\\BBW\\AG Prepaid Host\\Settlement Offline\\DATA\\" + fileName ) ) , new CRC32());
		} 
    	catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		byte[] buf = new byte[128];
		while(cis.read(buf)>=0){}
		
		long checksum = cis.getChecksum().getValue();
		String ChecksumHex = Long.toHexString(checksum);
		
		
		/*Print Footer*/
		FileWriter fw  		= new FileWriter("D:\\BBW\\AG Prepaid Host\\Settlement Offline\\DATA\\" + fileName, true);
		String footer 		= ( new StringBuilder() )
								.append("T")
								.append( util.padLeft( Integer.toString(lineNumber) , 8) )
								.append(ChecksumHex)
								.toString();
		fw.write(footer + "\n");
		fw.close();
		
		
		
		writer.close();
		
	}
	
	public static void main( String[] args ) throws IOException {
		generateSettlementFile();
	}
	
}
