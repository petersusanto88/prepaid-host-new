package bbw.prepaidhost.settlementservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSIssuers\"")
public class MSIssuers {

	@Id
	@Column(name="\"issuerId\"")
	private int issuerId;
	
	@Column(name="\"issuerCode\"")
	private String issuerCode;
	
	@Column(name="\"issuerName\"")
	private String issuerName;
	
	@Column(name="\"registerDate\"")
	private Date registerDate;
	
	@Column(name="\"bankCode\"")
	private String bankCode;
	
	@Column(name="\"useMezzoMoney\"")
	private short useMezzoMoney;
	
	private short status;
	
	@Column(name="\"statusDescription\"")
	private String statusDescription;
	
	@Column(name="\"adminChangeStatus\"")
	private int adminChangeStatus;
	
	@Column(name="\"approvalStatus\"")
	private short approvalStatus;
	
	@Column(name="\"adminApproval\"")
	private int adminApproval;
	
	@Column(name="\"approvalDescription\"")
	private String approvalDescription;
	
	@Column(name="\"dbHost\"")
	private String dbHost;
	
	@Column(name="\"dbUsername\"")
	private String dbUsername;
	
	@Column(name="\"dbPassword\"")
	private String dbPassword;
	
	@Column(name="\"dbPort\"")
	private int dbPort;
	
	@Column(name="\"issuerSettlement\"")
	private short issuerSettlement;

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerCode() {
		return issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	public short getUseMezzoMoney() {
		return useMezzoMoney;
	}

	public void setUseMezzoMoney(short useMezzoMoney) {
		this.useMezzoMoney = useMezzoMoney;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public int getAdminChangeStatus() {
		return adminChangeStatus;
	}

	public void setAdminChangeStatus(int adminChangeStatus) {
		this.adminChangeStatus = adminChangeStatus;
	}
	
	public short getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(short approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public int getAdminApproval() {
		return adminApproval;
	}

	public void setAdminApproval(int adminApproval) {
		this.adminApproval = adminApproval;
	}
	
	public String getApprovalDescription() {
		return approvalDescription;
	}

	public void setApprovalDescription(String approvalDescription) {
		this.approvalDescription = approvalDescription;
	}
	
	public String getDbHost() {
		return dbHost;
	}

	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}
	
	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	
	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
	public int getDbPort() {
		return dbPort;
	}

	public void setDbPort(int dbPort) {
		this.dbPort = dbPort;
	}
	
	public short getIssuerSettlement() {
		return issuerSettlement;
	}

	public void setIssuerSettlement(short issuerSettlement) {
		this.issuerSettlement = issuerSettlement;
	}
}
