package bbw.prepaidhost.settlementservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSMerchants\"")
public class MSMerchants {

	@Id
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Column(name="\"merchantName\"")
	private String merchantName;
	
	private String address;
	
	private short status;
	
	@Column(name="\"acquireId\"")
	private int acquireId;
	
	
	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}
	
	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	
	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}
	
	public int getAquireId() {
		return acquireId;
	}

	public void setAquireId(int acquireId) {
		this.acquireId = acquireId;
	}
	
}
