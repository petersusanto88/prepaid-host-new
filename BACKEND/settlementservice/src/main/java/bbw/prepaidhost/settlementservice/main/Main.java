package bbw.prepaidhost.settlementservice.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import bbw.prepaidhost.settlementservice.model.MSMerchants;
import bbw.prepaidhost.settlementservice.service.TransactionServiceImpl;
import bbw.prepaidhost.settlementservice.thread.MezzoSettlementServiceThread;
import bbw.prepaidhost.settlementservice.util.GlobalUtil;
import bbw.prepaidhost.settlementservice.util.HexaToDouble;

//import id.co.trisakti.mezzolib.LibMezzoSC;

public class Main {

	public static OutputStream out;
	private static FileOutputStream fos = null;
	private static boolean debug;
	private static String debugFilename = "";
	public static int interval = 1;
    private static Properties props = null;
    private static String fileDate = "";
    public static GlobalUtil util = new GlobalUtil();
    public static HexaToDouble hex = new HexaToDouble();
    public static TransactionServiceImpl ts = new TransactionServiceImpl();
    
    private static MezzoSettlementServiceThread mezzoSettlementServiceThread;
    
    String config = "config_mezzo_service_service.ini";
    public static String ipMezzoBackend = "";
    public static String aesKey = "";
    public static String hostnameHSM = "";
    public static int portHSM = 7777;
    
	public static void main(String[] args) {
		writeDebug("----------------------------------------------------------------");
		Main notif = new Main();       
    }
	
	public Main(){
		
		try {		
			getConfiguration(config);			
			fileDate		= util.currentDate();
			debug 			= true;			
			debugFilename	= props.getProperty("debugFilename");
			aesKey = props.getProperty("aeskey");
			portHSM = Integer.parseInt(props.getProperty("portHSM"));
			hostnameHSM = props.getProperty("hostnameHSM");
			interval		= Integer.parseInt( props.getProperty("interval") );
			openFileDebug();
			writeDebug("----------------------------------------------------------------");
            writeDebug((new StringBuilder()).append("Prepaid Host Settlement SERVICE, started at : ").append(util.currentDateTime()).toString());
            writeDebug("----------------------------------------------------------------");
//            
//            double RFU_Hex =    hex.getDouble("0000000000000000");
//            double Amount_Hex = hex.getDouble("88130000");
//            double Last_Hex =   hex.getDouble("204E0000");
//            double Normal_Hex = hex.getDouble("10270000");
//            double totalAmount = Amount_Hex+Last_Hex+Normal_Hex;
//            
//            System.out.println("RFU_Hex      : "+RFU_Hex);
//            System.out.println("Amount_Hex   : "+Amount_Hex);
//            System.out.println("Last_Hex     : "+Last_Hex);
//            System.out.println("Normal_Hex   : "+Normal_Hex);
//            
//            System.out.println("Total   : "+totalAmount);
//            int totalint = (int)totalAmount;
//            
//            String totstring = String.valueOf(totalint);
//            System.out.println("TotalString   : "+totstring);
//            String totHex = hex.decToHex(totalint);
//            System.out.println("TotalHEx   : "+totHex);
            
            
			
//			Date dateTime = new SimpleDateFormat("ddMMyyHHmmss").parse("100317112927");
//			SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			
//			String dates = simpledateformat.format(dateTime);
//			
//			System.out.println("Date                       : "+dates);
//			System.out.println("RFU_Hex                    : "+0);
//			System.out.println("Amount_Hex                 : "+5000);
//			System.out.println("Last_Hex                   : "+20000);
//			System.out.println("Normal_Hex                 : "+10000);
//			
//            String ResultSettlementPurchase = ts.callSPsettlementPurchase("0014030000000019", 
//																		  "201701240000000F", 
//																		  "01234567", 
//																		  "11223344", 
//																		  Integer.parseInt("11"), 
//																		  0, 
//																		  5000, 
//																		  20000, 
//																		  dateTime, 
//																		  "0E000000", 
//																		  "000000", 
//																		  Integer.parseInt("01"),
//																		  10000, 
//																		  "0011223344", 
//																		  Main.aesKey);
//            	System.out.println("ResultSettlementPurchase    : "+ResultSettlementPurchase);
//            int nomor = 1000;           
//            System.out.println("Nomor                  : "+String.format("%012d", nomor));
            
            mezzoSettlementServiceThread 			= new MezzoSettlementServiceThread(1);
            mezzoSettlementServiceThread.start();
            
        }catch(Exception exception) {
        	Main.writeDebug("[Mezzo Upload Service] Service Exception :  " + exception.toString());
        }
	}
	
	public static void writeDebug(String s) {
        try {
        	System.out.println(s);	
            if(out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
            if(debug) {
            	if(fileDate.compareTo(util.currentDate()) != 0) {
                    try {
                        if(fos != null)
                            fos.close();
                    }
                    catch(Exception exception) {
                        System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
                    }
                    fileDate = util.currentDate();
                    openFileDebug();
                }
                s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
                try {
                    fos.write(s.getBytes());
                }
                catch(IOException ioexception) {
                    System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
                }
            }
        }
        catch(Exception exception1) { }
    }
	public static void writeDebug(String s, int i) {
        try {
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
	
	private static void openFileDebug() {
        try {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            System.out.println("debugfilename  " + s);
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception) {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
	
	private void getConfiguration(String s) {
        if(s != null) {
            File file = new File(s);
            try {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception) {
                System.err.println(filenotfoundexception);
                Main.writeDebug("Exception getConfiguration :  " + filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception) {
                System.err.println(ioexception);
                Main.writeDebug("Exception getConfiguration :  " + ioexception);
                System.exit(1);
            }
        }
    }
	
}
