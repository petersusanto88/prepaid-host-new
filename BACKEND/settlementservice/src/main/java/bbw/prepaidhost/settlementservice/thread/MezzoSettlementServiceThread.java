package bbw.prepaidhost.settlementservice.thread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import bbw.prepaidhost.settlementservice.main.Constanta;
import bbw.prepaidhost.settlementservice.main.Main;
import bbw.prepaidhost.settlementservice.model.MSIssuers;
import bbw.prepaidhost.settlementservice.model.MSMerchants;
import bbw.prepaidhost.settlementservice.model.TRUploadSettlementLogs;
import bbw.prepaidhost.settlementservice.service.TransactionServiceImpl;
import bbw.prepaidhost.settlementservice.util.GlobalUtil;
import bbw.prepaidhost.settlementservice.util.HexaToDouble;

//import id.co.trisakti.mezzolib.LibMezzoSC;


/**
 * @author PETER SUSANTO
 * @version 1.0
 * <br><br>
 * <strong>Description</strong>:<br>
 * Thread ini berfungsi untuk memproses data settlement yang ada di file settlement.
 */
public class MezzoSettlementServiceThread extends Thread {
	String line;
	private static String NamaFileDB = "";
	private static TransactionServiceImpl transactionServiceImpl;
	private static GlobalUtil globalutil;
	public static HexaToDouble hex = new HexaToDouble();
	public static String SuffixData = ".data";
	public static String FileNameData = "";
	public static String SuffixACK = ".ack";
	public static String FileNameAck = "";
	public static String SuffixRCN = ".rcn";
	public static String FileNameRCN = "";
	public static String Header = "";
	public static String HCount_of_Row = "";
	public static String Summary_of_Amount = "";
	public static String Card_ID = "";
	public static String Card_ID_Content = "";
	public static String SAM_ID = "";
	public static String SAM_ID_Content = "";
	public static String Terminal_ID = "";
	public static String Terminal_ID_Content = "";
	public static String Merchant_ID = "";
	public static String Merchant_ID_Content = "";
	public static String Trans_Code = "";
	public static String Trans_Code_Content = "";
	public static String RFU = "";
	public static String RFU_Content = "";
	public static String Amount = "";
	public static String Amount_Content = "";
	public static String Last_Balance = "";
	public static String Last_Balance_Content = "";
	public static String Date_time = "";
	public static String Date_time_Content = "";
	public static String Counter_SAM = "";
	public static String Counter_SAM_Content = "";
	public static String Counter_Card = "";
	public static String Counter_Card_Content = "";
	public static String Index_Issuer = "";
	public static String Index_Issuer_Content = "";
	public static String Card_Type = "";
	public static String Card_Type_Content = "";
	public static String Key_Version = "";
	public static String Key_Version_Content = "";
	public static String MACIssuer = "";
	public static String MACIssuer_Content = "";
	public static String MACMezzo = "";
	public static String MACMezzo_Content = "";
	public static String Smartcard_Status = "";
	public static String Smartcard_Status_Content = "";
	public static String Sequence = "";
	public static String Sequence_Content = "";
	public static String Normal_Amount = "";
	public static String Normal_Amount_Content = "";
	public static String UID = "";
	public static String UID_Content = "";
	public static String Trailler = "";
	public static String TCount_of_Row = "";
	public static String CRC32 = "";
	public static String Records_indicator = "S";
	public static String Number_of_records_received = "";
	public static String Status = "";
	public static long logId;
	public static String Merchant_Code = "";
	public static int AquirerID;
	public static String Transaction_ID = "";
	public static String Transaction_ID_Content = "";
	public static String TotalAmountInFile = "";
	public static String NumOfRecordsSettled = "";
	public static String TotalAmountSettled = "";
	public static String NumOfRecordsRejected = "";
	public static String TotalAmountRejected = "";
	public static String fileName = "";
	public static int rfu_purchase;
	public static int amount_purchase;
	public static int last_purchase;
	public static int normal_purchase;
	public static int total_amount;
	public static int num_of_records_settled;
	public static int total_amount_settled;
	public static int num_of_records_rejected;
	public static int total_amount_rejected;
	
	private static boolean flagProccess = false; 
	
	Connection con = null;
	CallableStatement proc = null;

	FileReader frSettlement = null;
	BufferedReader brSettlement = null;
	FileReader frContent = null;
	BufferedReader brContent = null;
	FileReader frContentSettlement = null;
	BufferedReader brContentSettlement = null;
	
	
	
	public void run(){
		transactionServiceImpl = new TransactionServiceImpl();
		globalutil = new GlobalUtil();
		try {
			if( !this.stop ){
				while(true){
					Thread.sleep( Main.interval * 1000 );
					
					File diractoryFile = new File(Constanta.CONST_PATH_TO_FILE_DATA);
				    FilenameFilter filterData = new FilenameFilter() {
				    	public boolean accept (File diractoryFile, String name) { 
				            return name.endsWith(".DATA");
				        } 
				    }; 
				    
				    String[] fileData = diractoryFile.list(filterData);
				    if (fileData == null) {
				    	System.out.println("File Not Found");
				    } 
				    else { 
				    	
				    	fileName = "";
				    	
				    	for (int i=0; i< fileData.length; i++) {
				    		
				    		if(fileName==null){
				    			System.out.println("DataSettlement Empty [1]");
				    		}else{
				    			File[] files = diractoryFile.listFiles(filterData);
					            String filename = fileData[i];
					            System.out.println("File Name Settlement    : "+filename);
					        	FileNameData = filename;
					        	Merchant_Code = FileNameData.substring(0, 8);
					        	/*System.out.println("File Name  : "+FileNameData.substring(0, 23));
					        	System.out.println("MerchantID : "+FileNameData.substring(0, 8));
					        	System.out.println("Path and Name           : "+diractoryFile.getAbsolutePath()+"\\"+FileNameData);
*/						    	fileName = diractoryFile.getAbsolutePath()+"\\"+FileNameData;
				    		}
				    		
				    	}
			        	
//			        	System.out.println("------------------Start Proses File-------------------");
			        	if(fileName.isEmpty()){
			        		flagProccess = false;
			    		}else if( transactionServiceImpl.checkFileExists(FileNameData) ) {
			    			
			    			System.out.println("File exists");
			    			flagProccess = false;
			    		}
			    		else{
			    			
			    			flagProccess = true;
			    			
			    			try {
					    		frSettlement = new FileReader(fileName);
						    	brSettlement = new BufferedReader(frSettlement);
						    	line = brSettlement.readLine();
						    	FileNameRCN = "";//FileNameData.substring(0, 25)+SuffixRCN;
						    	PrintWriter writer = new PrintWriter(Constanta.CONST_PATH_TO_FILE_DATA + "fileContent.txt", "UTF-8");
						    	PrintWriter writerContent = new PrintWriter(Constanta.CONST_PATH_TO_FILE_DATA + "ContentSettlement.txt", "UTF-8");
//						    	PrintWriter writerRCN = new PrintWriter(FileNameRCN, "UTF-8");
						    	System.out.println("------Read Data Header in Line------");
						    	Header = line.substring(0,1);						System.out.println("Header                  : "+line.substring(0,1));
						    	HCount_of_Row = line.substring(1,9);				System.out.println("Count of Row            : "+line.substring(1,9));
						    	Summary_of_Amount = line.substring(9,21);			System.out.println("Summary of Amount       : "+line.substring(9,21));
//						    	writerRCN.println(Header+HCount_of_Row+Summary_of_Amount);
						    	writerContent.println(Header+HCount_of_Row+Summary_of_Amount);
						    	
						    	while( (line = brSettlement.readLine()) != null){
//						    	while(brSettlement.readLine() != null){
					    			if(line.substring(0, 1).equals("T")){
					    				break;
					    			}else{
					    				System.out.println("-----Read Data Content in Line------");
								    	Card_ID = line.substring(0,16);				System.out.println("Card ID              	: "+Card_ID);
						            	SAM_ID = line.substring(16,32);				System.out.println("SAM ID               	: "+SAM_ID);
						            	Terminal_ID = line.substring(32,40);		System.out.println("Terminal ID          	: "+Terminal_ID);
						            	Merchant_ID = line.substring(40,48);		System.out.println("Merchant ID          	: "+Merchant_ID);
						            	Trans_Code = line.substring(48,50);			System.out.println("Trans Code           	: "+Trans_Code);
						            	Amount = line.substring(50,58);				System.out.println("Amount               	: "+Amount);
						            	Last_Balance = line.substring(58,66);		System.out.println("Last Balance         	: "+Last_Balance);
						            	Date_time = line.substring(66,80);			System.out.println("Date-Time            	: "+Date_time);								            	
						            	Counter_SAM = line.substring(80,88);		System.out.println("Counter SAM          	: "+Counter_SAM);
						            	Counter_Card = line.substring(88,94);		System.out.println("Counter Card         	: "+Counter_Card);
						            	Index_Issuer = line.substring(94,98);		System.out.println("Index Issuer         	: "+Index_Issuer);
						            	Card_Type = line.substring(98,100);		    System.out.println("Card Type            	: "+Card_Type);
						            	RFU = line.substring(100,116);				System.out.println("RFU                  	: "+RFU);
						            	MACIssuer = line.substring(116,122);		System.out.println("MACIssuer            	: "+MACIssuer);
						            	MACMezzo = line.substring(122,128);	        System.out.println("MACMezzo             	: "+MACMezzo);
						            	Smartcard_Status = line.substring(128,132);	System.out.println("Smartcard Status     	: "+Smartcard_Status);	
						            	Sequence = line.substring(132,138);			System.out.println("Sequence             	: "+Sequence);
						            	Normal_Amount = line.substring(138,146);	System.out.println("Nominal Amount       	: "+Normal_Amount);
						            	UID = line.substring(146,162);				System.out.println("UID                  	: "+UID);
						            	Transaction_ID = line.substring(162,172);	System.out.println("Transaction ID       	: "+Transaction_ID);
						            	writer.println(Card_ID+SAM_ID+Terminal_ID+Merchant_ID+Trans_Code+Amount+Last_Balance+Date_time+Counter_SAM+Counter_Card+Index_Issuer+Card_Type+RFU+MACIssuer+MACMezzo+Smartcard_Status);//+UID);
						            	writerContent.println(Card_ID+SAM_ID+Terminal_ID+Merchant_ID+Trans_Code+Amount+Last_Balance+Date_time+Counter_SAM+Counter_Card+Index_Issuer+Card_Type+RFU+MACIssuer+MACMezzo+Smartcard_Status+Sequence+Normal_Amount+UID+Transaction_ID);
					    			}
						    	}
						    	writer.close();
						    	writerContent.close();
						    	System.out.println("------Read Data Footer in Line------");
						    	Trailler = line.substring(0,1);				System.out.println("Trailler                : "+line.substring(0,1));
			            		TCount_of_Row = line.substring(1,9);		System.out.println("TCount of Row           : "+line.substring(1,9));
			            		CRC32 = line.substring(9, 17); 				System.out.println("CRC32                   : "+line.substring(9,17));
			            													
			            		List<MSMerchants> list = Main.ts.checkMerchantId(Merchant_ID);
			                	for(MSMerchants logs : list){
			        		    	AquirerID = logs.getAquireId();
			        		    	System.out.println("AquirerID      		: "+AquirerID);
			                	}
			            		
			            		//4. Validate CRC32
						    	CheckedInputStream cis = null;
						    	
						    	File fileContent =new File(Constanta.CONST_PATH_TO_FILE_DATA + "fileContent.txt");
						    	File ContentSettlement =new File(Constanta.CONST_PATH_TO_FILE_DATA + "ContentSettlement.txt");

						    	try {
									cis = new CheckedInputStream(new FileInputStream(ContentSettlement), new CRC32());
								} 
						    	catch (Exception e) {
									System.out.println(e.getMessage());
								}
						    	
			            		byte[] buf = new byte[128];
			            		while(cis.read(buf)>=0){}
			            		
			            		long checksum = cis.getChecksum().getValue();
			            		String ChecksumHex = Long.toHexString(checksum);
			            		System.out.println("Result CRC32            : "+ChecksumHex);
						            		
			            		String StartProses = globalutil.localProsesTime();
	        					String EndProses = globalutil.localProsesTime();
	        					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
	        					Date resultStartProses;
	        					Date resultEndProses;
	        					resultStartProses = df.parse(StartProses);
								resultEndProses = df.parse(EndProses);
								
//								//Insert SettlementLog
								String ResultInsertSettlement = transactionServiceImpl.callSPInsertSettlement(AquirerID, FileNameData, "", "", resultStartProses, resultEndProses);
	        					System.out.println("Result Settlement       : "+ResultInsertSettlement);
	        					
	        					logId = Long.parseLong(ResultInsertSettlement);
	        					
			            		if (CRC32.equals(ChecksumHex)) {
			            			String LineContent = "";
			            			String LineSettlement = "";
			            			System.out.println("Validate CRC32          : 00");
			            			//Validate Card With SP
			            			try {		            					
			            				frContent = new FileReader(fileContent);
			            				brContent = new BufferedReader(frContent);
			            				
			            				frContentSettlement = new FileReader(ContentSettlement);
			            				brContentSettlement = new BufferedReader(frContentSettlement);
			            				
			            						int total_line = 0;
			            						
			            						// Karena mengandung header, maka dilewatkan headernya
			            						brContentSettlement.readLine();
			            						
			            						while((LineSettlement = brContentSettlement.readLine())!=null) {
			            							total_line++;
					            					System.out.println("Cont Settlement Hasil   : "+LineSettlement);
					            					Card_ID_Content = LineSettlement.substring(0,16);
					            					SAM_ID_Content = LineSettlement.substring(16,32);	
					            					Terminal_ID_Content = LineSettlement.substring(32,40);
									            	Merchant_ID_Content = LineSettlement.substring(40,48);
									            	Trans_Code_Content = LineSettlement.substring(48,50);
									            	Amount_Content = LineSettlement.substring(50,58);	
									            	Last_Balance_Content = LineSettlement.substring(58,66);
									            	Date_time_Content = LineSettlement.substring(66,80);									            	
									            	Counter_SAM_Content = LineSettlement.substring(80,88);	
									            	Counter_Card_Content = LineSettlement.substring(88,94);
									            	Index_Issuer_Content = LineSettlement.substring(94,98);
									            	Card_Type_Content = LineSettlement.substring(98,100);
									            	RFU_Content = LineSettlement.substring(100,116);
									            	MACIssuer_Content = LineSettlement.substring(116,122);
									            	MACMezzo_Content = LineSettlement.substring(122,128);
									            	Smartcard_Status = LineSettlement.substring(128,132);	
									            	Sequence_Content = LineSettlement.substring(132,138);	
									            	Normal_Amount_Content = LineSettlement.substring(138,146);
									            	UID_Content = LineSettlement.substring(146,162);
									            	Transaction_ID_Content = LineSettlement.substring(162,172);
									            	//SP VALIDATE
					            					String ResultValidateCard = transactionServiceImpl.callSPvalidateCard(Card_ID_Content, UID_Content, Card_Type_Content, Main.aesKey);
					            					System.out.println("Result ValidateCard     : "+ResultValidateCard);
					            					
					            					String arrResultValidateCard[]		= ResultValidateCard.split("\\|");
					            					
					            					if(arrResultValidateCard[0].equals("00")) {
					            						Status = "00";
					            						//Lanjut Deduct
						            					System.out.println("Next Process Deduct     : ----------------------------");
						            					//Insert Settlement Detail
						            					System.out.println("Card_ID                 : "+Card_ID_Content);
						            					System.out.println("SAM_ID                  : "+SAM_ID_Content);
						            					System.out.println("Terminal_ID             : "+Terminal_ID_Content);
						            					System.out.println("Merchant_ID             : "+Merchant_ID_Content);
						            					System.out.println("Trans_Code              : "+Trans_Code_Content);
						            					System.out.println("Amount                  : "+Amount_Content);
						            					System.out.println("Last_Balance            : "+Last_Balance_Content);
						            					System.out.println("Date_time               : "+Date_time_Content);
						            					System.out.println("Counter_SAM             : "+Counter_SAM_Content);
						            					System.out.println("Counter_Card            : "+Counter_Card_Content);
						            					System.out.println("Index_Issuer            : "+Index_Issuer_Content);
						            					System.out.println("Card_Type_Content       : "+Card_Type_Content);
						            					System.out.println("RFU                     : "+RFU_Content);
						            					System.out.println("MACIssuer_Content       : "+MACIssuer_Content);
						            					System.out.println("MACMezzo_Content        : "+MACMezzo_Content);
						            					System.out.println("Smartcard_Status        : "+Smartcard_Status);
						            					System.out.println("Sequence_Content        : "+Sequence_Content);
						            					System.out.println("Normal_Amount           : "+Normal_Amount);
						            					System.out.println("UID_Content             : "+UID_Content);
						            					System.out.println("Transaction_ID_Content  : "+Transaction_ID_Content);
						            										            			            
						            					String ResultInsertSettlementDetail = transactionServiceImpl.callSPInsertSettlementDetail(logId, " ", " ", " ",Card_ID_Content,SAM_ID_Content,Terminal_ID_Content, Merchant_ID_Content,Trans_Code_Content,Amount_Content,Last_Balance_Content,Date_time_Content,Counter_SAM_Content,Counter_Card_Content,Index_Issuer_Content,Card_Type_Content,RFU_Content,MACIssuer_Content,MACMezzo_Content, Smartcard_Status, Sequence_Content,Normal_Amount_Content,UID_Content,Transaction_ID_Content," "," "," ");
						            					System.out.println("Result Settlement Detail: "+ResultInsertSettlementDetail);
						            					
	
						            					double RFU_Hex = hex.getDouble(RFU_Content);
						            		            double Amount_Hex = Double.parseDouble(Amount);//hex.getDouble(Amount_Content);
						            		            double Last_Hex = Double.parseDouble(Last_Balance_Content);//hex.getDouble(Last_Balance_Content);
						            		            double Normal_Hex = Double.parseDouble(Normal_Amount);//hex.getDouble(Normal_Amount);
						            		            
						            		            rfu_purchase = (int)RFU_Hex;
						            		            amount_purchase = (int)Amount_Hex;
						            		            last_purchase = (int)Last_Hex;
						            		            normal_purchase = (int)Normal_Hex;
						            		            
						            					Date dateTime = new SimpleDateFormat("ddMMyyHHmmss").parse(Date_time_Content);
						            					SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						            					
						            					//String dates = simpledateformat.format(dateTime);
						            					
					            						String ResultSettlementPurchase = transactionServiceImpl.callSPsettlementPurchase(Card_ID_Content, 
					            																										  SAM_ID_Content, 
					            																										  Terminal_ID_Content, 
					            																										  Merchant_ID_Content, 
					            																										  Integer.parseInt(Trans_Code_Content), 
					            																										  Integer.parseInt(RFU_Content), 
					            																										  Integer.parseInt(Amount_Content), 
					            																										  Integer.parseInt(Last_Balance_Content), 
					            																										  dateTime , 
					            																										  Counter_SAM_Content, 
					            																										  Counter_Card_Content, 
					            																										  Long.parseLong(Index_Issuer_Content),
					            																										  Integer.parseInt(Normal_Amount), 
					            																										  Transaction_ID_Content, 
					            																										  Main.aesKey);
					            						System.out.println("Result Purchase         : "+ResultSettlementPurchase);
						            											            					
	//					            						writerRCN.println(Card_ID_Content+
	//						            									  SAM_ID_Content+
	//						            									  Terminal_ID_Content+
	//						            									  Merchant_ID_Content+
	//						            									  Trans_Code_Content+
	//						            									  Amount_Content+
	//						            									  Last_Balance_Content+
	//						            									  Date_time_Content+
	//						            									  Counter_SAM_Content+
	//						            									  Counter_Card_Content+
	//						            									  Index_Issuer_Content+
	//						            									  Card_Type_Content+
	//						            									  RFU_Content+
	//						            									  MACIssuer_Content+
	//						            									  MACMezzo_Content+
	//						            									  Smartcard_Status+
	//						            									  Sequence_Content+
	//						            									  Normal_Amount_Content+
	//						            									  UID_Content+
	//						            									  Transaction_ID_Content+
	//						            									  Status);
					            						
	//						            						  				            							  
						            					System.out.println("Status File             : Has Been Processed");
						            					System.out.println("----------------Proses Done Next File-----------------");
					            					}
					            					else if(ResultValidateCard.equals("105")) {
					            						Status = "01";
	//					            						writerRCN.println(Card_ID_Content+
	//						            									  SAM_ID_Content+
	//						            									  Terminal_ID_Content+
	//						            									  Merchant_ID_Content+
	//						            									  Trans_Code_Content+
	//						            									  Amount_Content+
	//						            									  Last_Balance_Content+
	//						            									  Date_time_Content+
	//						            									  Counter_SAM_Content+
	//						            									  Counter_Card_Content+
	//						            									  Index_Issuer_Content+
	//						            									  Card_Type_Content+
	//						            									  RFU_Content+
	//						            									  MACIssuer_Content+
	//						            									  MACMezzo_Content+
	//						            									  Smartcard_Status+
	//						            									  Sequence_Content+
	//						            									  Normal_Amount_Content+
	//						            									  UID_Content+
	//						            									  Transaction_ID_Content+
	//						            									  Status);
					            						System.out.println("Result ValidateCard     : Card not found");
							            				System.out.println("----------------Proses Done Next File-----------------");
					            					}
					            					else if(ResultValidateCard.equals("104")){
					            						Status = "02";
	//					            						writerRCN.println(Card_ID_Content+
	//						            									  SAM_ID_Content+
	//						            									  Terminal_ID_Content+
	//						            									  Merchant_ID_Content+
	//						            									  Trans_Code_Content+
	//						            									  Amount_Content+
	//						            									  Last_Balance_Content+
	//						            									  Date_time_Content+
	//						            									  Counter_SAM_Content+
	//						            									  Counter_Card_Content+
	//						            									  Index_Issuer_Content+
	//						            									  Card_Type_Content+
	//						            									  RFU_Content+
	//						            									  MACIssuer_Content+
	//						            									  MACMezzo_Content+
	//						            									  Smartcard_Status+
	//						            									  Sequence_Content+
	//						            									  Normal_Amount_Content+
	//						            									  UID_Content+
	//						            									  Transaction_ID_Content+
	//						            									  Status);
					            						System.out.println("Result ValidateCard     : Card Inactive");
							            				System.out.println("----------------Proses Done Next File-----------------");
					            					}
					            					else if(ResultValidateCard.equals("106")){
					            						Status = "03";
	//					            						writerRCN.println(Card_ID_Content+
	//						            									  SAM_ID_Content+
	//						            									  Terminal_ID_Content+
	//						            									  Merchant_ID_Content+
	//						            									  Trans_Code_Content+
	//						            									  Amount_Content+
	//						            									  Last_Balance_Content+
	//						            									  Date_time_Content+
	//						            									  Counter_SAM_Content+
	//						            									  Counter_Card_Content+
	//						            									  Index_Issuer_Content+
	//						            									  Card_Type_Content+
	//						            									  RFU_Content+
	//						            									  MACIssuer_Content+
	//						            									  MACMezzo_Content+
	//						            									  Smartcard_Status+
	//						            									  Sequence_Content+
	//						            									  Normal_Amount_Content+
	//						            									  UID_Content+
	//						            									  Transaction_ID_Content+
	//						            									  Status);
					            						System.out.println("Result ValidateCard     : Invalid TID/MID");
							            				System.out.println("----------------Proses Done Next File-----------------");
					            					}
					            					else if(ResultValidateCard.equals("103")){
					            						Status = "04";
	//					            						writerRCN.println(Card_ID_Content+
	//						            									  SAM_ID_Content+
	//						            									  Terminal_ID_Content+
	//						            									  Merchant_ID_Content+
	//						            									  Trans_Code_Content+
	//						            									  Amount_Content+
	//						            									  Last_Balance_Content+
	//						            									  Date_time_Content+
	//						            									  Counter_SAM_Content+
	//						            									  Counter_Card_Content+
	//						            									  Index_Issuer_Content+
	//						            									  Card_Type_Content+
	//						            									  RFU_Content+
	//						            									  MACIssuer_Content+
	//						            									  MACMezzo_Content+
	//						            									  Smartcard_Status+
	//						            									  Sequence_Content+
	//						            									  Normal_Amount_Content+
	//						            									  UID_Content+
	//						            									  Transaction_ID_Content+
	//						            									  Status);
					            						System.out.println("Result ValidateCard     : Card Blacklisted");
							            				System.out.println("----------------Proses Done Next File-----------------");
					            					}
					            				}
			            						System.out.println("Total Line              : "+total_line);
			            						total_amount = amount_purchase*total_line;
			            						num_of_records_settled = total_line;
			            						total_amount_settled = amount_purchase*total_line;
		            							num_of_records_rejected = 0;
		            							total_amount_rejected = 0;
		            							
//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F00")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_SETTLE_NOT_VALID");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F01")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_SETTLE_INVALID_MEZZO_MAC");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F02")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_SETTLE_INVALID_ISSUER_MAC");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F03")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_WRONG_LENGTH");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F03")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_WRONG_LENGTH");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F04")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_NOT_VALID_AMOUNT");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F05")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_NOT_VALID_TRANS");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F06")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_NOT_MATCH_AMOUNT");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F10")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_ERR_SOCKET_OPEN");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else if(Result_SettlementWithoutIssuerKey.equals("6F20")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_ERR_SOCKET_TRANS");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            					else {// if(Result_SettlementWithoutIssuerKey.equals("6F30")){
	//			            						System.out.println("Result Settlement KMS   : "+Result_SettlementWithoutIssuerKey);
	//			            						System.out.println("Result Message KMS      : MSG_ERR_SOCKET_CLOSE");
	//						            			System.out.println("----------------Proses Done Next File-----------------");
	//			            					}
	//			            				}
			            				//GENERATE FILE ACK
	//			            				System.out.println("GENERATE FILE ACK AND RECON");
	//			            				Status = "00";
			            				FileNameAck =  "" ;//FileNameData.substring(0, 25)+SuffixACK;
	//				            			PrintWriter writerAck = new PrintWriter(FileNameAck, "UTF-8");
	////				            			writerAck.println(Header+HCount_of_Row+Summary_of_Amount);
	//				            			writerAck.println(Records_indicator+Number_of_records_received+Status);
	////				            			writerAck.println(Trailler+CRC32);
	//				            			writerAck.close();
	//				            			File file_ACK =new File("D:\\Mezzo\\Project\\settlementservice\\target\\"+FileNameAck);
	//				            			System.out.println("Path n Name File ACK    :  "+file_ACK);
	//				            			
	//				            			TotalAmountInFile = String.format("%012d", total_amount);
	//	        							NumOfRecordsSettled = String.format("%08d", num_of_records_settled);
	//	        							TotalAmountSettled = String.format("%012d", total_amount);
	//	        							NumOfRecordsRejected = String.format("%08d", num_of_records_rejected);
	//	        							TotalAmountRejected = String.format("%08d", total_amount_rejected);
	//	        							
	//	        							System.out.println("TotalAmountInFile       :  "+TotalAmountInFile);
	//	        							System.out.println("NumOfRecordsSettled     :  "+NumOfRecordsSettled);
	//	        							System.out.println("TotalAmountSettled      :  "+TotalAmountSettled);
	//	        							System.out.println("NumOfRecordsRejected    :  "+NumOfRecordsRejected);
	//	        							System.out.println("TotalAmountRejected     :  "+TotalAmountRejected);
	//				            			//GENERATE FILE RECON
	//				            			writerRCN.println(Trailler+
	//				            							  TotalAmountInFile+
	//				            							  NumOfRecordsSettled+
	//				            							  TotalAmountSettled+
	//				            							  NumOfRecordsRejected+
	//				            							  TotalAmountRejected+
	//				            							  CRC32);
	//				            			writerRCN.close();
	//				            			File file_RCN =new File("D:\\Mezzo\\Project\\settlementservice\\target\\"+FileNameRCN);
	//				            			System.out.println("Path n Name File RCN    :  "+file_RCN);
				            			String StartProses1 = globalutil.localProsesTime();
		            					String EndProses1 = globalutil.localProsesTime();
		            					DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
		            					Date resultStartProses1;
		            					Date resultEndProses1;
		            					resultStartProses1 = df.parse(StartProses1);
										resultEndProses1 = df.parse(EndProses1);
										short status = 1;
				            			int ResultUpdateSettlement = transactionServiceImpl.updateStatusSettlementLog(logId, FileNameAck, FileNameRCN, status, resultStartProses1, resultEndProses1);
				            			System.out.println("Update Settlement       : "+ResultUpdateSettlement);
			            			}catch (Exception e) {
			            				e.printStackTrace();
									}finally {
										try{
											if(brContent!=null){
												brContent.close();
											}
											
											if(frContent!=null){
												frContent.close();
											}
											if(brContentSettlement!=null){
												brContentSettlement.close();
											}
											
											if(frContentSettlement!=null){
												frContentSettlement.close();
											}
											
										}catch (Exception e) {
											e.printStackTrace();
										}
									}
			            			fileContent.delete();
			            			ContentSettlement.delete();
			            			
			            		}
			            		
			            		/*WRITE ACK*/			            		
//			            		else{
//			            			System.out.println("CRC32 Salah Kirimkan File ACK");
//			            			Status = "01";
//			            			FileNameAck = FileNameData.substring(0, 25)+SuffixACK;
//			            			PrintWriter writerAck = new PrintWriter(FileNameAck, "UTF-8");
//			            			writerAck.println(Header+HCount_of_Row+Summary_of_Amount);
//			            			writerAck.println(Records_indicator+Number_of_records_received+Status);
//			            			writerAck.println(Trailler+CRC32);
//			            			writerAck.close();
//			            			File file_ACK =new File("D:\\Mezzo\\Project\\settlementservice\\target\\"+FileNameAck);
//			            			System.out.println("Path n Name File ACK"+file_ACK);
//			            			String StartProses1 = globalutil.localProsesTime();
//	            					String EndProses1 = globalutil.localProsesTime();
//	            					DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
//	            					Date resultStartProses1;
//	            					Date resultEndProses1;
//	            					resultStartProses1 = df.parse(StartProses1);
//									resultEndProses1 = df.parse(EndProses1);
//									short status = 1;
//			            			int ResultUpdateSettlement = transactionServiceImpl.updateStatusSettlementLog(logId, FileNameAck, "", status, resultStartProses1, resultEndProses1);
//			            			System.out.println("Update Settlement     : "+ResultUpdateSettlement);
//			            			System.out.println("----------------Proses Done Next File-----------------");
//			            		}
						            		
						    }catch (Exception e) {
						    	e.printStackTrace();
							} finally {
								try{
									if(brSettlement!=null){
										brSettlement.close();
									}
									if(frSettlement!=null){
										frSettlement.close();
									}
								}catch (Exception e) {
									e.printStackTrace();
								}
							}
			    		}
			        	
					    	
				        //Finish Proses
//				    	 System.out.println("-------------------End Proses File--------------------\n");
				    	 File fileDataProses = new File(Constanta.CONST_PATH_TO_FILE_DATA+FileNameData);
				    	 if( fileDataProses.exists() ) {
				    		 if(fileDataProses.length()==0){
					    			System.out.println("DataSettlement Empty");
					    	 }
					    	 else{
					    		 if(fileDataProses.renameTo(new File(Constanta.CONST_PATH_TO_FILE_MOVED+FileNameData))){
						    			System.out.println("Moving Process File     : Successfully");
						    	}
					    		 else{
					    			 
					    			 	if( !flagProccess ) {
					    			 		fileDataProses.delete();
					    			 	}else {
					    			 		System.out.println("File : " + Constanta.CONST_PATH_TO_FILE_MOVED+FileNameData);
							    			System.out.println("Moving Process File     : Failed");	
					    			 	}
					    			 
					    			 	
						    		}	
					    	 }
				    	 }else {
				    		 //System.out.println("No Data Found");
				    	 }
				    	 
				    } 
				}
			}
		}
		catch (Exception ex) {
			 Main.writeDebug("[Mezzo Upload Service] Exception MezzoSettlementServiceThread 1 : " + ex.getMessage());
		}
	}

	volatile boolean stop;
	final MezzoSettlementServiceThread this$0;
	int stat;
	public MezzoSettlementServiceThread(int status){
		super();
        this$0 = MezzoSettlementServiceThread.this;
        stop = false;
        try {
        	stat=status;
        }catch(Exception ex){
        	Main.writeDebug("[Mezzo Upload Service] Exception BatchRequestThread 2 : " + ex.toString());
        }
	    
	}
}