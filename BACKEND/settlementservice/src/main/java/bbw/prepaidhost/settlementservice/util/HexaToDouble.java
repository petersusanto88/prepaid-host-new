package bbw.prepaidhost.settlementservice.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HexaToDouble {
	
	private static final int sizeOfIntInHalfBytes = 8;
    private static final int numberOfBitsInAHalfByte = 4;
    private static final int halfByte = 0x0F;
    
    private static final char[] hexDigits = { 
      '0', '1', '2', '3', '4', '5', '6', '7', 
      '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
	 public double getDouble(String hexa) {
		 double x;
		  byte[] bAmount = hexToByte(hexa);
		  byte[] bAmountLSB = new byte[8];
		  
		  int index = 7;
		  
		  for( int i=0; i<bAmount.length;i++ ){   
		   bAmountLSB[index] = (byte)bAmount[i];
		   index--;
		  }
		  
//		  System.out.println("LSB Hexa : " + byteArrayToHexString(bAmountLSB));
//		  System.out.println("hex2decimal : " + hex2decimal(byteArrayToHexString(bAmountLSB)));
		  x = hex2decimal(byteArrayToHexString(bAmountLSB));
		 return x;
	 }
	 
	 
	 public static byte[] hexToByte(String s) {
	     int len = s.length();
	     byte[] data = new byte[len / 2];
	     for (int i = 0; i < len; i += 2) {
	         data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                              + Character.digit(s.charAt(i+1), 16));
	     }
	     return data;
	 }
	    
    public static String byteArrayToHexString(byte[] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
          int v = b[i] & 0xff;
          if (v < 16) {
            sb.append('0');
          }
          sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
	}
    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }	 
    
    public static String decToHex(int dec) {
        StringBuilder hexBuilder = new StringBuilder(sizeOfIntInHalfBytes);
        hexBuilder.setLength(sizeOfIntInHalfBytes);
        for (int i = sizeOfIntInHalfBytes - 1; i >= 0; --i)
        {
          int j = dec & halfByte;
          hexBuilder.setCharAt(i, hexDigits[j]);
          dec >>= numberOfBitsInAHalfByte;
        }
        return hexBuilder.toString(); 
    }
}