package bbw.prepaidhost.settlementservice.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bbw.prepaidhost.settlementservice.main.Main;
import bbw.prepaidhost.settlementservice.model.MSIssuers;
import bbw.prepaidhost.settlementservice.model.MSMerchants;
import bbw.prepaidhost.settlementservice.model.TRUploadSettlementLogs;
import bbw.prepaidhost.settlementservice.util.HibernateUtil;

public class TransactionServiceImpl {
	
	public TransactionServiceImpl(){}
	
	
	public List<TRUploadSettlementLogs> getPendingSettlementhHeader(){
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();
		
		Transaction tx 					= session.beginTransaction();
//		Query q 						= session.createQuery("FROM TRUploadSettlementLogs WHERE status = 0 ) ");		
		Query q 						= session.createQuery("FROM TRUploadSettlementLogs WHERE status = 1 OR status = 0 ) ");
		List<TRUploadSettlementLogs> hList 	= null;
		
		try{		
			
			hList						= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
//		        tx.commit();
				session.close();
		    }
		}
		
		return hList;
		
	}
	
	
	// Check Duplikasi Settlement file
	public boolean checkFileExists( String fileName ){
		
		boolean flag			= false;
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();
		
		Transaction tx 					= session.beginTransaction();
//		Query q 						= session.createQuery("FROM TRUploadSettlementLogs WHERE status = 0 ) ");		
		Query q 						= session.createQuery("FROM TRUploadSettlementLogs WHERE fileNameData = :fileNameData ");
		q.setString("fileNameData", fileName);
		
		TRUploadSettlementLogs log 		= null;
		
		try{		
			
			log							= (TRUploadSettlementLogs)q.uniqueResult();
			
			if( log != null ) {
				flag 					= true;
			}else {
				flag 					= false;
			}
			
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
//		        tx.commit();
				session.close();
		    }
		}
		
		return flag;
		
	}
	
	
	
	//Call SP ValidateCard
	public String callSPvalidateCard(String cardID, String UID,	String cardType, String aesKey) {
		
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();

		String sqlQuery 				= " SELECT  sp_validateCard(:vcardnumber,"
		 													     + ":vcarduid,"
																 + ":vcardtype,"
																 + ":vaeskey)";
		String spResult 				= "";
		
		try {
			
			SQLQuery q	 = (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vcardnumber", cardID)
											.setParameter("vcarduid", UID)
											.setParameter("vcardtype", cardType)
											.setParameter("vaeskey", aesKey);
			List lQResult 					= q.list();
			spResult 						= lQResult.get(0).toString();
		
		}catch( Exception ex ){
			Main.writeDebug("[TransactionServiceImpl] Exception<callSPvalidateCard> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
			}
			tx.commit();
		}
		return spResult;
	}
	
	public String callSPsettlementPurchase(String cardID, 
										   String samID,	
										   String terminalCode,
										   String merchantCode,
										   int transCode,
										   int rfu,
										   int amount,
										   int lastBalance,
										   Date dateTime,
										   String counterSam,
										   String counterCard,
										   long indexIssuer,
										   int normalAmount,
										   String traceNumber,
										   String aesKey) {
		
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();

		String sqlQuery 				= " SELECT  sp_settlement_purchase(:vcardnumber,"
		 													     		+ ":vsamcode,"
		 													     		+ ":vterminalcode,"
		 													     		+ ":vmerchantcode,"
		 													     		+ ":vtranscode,"
		 													     		+ ":vrfu,"
		 													     		+ ":vamount,"
		 													     		+ ":vlastbalance,"
		 													     		+ ":vdatetime,"
		 													     		+ ":vcountersam,"
		 													     		+ ":vcountercard,"
		 													     		+ ":vissuerid,"
		 													     		+ ":vnormalamount,"
		 													     		+ ":vtracenumber,"
		 													     		+ ":vaeskey)";
		String spResult 				= "";
		
		try {
			
			SQLQuery q	 = (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vcardnumber", cardID)
											.setParameter("vsamcode", samID)
											.setParameter("vterminalcode", terminalCode)
											.setParameter("vmerchantcode", merchantCode)
											.setParameter("vtranscode", transCode)
											.setParameter("vrfu", rfu)
											.setParameter("vamount", amount)
											.setParameter("vlastbalance", lastBalance)
											.setParameter("vdatetime", dateTime)
											.setParameter("vcountersam", counterSam)
											.setParameter("vcountercard", counterCard)
											.setParameter("vissuerid", indexIssuer)
											.setParameter("vnormalamount", normalAmount)
											.setParameter("vtracenumber", traceNumber)
											.setParameter("vaeskey", aesKey);
			List lQResult 					= q.list();
			spResult 						= lQResult.get(0).toString();
		
		}catch( Exception ex ){
			Main.writeDebug("[TransactionServiceImpl] Exception<callSPsettlementPurchase> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
			}
			tx.commit();
		}
		return spResult;
	}
	
	//Call SP ValidateCard
	public String callSPInsertSettlementDetail(long logId,
											   String headerIndicator,	
											   String countOfRow, 
											   String summaryOfAmount,
											   String cardNumber,
											   String samCode,
											   String terminalCode,
											   String merchantCode,
											   String transCode,
											   String amount,
											   String lastBalance,
											   String dateTime,
											   String counterSam,
											   String counterCard,
											   String indexIssuer,
											   String cardType,
											   String rfu,
											   String macIssuer,
											   String macMezzo,
											   String smartCardStatus,
											   String sequence,
											   String normalAmount,
											   String uid,
											   String transactionid,
											   String trailerIndicator,
											   String tcountOfRow,
											   String crc32) {
			
			
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();

		String sqlQuery 				= " SELECT  sp_insert_settlement_detail(:vlogid,"
		 													     			 + ":vheaderindicator,"
		 													     			 + ":vcountofrow,"
		 													     			 + ":vsummaryofamount,"
		 													     			 + ":vcardnumber,"
		 													     			 + ":vsamcode,"
		 													     			 + ":vterminalcode,"
		 													     			 + ":vmerchantcode,"
		 													     			 + ":vtranscode,"
		 													     			 + ":vamount,"
		 													     		 	 + ":vlastbalance,"
		 													     			 + ":vdatetime,"
		 													     			 + ":vcountersam,"
		 													     			 + ":vcountercard,"
		 													     			 + ":vindexissuer,"
		 													     			 + ":vcardtype,"
		 													     			 + ":vrfu,"
		 													     			 + ":vmacissuer,"
		 													     			 + ":vmacmezzo,"
		 													     			 + ":vsmartcardstatus,"
		 													     			 + ":vsequence,"
		 													     			 + ":vnormalamount,"
		 													     			 + ":vuid,"
		 													     			 + ":vtransactionid,"
		 													     			 + ":vtrailerindicator,"
		 													     			 + ":vtcountofrow,"
		 													     			 + ":vcrc32)";
		String spResult 				= "";
		
		try {
			
			SQLQuery q	 = (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vlogid", logId)
											.setParameter("vheaderindicator", headerIndicator)
											.setParameter("vcountofrow", countOfRow)
											.setParameter("vsummaryofamount", summaryOfAmount)
											.setParameter("vcardnumber", cardNumber)
											.setParameter("vsamcode", samCode)
											.setParameter("vterminalcode", terminalCode)
											.setParameter("vmerchantcode", merchantCode)
											.setParameter("vtranscode", transCode)
											.setParameter("vamount", amount)
											.setParameter("vlastbalance", lastBalance)
											.setParameter("vdatetime", dateTime)
											.setParameter("vcountersam", counterSam)
											.setParameter("vcountercard", counterCard)
											.setParameter("vindexissuer", indexIssuer)
											.setParameter("vcardtype", cardType)
											.setParameter("vrfu", rfu)
											.setParameter("vmacissuer", macIssuer)
											.setParameter("vmacmezzo", macMezzo)
											.setParameter("vsmartcardstatus", smartCardStatus)
											.setParameter("vsequence", sequence)
											.setParameter("vnormalamount", normalAmount)
											.setParameter("vuid", uid)
											.setParameter("vtransactionid", transactionid)
											.setParameter("vtrailerindicator", trailerIndicator)
											.setParameter("vtcountofrow", tcountOfRow)
											.setParameter("vcrc32", crc32);
			List lQResult 					= q.list();
			spResult 						= lQResult.get(0).toString();
		
		}catch( Exception ex ){
			Main.writeDebug("[TransactionServiceImpl] Exception<callSPInsertSettlementDetail> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
			}
			tx.commit();
		}
		return spResult;
	}
	public String callSPInsertSettlement(  int acquirerId,
										   String fileNameData,	
										   String fileNameAck, 
										   String fileNameRecon,
										   Date startProcess,
										   Date endProcess) {


		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		
		String sqlQuery 				= " SELECT  sp_insert_settlement(:vacquirerid,"
														     			 + ":vfilenamedata,"
														     			 + ":vfilenameack,"
														     			 + ":vfilenamerecon,"
														     			 + ":vstartprocess,"
														     			 + ":vendprocess)";
		String spResult 				= "";
		
		try {
		
			SQLQuery q	 = (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vacquirerid", acquirerId)
											.setParameter("vfilenamedata", fileNameData)
											.setParameter("vfilenameack", fileNameAck)
											.setParameter("vfilenamerecon", fileNameRecon)
											.setParameter("vstartprocess", startProcess)
											.setParameter("vendprocess", endProcess);
			List lQResult 					= q.list();
			spResult 						= lQResult.get(0).toString();
		
		}catch( Exception ex ){
			Main.writeDebug("[TransactionServiceImpl] Exception<callSPInsertSettlement> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		}
			tx.commit();
		}
		return spResult;
	}
	
	//Select check fileNameData in Database
	public List<TRUploadSettlementLogs> checkFileName( String fileNameData ){
		
		SessionFactory sessionFactory 				= HibernateUtil.getSessionFactory();
		Session session 							= sessionFactory.getCurrentSession();
		
		Transaction tx 								= session.beginTransaction();
		Query q 									= session.createQuery("FROM TRUploadSettlementLogs WHERE status = 1 OR status = 0 AND fileNameData = :fileNameData ");
		
		List<TRUploadSettlementLogs> accList 		= null;
		try{		
			
			q.setString("fileNameData", fileNameData);
			accList									= q.list();
			tx.commit();	        
			
		}catch( Exception ex ){
			System.out.println("Exception : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return accList;
	}
	
	public List<MSMerchants> checkMerchantId( String MerchantCode ){
		
		SessionFactory sessionFactory 				= HibernateUtil.getSessionFactory();
		Session session 							= sessionFactory.getCurrentSession();
		
		Transaction tx 								= session.beginTransaction();
		Query q 									= session.createQuery("FROM MSMerchants WHERE merchantCode LIKE :merchantCode ");
		
		List<MSMerchants> accList 		= null;
		try{		
			
			q.setString("merchantCode", MerchantCode);
			accList									= q.list();
			tx.commit();	        
			
		}catch( Exception ex ){
			System.out.println("Exception : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return accList;
	}
	
	
	public int updateStatusSettlementLog(long logId, String fileNameAck, String fileNameRecon, short status , Date startProcess, Date endProcess){
		
		int statusUpdate							= 0;
		
		SessionFactory sessionFactory 				= HibernateUtil.getSessionFactory();
		Session session 							= sessionFactory.getCurrentSession();
		
		Transaction tx 								= session.beginTransaction();
		Query q 									= session.createQuery(" UPDATE TRUploadSettlementLogs SET fileNameAck = :fileNameAck,"
																										   + "fileNameRecon = :fileNameRecon,"
																										   + "status = :status,"
																										   + "startProcess =:startProcess,"
																										   + "endProcess =:endProcess"
																									  + " WHERE logId = :logId ");

		
		try{		
			q.setParameter("fileNameAck", fileNameAck);
			q.setParameter("fileNameRecon", fileNameRecon);
			q.setParameter("status", status);
			q.setParameter("startProcess", startProcess);
			q.setParameter("endProcess", endProcess);
			q.setParameter("logId", logId);
			
			statusUpdate 							= q.executeUpdate();		
			tx.commit();
	        
		}catch( Exception ex ){
			System.out.println("Exception : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return statusUpdate;
		
	}
	
	public List<MSIssuers> checkIssuerSettlement(){
		
		SessionFactory sessionFactory 				= HibernateUtil.getSessionFactory();
		Session session 							= sessionFactory.getCurrentSession();
		
		Transaction tx 								= session.beginTransaction();
		Query q 									= session.createQuery("FROM MSIssuers WHERE issuerSettlement = 1 OR issuerSettlement = 0 ) ");		
		
		List<MSIssuers> hList 	= null;
		
		try{		
			
			hList						= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
//		        tx.commit();
				session.close();
		    }
		}
		
		return hList;
	}
	
}
