package bbw.prepaidhost.settlementservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRUploadSettlementLogsDetail\"")
public class TRUploadSettlementLogsDetail  {

	@Column(name="\"logDetailId\"")
	private long logDetailId;
	
	@Column(name="\"logId\"")
	private long logId;
	
	@Column(name="\"headerIndicator\"")
	private String headerIndicator;
	
	@Column(name="\"countOfRow\"")
	private String countOfRow;
	
	@Column(name="\"summaryOfAmount\"")
	private String summaryOfAmount;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"samCode\"")
	private String samCode;
	
	@Column(name="\"terminalCode\"")
	private String terminalCode;
	
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Column(name="\"transCode\"")
	private String transCode;
	
	private String rfu;
	
	private String amount;
	
	@Column(name="\"lastBalance\"")
	private String lastBalance;
	
	@Column(name="\"dateTime\"")
	private Date dateTime;
	
	@Column(name="\"counterSam\"")
	private String counterSam;
	
	@Column(name="\"counterCard\"")
	private String counterCard;
	
	@Column(name="\"indexIssuer\"")
	private String indexIssuer;
	
	@Column(name="\"cardType\"")
	private String cardType;
	
	@Column(name="\"keyVersion\"")
	private String keyVersion;
	
	@Column(name="\"macIssuer\"")
	private String macIssuer;
	
	@Column(name="\"macMezzo\"")
	private String macMezzo;
	
	@Column(name="\"statusWord\"")
	private String statusWord;
	
	private String sequence;
	
	@Column(name="\"normalAmount\"")
	private String normalAmount;
	
	private String uid;
	
	@Column(name="\"trailerIndicator\"")
	private String trailerIndicator;
	
	@Column(name="\"tcountOfRow\"")
	private String tcountOfRow;
	
	private String crc32;
	
	public long getLogDetailId() {
		return logDetailId;
	}
	public void setLogDetailId(long logDetailId) {
		this.logDetailId = logDetailId;
	}
	public long getLogId() {
		return logId;
	}
	public void setLogId(long logId) {
		this.logId = logId;
	}
	public String getHeaderIndicator() {
		return headerIndicator;
	}
	public void setHeaderIndicator(String headerIndicator) {
		this.headerIndicator = headerIndicator;
	}
	public String getCountOfRow() {
		return countOfRow;
	}
	public void setCountOfRow(String countOfRow) {
		this.countOfRow = countOfRow;
	}
	public String getSummaryOfAmount() {
		return summaryOfAmount;
	}
	public void setSummaryOfAmount(String summaryOfAmount) {
		this.summaryOfAmount = summaryOfAmount;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getSamCode() {
		return samCode;
	}
	public void setSamCode(String samCode) {
		this.samCode = samCode;
	}
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getTransCode() {
		return transCode;
	}
	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}
	
	public String getRfu() {
		return rfu;
	}
	public void setRfu(String rfu) {
		this.rfu = rfu;
	}
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getLastBalance() {
		return lastBalance;
	}
	public void setLastBalance(String lastBalance) {
		this.lastBalance = lastBalance;
	}
	
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getCounterSam() {
		return counterSam;
	}
	public void setCounterSam(String counterSam) {
		this.counterSam = counterSam;
	}
	public String getCounterCard() {
		return counterCard;
	}
	public void setCounterCard(String counterCard) {
		this.counterCard = counterCard;
	}
	
	public String getIndexIssuer() {
		return indexIssuer;
	}
	public void setIndexIssuer(String indexIssuer) {
		this.indexIssuer = indexIssuer;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getKeyVersion() {
		return keyVersion;
	}
	public void setKeyVersion(String keyVersion) {
		this.keyVersion = keyVersion;
	}
	public String getMacIssuer() {
		return macIssuer;
	}
	public void setMacIssuer(String macIssuer) {
		this.macIssuer = macIssuer;
	}
	public String getMacMezzo() {
		return macIssuer;
	}
	public void setMacMezzo(String macMezzo) {
		this.macMezzo = macMezzo;
	}
	public String getStatusWord() {
		return statusWord;
	}
	public void setStatusWordr(String statusWord) {
		this.statusWord = statusWord;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getTrailerIndicator() {
		return trailerIndicator;
	}
	public void setTrailerIndicator(String trailerIndicator) {
		this.trailerIndicator = trailerIndicator;
	}
	public String getTcountOfRow() {
		return tcountOfRow;
	}
	public void setTcountOfRow(String tcountOfRow) {
		this.tcountOfRow = tcountOfRow;
	}
	public String getCrc32() {
		return crc32;
	}
	public void setCrc32(String crc32) {
		this.crc32 = crc32;
	}
}
