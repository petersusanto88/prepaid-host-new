package bbw.prepaidhost.campaign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSLoyalties\"")
public class Loyalties {

	@Id
	@Column(name="\"loyaltyId\"")
	private long loyaltyId;
	
	@Column(name="\"statusPush\"")
	private short statusPush;

	public long getLoyaltyId() {
		return loyaltyId;
	}

	public void setLoyaltyId(long loyaltyId) {
		this.loyaltyId = loyaltyId;
	}

	public short getStatusPush() {
		return statusPush;
	}

	public void setStatusPush(short statusPush) {
		this.statusPush = statusPush;
	}
	
	
	
}
