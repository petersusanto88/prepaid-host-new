package bbw.prepaidhost.campaign.main;

import java.util.List;

import bbw.prepaidhost.campaign.model.ViewMerchantTerminal;

public interface MerchantTerminalService {
	
	public List<ViewMerchantTerminal> getMerchantTerminal();
	
}
