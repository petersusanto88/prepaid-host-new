package bbw.prepaidhost.campaign.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="v_expired_card")
public class ExpiredCard {

	@Id
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"expiredDate\"")
	private String expiredDate;

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	
	
	
}
