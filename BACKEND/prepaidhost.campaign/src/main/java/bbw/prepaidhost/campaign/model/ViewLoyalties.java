package bbw.prepaidhost.campaign.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="v_loyalties")
public class ViewLoyalties {

	@Id
	@Column(name="\"loyaltyId\"")
	private long loyaltyId;
	
	@Column(name="\"issuerId\"")
	private long issuerId;
	
	@Column(name="\"cardProgramId\"")
	private int cardProgramId;
	
	@Column(name="\"loyaltyName\"")
	private String loyaltyName;
	
	@Column(name="\"loyaltyType\"")
	private short loyaltyType;
	
	@Column(name="\"startDateTime\"")
	private Date startDateTime;
	
	@Column(name="\"endDateTime\"")
	private Date endDateTime;
	
	@Column(name="\"executionTime\"")
	private String executionTime;
	
	private double amount;
	private short status;
	
	@Column(name="\"statusPush\"")
	private short statusPush;
	
	@Column(name="\"isDelete\"")
	private short isDelete;
	
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Column(name="\"terminalId\"")
	private long terminalId;
	
	@Column(name="\"terminalCode\"")
	private String terminalCode;
	
	@Column(name="\"acquireId\"")
	private int acquireId;

	public long getLoyaltyId() {
		return loyaltyId;
	}

	public void setLoyaltyId(long loyaltyId) {
		this.loyaltyId = loyaltyId;
	}

	public long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(long issuerId) {
		this.issuerId = issuerId;
	}

	public int getCardProgramId() {
		return cardProgramId;
	}

	public void setCardProgramId(int cardProgramId) {
		this.cardProgramId = cardProgramId;
	}

	public String getLoyaltyName() {
		return loyaltyName;
	}

	public void setLoyaltyName(String loyaltyName) {
		this.loyaltyName = loyaltyName;
	}

	public short getLoyaltyType() {
		return loyaltyType;
	}

	public void setLoyaltyType(short loyaltyType) {
		this.loyaltyType = loyaltyType;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(short isDelete) {
		this.isDelete = isDelete;
	}

	public short getStatusPush() {
		return statusPush;
	}

	public void setStatusPush(short statusPush) {
		this.statusPush = statusPush;
	}

	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public int getAcquireId() {
		return acquireId;
	}

	public void setAcquireId(int acquireId) {
		this.acquireId = acquireId;
	}
	
	
	
}
