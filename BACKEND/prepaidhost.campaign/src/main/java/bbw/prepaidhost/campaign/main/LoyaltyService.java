package bbw.prepaidhost.campaign.main;

import java.io.IOException;
import java.text.ParseException;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface LoyaltyService {

	public void doSendCampaign( String sessionId,
							  	String token,
							  	JSONObject data) throws JsonParseException, JsonMappingException, IOException;
	
	public JSONObject doHandshake() throws ParseException;
	
	public JSONObject getLoyaltyList();
	
	public int updateStatusPushToTMS( long loyaltyId );
	
}
