package bbw.prepaidhost.campaign.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.model.ViewLoyalties;
import bbw.prepaidhost.campaign.model.ViewMerchantTerminal;
import bbw.prepaidhost.campaign.util.HibernateUtil;

public class MerchantTerminalRepository {

	public MerchantTerminalRepository(){}
	
	public List<ViewMerchantTerminal> getMerchantTerminal(){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sql 						= " from ViewMerchantTerminal ";
		Query q 						= session.createQuery(sql);
		List<ViewMerchantTerminal> lMT	= null;
		
		try{			
			lMT							= q.list();			
		}catch( Exception ex ){
			Service.writeDebug("[MerchantTerminalRepository] Exception<getMerchantTerminal> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return lMT;
		
	}
		
}
