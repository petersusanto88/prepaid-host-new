package bbw.prepaidhost.campaign.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.model.ViewLoyalties;
import bbw.prepaidhost.campaign.util.HibernateUtil;

public class LoyaltyRepository {

	public LoyaltyRepository(){}
	
	public int updateStatusPushToTMS( long loyaltyId ){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sql 						= " update Loyalties set statusPush = 1 where loyaltyId = :loyaltyId ";
		int result 					 	= 0;
		
		try{	
			Query q 						= session.createQuery(sql);
			q.setLong("loyaltyId", loyaltyId);		
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("[LoyaltyRepository] Exception<getLoyaltyList> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }	
			
		}
		
		return result;
		
	}
	
	public List<ViewLoyalties> getLoyaltyList(){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sql 						= " from ViewLoyalties ";
		Query q 						= session.createQuery(sql);
		List<ViewLoyalties> lLoyalty		= null;
		
		try{			
			lLoyalty					= q.list();			
		}catch( Exception ex ){
			Service.writeDebug("[LoyaltyRepository] Exception<getLoyaltyList> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return lLoyalty;
		
	}
	
}
