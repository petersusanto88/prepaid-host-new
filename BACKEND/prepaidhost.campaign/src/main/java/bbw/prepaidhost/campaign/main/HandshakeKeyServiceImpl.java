package bbw.prepaidhost.campaign.main;

import java.text.ParseException;
import java.util.Date;

import bbw.prepaidhost.campaign.model.TMSHandshakeKey;
import bbw.prepaidhost.campaign.util.MezzoUtil;

public class HandshakeKeyServiceImpl implements HandshakeKeyService {

	private static HandshakeKeyRepository repo		= new HandshakeKeyRepository();
	private static MezzoUtil util 					= new MezzoUtil();
	
	public HandshakeKeyServiceImpl(){}
	
	public long saveHandshakeKey( String sessionId,
							      String token) throws ParseException{
		
		long keyId					= 0;
		
		TMSHandshakeKey tmsKey 		= new TMSHandshakeKey();
		tmsKey.setSessionId(sessionId);
		tmsKey.setToken(token);
		tmsKey.setCreated(util.getDateTime());				
		repo.saveKey(tmsKey);
		
		keyId 						= tmsKey.getKeyId();
		
		return keyId;
		
	}
	
}
