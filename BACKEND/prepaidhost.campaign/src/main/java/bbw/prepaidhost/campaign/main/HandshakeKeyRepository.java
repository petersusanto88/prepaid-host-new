package bbw.prepaidhost.campaign.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.model.TMSHandshakeKey;
import bbw.prepaidhost.campaign.util.HibernateUtil;

public class HandshakeKeyRepository {

	public HandshakeKeyRepository(){}
	
	public long saveKey( TMSHandshakeKey data ){
		
		long keyId 			= 0;
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		
		try{
			session.save(data);
			keyId			= data.getKeyId();
		}catch( Exception ex ){
			Service.writeDebug("[LoyaltyRepository] Exception<saveKey> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return keyId;
		
	}
	
}
