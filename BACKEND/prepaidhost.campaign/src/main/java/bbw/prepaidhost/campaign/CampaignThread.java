package bbw.prepaidhost.campaign;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.main.LoyaltyServiceImpl;
import bbw.prepaidhost.campaign.util.JSON;

/**
 * 
 * @author PETER SUSANTO
 * @version 1.0
 * Class ini merupakan class thread yang berfungsi untuk proses yang berhubungan dengan campaign system (loyalty management).
 * Thread ini pada saat pertama kali dijalankan akan melakukan handshake dengan TMS untuk mendapatkan TOKEN yang dipakai untuk 
 * mengirim data campaign setiap kali ada antrian.
 * Thread ini juga mengambil data record dari table antrian campaign, dan kemudian akan meneruskan ke TMS disertai dengan token
 * yang didapat dari handshake.
 */
public class CampaignThread extends Thread {
	
	private static LoyaltyServiceImpl loyaltyService		= new LoyaltyServiceImpl();
	private static ObjectMapper objMap						= new ObjectMapper();
	
	public void run(){
			
		try{		
			
			if( !this.stop ){		
				
				Service.writeDebug("[CampaignThread] Starting Campaign Thread...");
				
				while( true ){
					
					Thread.sleep(Service.interval * 1000);
					
					if( Service.token.compareTo("") == 0 ){
						
						/* Apabila aplikasi :
						 * 		- Pertama kali dijalankan
						 * 		- Habis direstart
						 *   Maka aplikasi perlu melakukan handshake ke TMS dan simpan token*/
						
						JSONObject joTMSResult			= loyaltyService.doHandshake();
						Service.writeDebug("===================================================");
						Service.writeDebug("Start Get Handshake");
						Service.writeDebug("===================================================");
						if( JSON.get(joTMSResult, "errCode").compareTo("00") == 0 ){
							
							Service.writeDebug("-----------------------------------------------");
							Service.writeDebug("[CampaignThread] SessionId : " + Service.sessionId);
							Service.writeDebug("[CampaignThread] Token : " + Service.token);
							Service.writeDebug("-----------------------------------------------");
							
							Service.token 				= JSON.get(joTMSResult, "token");
							Service.sessionId			= JSON.get(joTMSResult, "sessionId");						
							
							
						}else{							
							Service.writeDebug("[CampaignThread] Handshake Failed " + joTMSResult.toString());							
						}
						
					}
					
					if( Service.token.compareTo("") != 0 ){
						
						/* Get Loyalty data*/
						JSONObject joLoyalty		= loyaltyService.getLoyaltyList();
						
						if( JSON.get(joLoyalty, "errCode").compareTo("00") == 0 ){			
							
							Object json 			= objMap.readValue(joLoyalty.toString(), Object.class);
													
							Service.writeDebug("---------------------------------------------------");
							Service.writeDebug("Loyalty Data : " + objMap.writerWithDefaultPrettyPrinter().writeValueAsString(json) );
							Service.writeDebug("---------------------------------------------------");
							
							Service.writeDebug("[CampaignThread] ===================================================");
							Service.writeDebug("[CampaignThread] Start Send Campaign");
							Service.writeDebug("[CampaignThread] ===================================================");
							loyaltyService.doSendCampaign(Service.sessionId, Service.token, joLoyalty);			
							
							
							
						}/*else{
							
							Service.writeDebug("[CampaignThread] Data loyalty not found");
							
						}*/
						
					}
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("[Campaign Thread] Exception : " + ex.getMessage());
		}/*catch( ParseException e ){
			Service.writeDebug("[Campaign Thread] ParseException : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			Service.writeDebug("[Campaign Thread] UnsupportedEncodingException : " + e.getMessage());
		}*/
		
	}
	
	volatile boolean stop;
	final CampaignThread this$0;
	private int stat;
	
	public CampaignThread(int status)
	{
		super();
        this$0 = CampaignThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("[Campaign Thread] Exception : " + ex.toString());
        }
	    
	}
	
}
