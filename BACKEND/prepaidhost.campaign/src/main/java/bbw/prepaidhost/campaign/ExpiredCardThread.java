package bbw.prepaidhost.campaign;

import java.util.List;

import bbw.prepaidhost.campaign.main.CardService;
import bbw.prepaidhost.campaign.main.CardServiceImpl;
import bbw.prepaidhost.campaign.model.ExpiredCard;

/**
 * 
 * @author PETER SUSANTO
 * @version 1.0
 * Class thread ini akan mengambil data kartu yang sudah expired dan kemudian melakukan update status kartu menjadi tidak aktif.
 */
public class ExpiredCardThread extends Thread {

	private static CardService cardService 				 = new CardServiceImpl();
	
	public void run(){
		
		try{		
			
			if( !this.stop ){		
				
				Service.writeDebug("[ExpiredCardThread] Starting Expired Card Thread...");
				
				while( true ){
					
					Thread.sleep(Service.interval * 1000);
					
					List<ExpiredCard> lExpiredCard			= cardService.getExpiredCard();
					
					if( lExpiredCard.size() > 0 ){
						
						for( ExpiredCard ec : lExpiredCard ){
							
							/*1. Update card to non active*/
							int updateCardStatus 			= cardService.updateStatusExpiredCard(ec.getCardId());
							if( updateCardStatus > 0 ){					
								Service.writeDebug("[ExpiredCardThread] Update status card successfully.");	
								Service.writeDebug("[ExpiredCardThread] Card Id : " + ec.getCardId() + " Card Number : " + ec.getCardNumber());
							}else{								
								Service.writeDebug("[ExpiredCardThread] Update status card failed.");
								Service.writeDebug("[ExpiredCardThread] Card Id : " + ec.getCardId() + " Card Number : " + ec.getCardNumber());								
							}
							
							/*2. Send data to TMS*/
							
							
							
						}
						
					}
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("[Campaign Thread] Exception : " + ex.getMessage());
		}
		
	}
	
	volatile boolean stop;
	final ExpiredCardThread this$0;
	private int stat;
	
	public ExpiredCardThread(int status)
	{
		super();
        this$0 = ExpiredCardThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("[ExpiredCardThread] Exception : " + ex.toString());
        }
	    
	}
	
}
