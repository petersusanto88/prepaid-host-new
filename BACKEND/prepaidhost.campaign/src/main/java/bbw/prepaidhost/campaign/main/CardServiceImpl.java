package bbw.prepaidhost.campaign.main;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.model.Card;
import bbw.prepaidhost.campaign.model.CardProgram;
import bbw.prepaidhost.campaign.model.ExpiredCard;
import bbw.prepaidhost.campaign.model.ViewMerchantTerminal;
import bbw.prepaidhost.campaign.util.AES;
import bbw.prepaidhost.campaign.util.JSON;

public class CardServiceImpl implements CardService {
	
	private static CardRepository repo 						= new CardRepository();
	private static MerchantTerminalService mtService 		= new MerchantTerminalServiceImpl();
	private static ObjectMapper objMap						= new ObjectMapper();
	
	private static AES aes 									= new AES();

	public CardServiceImpl(){}
	
	public List<ExpiredCard> getExpiredCard(){		
		return repo.getExpiredCard();		
	}
	
	public int updateStatusExpiredCard( long cardId ){
		return repo.updateStatusExpiredCard(cardId);
	}
	
	public int updateStatusPushToTMS( long cardId ){
		return repo.updateStatusPushToTMS( cardId );
	}
	
	public JSONObject getBlacklistCard(){
		
		JSONObject joResult 		= new JSONObject();
		JSONArray jaBlackList 		= new JSONArray();
		JSONObject joBlackList 		= null;
		
		List<Object[]> lCard 		= repo.getBlacklistCard();
		
		if( lCard.size() > 0 ){
			
			joResult.put("errCode", "00");
			joResult.put("errMsg", "OK");
			
			for( Object[] c : lCard ){
				
				joBlackList 				= new JSONObject();
					
				Card card 					= (Card)c[0];
				CardProgram cardProgram		= (CardProgram)c[1];
				
				joBlackList.put("issuerId", String.format("%02X",cardProgram.getIssuerId()));
				joBlackList.put("cardNumber", card.getCardNumber());
				joBlackList.put("cardProgramId", String.format("%02X",cardProgram.getCardProgramId()));
				joBlackList.put("cardId", card.getCardId());
				
				jaBlackList.add(joBlackList);
				
			}
			
			joResult.put("data", jaBlackList);
			
		}
		
		return joResult;
		
	}
	
	public void doSendBlackList( String sessionId,
								 String token,
							     JSONObject data) throws JsonParseException, JsonMappingException, IOException{
		
		JSONObject joResult 		= new JSONObject();
		
		JSONObject joBody 			= null;
		JSONObject joData 			= new JSONObject();
		JSONArray jaData 			= new JSONArray();
		
		jaData 						= JSON.newJSONArray( JSON.get(data, "data").toString() );
		
		if( jaData.size() > 0 ){
			
			/*Loop data blacklist*/
			for( int i = 0; i < jaData.size(); i++ ){
				
				joData				= JSON.newJSONObject( jaData.get(i).toString() );
				joBody 				= new JSONObject();
				
				/*Loop data merchant_id and terminal_id*/
				List<ViewMerchantTerminal> lMt			= mtService.getMerchantTerminal();
				
				for( ViewMerchantTerminal vmt : lMt ){
					
					joBody.put("session_id", sessionId);
					joBody.put("token", token);
					joBody.put("issuer_id", JSON.get(joData, "issuerId"));
					joBody.put("card_id", JSON.get(joData, "cardNumber"));
					joBody.put("card_type", JSON.get(joData, "cardProgramId"));
					joBody.put("merchant_id", vmt.getMerchantCode());
					joBody.put("terminal_id", vmt.getTerminalCode());
					joBody.put("sam_status", "00");
					joBody.put("timestamp", Service.util.getCurrentTimestamp());
					
					Service.writeDebug("[BlackListCardThread] ---------------------------------------------------");
					Service.writeDebug("[BlackListCardThread] Start Push To TMS");
					Service.writeDebug("[BlackListCardThread] ---------------------------------------------------");
					Object json 						= objMap.readValue(joBody.toString(), Object.class);
					Service.writeDebug("[BlackListCardThread] Body : " + objMap.writerWithDefaultPrettyPrinter().writeValueAsString(json));
					
					String result 				= Service.util.sendRequest(Service.tmsHost + "/blacklist", 
							   									   		   joBody.toString(), 
							   									   		   "application/json");
					
					json 						= objMap.readValue(result, Object.class);
					
					Service.writeDebug("[BlackListCardThread] Result : " + json);
					Service.writeDebug("[BlackListCardThread] ---------------------------------------------------");
					
					updateStatusPushToTMS( Long.parseLong( JSON.get(joData, "cardId") ) );
					
				}				
				
			}
			
		}
		
	}
	
}
