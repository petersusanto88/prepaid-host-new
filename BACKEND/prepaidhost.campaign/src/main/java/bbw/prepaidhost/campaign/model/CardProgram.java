package bbw.prepaidhost.campaign.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="\"MSIssuerCardPrograms\"")
public class CardProgram {
	
	public CardProgram(){}

	@Id
	@Column(name="\"cardProgramId\"")
	private long cardProgramId;
	
	@Column(name="\"cardProgramCode\"")
	private String cardProgramCode;
	
	@Column(name="\"cardProgramName\"")
	private String cardProgramName;
	
	@Column(name="\"issuerId\"")
	private int issuerId;
	
	@OneToMany(mappedBy = "cardProgram")
	private Set<Card> cards;
	
	public long getCardProgramId() {
		return cardProgramId;
	}

	public void setCardProgramId(long cardProgramId) {
		this.cardProgramId = cardProgramId;
	}

	public String getCardProgramCode() {
		return cardProgramCode;
	}

	public void setCardProgramCode(String cardProgramCode) {
		this.cardProgramCode = cardProgramCode;
	}

	public String getCardProgramName() {
		return cardProgramName;
	}

	public void setCardProgramName(String cardProgramName) {
		this.cardProgramName = cardProgramName;
	}

	public int getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(int issuerId) {
		this.issuerId = issuerId;
	}

	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}

	public Set<Card> getCards() {
		return cards;
	}
	
	
	
}
