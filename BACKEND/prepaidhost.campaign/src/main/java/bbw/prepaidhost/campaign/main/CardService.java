package bbw.prepaidhost.campaign.main;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import bbw.prepaidhost.campaign.model.ExpiredCard;

public interface CardService {

	public List<ExpiredCard> getExpiredCard();
	public int updateStatusExpiredCard( long cardId );
	public int updateStatusPushToTMS( long cardId );
	public JSONObject getBlacklistCard();
	public void doSendBlackList( String sessionId,
								 String token,
							     JSONObject data) throws JsonParseException, JsonMappingException, IOException;
	
	
}
