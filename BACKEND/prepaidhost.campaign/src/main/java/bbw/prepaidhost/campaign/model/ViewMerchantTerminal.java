package bbw.prepaidhost.campaign.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"v_merchant_terminal\"")
public class ViewMerchantTerminal implements Serializable {

	@Id
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Id
	@Column(name="\"terminalCode\"")
	private String terminalCode;

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	
	
	
	
}
