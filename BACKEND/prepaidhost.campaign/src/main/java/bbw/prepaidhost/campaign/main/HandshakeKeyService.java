package bbw.prepaidhost.campaign.main;

import java.text.ParseException;

public interface HandshakeKeyService {

	public long saveHandshakeKey( String sessionId,
		     					  String token) throws ParseException;
	
}
