package bbw.prepaidhost.campaign.main;

import java.util.List;

import bbw.prepaidhost.campaign.model.ViewMerchantTerminal;

public class MerchantTerminalServiceImpl implements MerchantTerminalService {

	private static MerchantTerminalRepository mtRepo = new MerchantTerminalRepository();
	
	public MerchantTerminalServiceImpl(){}
	
	public List<ViewMerchantTerminal> getMerchantTerminal(){		
		return mtRepo.getMerchantTerminal();		
	}
	
}
