package bbw.prepaidhost.campaign.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.campaign.Service;
import bbw.prepaidhost.campaign.model.Card;
import bbw.prepaidhost.campaign.model.ExpiredCard;
import bbw.prepaidhost.campaign.model.ViewLoyalties;
import bbw.prepaidhost.campaign.util.AES;
import bbw.prepaidhost.campaign.util.HibernateUtil;

public class CardRepository {

	public CardRepository(){}
	
	public int updateStatusExpiredCard( long cardId ){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		int result 						= 0;
		
		String sql 						= " update Card set status = -2 where cardId = :cardId ";
		Query q 						= session.createQuery(sql);
		q.setLong("cardId", cardId);
		
		try{
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("[CardRepository] Exception<updateStatusExpiredCard> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			
			
		}
		
		return result;
		
	}
	
	public int updateStatusPushToTMS( long cardId ){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		int result 						= 0;
		
		String sql 						= " update Card set statusPushToTMS = 1 where cardId = :cardId ";
		Query q 						= session.createQuery(sql);
		q.setLong("cardId", cardId);
		
		try{
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("[BlackListCardThread] Exception<updateStatusPushToTMS> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			
			
		}
		
		return result;
		
	}
	
	public List<ExpiredCard> getExpiredCard(){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sql 						= " from ExpiredCard ";
		Query q 						= session.createQuery(sql);
		List<ExpiredCard> expCard		= null;
		
		try{			
			expCard						= q.list();			
		}catch( Exception ex ){
			Service.writeDebug("[CardRepository] Exception<getExpiredCards> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return expCard;
		
	}
	
	public List<Object[]> getBlacklistCard(){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sql 						= " from Card c inner join c.cardProgram where ( c.status = -1 OR c.status = -2 ) and c.statusPushToTMS = 0 ";
		Query q 						= session.createQuery(sql);
		List<Object[]> lCard			= null;
		
		try{			
			lCard						= q.list();			
		}catch( Exception ex ){
			Service.writeDebug("[CardRepository] Exception<getBlacklistCard> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return lCard;
		
	}
	
	public int updateStatusPushBlackListCard( long cardId ){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		int result 						= 0;
		
		String sql 						= " update Card set statusPushToTMS = 1 where cardId = :cardId ";
		Query q 						= session.createQuery(sql);
		q.setLong("cardId", cardId);
		
		try{
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("[CardRepository] Exception<updateStatusPushBlackListCard> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			
			
		}
		
		return result;
		
	}
}
