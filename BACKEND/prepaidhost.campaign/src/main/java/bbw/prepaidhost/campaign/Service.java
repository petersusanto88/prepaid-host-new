package bbw.prepaidhost.campaign;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import bbw.prepaidhost.campaign.util.MezzoUtil;

/**
 * 
 * @author PETER SUSANTO
 * @version 1.0
 * Class ini merupakan main class. Saat aplikasi dijalankan, class ini yang akan dipanggil pertama kali.
 */
public class Service {

	private static Properties props 					= null;
	public static int timeout 							= 0;
    public static int interval 							= 1;
    private static boolean debug 						= false;
    private static FileOutputStream fos 				= null;
    private static String fileDate 						= "";
    private static String debugFilename 				= "";
    public static OutputStream out;
    
    public static MezzoUtil util 						= new MezzoUtil();
    
    public static String hostDBMaster 					= "";
    public static int portDBMaster						= 0;
    public static String nameDBMaster 					= "";
    public static String userDBMaster 					= "";
    public static String passwordDBMaster 				= "";
    
    public static String sessionId 						= "";
	public static String token 							= "";
	
	public static String tmsHost 						= "";
    
    String config 										= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_campaign.ini";
	
    public static void main( String[] args ) throws IOException, InterruptedException
    {
		Service service = new Service(args);
    }
    
    /**
     * 
     * @param as
     * <u><b>Description</b></u>:<br>
     * Method ini adalah constructor yang didalamnya terdapat konfigurasi variable public dan database.
     * Di dalam method ini akan dilakukan start terhadapa 3 thread yaitu :<br>
     * <ul>
     * 	<li>Campaign Thread</li>
     *  <li>Expired Thread</li>
     *  <li>Blacklist Thread</li>
     * </ul>
     */
    public Service(String as[]){
    	
    	try{
    	
	    	getConfiguration(config);	
	    	
	    	fileDate				= util.currentDate();
			debug 					= true;			
			debugFilename			= props.getProperty("debugFilename");
			interval				= Integer.parseInt( props.getProperty("interval") );
			
			hostDBMaster 			= props.getProperty("hostDBMaster");
			portDBMaster 			= Integer.parseInt( props.getProperty("portDBMaster") );
			nameDBMaster 			= props.getProperty("nameDBMaster");
			userDBMaster 			= props.getProperty("userDBMaster");
			passwordDBMaster 		= props.getProperty("passwordDBMaster");
			
			tmsHost					= props.getProperty("tmsHost");
			
			openFileDebug();
			writeDebug("----------------------------------------------------------------");
	        writeDebug((new StringBuilder()).append("MEZZO CAMPAIGN SERVICE, started at : ").append(util.currentDateTime()).toString());
	        writeDebug("----------------------------------------------------------------");	
	        
	        CampaignThread ct 			= new CampaignThread(1);
	        ct.start();
	        
	        Thread.sleep(5000);
	        
	        ExpiredCardThread ect 		= new ExpiredCardThread(1);
	        ect.start();
	        
	        Thread.sleep(5000);
	        
	        BlackListCardThread bct 	= new BlackListCardThread(1);
	        bct.start();
	        
    	}catch(Exception exception)
        {
        	Service.writeDebug("[Mezzo Campaign Thread] Exception :  " + exception.toString());   	
        }
    	
    }
    
    public static void writeDebug(String s)
    {
        try
        {
        	System.out.println(s);	
            if(out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
            if(debug)
            {
            	if(fileDate.compareTo(util.currentDate()) != 0)
                {
                    try
                    {
                    	
                        if(fos != null)
                            fos.close();
                    }
                    catch(Exception exception)
                    {
                        System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
                    }
                    fileDate = util.currentDate();
                    openFileDebug();
                }
                s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
                try
                {
                    fos.write(s.getBytes());
                }
                catch(IOException ioexception)
                {
                    System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
                }
            }
        }
        catch(Exception exception1) { }
    }
	public static void writeDebug(String s, int i)
    {
        try
        {
        	
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
	
	private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            System.out.println("debugfilename  " + s);
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
	
	private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                Service.writeDebug("Exception getConfiguration :  " + filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                Service.writeDebug("Exception getConfiguration :  " + ioexception);
                System.exit(1);
            }
        }
    }
    
}
