package bbw.prepaidhost.campaign;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import bbw.prepaidhost.campaign.main.CardService;
import bbw.prepaidhost.campaign.main.CardServiceImpl;
import bbw.prepaidhost.campaign.main.LoyaltyService;
import bbw.prepaidhost.campaign.main.LoyaltyServiceImpl;
import bbw.prepaidhost.campaign.util.JSON;

/**
 * 
 * @author PETER SUSANTO
 * @version 1.0
 * Thread ini bertugas untuk mengambil data kartu yang expired dan terblokir dan
 * mengirimkannya ke TMS.
 */
public class BlackListCardThread extends Thread {

	private static CardService cardService 					= new CardServiceImpl();	 
	private static LoyaltyService loyaltyService			= new LoyaltyServiceImpl();
	private static ObjectMapper objMap						= new ObjectMapper();
	
	public void run(){
		
		try{		
			
			if( !this.stop ){		
				
				Service.writeDebug("[CampaignThread] Starting Blacklist Thread...");
				
				while( true ){
					
					Thread.sleep(Service.interval * 1000);
					
					if( Service.token.compareTo("") == 0 ){
						
						/* Apabila aplikasi :
						 * 		- Pertama kali dijalankan
						 * 		- Habis direstart
						 *   Maka aplikasi perlu melakukan handshake ke TMS dan simpan token*/
						
						JSONObject joTMSResult			= loyaltyService.doHandshake();
						Service.writeDebug("===================================================");
						Service.writeDebug("Start Get Handshake");
						Service.writeDebug("===================================================");
						if( JSON.get(joTMSResult, "errCode").compareTo("00") == 0 ){
							
							Service.writeDebug("-----------------------------------------------");
							Service.writeDebug("[Blacklist Card Thread] SessionId : " + Service.sessionId);
							Service.writeDebug("[Blacklist Card Thread] Token : " + Service.token);
							Service.writeDebug("-----------------------------------------------");
							
							Service.token 				= JSON.get(joTMSResult, "token");
							Service.sessionId			= JSON.get(joTMSResult, "sessionId");						
							
							
						}else{							
							Service.writeDebug("[Blacklist Card Thread] Handshake Failed " + joTMSResult.toString());							
						}
						
					}
					
					if( Service.token.compareTo("") != 0 ){
						
						/* Get Blacklist data*/
						JSONObject joBlacklist		= cardService.getBlacklistCard();
						
						if( JSON.get(joBlacklist, "errCode").compareTo("00") == 0 ){			
							
							Object json 			= objMap.readValue(joBlacklist.toString(), Object.class);
													
							/*Service.writeDebug("---------------------------------------------------");
							Service.writeDebug("Blacklist Data : " + objMap.writerWithDefaultPrettyPrinter().writeValueAsString(json) );
							Service.writeDebug("---------------------------------------------------");*/
							
							cardService.doSendBlackList(Service.sessionId, Service.token, joBlacklist);								
							
						}/*else{
							
							Service.writeDebug("[CampaignThread] Data loyalty not found");
							
						}*/
						
					}
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("[Blacklist Card Thread] Exception : " + ex.getMessage());
		}/*catch( ParseException e ){
			Service.writeDebug("[Campaign Thread] ParseException : " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			Service.writeDebug("[Campaign Thread] UnsupportedEncodingException : " + e.getMessage());
		}*/
		
	}
	
	volatile boolean stop;
	final BlackListCardThread this$0;
	private int stat;
	
	public BlackListCardThread(int status)
	{
		super();
        this$0 = BlackListCardThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("[Blacklist Card Thread] Exception : " + ex.toString());
        }
	    
	}
	
}
