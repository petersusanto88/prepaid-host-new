package bbw.prepaidhost.altoservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class entity dari "TRCardTransactions"
 *
 */
@Entity
@Table(name="\"TRCardTransactions\"")
public class CardTransaction implements Serializable{

	@Id
	@Column(name="\"transactionId\"")
	private long transactionId;
	
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"issuerId\"")
	private long issuerId;
	
	@Column(name="\"acquirerId\"")
	private long acquirerId;
	
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"terminalId\"")
	private long terminalId;
	
	@Column(name="\"samId\"")
	private Long samId;
	
	@Column(name="\"transactionDate\"")
	private Date transactionDate;
	
	@Column(name="\"transactionCode\"")
	private String transactionCode;
	
	private Double amount;
	private short status;
	
	@Column(name="\"cardBalance\"")
	private Double cardBalance;
	
	@Column(name="\"lastBalance\"")
	private Double lastBalance;
	
	@Column(name="\"isoTraceNumber\"")
	private String isoTraceNumber;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(long issuerId) {
		this.issuerId = issuerId;
	}

	public long getAcquirerId() {
		return acquirerId;
	}

	public void setAcquirerId(long acquirerId) {
		this.acquirerId = acquirerId;
	}

	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}

	public long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	public Long getSamId() {
		return samId;
	}

	public void setSamId(Long samId) {
		this.samId = samId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public Double getCardBalance() {
		return cardBalance;
	}

	public void setCardBalance(Double cardBalance) {
		this.cardBalance = cardBalance;
	}

	public Double getLastBalance() {
		return lastBalance;
	}

	public void setLastBalance(Double lastBalance) {
		this.lastBalance = lastBalance;
	}

	public String getIsoTraceNumber() {
		return isoTraceNumber;
	}

	public void setIsoTraceNumber(String isoTraceNumber) {
		this.isoTraceNumber = isoTraceNumber;
	}
	
	
	
}
