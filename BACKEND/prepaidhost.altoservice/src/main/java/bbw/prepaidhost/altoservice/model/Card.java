package bbw.prepaidhost.altoservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class entity dari "MSCards"
 */
@Entity
@Table(name="\"MSCards\"")

@NamedNativeQueries({
	
	@NamedNativeQuery(
			name	= "spValidateCard",
			query	= "select public.sp_validateCard( ?1, ?2, ?3, ?4 )"
	)
	
})

public class Card implements Serializable {

	@Id
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardProgramId\"")
	private int cardProgramId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"cardUID\"")
	private String cardUID;
	
	@Column(name="\"isRegistered\"")
	private short isRegistered;
	
	@Column(name="\"expiredDate\"")
	private String expiredDate;
	
	@Column(name="\"startBalance\"")
	private Double startBalance;
	
	@Column(name="\"minBalance\"")
	private Double minBalance;
	
	@Column(name="\"maxBalance\"")
	private Double maxBalance;
	
	private short status;

	public int getCardProgramId() {
		return cardProgramId;
	}

	public void setCardProgramId(int cardProgramId) {
		this.cardProgramId = cardProgramId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardUID() {
		return cardUID;
	}

	public void setCardUID(String cardUID) {
		this.cardUID = cardUID;
	}

	public short getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(short isRegistered) {
		this.isRegistered = isRegistered;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Double getStartBalance() {
		return startBalance;
	}

	public void setStartBalance(Double startBalance) {
		this.startBalance = startBalance;
	}

	public Double getMinBalance() {
		return minBalance;
	}

	public void setMinBalance(Double minBalance) {
		this.minBalance = minBalance;
	}

	public Double getMaxBalance() {
		return maxBalance;
	}

	public void setMaxBalance(Double maxBalance) {
		this.maxBalance = maxBalance;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	} 
	
	
	
	
}
