package bbw.prepaidhost.altoservice.main;

import java.text.ParseException;
import java.util.Date;

import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.KwpALTO;
import bbw.prepaidhost.altoservice.util.JSON;
import bbw.prepaidhost.altoservice.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait log KWP lagi ALTO.
 *
 */
public class KwpALTOServiceImpl implements KwpALTOService{

	public KwpALTOServiceImpl(){}
	
	private KwpALTORepository kwpAltoRepo		= new KwpALTORepository();
	private MezzoUtil util 						= new MezzoUtil();
	
	public JSONObject addKwpALTO( String kwp, 
								  String zpkUnderZMK, 
								  String kcv) throws ParseException{
		
		JSONObject joResult 		= new JSONObject();
		
		JSONObject joResultImport	= Service.hsm.importKey(Service.zmkUnderLMK, "01", "00", zpkUnderZMK);
		
		if( JSON.get(joResultImport, "errCode").compareTo("00") == 0 ) {
			
			KwpALTO data 			= new KwpALTO();
			data.setLogDate( util.getDateTime() );
			data.setKwp( kwp );
			data.setZpkUnderLMK( JSON.get(joResultImport, "ZPKLMK") );
			data.setZpkUnderZMK(zpkUnderZMK);
			data.setKcv(kcv);
			
			Service.writeDebug("[AFEP RESPONSE THREAD] Start Add KWP ALTO");
			joResult 				= kwpAltoRepo.addKwpALTO(data);
			
			Service.writeDebug("[AFEP RESPONSE THREAD] Result Add KWP ALTO: " + joResult.toString());
			
		}else {
			
			joResult.put("errCode", "96");
			joResult.put("errMsg", JSON.get(joResultImport, "errMsg"));
			
		}
		
		return joResult;
		
	}
	
}
