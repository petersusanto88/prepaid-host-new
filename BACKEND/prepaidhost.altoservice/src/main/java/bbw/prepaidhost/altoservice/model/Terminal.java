package bbw.prepaidhost.altoservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class table dari "MSTerminals"
 *
 */
@Entity
@Table(name="\"MSTerminals\"")
public class Terminal {

	@Id
	@Column(name="\"terminalId\"")
	private long terminalId;
	
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"terminalCode\"")
	private String terminalCode;
	
	private short status;
	
	@Column(name="\"isDelete\"")
	private short isDelete;

	public long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(short isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
}
