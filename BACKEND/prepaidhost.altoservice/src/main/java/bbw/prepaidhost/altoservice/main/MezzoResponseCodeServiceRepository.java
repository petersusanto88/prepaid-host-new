package bbw.prepaidhost.altoservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.MezzoResponseCode;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan untuk operasi database yang berhubungan dengan konversi response code dari PrepaidHost ke response code ALTO.
 *
 */
public class MezzoResponseCodeServiceRepository {

	public MezzoResponseCodeServiceRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method untuk mengambil ALTO Response Code.
	 * @param mezzoResponseCode
	 * @return
	 */
	public String getALTOResponseCode( String mezzoResponseCode ){
		
		String altoRC 						= "";				
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			tx								= session.beginTransaction();
			
			Query q							= session.createQuery( "from MezzoResponseCode where responseCode = :rc " )
												.setString("rc", mezzoResponseCode);
			
			List<MezzoResponseCode> lRC		= q.list();
			for( MezzoResponseCode mrc : lRC ){
				altoRC						= mrc.getAltoResponseCode();
			}
			
		}catch( Exception ex ){
			
			Service.writeDebug("[MezzoResponseCodeServiceRepository] Exception<getALTOResponseCode> : " + ex.getMessage());
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return altoRC;
		
	}
	
}
