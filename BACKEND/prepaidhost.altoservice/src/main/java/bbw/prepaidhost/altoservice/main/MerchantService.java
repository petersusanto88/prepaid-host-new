package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan data merchant.
 *
 */
public interface MerchantService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengambil detail merchant info dari database berdasarkan kode merchant.
	 * @param merchantCode
	 * @return
	 */
	public JSONObject getMerchantInfoDetail( String merchantCode );
	
	
}
