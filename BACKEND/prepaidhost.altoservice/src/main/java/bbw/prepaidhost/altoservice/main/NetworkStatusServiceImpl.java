package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait data network status.
 *
 */
public class NetworkStatusServiceImpl implements NetworkStatusService{
	
	private static NetworkStatusRepository networkRepo		= new NetworkStatusRepository();

	public JSONObject updateAFEPNetworkStatus( short type, short status, short statusSignOn ) {
		
		return networkRepo.updateAFEPNetworkStatus(type, status, statusSignOn);
		
	}
	
}
