package bbw.prepaidhost.altoservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.text.ParseException;

import org.hsm.safenet.HSM;
import org.json.simple.JSONObject;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;

import com.solab.iso8583.MessageFactory;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.main.CardService;
import bbw.prepaidhost.altoservice.main.CardServiceImpl;
import bbw.prepaidhost.altoservice.main.ISOAFEPLogService;
import bbw.prepaidhost.altoservice.main.ISOAFEPLogServiceImpl;
import bbw.prepaidhost.altoservice.main.KwpALTOService;
import bbw.prepaidhost.altoservice.main.KwpALTOServiceImpl;
import bbw.prepaidhost.altoservice.main.MerchantServiceImpl;
import bbw.prepaidhost.altoservice.main.MezzoResponseCodeService;
import bbw.prepaidhost.altoservice.main.MezzoResponseCodeServiceImpl;
import bbw.prepaidhost.altoservice.main.NetworkStatusService;
import bbw.prepaidhost.altoservice.main.NetworkStatusServiceImpl;
import bbw.prepaidhost.altoservice.main.TransactionService;
import bbw.prepaidhost.altoservice.main.TransactionServiceImpl;
import bbw.prepaidhost.altoservice.util.JSON;
import bbw.prepaidhost.altoservice.util.KMSUtil;



/** 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ServiceAFEPResponseThread digunakan sebagai thread yang menghandle message yang masuk dari ALTO.
 */
public class ServiceAFEPResponseThread extends Thread {
	
	private static CardService cardService 					= new CardServiceImpl();
	private static TransactionService 	transService 		= new TransactionServiceImpl();
	private static MezzoResponseCodeService mezzoRC			= new MezzoResponseCodeServiceImpl();
	private static ISOAFEPLogService isoLog					= new ISOAFEPLogServiceImpl();
	private static NetworkStatusService networkStatusService= new NetworkStatusServiceImpl();
	private static KwpALTOService kwpALTOService 			= new KwpALTOServiceImpl();

	private class Processor implements Runnable{
		
		public void run(){
			
			try{
				
				Service.writeDebug("=================================================");
				Service.writeDebug("[AFEP Response Thread] Parsing incoming from AFEP");
				Service.writeDebug("=================================================");	
				
				IsoMessage isoMessage		= mfact.parseMessage(msg, 0);
				String mti 					= Integer.toHexString( isoMessage.getType() ).toUpperCase();	
				Service.writeDebug("[AFEP Response Thread] MTI:" + mti );
				Service.writeDebug("[AFEP Response Thread] DATA:" +  new String( isoMessage.writeData() ));
				
				
				if( mti.compareTo("800") == 0 ){
					
					try{
						
						Service.parsingField(isoMessage, "AFEP Response Thread");
						_800( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[AFEP Response Thread] Exception Request 0800 : " + ex.getMessage() );
					}
					
				}else if( mti.compareTo("810") == 0 ){
					
					try{
						
						Service.parsingField(isoMessage, "AFEP Response Thread");	
						_810( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[AFEP Response Thread] Exception Response 0810 : " + ex.getMessage() );
					}
					
				}else if( mti.compareTo("200") ==  0 ){
					
					try{
						
						Service.parsingField(isoMessage, "AFEP Response Thread");
						_200( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[AFEP Response Thread] Exception 0200 : " + ex.getMessage());
					}
					
				}else if( mti.compareTo("420") ==  0 || mti.compareTo("421") ==  0 ){
					
					try{
						
						Service.parsingField(isoMessage, "AFEP Response Thread");
						_420( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[AFEP Response Thread] Exception 0200 : " + ex.getMessage());
					}
					
				}else if( mti.compareTo("9200") ==  0 ){
					
					try{
						
						Service.parsingField(isoMessage, "AFEP Response Thread");
						_9200( isoMessage );
						
					}catch( Exception ex ){
						Service.writeDebug("[AFEP Response Thread] Exception 09200 : " + ex.getMessage());
					}
					
				}
				
			}catch( ParseException e ){
				Service.writeDebug("[AFEP Response Thread] ParseException : " + e.getMessage());
			} catch (UnsupportedEncodingException e) {
				Service.writeDebug("[AFEP Response Thread] UnsupportedEncodingException : " + e.getMessage());
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk menghandle message 0800 dari ALTO. 
		 * 0800 dari ALTO meliputi : <br>
		 * <ul>
		 * 	<li>Key Exchange</li>
		 *  <li>Sign On</li>
		 *  <li>Sign Off</li>
		 * </ul> 
		 * @param isoMsg
		 * @throws Exception
		 
		 */
		private void _800( IsoMessage isoMsg ) throws Exception{
			
			String f2						= ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
			String f7						= ( isoMsg.hasField(7) ? isoMsg.getField(7).toString() : "" );
			String f11						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f33						= ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" );
			String f48						= ( isoMsg.hasField(48) ? isoMsg.getField(48).toString() : "" );
			String f70						= ( isoMsg.hasField(70) ? isoMsg.getField(70).toString() : "" );
			
			String keyExchange 				= "";
			String zpkUnderZMK 				= "";
			String kcvZPK 					= "";
			
			String f39 						= "";
			
			/*1. Save AFEP Iso Log*/
			isoLog.saveMessageAFEP(isoMsg, (short)1);
			
			/*2. Process KWP*/
			if( f70.compareTo("101") == 0 ){ // Key Exchange
				
				keyExchange 				= f48.substring(10,f48.length());
				zpkUnderZMK 				= keyExchange.substring(4, (Service.keyLength + 4 ));
				kcvZPK 						= keyExchange.substring((Service.keyLength + 4 ), (Service.keyLength + 10 ));
				
				JSONObject joResultImport 	= kwpALTOService.addKwpALTO(keyExchange, zpkUnderZMK, kcvZPK);
				
				f39 						= JSON.get(joResultImport, "errCode");		
				
				Service.writeDebug("[AFEP Response Thread] Result From HSM : " + joResultImport.toString());
				
				this.response810(isoMsg, f39);
				
			}else if( f70.compareTo("061") == 0 ){ // Sign On
				
				f39 							= "00";
				this.response810(isoMsg, f39);
				
				networkStatusService.updateAFEPNetworkStatus((short)2, (short)0, (short)1);
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br> 
		 * Method ini digunakan untk menghandle message response 0810 dari ALTO
		 * @param isoMsg
		 * @throws Exception		 
		 */
		private void _810( IsoMessage isoMsg ) throws Exception{
			
			String f70						= ( isoMsg.hasField(70) ? isoMsg.getField(70).toString() : "" );
			
			if( f70.compareTo("301") == 0 ) {
				
				networkStatusService.updateAFEPNetworkStatus((short)1, (short)1, (short)0);
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk membuat message 0810 yang akan dikirimkan kembali ke ALTO
		 * @param isoMsg
		 * @param f39
		 * @throws Exception
		 * 
		 */
		private void response810( IsoMessage isoMsg, String f39 ) throws Exception{
			
			IsoMessage msg		= mfact.newMessage(2064);
			
			/*1. Construct 0810*/
			msg.setField(2, isoMsg.getField(2));
			msg.setField(7, isoMsg.getField(7));
			msg.setField(11, isoMsg.getField(11));
			msg.setField(33, isoMsg.getField(33));
			msg.setValue(39, f39, IsoType.ALPHA, 2);
			msg.setField(70, isoMsg.getField(70));
			
			/*2. Save AFEP Iso Log*/
			Service.writeDebug("[AFEP Response Thread] Response 0810 to AFEP : ");
			isoLog.saveMessageAFEP(msg, (short)2);
			Service.parsingField(msg, "AFEP Response Thread");
			Service.sendISO(s, msg, "AFEP Response Thread");				
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Message ini digunakan untuk menghandle message 420 (Reversal) dari ALTO.
		 * Reversal ini dikirim apabila : 
		 * <ul>
		 * 	<li>Write ke kartu gagal</li>
		 *  <li>Timeout antara ALTO dan Prepaid Host</li>
		 * </ul>
		 * @param isoMsg
		 * @throws Exception
		 */
		private void _420( IsoMessage isoMsg ) throws Exception{
			
			String f2						= ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
			String f3						= ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" );
			String f4						= ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "0" );
			String f11						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f32						= ( isoMsg.hasField(32) ? isoMsg.getField(32).toString() : "" );
			String f33						= ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" );
			String f37						= ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
			String f57						= ( isoMsg.hasField(57) ? isoMsg.getField(57).toString() : "" );
			String f90						= ( isoMsg.hasField(90) ? isoMsg.getField(90).toString() : "" );
			String f95						= ( isoMsg.hasField(95) ? isoMsg.getField(95).toString() : "" );
			String f39 						= "";
			String f48						= "";
			
			JSONObject joCardInfo 			= Service.util.parseField57(f57);
			
			String mtiOriginal 				= "";
			String f7Original 				= "";
			String f11Original 				= "";
			String f32Original				= "";
			String f33Original 				= "";
			
			String f48Response				= "";
			String f57Response 				= "";
			
			JSONObject joField90 			= Service.util.parseField90(f90);
			
			JSONObject joResultSP 			= null;
			JSONObject joCardVerifyLib 		= new JSONObject();
			boolean cardVerifyLib			= false;
			
			String traceNumber 				= JSON.get(joField90, "field_7") + 
											  JSON.get(joField90, "field_11") + 
											  JSON.get(joField90, "field_32") + 
											  JSON.get(joField90, "field_33") + 
											  JSON.get(joField90, "field_37");
			
			System.out.println("### trace number : " + traceNumber);
			
			// Save iso message log from AFEP to database
			isoLog.saveMessageAFEP(isoMsg, (short)1);	
			
			// Jika processing code = Topup Online
			if( f3.compareTo("850000") == 0 ){
				
				/*1. Validasi kartu*/
				JSONObject joValidateCard	= new JSONObject();
				
				Service.writeDebug("[ServiceMezzoAFEPResponseThread] Start Validate Card");
				
				joValidateCard 				= cardService.validateCard(f2, 
																	   JSON.get(joCardInfo, "CARD_UID"), 
																	   JSON.get(joCardInfo, "CARD_TYPE"), 
																	   Service.aesKey);	
				
				if( JSON.get(joValidateCard, "errCode").compareTo("00") == 0 ){
					
					Service.writeDebug("[ServiceMezzoAFEPResponseThread] Verify Card into Database : Valid");
					
					/*2. Verifikasi kartu dengan menggunakan lib*/
					
					joCardVerifyLib				= KMSUtil.KMS_unwrapTransaction( Service.hostHSM,
																		         Service.portHSM,
																		         JSON.get(joCardInfo, "TXN_TOKEN"),
																		         JSON.getInt(joCardInfo, "AMOUNT"),
																		         joCardInfo);
					
					if( JSON.get(joCardVerifyLib, "errCode").compareTo("00") == 0 ){
						
						Service.writeDebug("[ServiceMezzoTMSResponseThread] Processing Reversal...");
						
						joResultSP 				= transService.voidTopupOnline(traceNumber, Long.parseLong( f4 ));
						
						if( JSON.get(joResultSP, "errCode").compareTo("00")  == 0 ){
							
							f39 					= "00";
							
							/*Construct field 48*/
							f48Response 			= ""; 
							
							/*Construct field 57*/
							f57Response 			= "";
							
							this.response430(isoMsg, f39, f48, f57);
							
						}
						
					}else{
						
						f39 					= "12";
						this.response430(isoMsg, f39, f48, "");
						Service.writeDebug("[ServiceMezzoTMSResponseThread] Verify Card into KMS : Invalid");
						
					}
					
				}else{
					
					Service.writeDebug("[ServiceMezzoAFEPResponseThread] RC Validate Card : " + JSON.get(joValidateCard, "errCode"));
					f39 			= mezzoRC.getServiceALTOResponseCode(JSON.get(joResultSP, "errCode"));
					this.response430(isoMsg, f39, f48, "");
					
				}
				
			}else{
				
				Service.writeDebug("[CMS Response Thread] Processing Code not valid");
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan sebagai format ACK (Acknowledge), dimana terminal yang berhasil update balance di kartu harus
		 * menginformasikan ke prepaid host sehingga di sisi prepaid host status transaksi adalah berhasil.
		 * @param isoMsg		 
		 */
		private void _9200( IsoMessage isoMsg ){
			
			String f3						= ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" );
			String f4 						= ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" );
			String f7 						= ( isoMsg.hasField(7) ? isoMsg.getField(7).toString() : "" );
			String f11 						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f32 						= ( isoMsg.hasField(32) ? isoMsg.getField(32).toString() : "" );
			String f33 						= ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" );
			String f37 						= ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
			String f39 						= ( isoMsg.hasField(39) ? isoMsg.getField(39).toString() : "" );
			String f57 						= ( isoMsg.hasField(57) ? isoMsg.getField(57).toString() : "" );
			
			JSONObject joCardInfo 			= Service.util.parseField57(f57);
			
			String isoTraceNumber 			= (new StringBuilder())
												.append(f7)
												.append(f11)
												.append(f32)
												.append(f33)
												.append(f37).toString();
			
			JSONObject joVoid				= null;
			
			// Jika processing code = Topup Online
			if( f3.compareTo("850000") == 0 || f3.compareTo("860000") == 0 ){
				
				/*Note: untuk asumsi, apabila write to card gagal, RC Code = 29*/
				if( f39.compareTo("29") == 0 ){
					
					/*Update to transaction record*/
					JSONObject joUpdateTransaction			= transService.updateStatusTransaction((short)-1, 
																								   isoTraceNumber);
					
					/*Do Void transaction*/
					if( JSON.get(joCardInfo, "TXN_TYPE").compareTo("TOPU") == 0 ){
						
						joVoid 								= transService.voidTopupOnline(isoTraceNumber, Long.parseLong( f4 ));
						
					}
					
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Update Card Transaction after write to card : ");
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Status Ack : 29");		
					Service.writeDebug("[TMS Response Thread] Response Void Topup Cash : " + joVoid.toString());
					
				}else if( f39.compareTo("00") == 0 ){
					
					/*Update to transaction record*/
					JSONObject joUpdateTransaction			= transService.updateStatusTransaction((short)2, 
																								   isoTraceNumber);
					
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Update Card Transaction after write to card : ");
					Service.writeDebug("[TMS Response Thread] **********************************************");
					Service.writeDebug("[TMS Response Thread] Status Ack : 00");		
					Service.writeDebug("[TMS Response Thread] Response Update to Database : " + joUpdateTransaction.toString());
				
				}
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk handle message format 0200 dari ALTO.
		 * Message 0200 ini meliputi : 
		 * <ul>
		 * 	<li>Topup Pending Balance</li>
		 *  <li>Topup Online</li>
		 * </ul>
		 * @param isoMsg
		 * @throws Exception		 
		 */
		private void _200( IsoMessage isoMsg ) throws Exception{
		
			String f3						= ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" );
			String f2 						= ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
			String f4 						= ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" );
			String f7 						= ( isoMsg.hasField(7) ? isoMsg.getField(7).toString() : "" );
			String f11 						= ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
			String f18 						= ( isoMsg.hasField(18) ? isoMsg.getField(18).toString() : "" );
			String f32 						= ( isoMsg.hasField(32) ? isoMsg.getField(32).toString() : "" );
			String f33 						= ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" );
			String f37 						= ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
			String f41 						= ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" );
			String f42 						= ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" );
			String f57						= ( isoMsg.hasField(57) ? isoMsg.getField(57).toString() : "" );
			String f104						= ( isoMsg.hasField(104) ? isoMsg.getField(104).toString() : "" );
			String f39 						= "";
			String f48						= "";
			
			JSONObject joCardInfo 			= Service.util.parseField57(f57);
			
			String transType 				= "";
			String f57Response				= "";
			String f48Response 				= "";
			
			String amountTopup				= "";
			String lastCardBalance 			= "";
			String currentBalance 			= "";
			String lastPendingBalance 		= "";
			
			String walletAccount 			= "";
			
			short topupType 				= 0;
			
			JSONObject joResultSP 			= null;
			JSONObject joCardVerifyLib 		= new JSONObject();
			boolean cardVerifyLib			= false; 
			
			String traceNumber 				= f7 + f11 + f32 + f33 + f37;
			
			// Save iso message log from AFEP to database
			isoLog.saveMessageAFEP(isoMsg, (short)1);
			
			// Jika processing code = Topup Online atau Topup Pending Balance
			if( f3.compareTo("850000") == 0 || f3.compareTo("860000") == 0 ){
				
				if( f3.compareTo("850000") == 0 ){
					topupType				= 2;
				}else if( f3.compareTo("860000") == 0 ){
					topupType				= 1;
				}
				
				/*1. Validasi kartu*/
				JSONObject joValidateCard	= new JSONObject();
				
				Service.writeDebug("[ServiceMezzoAFEPResponseThread] Start Validate Card...");
				
				System.out.println("### BEFORE VALIDATE CARD");
				joValidateCard 				= cardService.validateCard(f2, 
																	   JSON.get(joCardInfo, "CARD_UID"), 
																	   JSON.get(joCardInfo, "CARD_TYPE"), 
																	   Service.aesKey);
				
				System.out.println("### AFTER VALIDATE CARD : " + joValidateCard.toString());
				
				if( JSON.get(joValidateCard, "errCode").compareTo("00") == 0 ){
					
					Service.writeDebug("[ServiceMezzoAFEPResponseThread] Verify Card into Database : Valid");
					
					/*2. Verifikasi kartu dengan menggunakan lib*/
					
					/*joCardVerifyLib				= KMSUtil.KMS_unwrapTransaction( Service.hostHSM,
																		         Service.portHSM,
																		         JSON.get(joCardInfo, "TXN_TOKEN"),
																		         JSON.getInt(joCardInfo, "AMOUNT"),
																		         joCardInfo);*/
					
					// Sementara di bypass dulu untuk kebutuhan mobile
					//if( JSON.get(joCardVerifyLib, "errCode").compareTo("00") == 0 ){
					
					if(true){
						
						Service.writeDebug("[ServiceMezzoAFEPResponseThread] Verify Card into KMS : Valid");
						
						/*3. Process depends on transaction type*/
						// 3.1 Topup Online
						if( JSON.get(joCardInfo, "TXN_TYPE").compareTo("TOPU") == 0 ){
							
							Service.writeDebug("[ServiceMezzoTMSResponseThread] Processing Online Topup...");
							
							joResultSP 		 = transService.topupOnline(f2, 
																		Double.parseDouble( JSON.get(joCardInfo, "CARD_BALANCE") ), 
																		Double.parseDouble( f4 ), 
																		f42, 
																		f41, 
																		traceNumber, 
																		f18, 
																		topupType,
																		Service.aesKey);
							
							if( JSON.get(joResultSP, "errCode").compareTo("00")  == 0 ){
								
								f39 					= "00";
								
								lastPendingBalance 		= JSON.get(joResultSP, "lastPendingBalance");
								amountTopup				= JSON.get(joResultSP, "amountTopup");
								lastCardBalance			= JSON.get(joResultSP, "lastCardBalance");
								currentBalance			= JSON.get(joResultSP, "currentBalance");
								
								/*Define Trans Type for response to AFEP*/
								if( JSON.get(joResultSP, "flagPendingBalance") != null && JSON.get(joResultSP, "flagPendingBalance").compareTo("t") == 0 ){
									transType 			= "TUBL";
								}else{
									transType			= "UPBL";
								}
								
								/*Construct field 48*/
								f48Response 		= Service.util.constructField48(traceNumber, 
																				    "", 
																				    f2, 
																				    f4, 
																				    lastCardBalance, 
																				    lastPendingBalance, 
																				    currentBalance, 
																				    "");
								
								/*Construct field 57*/
								f57Response 		= Service.util.constructField57(JSON.get(joCardInfo, "TXN_ID"), 
																					JSON.get(joCardInfo, "TXN_TOKEN"), 
																					JSON.get(joCardInfo, "TXN_TYPE"), 
																					JSON.get(joCardInfo, "TXN_CHANNEL"), 
																					JSON.get(joCardInfo, "TID"), 
																					JSON.get(joCardInfo, "MID"), 
																					JSON.get(joCardInfo, "CARD_ID"), 
																					JSON.get(joCardInfo, "CARD_TYPE"), 
																					JSON.get(joCardInfo, "AMOUNT"), 
																					JSON.get(joCardInfo, "CARD_BALANCE"));
								
								this.response210(isoMsg, f39, f48Response, f57Response);
								
							}else{
								
								f39 			= mezzoRC.getServiceALTOResponseCode(JSON.get(joResultSP, "errCode"));
								this.response210(isoMsg, f39, f48, "");
								
							}
						}
						
					}else{
						
						f39 					= "12";
						this.response210(isoMsg, f39, f48, "");
						Service.writeDebug("[ServiceMezzoTMSResponseThread] 1) Verify Card into KMS : Invalid");
						
					}
					
				}else{
					
					System.out.println("RC Validate Card : " + JSON.get(joValidateCard, "errCode"));
					
					f39 			= mezzoRC.getServiceALTOResponseCode(JSON.get(joValidateCard, "errCode"));
					this.response210(isoMsg, f39, f48, "");
					
				}
			}else if( f3.compareTo("870000") == 0 ){ // => Topup to wallet
				
				walletAccount 	 = f104.substring(6,f104.length());
				
				joResultSP 		 = transService.topupWallet(walletAccount, 
															Double.parseDouble(f4), 
															f42, 
															f41, 
															traceNumber, 
															f18, 
															Service.aesKey);

				if( JSON.get(joResultSP, "errCode").compareTo("00")  == 0 ){

					f39 		= "00";
						
					this.response210(isoMsg, f39, "", "");
						
				}else{
					
						
					
				}
				
			}
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk membuat response message 0210 yang akan dikirim kembali ke ALTO sebagai response dari
		 * message 0200.
		 * @param reqMsg
		 * @param f39
		 * @param f48
		 * @param f57
		 * @throws Exception
		 
		 */
		private void response210(IsoMessage reqMsg,
								 String f39,
								 String f48,
								 String f57) throws Exception{
			
			IsoMessage msg		= mfact.newMessage(528);
			
			msg.setField(2, reqMsg.getField(2));
			msg.setField(3, reqMsg.getField(3));
			msg.setField(4, reqMsg.getField(4));
			msg.setField(7, reqMsg.getField(7));
			msg.setField(11, reqMsg.getField(11));
			msg.setField(15, reqMsg.getField(15));
			msg.setField(18, reqMsg.getField(18));
			msg.setField(32, reqMsg.getField(32));
			msg.setField(33, reqMsg.getField(33));
			msg.setField(37, reqMsg.getField(37));
			msg.setValue(39, f39, IsoType.ALPHA, 2);
			msg.setField(41, reqMsg.getField(41));
			msg.setField(42, reqMsg.getField(42));
			msg.setField(43, reqMsg.getField(43));
			
			if( f39.compareTo("00") == 0 ){
				msg.setValue(57, f57, IsoType.LLLVAR, 0);
				msg.setValue(48, f48, IsoType.LLLVAR, 0);
			}
			
			Service.writeDebug("[CMS Response Thread] Response 0210 to AFEP : ");
			Service.parsingField(msg, "CMS Response Thread");
			Service.sendISO(s, msg, "CMS Response Thread");
			
			// Save iso message log from AFEP to database
			isoLog.saveMessageAFEP(msg, (short)2);
			
		}
		
		/**
		 * <strong>Description</strong>:<br>
		 * Method ini digunakan untuk membuat message 0430 yang merupakan message response dari 0420 yang akan dikirimkan kembali
		 * ke ALTO
		 * @param reqMsg
		 * @param f39
		 * @param f48
		 * @param f57
		 * @throws Exception
		 
		 */
		private void response430(IsoMessage reqMsg,
								 String f39,
								 String f48,
								 String f57) throws Exception{	

			
			IsoMessage msg		= mfact.newMessage(1072);
			
			msg.setField(2, reqMsg.getField(2));
			msg.setField(3, reqMsg.getField(3));
			msg.setField(4, reqMsg.getField(4));
			msg.setField(7, reqMsg.getField(7));
			msg.setField(11, reqMsg.getField(11));
			msg.setField(18, reqMsg.getField(18));
			msg.setField(32, reqMsg.getField(32));
			msg.setField(33, reqMsg.getField(33));
			msg.setField(37, reqMsg.getField(37));
			msg.setValue(39, f39, IsoType.ALPHA, 2);
			msg.setField(41, reqMsg.getField(41));
			msg.setField(42, reqMsg.getField(42));
			msg.setField(43, reqMsg.getField(43));
			
			if( f39.compareTo("00") == 0 ){
				msg.setValue(57, f57, IsoType.LLLVAR, 0);
				msg.setValue(48, f48, IsoType.LLLVAR, 0);
			}
			
			msg.setField(90, reqMsg.getField(90));
			
			Service.writeDebug("[CMS Response Thread] Response 0430 to AFEP : ");
			Service.parsingField(msg, "CMS Response Thread");
			Service.sendISO(s, msg, "CMS Response Thread");
			
			// Save iso message log from AFEP to database
			isoLog.saveMessageAFEP(msg, (short)2);		
		
			
		}
		
		
		private byte msg[];
        private Socket sock;
        final ServiceAFEPResponseThread this$1;

        Processor(byte abyte0[], Socket socket)
        {
        	super();
            this$1 = ServiceAFEPResponseThread.this;
            
            msg = abyte0;
            sock = socket;
        }
		
	}
	
	public void run()
    {
        int i = 0;
        byte abyte0[] = new byte[2];
        try
        {
            do
            {
                if(stop)
                    break;
                do
                {
                    if(s == null || !s.isConnected())
                        break;
                    if(s.getInputStream().read(abyte0) == 2)
                    {
                        int j = (abyte0[0] & 0xff) << 8 | abyte0[1] & 0xff;
                        System.out.printf("[AFEP Response Thread] Read %d bytes (%02x%02x)%n", new Object[] {
                            Integer.valueOf(j), Byte.valueOf(abyte0[0]), Byte.valueOf(abyte0[1])
                        });
                        byte abyte1[] = new byte[j];
                        s.getInputStream().read(abyte1);
                        Service.writeDebug("[AFEP Response Thread] LENGTH J : " + j);
                        i++;
                        Processor processor = new Processor(abyte1, s);
                        (new Thread(processor)).start();
                    }
                } while(true);
                try
                {
                    if(!s.isClosed())
                        s.close();
                }
                catch(IOException ioexception) { }
            } while(true);
        }
        catch(IOException ioexception1)
        {
        	Service.writeDebug((new StringBuilder()).append("[AFEP Response Thread] ResponseThread Exception occurred... ").append(ioexception1.toString()).toString());
            try
            {
                if(!s.isClosed())
                    s.close();
            }
            catch(IOException ioexception2) { }
        }
        Service.writeDebug((new StringBuilder()).append("[AFEP Response Thread] ResponseThread Exiting after reading ").append(i).append(" requests").toString());
    }
	
	private Socket s;
    private InputStream is;
    private MessageFactory mfact;
    volatile boolean stop;
    
    /**
     * <strong>Description</strong>:<br>
     * Constructor dari ServiceAFEPResponseThread.
     * @param socket
     * @param messagefactory
     */
    public ServiceAFEPResponseThread(Socket socket, MessageFactory messagefactory)
    {
    	super();
        //this$0 = ISO8583.this;
        stop = false;
        try
        {
            s = socket;
            is = s.getInputStream();
            mfact = messagefactory;
        }
        catch(Exception exception)
        {
            try
            {
                if(!s.isClosed())
                    s.close();
                Service.writeDebug("Connection Closed", 1);
            }
            catch(Exception exception1) { }
        }
    }
	
}
