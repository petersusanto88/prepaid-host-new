package bbw.prepaidhost.altoservice.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.AFEPISOLog;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan log message dari AFEP
 *
 */
public class ISOAFEPLogRepository {
	
	public ISOAFEPLogRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menyimpan data log dari ALTO ke database.
	 * @param data
	 * @return
	 */
	public long saveAFEPLog( AFEPISOLog data ){
		
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		long logId 							= 0;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			session.beginTransaction();
			
			session.save( data );	
			
			session.getTransaction().commit();
			
			logId 							= data.getId();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[ISOAFEPLogRepository] Exception<saveAFEPLog> : " + ex.getMessage());
			session.getTransaction().rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
		}
		
		return logId;
		
	}

}
