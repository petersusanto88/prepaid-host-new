package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

import com.solab.iso8583.IsoMessage;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.AFEPISOLog;
import bbw.prepaidhost.altoservice.util.AES;
import bbw.prepaidhost.altoservice.util.JSON;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan implements dari class ISOAFEPLogService yang digunakan sebagai bagian layer logical terkait log AFEP.
 *
 */
public class ISOAFEPLogServiceImpl implements ISOAFEPLogService {
	
	public ISOAFEPLogServiceImpl(){}
	
	private MerchantService merchantService			= new MerchantServiceImpl();
	private TerminalService terminalService 		= new TerminalServiceImpl();
	private CardServiceImpl cardServiceImpl 		= new CardServiceImpl();
	private AES aes 								= new AES();
	private ISOAFEPLogRepository afepRepo 			= new ISOAFEPLogRepository();
	
	public void saveMessageAFEP( IsoMessage isoMsg, short logType ) throws Exception{
		
		long logId 					 = 0;
		
		String merchantCode 		 = ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" );		
		String terminalCode 		 = ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" );		
		String cardNumber 		 	 = ( isoMsg.hasField(2) ? isoMsg.getField(2).toString() : "" );
		String f11 		 	 		 = ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" );
		String f37 		 	 		 = ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" );
		String traceNumber 			 = f11 + f37;
		
		JSONObject joMerchantInfo	 = null;
		long terminalId 			 = 0;
		long cardId 				 = 0;
		
		int index 					 = 0;

		if( terminalId != 0 && cardId != 0 && merchantCode.compareTo("") != 0 ){
			joMerchantInfo 				 = merchantService.getMerchantInfoDetail(merchantCode);
			terminalId 					 = terminalService.getTerminalIdByTerminalCode(terminalCode);
			cardId 						 = cardServiceImpl.getCardIdByCardNumber( aes.encrypt( cardNumber, Service.aesKey) );
		}
		
		AFEPISOLog isoLog 			 = new AFEPISOLog();
		
		if( joMerchantInfo != null ){
			isoLog.setAcquirerId( ( joMerchantInfo.containsKey("acquireId")  ? JSON.getInt(joMerchantInfo, "acquireId") : 0 ) );
			isoLog.setMerchantId( ( joMerchantInfo.containsKey("merchantId") ? Long.parseLong( JSON.get(joMerchantInfo, "merchantId") ) : 0 )  );
		}
		
		isoLog.setTerminalId(terminalId);
		isoLog.setCardId(cardId);
		isoLog.setCardNumber(cardNumber);
		isoLog.setTraceNumber(traceNumber);
		isoLog.setIsoData( new String( isoMsg.writeData() ) );
		isoLog.setMti( Integer.toHexString( isoMsg.getType() ).toUpperCase() );
		isoLog.setF2( cardNumber );
		isoLog.setF3( ( isoMsg.hasField(3) ? isoMsg.getField(3).toString() : "" ) );
		isoLog.setF4( ( isoMsg.hasField(4) ? isoMsg.getField(4).toString() : "" ) );
		isoLog.setF7( ( isoMsg.hasField(7) ? isoMsg.getField(7).toString() : "" ) );
		isoLog.setF11( ( isoMsg.hasField(11) ? isoMsg.getField(11).toString() : "" ) );
		isoLog.setF12( ( isoMsg.hasField(12) ? isoMsg.getField(12).toString() : "" ) );
		isoLog.setF13( ( isoMsg.hasField(13) ? isoMsg.getField(13).toString() : "" ) );
		isoLog.setF15( ( isoMsg.hasField(15) ? isoMsg.getField(15).toString() : "" ) );
		isoLog.setF18( ( isoMsg.hasField(18) ? isoMsg.getField(18).toString() : "" ) );
		isoLog.setF22( ( isoMsg.hasField(22) ? isoMsg.getField(22).toString() : "" ) );
		isoLog.setF26( ( isoMsg.hasField(26) ? isoMsg.getField(26).toString() : "" ) );
		isoLog.setF31( ( isoMsg.hasField(31) ? isoMsg.getField(31).toString() : "" ) );
		isoLog.setF32( ( isoMsg.hasField(32) ? isoMsg.getField(32).toString() : "" ) );
		isoLog.setF33( ( isoMsg.hasField(33) ? isoMsg.getField(33).toString() : "" ) );
		isoLog.setF37( ( isoMsg.hasField(37) ? isoMsg.getField(37).toString() : "" ) );
		isoLog.setF38( ( isoMsg.hasField(38) ? isoMsg.getField(38).toString() : "" ) );
		isoLog.setF39( ( isoMsg.hasField(39) ? isoMsg.getField(39).toString() : "" ) );
		isoLog.setF41( ( isoMsg.hasField(41) ? isoMsg.getField(41).toString() : "" ) );
		isoLog.setF42( ( isoMsg.hasField(42) ? isoMsg.getField(42).toString() : "" ) );
		isoLog.setF43( ( isoMsg.hasField(43) ? isoMsg.getField(43).toString() : "" ) );
		isoLog.setF48( ( isoMsg.hasField(48) ? isoMsg.getField(48).toString() : "" ) );
		isoLog.setF49( ( isoMsg.hasField(49) ? isoMsg.getField(49).toString() : "" ) );
		isoLog.setF61( ( isoMsg.hasField(61) ? isoMsg.getField(61).toString() : "" ) );
		isoLog.setF63( ( isoMsg.hasField(63) ? isoMsg.getField(63).toString() : "" ) );
		isoLog.setF70( ( isoMsg.hasField(70) ? isoMsg.getField(70).toString() : "" ) );
		isoLog.setF102( ( isoMsg.hasField(102) ? isoMsg.getField(102).toString() : "" ) );
		isoLog.setF104( ( isoMsg.hasField(104) ? isoMsg.getField(104).toString() : "" ) );
		isoLog.setLogDate( Service.util.getDateTime() );
		isoLog.setLogType(logType);
		
		logId 						= afepRepo.saveAFEPLog(isoLog);
		
		Service.writeDebug("[ServiceMezzoAFEPResponseThread] Save AFEP Log Successfully. logId = " + logId);
				
	}
	
}
