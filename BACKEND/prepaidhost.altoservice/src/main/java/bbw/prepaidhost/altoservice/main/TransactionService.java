package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan transaksi kartu.
 *
 */
public interface TransactionService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk transaksi topup kartu (topup online + topup cash) dengan memanggil FUNCTION di postgresql 
	 * @param cardNumber
	 * @param cardBalance
	 * @param topupAmount
	 * @param merchantCode
	 * @param terminalCode
	 * @param traceNumber
	 * @param channelType
	 * @param topupType
	 * @param aesKey
	 * @return
	 */
	public JSONObject topupOnline( String cardNumber,
								   double cardBalance,
								   double topupAmount,
								   String merchantCode,
								   String terminalCode,
								   String traceNumber,
								   String channelType,
								   short topupType,
								   String aesKey );
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk melakukan void topup online yang gagal dikarenakan write ke kartu gagal atau koneksi terputus
	 * sehingga di sisi database perlu penanda transaksi gagal atau sukses.
	 * @param isoTraceNumber
	 * @param amount
	 * @return
	 */
	public JSONObject voidTopupOnline( String isoTraceNumber, long amount );
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan sebagai operasi update status transaksi.
	 * @param status
	 * @param isoTraceNumber
	 * @return
	 */
	public JSONObject updateStatusTransaction( short status, String isoTraceNumber );
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan sebagai operasi topoup ke server based account (Wallet) dengan memanggil FUNCTION di database.
	 * @param walletAccount
	 * @param topupAmount
	 * @param merchantCode
	 * @param terminalCode
	 * @param traceNumber
	 * @param channelType
	 * @param aesKey
	 * @return
	 */
	public JSONObject topupWallet( String walletAccount,
								   double topupAmount,
								   String merchantCode,
								   String terminalCode,
								   String traceNumber,
								   String channelType,
								   String aesKey );
	
}
