package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait data response code.
 *
 */
public class MezzoResponseCodeServiceImpl extends MezzoResponseCodeServiceRepository implements MezzoResponseCodeService {

	public MezzoResponseCodeServiceImpl(){}
	
	public String getServiceALTOResponseCode( String mezzoResponseCode ){
		
		JSONObject joResult		= new JSONObject();
		
		String altoResponseCode = this.getALTOResponseCode(mezzoResponseCode);
		
		return altoResponseCode;
		
	}
	
}
