package bbw.prepaidhost.altoservice.main;

import java.text.ParseException;

import org.json.simple.JSONObject;

public interface KwpALTOService {

	public JSONObject addKwpALTO( String kwp, 
								  String zpkUnderZMK, 
								  String kcv)throws ParseException;
	
}
