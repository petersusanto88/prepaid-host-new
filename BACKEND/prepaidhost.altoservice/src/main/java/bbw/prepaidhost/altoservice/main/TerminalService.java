package bbw.prepaidhost.altoservice.main;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan data terminal
 */
public interface TerminalService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengambil id terminal berdasarkan kode terminal di database.
	 * @param terminalCode
	 * @return
	 */
	public long getTerminalIdByTerminalCode( String terminalCode );
	
}
