package bbw.prepaidhost.altoservice.main;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Constanta;
import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.util.HibernateUtil;
import bbw.prepaidhost.altoservice.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan data status network. (AFEP & TMS)
 */
public class NetworkStatusRepository extends Constanta {
	
	private MezzoUtil util			= new MezzoUtil();

	public NetworkStatusRepository() {
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Function yang digunakan untuk update status network koneksi ke AFEP <br>
	 * type = 1 : update status network
	 * type = 2 : update status sign on 
	 * 
	 * @param type
	 * @param status
	 * @param statusSignOn
	 * @return
	 */
	public JSONObject updateAFEPNetworkStatus(  short type,
												short status,
												short statusSignOn
											   ) {
		
		JSONObject joResult 				= new JSONObject();
		int resultUpdate 					= 0;
		SessionFactory sf					= HibernateUtil.getSessionFactory();
		Session session 					= sf.getCurrentSession();
		Transaction tx 						= session.beginTransaction();
		Query q 							= null;
		
		try {
			
			if( type == 1 ) {
				
				q 								= session.createQuery(" UPDATE NetworkStatus"
																	+ " SET lastTry = :lastTry,"
																	+ "		status = :status"
																	+ " WHERE networkId = :id ");
				
				q.setDate("lastTry", util.getDateTime());
				q.setShort("status",status);
				q.setShort("id", CONSTANT_AFEP_ID);
				
			}else if( type == 2 ) {
				
				if( statusSignOn == 1 ) {
					
					q 								= session.createQuery(" UPDATE NetworkStatus"
																		+ " SET statusSignOn = :statusSignOn,"
																		+ "		lastSignOn = :lastSignOn"
																		+ " WHERE networkId = :id ");
					
					q.setShort("statusSignOn", statusSignOn);
					q.setDate("lastSignOn",util.getDateTime());
					q.setShort("id", CONSTANT_AFEP_ID);
					
				}else if( statusSignOn == 0 ) {
					
					q 								= session.createQuery(" UPDATE NetworkStatus"
																		+ " SET statusSignOn = :statusSignOn,"
																		+ "		lastSignOff = :lastSignOff"
																		+ " WHERE networkId = :id ");
					
					q.setShort("statusSignOn", statusSignOn);
					q.setDate("lastSignOff",util.getDateTime());
					q.setShort("id", CONSTANT_AFEP_ID);
					
				}
				
				
				
			}
			
			resultUpdate				= q.executeUpdate();
			tx.commit();
			
			if( resultUpdate == 1 ){
				
				joResult.put("errCode", "00");
				joResult.put("errMsg", "Update network status success");
				
			}else{
				
				joResult.put("errCode", "109");
				joResult.put("errMsg", "Update network status failed");
				
			}
			
		}catch( Exception ex ){
			joResult.put("errCode", "109");
			joResult.put("errMsg", "Update network status failed. Error : " + ex.getMessage());
			Service.writeDebug("Exception <updateAFEPNetworkStatus> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}	
		
		
		return joResult;
		
	}
	
}

