package bbw.prepaidhost.altoservice;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hsm.safenet.HSM;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.impl.SimpleTraceGenerator;
import com.solab.iso8583.parse.ConfigParser;

import bbw.prepaidhost.altoservice.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * @version 1.0
 * Class ini merupakan main class. Saat aplikasi dijalankan, class ini yang akan dipanggil terlebih dahulu.
 */
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
public class Service {

	private static String xmlconfig 			= "";
	private static Properties props 			= null;
	public static int timeout 					= 0;
    public static int interval 					= 1;
    private static boolean debug 				= false;
    private static FileOutputStream fos 		= null;
    private static String fileDate 				= "";
    private static String debugFilename 		= "";
    public static OutputStream out;
    public static boolean ReceiveResponse 		= true;
    public static MessageFactory mfact;
    private static boolean hasKWP 				= false;
    private static SimpleTraceGenerator tracer 	= null;
    public static MezzoUtil util 				= new MezzoUtil();
    
    //Host yang digunakan untuk connect ke AFEP
    public static String hostAFEP;
    //Port yang digunakan untuk connect ke AFEP
    public static int portAFEP;
    //Socket yang digunakan connect ke AFEP
    public static Socket sockAFEP;
    
    public static int listeningPort;
    public static String cbcMezzo;
    
    public static String aesKey 				= "";
    
    public static String hostHSM 				= "";
    public static int portHSM 					= 0;
    
    public static String zmkUnderLMK 			= "";
    public static int keyLength 				= 0;
    
    public static HSM hsm 						= null; 
    
    String config 								= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_cms.ini";
//    String config 								= "config_service_cms.ini";
    
    public static ServiceAFEPResponseThread serviceAFEPResponseThread;
    
    
    /**
     * <u><b>Description</b></u>:<br>
     * Bagian ini merupakan main dari aplikasi ini. Dimana pada main ini akan memanggil constructor Service()
     * */
    public static void main(String args[]) throws IOException, InterruptedException
    {
        /*if(args.length > 0 && (args[0].equals("-h") || args[0].equals("-help")) || args.length == 0)
        {
            System.out.println("usage: java -jar MezzoCMSService.jar");
            System.exit(1);
        }*/
        Service iso8583 = new Service(args);
    }
    
    /**
     * <u><b>Description</b></u>:<br>
     * Bagian ini merupakan constructor dari class Service.<br>
     * Pada bagian ini merupakan inisialisasi dari variable public yang akan digunakan di berbagai class yang 
     * bersangkutan.<br>
     * Pada bagian ini juga termasuk inisialisasi socket dan thread ke AFEP (ALTO Switching)
     * */
    public Service(String as[]) throws IOException, InterruptedException{
    	
    	getConfiguration(config);
    	fileDate 					= util.currentDate();
		debugFilename 				= props.getProperty("debugFilename");
		listeningPort 				= Integer.parseInt( props.getProperty("listeningPort") );
		
		cbcMezzo 					= props.getProperty("cbcMezzo");
		
		hostAFEP 					= props.getProperty("hostAFEP");
		portAFEP 					= Integer.parseInt( props.getProperty("portAFEP") );
		
		hostHSM 					= props.getProperty("hostHSM");
		portHSM 					= Integer.parseInt( props.getProperty("portHSM") );
		keyLength 					= Integer.parseInt( props.getProperty("keyLength") );
		hsm 						= new HSM(hostHSM, portHSM);		
		
		zmkUnderLMK 				= props.getProperty("key6");
		
		aesKey 						= props.getProperty("dbAESKey");
		
		if( props.getProperty("debugToFile").compareTo("1") == 0 ){
			debug					= true;
			openFileDebug();
		}
		
		xmlconfig 					= props.getProperty("xmlconfig");
		mfact 						= ConfigParser.createFromClasspathConfig(xmlconfig);
		mfact.setAssignDate(false);
		
		tracer 					= new SimpleTraceGenerator((int)(System.currentTimeMillis() % 10000L));
        mfact.setTraceNumberGenerator(tracer);
		
		writeDebug("----------------------------------------------------------------");
        writeDebug((new StringBuilder()).append("MEZZO CMS Service, started at : ").append(util.currentDateTime()).toString());
        writeDebug("----------------------------------------------------------------");
    	
        /*======================== OPEN Socket for Connect to AFEP ========================*/
		sockAFEP 					= null;
		sockAFEP					= new Socket( hostAFEP, portAFEP );
		sockAFEP.setKeepAlive(true);
		/*======================== Start Connection to AFEP ========================*/
		ServiceAFEPRequestThread srvMezzoAFEPReq = new ServiceAFEPRequestThread( sockAFEP );
		/*======================== Listening Request / Response From ALTO ========================*/
		serviceAFEPResponseThread				= new ServiceAFEPResponseThread(sockAFEP, mfact);
		serviceAFEPResponseThread.start();
        
    }
    
    private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
    
    public static void writeDebug(String s, int i)
    {
        try
        {
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
    
    public static void writeDebug(String s){
		try{
			
			System.out.println(s);
			if(out != null)
				out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
		
		
			if(fileDate.compareTo(util.currentDate()) != 0){
				try{
					if(fos != null)
					  fos.close();
				}catch(Exception exception){
					System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
				}
				fileDate = util.currentDate();
				openFileDebug();
			}
			s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
			try{
				fos.write(s.getBytes());
			}
			catch(IOException ioexception){
				System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
			}
		
		
		}catch(Exception exception1) { }
	}
    
    private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                System.exit(1);
            }
        }
    }
    
    public static void sendISO( Socket socket, IsoMessage isomessage, String section )
	{
		try{
			
			sendISONew( new String( isomessage.writeData() ), socket, section );	
			
		}catch (Exception e) {  }
	}
	
	
	 public static String sendISONew( String message,Socket s, String section ){
	     String responseServer="";
	     
	     OutputStream outToServer = null;
	     DataOutputStream out = null;
	     
	     try
           {
      
		      String mti=message.substring(0,4);
		       
		      outToServer = s.getOutputStream();
		      out = new DataOutputStream(outToServer);
		      
		      writeDebug("[" + section + "] Send ("+mti+") : " + message);
		      out.writeUTF(message);
          }catch(IOException e)
          {
           writeDebug( "IOException.services.send() "+e.getMessage() );
          }finally {
			
          }
	          
	     return responseServer;
	 }
	 
	 public static void parsingField( IsoMessage isomessage, String section ){
    	
        for(int j1 = 0; j1 <= 104; j1++)
            if(isomessage.hasField(j1))
            	Service.writeDebug((new StringBuilder()).append("[" + section + "] Field ").append(j1).append(" : ").append(isomessage.getField(j1).toString()).toString());
     }
    
}
