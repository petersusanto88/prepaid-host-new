package bbw.prepaidhost.altoservice.util;

import org.json.simple.JSONObject;
import id.co.trisakti.mezzolib.LibMezzoSC;

public class KMSUtil {
	
	public static void main( String args[] ){
		
		String token = "064500C30300409AC2667171FAFA39FA39921E9087A910208828903A1165A8AED5E132229342D51E2137BEF179EF89F5A7BBD78DB9B430BC3C6E36E8FCB80834B21958C7D4D0599000044132125D478000";
		
		JSONObject jo = KMS_unwrapTransaction( "210.210.184.89",
											   7777,
											   "064500C30300409AC2667171FAFA39FA39921E9087A910208828903A1165A8AED5E132229342D51E2137BEF179EF89F5A7BBD78DB9B430BC3C6E36E8FCB80834B21958C7D4D0599000044132125D478000",
											   50000,
											   null);
		
		//System.out.println("01234567112233440150C3000000000000100317113047001403000000001900010712170101850B76000000000000000000000000000000".substring(0, 8));
		//System.out.println("01234567112233440150C3000000000000100317113047001403000000001900010712170101850B76000000000000000000000000000000".substring(8, 16));
	
		//System.out.println(token.substring(Math.max(0, token.length()-16)));\
		
		System.out.println("JO Result : " + jo.toString());
		
	}

	public static JSONObject KMS_unwrapTransaction( String hsmHost,
											 		int hsmPort,
											 		String token,
											 		int amount,
											 		JSONObject joCardInfoField57){
		
		JSONObject joResult 		= new JSONObject();
		String cardUID 				= token.substring(Math.max(0, token.length()-16));
		
		/*Call unwrapTransaction Function*/
		String unwrapTransResult 	= LibMezzoSC.unwrapTransaction(hsmHost, 
																   hsmPort, 
																   token, 
																   amount);
		
		if( unwrapTransResult.length() > 4 ){
			
			/*Parse unwrapTransactionResult*/
			joResult 					= parseUnwrapTransactionResult(unwrapTransResult);
			
			if( joCardInfoField57 != null ){
				/*Cross Check data between KMS and Data from TMS*/
				if( JSON.get(joCardInfoField57, "TID").compareTo( JSON.get(joResult, "terminalId") ) == 0 && 
					JSON.get(joCardInfoField57, "MID").compareTo( JSON.get(joResult, "merchantId") ) == 0 &&
					JSON.get(joCardInfoField57, "AMOUNT").compareTo( JSON.get(joResult, "amount") ) == 0 &&
					JSON.get(joCardInfoField57, "CARD_BALANCE").compareTo( JSON.get(joResult, "balance") ) == 0 &&
					JSON.get(joCardInfoField57, "CARD_ID").compareTo( JSON.get(joResult, "cardId") ) == 0 &&
					JSON.get(joCardInfoField57, "CARD_TYPE").compareTo( JSON.get(joResult, "cardType") ) == 0 &&
					JSON.get(joCardInfoField57, "CARD_UID").compareTo( cardUID ) != 0 ){
					
					joResult 				= new JSONObject();
					joResult.put("errCode", "-99");
					joResult.put("errCode", "Data card is not the same as that sent by the terminal.");
					
				}
				
			}
			
		}else{
			
			joResult.put("errCode", "-99");
			joResult.put("kmsCode", unwrapTransResult);
			
		}
		
		
		return joResult;
		
	}
	
	private static JSONObject parseUnwrapTransactionResult( String result ){
		
		JSONObject joResult 		= new JSONObject();
		
		joResult.put("errCode", "00");
		joResult.put("errMsg", "OK");
		
		// TerminalID
		joResult.put("terminalId", result.substring(0,8));
		joResult.put("merchantId", result.substring(8,16));
		joResult.put("transactionCode", result.substring(16,18));
		joResult.put("amount", result.substring(18,26));
		joResult.put("balance", result.substring(26,34));
		joResult.put("dateTime", result.substring(34,46));
		joResult.put("cardId", result.substring(46,62));
		joResult.put("issuerIndex", result.substring(62,66));
		joResult.put("expiredDate", result.substring(66,72));
		joResult.put("cardType", result.substring(72,74));
		joResult.put("appletVersion", result.substring(74,76));
		joResult.put("macFile", result.substring(76,82));
		joResult.put("RFU", result.substring(82,98));
		joResult.put("padding", result.substring(98,112));
		
		return joResult;
	}
	
}
