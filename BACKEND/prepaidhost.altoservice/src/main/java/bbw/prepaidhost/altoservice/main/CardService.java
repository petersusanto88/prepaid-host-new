package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan kartu.
 *
 */
public interface CardService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk validasi kartu yang masuk ke dalam prepaid host. Method ini akan memanggil FUNCTION di postgresql.
	 * @param cardNumber
	 * @param cardUID
	 * @param cardType
	 * @param dbAESKey
	 * @return
	 */
	public JSONObject validateCard( String cardNumber,
									String cardUID,
									String cardType,
									String dbAESKey);
	
}
