package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

public interface NetworkStatusService {

	public JSONObject updateAFEPNetworkStatus( short type, short status, short statusSignOn );
	
}
