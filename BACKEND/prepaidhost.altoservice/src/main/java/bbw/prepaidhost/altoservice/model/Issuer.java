package bbw.prepaidhost.altoservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class table dari "MSIssuers"
 *
 */
@Entity
@Table(name="\"MSIssuers\"")
public class Issuer {

	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Id
	private Integer issuerId;
	
	@Column(name="\"issuerCode\"")
	private String issuerCode;
	
	@Column(name="\"issuerName\"")
	private String issuerName;
	public Integer getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}
	public String getIssuerCode() {
		return issuerCode;
	}
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	
	
}
