package bbw.prepaidhost.altoservice.main;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.Card;
import bbw.prepaidhost.altoservice.model.Terminal;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan data kartu.
 */
public class CardServiceRepository {

	public CardServiceRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method yang digunakan untuk memanggil FUNCTION di postgresql terkait Validasi Kartu.
	 * @param cardNumber
	 * @param cardUID
	 * @param cardType
	 * @param dbAESKey
	 * @return
	 */
	public String callSPValidateCard( String cardNumber,
									  String cardUID,
									  String cardType,
									  String dbAESKey){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sqlQuery 				= " SELECT sp_validateCard(:1,:2,:3,:4)";
		String spResult 				= "";
		
		try{
		
			SQLQuery q					= (SQLQuery)session.createSQLQuery(sqlQuery)
												.setParameter("1", cardNumber)
												.setParameter("2", cardUID)
												.setParameter("3", cardType)
												.setParameter("4", dbAESKey);
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TransactionServiceRepository] Exception<callSPValidateCard> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return spResult;
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Digunakan untuk mengambil cardId berdasarkan nomor kartu
	 * @param cardNumber
	 * @return
	 */
	public long getCardIdByCardNumber( String cardNumber ){
		
		long cardId 						= 0;
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		Card c								= null;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			tx								= session.beginTransaction();
			
			Query q							= session.createQuery( "from Card where cardNumber = :cardNumber " )
												.setString("cardNumber", cardNumber);
			
			c								= (Card)q.uniqueResult();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[CardServiceRepository] Exception<getCardIdByCardNumber> : " + ex.getMessage());
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return c.getCardId();
		
	}
	
}
