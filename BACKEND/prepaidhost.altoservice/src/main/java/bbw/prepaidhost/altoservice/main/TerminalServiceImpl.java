package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.model.Merchant;
import bbw.prepaidhost.altoservice.model.Terminal;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait data Terminal.
 *
 */
public class TerminalServiceImpl implements TerminalService {

	public TerminalServiceImpl(){}
	
	private TerminalRepository terminalRepo = new TerminalRepository();
	
	public long getTerminalIdByTerminalCode( String terminalCode ){
		
		JSONObject joResult 	= new JSONObject();		
		long terminalId 		= 0;		
		Terminal t 				= terminalRepo.getTerminalIdByTerminalCode(terminalCode);
		
		if( t != null ){
			
			terminalId			= t.getTerminalId();  
			
		}
		
		return terminalId;
		
	}
	
}
