package bbw.prepaidhost.altoservice.main;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.Terminal;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan data terminal.
 */
public class TerminalRepository {
	
	public TerminalRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method yang digunakan untuk mengambil informasi mengenai terminal dari database berdasarkan kode terminal.
	 * @param terminalCode
	 * @return
	 */
	public Terminal getTerminalIdByTerminalCode( String terminalCode ){
		
		JSONObject joResult 				= new JSONObject();			
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		Terminal t							= null;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			tx								= session.beginTransaction();
			
			Query q							= session.createQuery( "from Terminal where terminalCode = :terminalCode " )
												.setString("terminalCode", terminalCode);
			
			t								= (Terminal)q.uniqueResult();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TerminalRepository] Exception<getTerminalIdByTerminalCode> : " + ex.getMessage());
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return t;	
		
	}

}
