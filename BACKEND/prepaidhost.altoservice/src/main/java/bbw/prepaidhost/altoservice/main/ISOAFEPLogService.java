package bbw.prepaidhost.altoservice.main;

import com.solab.iso8583.IsoMessage;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan message ISO8583 dari ALTO
 */
public interface ISOAFEPLogService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menyimpan log message ISO8583 dari AFEP ke database.
	 * @param isoMsg
	 * @param logType
	 * @throws Exception
	 */
	public void saveMessageAFEP( IsoMessage isoMsg, short logType ) throws Exception;
	
}
