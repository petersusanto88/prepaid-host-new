package bbw.prepaidhost.altoservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini dipakai sebagai operasi database yang berhubungan dengan transaksi.
 *
 */
public class TransactionServiceRepository {

	public TransactionServiceRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Methody yang digunakan untuk memanggil FUNCTION Postgresql terkait transaksi topup (Pending balance dan online)
	 * @param cardNumber
	 * @param cardBalance
	 * @param topupAmount
	 * @param merchantCode
	 * @param terminalCode
	 * @param traceNumber
	 * @param channelType
	 * @param topupType
	 * @param aesKey
	 * @return
	 */
	public String callSPTopupOnline( String cardNumber,
									 double cardBalance,
									 double topupAmount,
									 String merchantCode,
									 String terminalCode,
									 String traceNumber,
									 String channelType,
									 short topupType,
									 String aesKey){
		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		
		String sqlQuery 				= "SELECT sp_topup_online(:vCardNumber,"
															   + ":vCardBalance,"
															   + ":vAmount,"
															   + ":vMerchantCode,"
															   + ":vTerminalCode,"
															   + ":vTraceNumber,"
															   + ":vChannelType,"
															   + ":vTopupType,"
															   + ":vAESKey )";
		
		String spResult 				= "";
		
		try{
			
			SQLQuery q 					= (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vCardNumber", cardNumber)
											.setParameter("vCardBalance", cardBalance)
											.setParameter("vAmount", topupAmount)
											.setParameter("vMerchantCode", merchantCode.trim())
											.setParameter("vTerminalCode", terminalCode.trim())
											.setParameter("vTraceNumber", traceNumber)
											.setParameter("vChannelType", channelType)
											.setParameter("vTopupType", topupType)
											.setParameter("vAESKey", aesKey);
			
			List lQResult 				= q.list();			
			spResult 					= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			Service.writeDebug("[CMS Response Thread] Exception<callSPTopupOnline> : " + ex.getMessage());
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		Service.writeDebug("[CMS Response Thread] <callSPTopupOnline>" + spResult + " ###");
		
		return spResult;
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method yang digunakan untuk memanggil FUNCTION postgresql terkait transaksi reversal.
	 * @param isoTraceNumber
	 * @param amount
	 * @return
	 */
	public String callSPVoidTopupOnline( String isoTraceNumber, long amount ){
		

		
		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		String sqlQuery 				= " SELECT  sp_void_topup_online(:vIsoTraceNumber,"
															   		+ ":vAmount,"
															   		+ ":vAESKey)";
		String spResult 				= "";
		
		try{
		
			SQLQuery q						= (SQLQuery)session.createSQLQuery(sqlQuery)
												.setParameter("vIsoTraceNumber", isoTraceNumber)
												.setParameter("vAmount", amount)
												.setParameter("vAESKey", Service.aesKey);
			
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TransactionServiceRepository] Exception<callSPVoidTopupCash> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		Service.writeDebug("[TMS Response Thread] <callSPVoidTopupCash>" + spResult + " ###");
		
		return spResult;	
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini dipanggil pada saat acknowledge transaksi topup dan melakukan update ke record transaksi di database.
	 * @param status
	 * @param isoTraceNumber
	 * @return
	 */
	public JSONObject repo_UpdateCardTransactionStatus( short status, String isoTraceNumber){
		
		int resultUpdate 				= 0;
		JSONObject joResult 			= new JSONObject();
		
		SessionFactory sf				= HibernateUtil.getSessionFactory();
		Session session 				= sf.getCurrentSession();
		
		Transaction tx					= session.beginTransaction();
		Query q 						= session.createQuery("update CardTransaction set status = :status where isoTraceNumber = :isoTraceNumber");
		
		try{		
			
			q.setShort("status", status);
			q.setString("isoTraceNumber", isoTraceNumber);
			resultUpdate				= q.executeUpdate();
			tx.commit();
			
			if( resultUpdate == 1 ){
				
				joResult.put("errCode", "00");
				joResult.put("errMsg", "Update transaction status success");
				
			}else{
				
				joResult.put("errCode", "109");
				joResult.put("errMsg", "Update transaction status failed");
				
			}
			
		}catch( Exception ex ){
			joResult.put("errCode", "109");
			joResult.put("errMsg", "Update transaction status failed. Error : " + ex.getMessage());
			Service.writeDebug("Exception <updateStatusTransaction> : " + ex.getMessage());
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return joResult;
		
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menghandle topup wallet (Server Base) dengan memanggil FUNCTION yang ada di postgresql.
	 * @param walletAccount
	 * @param topupAmount
	 * @param merchantCode
	 * @param terminalCode
	 * @param traceNumber
	 * @param channelType
	 * @param aesKey
	 * @return
	 */
	public String callSPTopupWallet( String walletAccount,
									 double topupAmount,
									 String merchantCode,
									 String terminalCode,
									 String traceNumber,
									 String channelType,
									 String aesKey){

		SessionFactory sessionFactory 	= HibernateUtil.getSessionFactory();
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		
		String sqlQuery 				= "SELECT sp_topup_wallet(:vWalletAccount,"
															   + ":vAmount,"
															   + ":vMerchantCode,"
															   + ":vTerminalCode,"
															   + ":vTraceNumber,"
															   + ":vChannelType,"
															   + ":vAESKey )";
		
		String spResult 				= "";
		
		try{
		
			SQLQuery q 					= (SQLQuery)session.createSQLQuery(sqlQuery)
												.setParameter("vWalletAccount", walletAccount)
												.setParameter("vAmount", topupAmount)
												.setParameter("vMerchantCode", merchantCode.trim())
												.setParameter("vTerminalCode", terminalCode.trim())
												.setParameter("vTraceNumber", traceNumber)
												.setParameter("vChannelType", channelType)
												.setParameter("vAESKey", aesKey);
			
			List lQResult 				= q.list();			
			spResult 					= lQResult.get(0).toString();
		
		}catch( Exception ex ){
			Service.writeDebug("[CMS Response Thread] Exception<callSPTopupWallet> : " + ex.getMessage());
		}finally{	
			if (session != null && session.isOpen()) {
				session.close();
			}
			
			tx.commit();
			
		}
	
		Service.writeDebug("[CMS Response Thread] <callSPTopupWallet>" + spResult + " ###");
	
		return spResult;
	
	}
	
}
