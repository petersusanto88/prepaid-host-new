package bbw.prepaidhost.altoservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.Merchant;
import bbw.prepaidhost.altoservice.model.MezzoResponseCode;
import bbw.prepaidhost.altoservice.util.HibernateUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan data merchant
 *
 */
public class MerchantRepository {

	public MerchantRepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Mengambil informasi merchant berdasarkan kode merchant
	 * @param merchantCode
	 * @return
	 */
	public Merchant getMerchantInfoByMerchantCode( String merchantCode ){
		
		JSONObject joResult 				= new JSONObject();			
		SessionFactory sf					= null;
		Session session 					= null;		
		Transaction tx						= null;
		
		Merchant m 							= null;
		
		try{
			
			sf								= HibernateUtil.getSessionFactory();
			session 						= sf.getCurrentSession();
			tx								= session.beginTransaction();
			
			Query q							= session.createQuery( "from Merchant where merchantCode = :merchantCode " )
												.setString("merchantCode", merchantCode.trim());
			
			m								= (Merchant)q.uniqueResult();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[TerminalRepository] Exception<getMerchantInfoByMerchantCode> : " + ex.getMessage());
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		return m;	
		
	}
	
}
