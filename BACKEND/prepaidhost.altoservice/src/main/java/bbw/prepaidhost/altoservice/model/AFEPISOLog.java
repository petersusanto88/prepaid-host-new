package bbw.prepaidhost.altoservice.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class entity dari "TRAFEPISOLogs"
 *
 */
@Entity
@Table(name="\"TRAFEPISOLogs\"")
public class AFEPISOLog implements Serializable {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id	
	@Column(name="logId")
	private Long id;
	
	@Column(name="\"acquirerId\"")
	private int acquirerId;
	
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"terminalId\"")
	private long terminalId;
	
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"traceNumber\"")
	private String traceNumber;
	
	@Column(name="\"isoData\"")
	private String isoData;	
	
	private String mti;
	private String f2;
	private String f3;
	private String f4;
	private String f7;
	private String f11;
	private String f12;
	private String f13;
	private String f15;
	private String f18;
	private String f22;
	private String f26;
	private String f31;
	private String f32;
	private String f33;
	private String f37;
	private String f38;
	private String f39;
	private String f41;
	private String f42;
	private String f43;
	private String f48;
	private String f49;
	private String f61;
	private String f63;
	private String f70;
	private String f102;
	private String f104;
	
	@Column(name="\"logDate\"")
	private Date logDate;
	
	@Column(name="\"logType\"")
	private short logType;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(int acquirerId) {
		this.acquirerId = acquirerId;
	}
	public long getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}
	public long getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}
	public long getCardId() {
		return cardId;
	}
	public void setCardId(long cardId) {
		this.cardId = cardId;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	public String getIsoData() {
		return isoData;
	}
	public void setIsoData(String isoData) {
		this.isoData = isoData;
	}
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getF2() {
		return f2;
	}
	public void setF2(String f2) {
		this.f2 = f2;
	}
	public String getF3() {
		return f3;
	}
	public void setF3(String f3) {
		this.f3 = f3;
	}
	public String getF4() {
		return f4;
	}
	public void setF4(String f4) {
		this.f4 = f4;
	}
	public String getF7() {
		return f7;
	}
	public void setF7(String f7) {
		this.f7 = f7;
	}
	public String getF11() {
		return f11;
	}
	public void setF11(String f11) {
		this.f11 = f11;
	}
	public String getF12() {
		return f12;
	}
	public void setF12(String f12) {
		this.f12 = f12;
	}
	public String getF13() {
		return f13;
	}
	public void setF13(String f13) {
		this.f13 = f13;
	}
	public String getF15() {
		return f15;
	}
	public void setF15(String f15) {
		this.f15 = f15;
	}
	public String getF18() {
		return f18;
	}
	public void setF18(String f18) {
		this.f18 = f18;
	}
	public String getF22() {
		return f22;
	}
	public void setF22(String f22) {
		this.f22 = f22;
	}
	public String getF26() {
		return f26;
	}
	public void setF26(String f26) {
		this.f26 = f26;
	}
	public String getF31() {
		return f31;
	}
	public void setF31(String f31) {
		this.f31 = f31;
	}
	public String getF32() {
		return f32;
	}
	public void setF32(String f32) {
		this.f32 = f32;
	}
	public String getF33() {
		return f33;
	}
	public void setF33(String f33) {
		this.f33 = f33;
	}
	public String getF37() {
		return f37;
	}
	public void setF37(String f37) {
		this.f37 = f37;
	}
	public String getF38() {
		return f38;
	}
	public void setF38(String f38) {
		this.f38 = f38;
	}
	public String getF39() {
		return f39;
	}
	public void setF39(String f39) {
		this.f39 = f39;
	}
	public String getF41() {
		return f41;
	}
	public void setF41(String f41) {
		this.f41 = f41;
	}
	public String getF43() {
		return f43;
	}
	public void setF43(String f43) {
		this.f43 = f43;
	}
	public String getF48() {
		return f48;
	}
	public void setF48(String f48) {
		this.f48 = f48;
	}
	public String getF49() {
		return f49;
	}
	public void setF49(String f49) {
		this.f49 = f49;
	}
	public String getF61() {
		return f61;
	}
	public void setF61(String f61) {
		this.f61 = f61;
	}
	public String getF63() {
		return f63;
	}
	public void setF63(String f63) {
		this.f63 = f63;
	}
	public String getF102() {
		return f102;
	}
	public void setF102(String f102) {
		this.f102 = f102;
	}
	public String getF104() {
		return f104;
	}
	public void setF104(String f104) {
		this.f104 = f104;
	}
	public String getF42() {
		return f42;
	}
	public void setF42(String f42) {
		this.f42 = f42;
	}
	public Date getLogDate() {
		return logDate;
	}
	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}
	public short getLogType() {
		return logType;
	}
	public void setLogType(short logType) {
		this.logType = logType;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getF70() {
		return f70;
	}
	public void setF70(String f70) {
		this.f70 = f70;
	}
	
	
	
}
