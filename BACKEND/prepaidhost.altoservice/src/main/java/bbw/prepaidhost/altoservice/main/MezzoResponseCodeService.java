package bbw.prepaidhost.altoservice.main;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang digunakan sebagai operasi yang berhubungan dengan ResponseCode
 */
public interface MezzoResponseCodeService {

	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk konversi response code ke response code ALTO
	 * @param mezzoResponseCode
	 * @return
	 */
	public String getServiceALTOResponseCode( String mezzoResponseCode );
	
}
