package bbw.prepaidhost.altoservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class table dari "MSMerchants"
 *
 */
@Entity
@Table(name="\"MSMerchants\"")
public class Merchant {

	@Id
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"acquireId\"")
	private int acquireId;
	
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Column(name="\"merchantName\"")
	private String merchantName;
	
	private short status;
	
	@Column(name="\"isDelete\"")
	private short isDelete;

	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}

	public int getAcquireId() {
		return acquireId;
	}

	public void setAcquireId(int acquireId) {
		this.acquireId = acquireId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(short isDelete) {
		this.isDelete = isDelete;
	}
	
	
	
}
