package bbw.prepaidhost.altoservice.main;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.altoservice.Service;
import bbw.prepaidhost.altoservice.model.KwpALTO;
import bbw.prepaidhost.altoservice.util.HibernateUtil;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai operasi database yang berhubungan dengan log KWP (Key Exchange dengan ALTO)
 */
public class KwpALTORepository {

	public KwpALTORepository(){}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk menyimpan data key exchange dari ALTO ke database
	 * @param data
	 * @return
	 */
	public JSONObject addKwpALTO( KwpALTO data ){
		
		int resultUpdate 				= 0;
		JSONObject joResult 			= new JSONObject();
		long logId 						= 0;
		
		SessionFactory sf				= HibernateUtil.getSessionFactory();
		Session session 				= sf.getCurrentSession();		
		
		try{		
			
			session.beginTransaction();
			
			session.save( data );	
			
			session.getTransaction().commit();
			
			logId 							= data.getId();
			
			joResult.put("errCode", "00");
			joResult.put("errMsg", "OK");
			joResult.put("logId", logId);
			
		}catch( Exception ex ){
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Add KWP Failed. Error : " + ex.getMessage());
			Service.writeDebug("Exception <addKwpALTO> : " + ex.getMessage());
			session.getTransaction().rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return joResult;
		
	}
	
	
	
}
