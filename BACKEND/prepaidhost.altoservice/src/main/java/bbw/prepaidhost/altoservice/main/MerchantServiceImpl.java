package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bbw.prepaidhost.altoservice.model.Merchant;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait data Merchant.
 *
 */
public class MerchantServiceImpl implements MerchantService{
	
	private MerchantRepository merchantRepo = new MerchantRepository();
	
	public MerchantServiceImpl(){}
	
	public JSONObject getMerchantInfoDetail( String merchantCode ){
		
		JSONObject joResult 	= new JSONObject();
		
		Merchant m 				= merchantRepo.getMerchantInfoByMerchantCode(merchantCode);
		
		if( m != null ){
			
			joResult.put("errCode", "00");
			joResult.put("errMsg", "OK");
			joResult.put("merchantId", m.getMerchantId());
			joResult.put("acquireId", m.getAcquireId());
			
		}else{
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Merchant not found");
			
		}
		
		return joResult;
		
	}
	
}
