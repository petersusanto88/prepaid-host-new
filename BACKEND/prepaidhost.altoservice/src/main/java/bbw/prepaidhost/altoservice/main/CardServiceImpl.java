package bbw.prepaidhost.altoservice.main;

import java.util.List;

import org.json.simple.JSONObject;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan implements dari interface CardService. Class ini digunakan untuk semua operasi mengenai kartu.
 */
public class CardServiceImpl extends CardServiceRepository implements CardService{

	public CardServiceImpl(){}
	
	
	public JSONObject validateCard( String cardNumber,
									String cardUID,
									String cardType,
									String dbAESKey){
		
		JSONObject joResult			= new JSONObject();
		
		System.out.println("Card Number : " + cardNumber);
		System.out.println("Card UID : " + cardUID);
		System.out.println("Card Type : " + cardType);
		System.out.println("AES : " + dbAESKey);
		
		String spResult				= this.callSPValidateCard(cardNumber, 
															  cardUID, 
															  cardType, 
															  dbAESKey);
		
		System.out.println("### SP Validate Card : " + spResult);
		
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Validate card failed.");		
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			
		}
		
		return joResult;
		
	}
	
}
