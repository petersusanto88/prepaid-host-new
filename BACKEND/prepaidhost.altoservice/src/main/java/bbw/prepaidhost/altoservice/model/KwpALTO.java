package bbw.prepaidhost.altoservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class table dari "TRKWPALTO"
 *
 */
@Entity
@Table(name="\"TRKWPALTO\"")
public class KwpALTO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="logId")
	private Long id;
	
	@Column(name="\"logDate\"")
	private Date logDate;
	
	private String kwp;
	private String kcv;
	
	@Column(name="\"ZPKUnderZMK\"")
	private String zpkUnderZMK;
	
	@Column(name="\"ZPKUnderLMK\"")
	private String zpkUnderLMK;	

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getKwp() {
		return kwp;
	}

	public void setKwp(String kwp) {
		this.kwp = kwp;
	}

	public String getKcv() {
		return kcv;
	}

	public void setKcv(String kcv) {
		this.kcv = kcv;
	}

	public String getZpkUnderZMK() {
		return zpkUnderZMK;
	}

	public void setZpkUnderZMK(String zpkUnderZMK) {
		this.zpkUnderZMK = zpkUnderZMK;
	}

	public String getZpkUnderLMK() {
		return zpkUnderLMK;
	}

	public void setZpkUnderLMK(String zpkUnderLMK) {
		this.zpkUnderLMK = zpkUnderLMK;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
}
