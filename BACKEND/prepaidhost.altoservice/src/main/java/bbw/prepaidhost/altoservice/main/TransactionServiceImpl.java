package bbw.prepaidhost.altoservice.main;

import org.json.simple.JSONObject;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai bagian layer logical terkait data dan aktifitas transaction.
 *
 */
public class TransactionServiceImpl implements TransactionService {
	
	private TransactionServiceRepository transRepo		= new TransactionServiceRepository();
	
	public TransactionServiceImpl(){
		
	}
	
	public JSONObject topupOnline( String cardNumber,
								   double cardBalance,
								   double topupAmount,
								   String merchantCode,
								   String terminalCode,
								   String traceNumber,
								   String channelType,
								   short topupType,
								   String aesKey ){
		
		JSONObject joResult 		= new JSONObject();
		
		String spResult 			= transRepo.callSPTopupOnline(cardNumber, 
																  cardBalance, 
																  topupAmount, 
																  merchantCode, 
																  terminalCode, 
																  traceNumber, 
																  channelType, 
																  topupType,
																  aesKey);
		
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Topup online failed.");
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMsg", arrSPResult[3]);
			joResult.put("flagPendingBalance", arrSPResult[4]);
			joResult.put("lastPendingBalance", arrSPResult[5]);
			joResult.put("amountTopup", arrSPResult[6]);
			joResult.put("lastCardBalance", arrSPResult[7]);
			joResult.put("currentBalance", arrSPResult[8]);
			
		}
		
		return joResult;
		
	}
	
	public JSONObject voidTopupOnline( String isoTraceNumber, long amount ){
		

		
		JSONObject joResult			= new JSONObject();
		
		String spResult				= transRepo.callSPVoidTopupOnline(isoTraceNumber, amount);
				
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Void Topup cash failed.");		
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			
		}
		
		return joResult;
		
	
		
	}
	
	public JSONObject topupWallet( String walletAccount,
								   double topupAmount,
								   String merchantCode,
								   String terminalCode,
								   String traceNumber,
								   String channelType,
								   String aesKey ){

		JSONObject joResult 		= new JSONObject();

		String spResult 			= transRepo.callSPTopupWallet(walletAccount,
																  topupAmount, 
																  merchantCode, 
																  terminalCode, 
																  traceNumber, 
																  channelType,
																  aesKey);

		if( spResult.compareTo("") == 0 ){

			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Topup wallet failed.");

		}else{

			String[] arrSPResult	= spResult.split("\\|");			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMsg", arrSPResult[3]);
			joResult.put("amountTopup", arrSPResult[4]);
			joResult.put("lastWalletBalance", arrSPResult[5]);
			joResult.put("previousWalletBalance", arrSPResult[6]);

		}

		return joResult;

	}
	
	public JSONObject updateStatusTransaction( short status, String isoTraceNumber ){
		
		return transRepo.repo_UpdateCardTransactionStatus(status, isoTraceNumber);
		
	}

}
