package bbw.prepaidhost.altoservice.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class table dari "TRNetworkStatus"
 *
 */
@Entity
@Table(name="public.\"TRNetworkStatus\"")
public class NetworkStatus {

	@Id
	@Column(name="\"networkId\"")
	private short networkId;
	
	@Column(name="\"networkName\"")
	private String networkName;

	@Column(name="\"IP\"")
	private String IP;
	
	@Column(name="\"lastTry\"")
	private Date lastTry;
	private short status;
	
	@Column(name="\"statusSignOn\"")
	private Short statusSignOn;
	
	@Column(name="\"lastSignOn\"")
	private Date lastSignOn;
	
	@Column(name="\"lastSignOff\"")
	private Date lastSignOff;
	public short getNetworkId() {
		return networkId;
	}
	public void setNetworkId(short networkId) {
		this.networkId = networkId;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public Date getLastTry() {
		return lastTry;
	}
	public void setLastTry(Date lastTry) {
		this.lastTry = lastTry;
	}
	public short getStatus() {
		return status;
	}
	public void setStatus(short status) {
		this.status = status;
	}
	public Short getStatusSignOn() {
		return statusSignOn;
	}
	public void setStatusSignOn(Short statusSignOn) {
		this.statusSignOn = statusSignOn;
	}
	public Date getLastSignOn() {
		return lastSignOn;
	}
	public void setLastSignOn(Date lastSignOn) {
		this.lastSignOn = lastSignOn;
	}
	public Date getLastSignOff() {
		return lastSignOff;
	}
	public void setLastSignOff(Date lastSignOff) {
		this.lastSignOff = lastSignOff;
	}
	
	
	
}
