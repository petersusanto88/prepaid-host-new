package bbw.prepaidhost.cardnotification.main;

import java.util.List;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.model.master.Notification;
import bbw.prepaidhost.cardnotification.model.master.Setting;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class NotificationServiceImpl implements NotificationService {
	
	private static NotificationRepository notifRepo = new NotificationRepository();
	private static String debugLabel = "NotificationServiceImpl";
	
	public NotificationServiceImpl(){}
	
	public List<Notification> getNotification( SessionFactory sessionFactory ){
		return notifRepo.getNotification(sessionFactory);
	}
	
	public int updateNotificationStatusSend( SessionFactory sessionFactory,
			 								 long notificationId ){
		return notifRepo.updateNotificationStatusSend(sessionFactory, notificationId);
	}
	
	public int updateNotificationStatusResponse( SessionFactory sessionFactory,
												 long notificationId,
												 short statusResponse){
		return notifRepo.updateNotificationStatusResponse(sessionFactory, notificationId, statusResponse);
	}
	
	public JSONObject sendEmail( String email,
								 String subject,
								 String body,
								 String hostMail,
								 int portMail,
								 String usernameMail,
								 String passwordMail){
		
		JSONObject joResult 	= new JSONObject();
		
		Properties props 		= new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
  		props.put("mail.smtp.host", hostMail);
  		props.put("mail.smtp.port", portMail);
  		
  		//System.out.println("Email : " + usernameMail);
  		
  		Session session = Session.getInstance(props,
   			new javax.mail.Authenticator() {
   			protected PasswordAuthentication getPasswordAuthentication() {
   				return new PasswordAuthentication(usernameMail, passwordMail);
   			}
   		 });
  		
  		session.setDebug(true);
  		
  		try{
  			
  			MimeMessage message = new MimeMessage(session);
   			message.setFrom(new InternetAddress(usernameMail));
   			
   			if( !email.equals("") && email != null && !email.equals("''") && !email.equals("null") ){
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject(subject);
				message.setContent(body, "text/html");
				
				Transport transport = session.getTransport("smtp");
				transport.connect (hostMail, portMail, usernameMail, passwordMail);
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();    			
				
				joResult.put("errCode", "00");
				joResult.put("errMsg", "Send email successfully");
   			}else{
   				
   				joResult.put("errCode", "-99");
				joResult.put("errMsg", "Can't send email. Email destination not valid.");
   				
   			}
  			
  		}catch (MessagingException e) {
  			Service.writeDebug("Exception<sendEmail> : " + e.getMessage(), debugLabel);
  			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Exception : " + e.getMessage());
  			throw new RuntimeException(e);
 		}
		
		return joResult;
		
	}
	
	public long addNotification( SessionFactory sessionMaster,
			 					 Notification notification ) {
		return notifRepo.addNotification(sessionMaster, notification);
	}
	
	public Setting getSettingValue( SessionFactory sessionFactory,
								    String category,
								    String name) {
		return notifRepo.getSettingValue(sessionFactory, category, name);
	}

}
