package bbw.prepaidhost.cardnotification.model.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSIssuers\"")
public class Issuer {

	@Id
	@Column(name="\"issuerId\"")
	private long issuerId;
	
	@Column(name="\"issuerName\"")
	private String issuerName;
	
	@Column(name="\"dbHost\"")
	private String dbHost;
	
	@Column(name="\"dbUsername\"")
	private String dbUsername;
	
	@Column(name="\"dbPassword\"")
	private String dbPassword;
	
	@Column(name="\"dbPort\"")
	private int dbPort;
	
	@Column(name="\"dbName\"")
	private String dbName;
	
	@Column(name="\"dbReportName\"")
	private String dbReportName;
	
	@Column(name="\"approvalStatus\"")
	private short approvalStatus;
	
	private short status;
	
	public long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(long issuerId) {
		this.issuerId = issuerId;
	}

	public String getDbHost() {
		return dbHost;
	}

	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public int getDbPort() {
		return dbPort;
	}

	public void setDbPort(int dbPort) {
		this.dbPort = dbPort;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbReportName() {
		return dbReportName;
	}

	public void setDbReportName(String dbReportName) {
		this.dbReportName = dbReportName;
	}

	public short getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(short approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	
	
}
