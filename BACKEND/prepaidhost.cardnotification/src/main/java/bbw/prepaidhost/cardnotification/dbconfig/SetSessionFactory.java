package bbw.prepaidhost.cardnotification.dbconfig;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import bbw.prepaidhost.cardnotification.model.client.Card;
import bbw.prepaidhost.cardnotification.model.client.ViewCardWillExpire;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationLog;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationScheduleHistory;
import bbw.prepaidhost.cardnotification.model.master.Issuer;
import bbw.prepaidhost.cardnotification.model.master.Notification;
import bbw.prepaidhost.cardnotification.model.master.Setting;

public class SetSessionFactory {

	private static String hostDB;
	private static String usernameDB;
	private static String passwordDB;
	private static int portDB;
	private static String nameDB;
	
	public SetSessionFactory(){}
	
	public static SessionFactory setSession( String host, String username, String password, int portDB, String nameDB, short type){
		
		String connectionURL		= "jdbc:postgresql://" + host + ":" + portDB + "/" + nameDB;
		SessionFactory sessionFact 	= null;
		
		try{
			
			Configuration conf = new Configuration();
			
			conf.configure("hibernate-annotation.cfg.xml");
			conf.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
			conf.setProperty("hibernate.current_session_context_class", "thread");
			conf.setProperty("hibernate.connection.url", connectionURL);
			conf.setProperty("hibernate.connection.username", username);
			conf.setProperty("hibernate.connection.password", password);
			conf.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
				
						
			if( type == 1 ){ // master				
				conf.addAnnotatedClass(Card.class);
				conf.addAnnotatedClass(Notification.class);
				conf.addAnnotatedClass(Issuer.class);
				conf.addAnnotatedClass(Setting.class);
				conf.addAnnotatedClass(CardNotificationLog.class);
				conf.addAnnotatedClass(CardNotificationScheduleHistory.class);
			}else if( type == 2 ){ // client	
				conf.addAnnotatedClass(Card.class);
				conf.addAnnotatedClass(ViewCardWillExpire.class);
			}
			
			
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder()
																		.applySettings(conf.getProperties());
			
			sessionFact							  				  = conf.buildSessionFactory(serviceRegistryBuilder.build());
			
			
			
		}catch( Throwable ex ){
			throw new ExceptionInInitializerError(ex);
		}
		
		return sessionFact;
		
	}
	
}
