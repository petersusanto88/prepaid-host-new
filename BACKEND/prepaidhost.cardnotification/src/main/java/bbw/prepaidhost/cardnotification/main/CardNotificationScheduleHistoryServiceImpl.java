package bbw.prepaidhost.cardnotification.main;

import java.text.ParseException;
import java.util.Date;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.model.master.CardNotificationScheduleHistory;
import bbw.prepaidhost.cardnotification.util.MezzoUtil;

public class CardNotificationScheduleHistoryServiceImpl implements CardNotificationScheduleHistoryService {

	private static CardNotificationScheduleHistoryRepository scheduleHistoryRepo	= new CardNotificationScheduleHistoryRepository();
	private static MezzoUtil util = new MezzoUtil();
	
	public CardNotificationScheduleHistoryServiceImpl() {}
	
	public boolean isHasRunToday( SessionFactory session, long issuerId ) throws ParseException {
		
		boolean flagHasRun						= false;
		Date currentDate 						= util.getCurrentDate();		
		
		CardNotificationScheduleHistory data 	= scheduleHistoryRepo.getScheduleHistory( session,
																					      currentDate, 
																					      issuerId);
		
		if( data != null ) {			
			flagHasRun = true;
		}
			
		return flagHasRun;
		
	}
	
	public void addScheduleHistory( SessionFactory sf, long issuerId ) throws ParseException {
		
		CardNotificationScheduleHistory data = new CardNotificationScheduleHistory();
		
		data.setIssuerId(issuerId);
		data.setLogDate(util.getCurrentDate());		
		scheduleHistoryRepo.addScheduleHistory(sf, data);
	}
}
