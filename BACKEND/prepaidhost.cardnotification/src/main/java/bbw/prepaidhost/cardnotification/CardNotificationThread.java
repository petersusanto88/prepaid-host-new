package bbw.prepaidhost.cardnotification;

import java.util.List;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.main.IssuerService;
import bbw.prepaidhost.cardnotification.main.IssuerServiceImpl;
import bbw.prepaidhost.cardnotification.model.master.Issuer;
import bbw.prepaidhost.cardnotification.util.AES;

/**
 * 
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai thread yang membuat sub-thread notifikasi kartu untuk masing-masing issuer.
 * Thread ini akan membuat sub-thread sejumlah issuer yang ada di database. 
 */
public class CardNotificationThread extends Thread {
	
	private static String debugLabel = "CardNotificationThread"; 
	private static IssuerService issuerService = new IssuerServiceImpl();
	private static AES aes = new AES();

	public void run(){
		
		try{
			
			if( !this.stop ) {
				
				Service.writeDebug("Starting Card Notification Service Thread...", debugLabel);
				Service.setSessionHibernateMaster();
				
				/*1. Get All Issuer*/
				List<Issuer> lIssuer 			= issuerService.getIssuerDBInfo();
				for( Issuer issuer : lIssuer ) {
					
					Service.writeDebug("====================================", debugLabel);
					Service.writeDebug("              ISSUER DATA			", debugLabel);
					Service.writeDebug("====================================", debugLabel);
					Service.writeDebug("Issuer ID : " + issuer.getIssuerId(), debugLabel);
					Service.writeDebug("Issuer Name : " + issuer.getIssuerName(), debugLabel);					
					Service.writeDebug("Host : " + aes.decrypt( issuer.getDbHost(), Service.aesKey ), debugLabel);
					Service.writeDebug("Username : " + aes.decrypt( issuer.getDbUsername(), Service.aesKey ), debugLabel);
					Service.writeDebug("Password : " + aes.decrypt( issuer.getDbPassword(), Service.aesKey ), debugLabel);
					Service.writeDebug("Username : " + aes.decrypt( issuer.getDbName(), Service.aesKey ), debugLabel);
					Service.writeDebug("Port : " + issuer.getDbPort(), debugLabel);					
					Service.writeDebug("------------------------------------", debugLabel);		
					
					SessionFactory sessionClient = Service.setSessionHibernateClient(aes.decrypt( issuer.getDbHost(), Service.aesKey ), 
																					 aes.decrypt( issuer.getDbUsername(), Service.aesKey ), 
																					 aes.decrypt( issuer.getDbPassword(), Service.aesKey ), 
																					 issuer.getDbPort(), 
																				  	 aes.decrypt( issuer.getDbName(), Service.aesKey ));
					
					(new IssuerCardNotificationThread( sessionClient, issuer )).start();
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("Exception : " + ex.getMessage(), debugLabel);
		}
		
	}
	
	volatile boolean stop;
	final CardNotificationThread this$0;
	private int stat;
	
	public CardNotificationThread(int status)
	{
		super();
        this$0 = CardNotificationThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("Exception : " + ex.toString(), debugLabel);
        }
	    
	}
	
}
