package bbw.prepaidhost.cardnotification.main;

import java.util.List;

import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;

import bbw.prepaidhost.cardnotification.model.master.Notification;
import bbw.prepaidhost.cardnotification.model.master.Setting;

public interface NotificationService {

	public List<Notification> getNotification( SessionFactory sessionFactory );
	public int updateNotificationStatusSend( SessionFactory sessionFactory, long notificationId );
	public int updateNotificationStatusResponse( SessionFactory sessionFactory,  long notificationId, short statusResponse);
	public JSONObject sendEmail( String email,
								 String subject,
								 String body,
								 String hostMail,
								 int portMail,
								 String usernameMail,
								 String passwordMail);
	public long addNotification( SessionFactory sessionMaster,
			 					 Notification notification );
	
	public Setting getSettingValue( SessionFactory sessionFactory,
								    String category,
								    String name);
	
}
