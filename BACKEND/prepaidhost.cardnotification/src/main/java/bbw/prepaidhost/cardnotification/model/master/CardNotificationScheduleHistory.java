package bbw.prepaidhost.cardnotification.model.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRCardNotificationScheduleHistories\"")
public class CardNotificationScheduleHistory implements Serializable {

	@Id
	@Column(name="\"logDate\"")
	private Date logDate;
	
	@Id
	@Column(name="\"issuerId\"")
	private long issuerId;
	
	@Column(name="\"startTime\"")
	private Date startTime;
	
	@Column(name="\"endTime\"")
	private Date endTime;

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(long issuerId) {
		this.issuerId = issuerId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	
	
}
