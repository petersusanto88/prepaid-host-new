package bbw.prepaidhost.cardnotification.model.client;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class ViewCardWillExpire{

	@Id
	@Column(name="\"vcardnumber\"")
	private String cardNumber;
	
	@Column(name="\"vcustomername\"")
	private String customerName;
	
	@Column(name="\"vemail\"")
	private String email;
	
	@Column(name="\"vmobilephonenumber\"")
	private String mobilePhoneNumber;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	
	
	
	
}
