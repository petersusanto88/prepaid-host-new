package bbw.prepaidhost.cardnotification;

import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.main.CardNotificationScheduleHistoryService;
import bbw.prepaidhost.cardnotification.main.CardNotificationScheduleHistoryServiceImpl;
import bbw.prepaidhost.cardnotification.main.CardNotificationService;
import bbw.prepaidhost.cardnotification.main.CardNotificationServiceImpl;
import bbw.prepaidhost.cardnotification.main.NotificationService;
import bbw.prepaidhost.cardnotification.main.NotificationServiceImpl;
import bbw.prepaidhost.cardnotification.model.client.ViewCardWillExpire;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationLog;
import bbw.prepaidhost.cardnotification.model.master.Issuer;
import bbw.prepaidhost.cardnotification.model.master.Notification;
import bbw.prepaidhost.cardnotification.model.master.Setting;
import bbw.prepaidhost.cardnotification.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan class sub-thread dari <strong>CardNotificationThread</strong> yang digunakan untuk 
 * mengambil data kartu yang akan expired.
 * Cara kerja thread ini adalah :<br>
 * <ul>
 * 	<li>Cek apakah notification card sudah mesti jalan sesuai setting schedule di database. (Tabel : MSSettings, Setting Category : SCHEDULER, Setting Name : CARD_EXPIRED )</li>
 *  <li>Cek ke log apakah notifikasi sudah pernah jalan untuk hari ini atau belum.</li>
 * </ul>
 */
public class IssuerCardNotificationThread extends Thread {
	
	private SessionFactory sessionClient;
	private static CardNotificationService cardNotifService = new CardNotificationServiceImpl();
	private static NotificationService notifService = new NotificationServiceImpl();
	private static CardNotificationScheduleHistoryService scheduleHistoryService = new CardNotificationScheduleHistoryServiceImpl();
	private static String debugLabel = "IssuerConnectionThread";
	private static Issuer issuer;
	private static Setting setting = null;
	private static String subjectTemp = "";
	private static String bodyTemp = "";
	private static String subject = "";
	private static String body = "";
	private static long notificationLogId = 0;
	private static Notification notification = null;
	private static boolean existLog = false; 
	private static String currentTime;
	private static Date dateToday;
	private static MezzoUtil util = new MezzoUtil();

	public IssuerCardNotificationThread() {
		super();
	}
	
	IssuerCardNotificationThread( SessionFactory session, Issuer issuer ){
		this.sessionClient = session;
		this.issuer = issuer;
	}
	
	public void run() {
		
		Service.setSessionHibernateMaster();
		
		Service.writeDebug("Start Thread Issuer [" + issuer.getIssuerId() + "]", debugLabel);				
		
		/*1. Get Expire Card Message Content*/
		setting 			= notifService.getSettingValue(Service.masterSessionFactory, "CARD_NOTIFICATION_EXPIRED", "subject");
		subjectTemp 		= setting.getSettingValue();
		setting 			= notifService.getSettingValue(Service.masterSessionFactory, "CARD_NOTIFICATION_EXPIRED", "body");
		bodyTemp			= setting.getSettingValue();
		
		do {
			
			/*Check if today has run or not yet*/
			try {
				if( !scheduleHistoryService.isHasRunToday(Service.masterSessionFactory, issuer.getIssuerId()) ) {
					
					/*Add to schedule notification history*/
					scheduleHistoryService.addScheduleHistory(Service.masterSessionFactory, issuer.getIssuerId());
				
					/*0. Check current time for running this thread*/
					currentTime 		= util.getCurrentTime();
					setting 			= notifService.getSettingValue(Service.masterSessionFactory, "SCHEDULER", "CARD_EXPIRED");
					
					if( setting.getSettingValue().compareTo(currentTime) == 0 ) {
				
						try {
						
							dateToday 			= util.getCurrentDate();
							Service.writeDebug("************************************", debugLabel);
							Service.writeDebug("		List Card Will Expire	    ", debugLabel);
							Service.writeDebug("************************************", debugLabel);
							Service.writeDebug("Issuer ID : " + this.issuer.getIssuerId(), debugLabel);
							Service.writeDebug("Issuer Name : " + this.issuer.getIssuerId(), debugLabel);
							Service.writeDebug("------------------------------------", debugLabel);
							
							/*1. Get Card will expired*/
							List<Object[]> lCard 		= cardNotifService.getCardWillExpire(this.sessionClient, Service.aesKey);
							
							if( lCard.size() > 0 ) {
								
								Iterator<Object[]> it	= lCard.iterator();
								
								while( it.hasNext() ) {
									
									body 				= bodyTemp;
									subject 			= subjectTemp;
									
									Object[] result 	= (Object[])it.next();
									
									body 				= body.replace("[#card_number#]", (String)result[0]);
									
									Service.writeDebug("Card Number : " + (String)result[0], debugLabel);
									Service.writeDebug("Customer Name : " + (String)result[1], debugLabel);
									Service.writeDebug("Email : " + (String)result[2], debugLabel);
									Service.writeDebug("Mobile Phone Number : " + (String)result[3], debugLabel);
									Service.writeDebug("### Start Add Notification", debugLabel);
									Service.writeDebug("Subject : " + subject, debugLabel);
									Service.writeDebug("Body : " + body, debugLabel);
									
									/*2. Check if exists card notification log today*/
									existLog 			= cardNotifService.isExistCardNotificationLog(Service.masterSessionFactory, 
																									  dateToday, 
																									  (String)result[0], 
																									  (short)1);
									
									if( !existLog ) {
										
										notification		= new Notification();
										notification.setSubject(subject);
										notification.setMessage(body);
										notification.setDestination((String)result[2]);
										notification.setType( (short)1 );
										notification.setStatusSend( (short)0 );
										notificationLogId	= notifService.addNotification(Service.masterSessionFactory, notification);
										Service.writeDebug("Log ID : " + notificationLogId, debugLabel);
										Service.writeDebug("### End Add Notification", debugLabel);
										
										CardNotificationLog cardNotifLog	= new CardNotificationLog();
										cardNotifLog.setLogDate(util.getCurrentDate());
										cardNotifLog.setCardNumber((String)result[0]);
										cardNotifLog.setNotificationId(notificationLogId);
										cardNotifLog.setNotificationType((short)1);
										cardNotifService.addCardNotificationLog(Service.masterSessionFactory, cardNotifLog);
										
									}			
									
									
									Service.writeDebug("------------------------------------", debugLabel);
									
								}
								
							}		
						}catch( ParseException ex ) {
							Service.writeDebug("ParseException <run()> : " + ex.getMessage(), debugLabel);
						}catch( Exception ex ) {
							Service.writeDebug("Exception <run()> : " + ex.getMessage(), debugLabel);
						}
					
					}
					
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}while(true);
		
	}
	
}
