package bbw.prepaidhost.cardnotification.model.master;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRNotifications\"")
public class Notification {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long notificationId;
	
	private short type;
	private String destination;
	private String subject;
	private String message;
	
	@Column(name="\"sendDate\"")
	private Date sendDate;
	
	@Column(name="\"statusSend\"")
	private short statusSend;
	
	@Column(name="\"statusResponse\"")
	private Short statusResponse;
	
	@Column(name="\"statusMessage\"")
	private String statusMessage;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public short getStatusSend() {
		return statusSend;
	}

	public void setStatusSend(short statusSend) {
		this.statusSend = statusSend;
	}

	public Short getStatusResponse() {
		return statusResponse;
	}

	public void setStatusResponse(Short statusResponse) {
		this.statusResponse = statusResponse;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	
	
}
