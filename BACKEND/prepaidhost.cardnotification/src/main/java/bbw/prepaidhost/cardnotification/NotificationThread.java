package bbw.prepaidhost.cardnotification;

import java.util.List;

import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.main.NotificationService;
import bbw.prepaidhost.cardnotification.main.NotificationServiceImpl;
import bbw.prepaidhost.cardnotification.model.master.Notification;


/**
 * @author PETER SUSANTO
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini digunakan sebagai thread yang akan mengambil data antrian notifikasi (email dan sms) dari database.
 * 
 */
public class NotificationThread extends Thread {
	
	private static NotificationService notificationService 	   = new NotificationServiceImpl();
	private static ObjectMapper objMap					   	   = new ObjectMapper();
	private static String debugLabel 						   = "NotificationThread";

	public void run(){
		
		try{
			
			if( !this.stop ){
				
				Service.writeDebug("Starting Notification Thread...", debugLabel);
				
				Service.setSessionHibernateMaster();
				
				while(true){
					
					Thread.sleep(Service.interval * 1000);
					
					List<Notification> lNotification			= notificationService.getNotification(Service.masterSessionFactory);
					
					for( Notification notification : lNotification ){
						
						Service.writeDebug("----------------------------------------------------------", debugLabel);
						Service.writeDebug("[NotificationThread] ID : " + notification.getNotificationId(), debugLabel);
						Service.writeDebug("[NotificationThread] Type : " + notification.getType(), debugLabel);
						Service.writeDebug("[NotificationThread] Destination : " + notification.getDestination(), debugLabel);
						Service.writeDebug("[NotificationThread] Subject : " + notification.getSubject(), debugLabel);
						Service.writeDebug("[NotificationThread] Message : " + notification.getMessage(), debugLabel);
						
						//Email
						if( notification.getType() == 1 ){
						
							JSONObject joResultSendEmail		= notificationService.sendEmail(notification.getDestination(), 
																								notification.getSubject(), 
																								notification.getMessage(), 
																						 		Service.mailHost, 
																						 		Service.mailPort, 
																						 		Service.mailUsername, 
																						 		Service.mailPassword);
							
							Object json 						= objMap.readValue(joResultSendEmail.toString(), Object.class);
							Service.writeDebug("Result Send Email : " + objMap.writerWithDefaultPrettyPrinter().writeValueAsString(json), debugLabel);
							notificationService.updateNotificationStatusSend(Service.masterSessionFactory, notification.getNotificationId());
							Service.writeDebug("----------------------------------------------------------", debugLabel);
							
						}else{ // SMS, etc
							
							
							
						}
					
					}
					
					Thread.sleep(Service.interval * 1000);
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("Exception : " + ex.getMessage(), debugLabel);
		}
		
	}
	
	volatile boolean stop;
	final NotificationThread this$0;
	private int stat;
	
	public NotificationThread(int status)
	{
		super();
        this$0 = NotificationThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("Exception : " + ex.toString(), debugLabel);
        }
	    
	}
	
}
