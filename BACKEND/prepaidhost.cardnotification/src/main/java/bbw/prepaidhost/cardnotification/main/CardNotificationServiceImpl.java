package bbw.prepaidhost.cardnotification.main;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.model.client.ViewCardWillExpire;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationLog;

public class CardNotificationServiceImpl implements CardNotificationService  {

	private static CardNotificationRepository  cardNotifRepo = new CardNotificationRepository();
	
	public CardNotificationServiceImpl(){}
	
	public List<Object[]> getCardWillExpire( SessionFactory sessionFactory,
			   										   String dbAESKey ){
		
		return cardNotifRepo.getCardWillExpire(sessionFactory, dbAESKey);
		
	}
	
	public boolean isExistCardNotificationLog( SessionFactory session, 
											   Date logDate,
											   String cardNumber,
											   short notificationType) {
		
		CardNotificationLog log 	= cardNotifRepo.getCardNotificationLog(session, 
																		   logDate, 
																		   cardNumber, 
																		   notificationType);		
		
		if( log != null ) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public void addCardNotificationLog( SessionFactory session,
										CardNotificationLog data) {
		
		cardNotifRepo.addCardNotificationLog(session, data);
		
	}

}
