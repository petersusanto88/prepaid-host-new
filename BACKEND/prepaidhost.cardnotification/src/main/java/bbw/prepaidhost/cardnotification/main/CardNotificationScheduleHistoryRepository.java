package bbw.prepaidhost.cardnotification.main;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationScheduleHistory;

public class CardNotificationScheduleHistoryRepository {

	private static String debugLabel = "CardNotificationScheduleHistoryRepository";
	
	public CardNotificationScheduleHistoryRepository() {}
	
	public CardNotificationScheduleHistory getScheduleHistory( SessionFactory sf,
															   Date logDate,
															   long issuerId) {
		
		Session session 						= sf.getCurrentSession();
		CardNotificationScheduleHistory	data 	= null;
		Transaction tx 							= session.beginTransaction();
		
		Query q 								= session.createQuery( " from CardNotificationScheduleHistory "
																	 + " where logDate = :date"
																	 + " and issuerId = :issuerId " )
													.setParameter("date", logDate)
													.setParameter("issuerId", issuerId);
		
		try {
			
			data 								= (CardNotificationScheduleHistory)q.uniqueResult();
			tx.commit();
			
		}catch( Exception ex ) {
			Service.writeDebug("Exception <getScheduleHistory> : " + ex.getMessage(), "CardNotificationScheduleHistory");
		}finally {
			if( session != null && session.isOpen() ) {
				session.close();
			}
		}
		
		return data;
	}
	
	public void addScheduleHistory( SessionFactory sf, CardNotificationScheduleHistory data ) {
		
		Session session					= sf.getCurrentSession();
		Transaction tx 					= null;
		
		try {
			tx 							= session.beginTransaction();
			session.save(data);
			tx.commit();
		}catch( Exception ex ) {
			Service.writeDebug("Exception<addScheduleHistory> : " + ex.getMessage(), "CardNotificationScheduleHistoryRepository");
			tx.rollback();
		}finally {
			if( session != null && session.isOpen() ) {
				session.close();
			}
		}
		
	}
	
}
