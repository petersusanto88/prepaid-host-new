package bbw.prepaidhost.cardnotification.model.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"MSSettings\"")
public class Setting {

	@Id
	@Column(name="\"settingId\"")
	private long settingId;
	
	@Column(name="\"settingCategory\"")
	private String settingCategory;
	
	@Column(name="\"settingName\"")
	private String settingName;
	
	@Column(name="\"settingValue\"")
	private String settingValue;

	public long getSettingId() {
		return settingId;
	}

	public void setSettingId(long settingId) {
		this.settingId = settingId;
	}

	public String getSettingCategory() {
		return settingCategory;
	}

	public void setSettingCategory(String settingCategory) {
		this.settingCategory = settingCategory;
	}

	public String getSettingName() {
		return settingName;
	}

	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}
	
	
	
}
