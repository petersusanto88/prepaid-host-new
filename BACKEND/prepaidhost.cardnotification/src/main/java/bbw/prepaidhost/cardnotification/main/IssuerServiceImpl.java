package bbw.prepaidhost.cardnotification.main;

import java.util.List;

import bbw.prepaidhost.cardnotification.model.master.Issuer;

public class IssuerServiceImpl implements IssuerService {
	
	private static IssuerRepository issuerRepo = new IssuerRepository();

	public IssuerServiceImpl(){}
	
	public List<Issuer> getIssuerDBInfo(){	
		return issuerRepo.getIssuerDBInfo();
	}
	
	
	
}
