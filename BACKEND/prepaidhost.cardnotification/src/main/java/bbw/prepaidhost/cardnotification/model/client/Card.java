package bbw.prepaidhost.cardnotification.model.client;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="\"MSCards\"")
public class Card {

	public Card(){}	
	
	@Id
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber; 	
	
	private short status;
	
	@Column(name="\"statusPushToTMS\"")
	private short statusPushToTMS;

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public short getStatusPushToTMS() {
		return statusPushToTMS;
	}

	public void setStatusPushToTMS(short statusPushToTMS) {
		this.statusPushToTMS = statusPushToTMS;
	}
	
	
	
}
