package bbw.prepaidhost.cardnotification.main;

import java.util.List;

import bbw.prepaidhost.cardnotification.model.master.Issuer;

public interface IssuerService {

	public List<Issuer> getIssuerDBInfo();
	
}
