package bbw.prepaidhost.cardnotification.main;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.model.client.ViewCardWillExpire;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationLog;

public class CardNotificationRepository {
	
	private static String debugLabel = "CardNotificationRepositoryx";

	public CardNotificationRepository(){}
	
	public List<Object[]> getCardWillExpire( SessionFactory sessionFactory,
											 String dbAESKey	){	
		
		Session session 						= sessionFactory.getCurrentSession();
		List<Object[]> lCard					= null;
		Transaction tx 							= session.beginTransaction();
		String sqlQuery 						= " SELECT * FROM sp_get_card_will_expire(:aesKey) ";
		
		try {
			
			SQLQuery q 							= (SQLQuery)session.createSQLQuery( sqlQuery )
														.setParameter("aesKey", dbAESKey );			
			lCard 								= q.list();
			tx.commit();
			
		}catch( Exception ex ) {
			
			Service.writeDebug("Exception<getCardWillExpire> : " + ex.getMessage(), debugLabel);
			tx.rollback();
			
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }	
			
		}
		
		return lCard;
		
	}
	
	public CardNotificationLog getCardNotificationLog( SessionFactory sf, Date logDate, String cardNumber, short notificationType ) {
		
		Session session 				= sf.getCurrentSession();
		CardNotificationLog log			= null;
		Transaction tx 					= session.beginTransaction();
		
		Query q 						= session.createQuery(" from CardNotificationLog "
															+ " where logDate = :date"
															+ " and cardNumber = :cardNumber"
															+ " and notificationType = :type ")
											.setParameter("date", logDate)
											.setParameter("cardNumber", cardNumber)
											.setParameter("type", notificationType);
		
		try {
			
			log 						= (CardNotificationLog)q.uniqueResult();
			tx.commit();
			
		}catch( Exception ex ) {
			Service.writeDebug("Exception <getCardNotificationLog> : " + ex.getMessage(), debugLabel);
		}finally {
			if( session != null && session.isOpen() ) {
				session.close();
			}
		}
		
		return log;
		
	}
	
	public void addCardNotificationLog( SessionFactory sessionMaster, CardNotificationLog data ) {
		
		long cardNotifLogId 			= 0;
		Session session					= sessionMaster.getCurrentSession();
		Transaction tx 					= null;
		
		try {
			Service.writeDebug("---> Add Card Notification Log", debugLabel);
			tx 							= session.beginTransaction();
			session.save(data);
			tx.commit();
			
		}catch( Exception ex ) {
			Service.writeDebug("Exception <addCardNotificationLog> : " + ex.getMessage(), debugLabel);
			tx.rollback();
		}finally {
			if( session != null && session.isOpen() ) {
				session.close();
			}
		}
		
	}
	
	
}
