package bbw.prepaidhost.cardnotification.main;

import java.text.ParseException;
import java.util.Date;

import org.hibernate.SessionFactory;

public interface CardNotificationScheduleHistoryService {

	public boolean isHasRunToday( SessionFactory session, long issuerId ) throws ParseException;
	public void addScheduleHistory( SessionFactory sf, long issuerId ) throws ParseException;
	
}
