package bbw.prepaidhost.cardnotification.model.master;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRCardNotificationLogs\"")
public class CardNotificationLog implements Serializable{

	@Id
	@Column(name="\"logDate\"")
	private Date logDate;
	
	@Id
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"notificationType\"")
	private Short notificationType;
	
	@Column(name="\"notificationId\"")
	private Long notificationId;

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Short getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(Short notificationType) {
		this.notificationType = notificationType;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}
	
	
}
