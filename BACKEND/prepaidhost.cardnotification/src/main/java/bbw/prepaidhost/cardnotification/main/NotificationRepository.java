package bbw.prepaidhost.cardnotification.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.model.master.Notification;
import bbw.prepaidhost.cardnotification.model.master.Setting;

public class NotificationRepository {
	
	private static String debugLabel = "NotificationRepository";

	public NotificationRepository(){}
	
	public List<Notification> getNotification( SessionFactory sessionFactory ){
		
		Session session 							= sessionFactory.getCurrentSession();
		List<Notification> list						= null;
		Transaction tx 							 	= session.beginTransaction();
		Query q 									= session.createQuery( "from Notification where statusSend = 0" );
		
		try{
			
			list  									= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
			System.out.println("Exception : " + ex.getMessage());
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return list;
		
	}
	
	public Setting getSettingValue( SessionFactory sessionFactory,
								    String category,
								    String name){
		
		Session session 							= sessionFactory.getCurrentSession();
		Setting setting								= null;
		Transaction tx 							 	= session.beginTransaction();
		Query q 									= session.createQuery( " from Setting "
																		 + " where settingCategory = :category "
																		 + " and settingName = :name " )
														.setParameter("category", category)
														.setParameter("name", name);
		
		try{
			
			setting  								= (Setting)q.uniqueResult();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
			Service.writeDebug("Exception : " + ex.getMessage(), debugLabel);
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return setting;
		
	}
	
	public int updateNotificationStatusSend( SessionFactory sessionFactory,
											 long notificationId ){
		
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		int result 						= 0;
		
		String sql 						= " update Notification set statusSend = 1 where notificationId = :id ";
		Query q 						= session.createQuery(sql);
		q.setLong("id", notificationId);
		
		try{
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("Exception<updateNotificationStatusSend> : " + ex.getMessage(), debugLabel);
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }	
			
		}
		
		return result;
		
	}
	
	public int updateNotificationStatusResponse( SessionFactory sessionFactory,
												 long notificationId,
												 short statusResponse){
		
		Session session 				= sessionFactory.getCurrentSession();		
		Transaction tx 					= session.beginTransaction();
		int result 						= 0;
		
		String sql 						= " update Notification set statusResponse = :status where notificationId = :id ";
		Query q 						= session.createQuery(sql);
		q.setShort("status", statusResponse);
		q.setLong("id", notificationId);
		
		try{
			result 						= q.executeUpdate();
			tx.commit();
		}catch( Exception ex ){
			Service.writeDebug("Exception<updateNotificationStatusResponse> : " + ex.getMessage(), debugLabel);
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }	
			
		}
		
		return result;
		
	}
	
	public long addNotification( SessionFactory sessionMaster,
								 Notification notification ) {
		
		long notificationLogId 				= 0;		
		Session session 					= sessionMaster.getCurrentSession();
		Transaction tx 						= null;
		
		try {
			
			tx 								= session.beginTransaction();
			session.save(notification);
			notificationLogId 				= notification.getNotificationId();			
			tx.commit();
			
		}catch( Exception ex ) {
			Service.writeDebug("Exception <addNotification> : " + ex.getMessage(), debugLabel);
			tx.rollback();
		}finally {
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return notificationLogId;
		
	}
	
}
