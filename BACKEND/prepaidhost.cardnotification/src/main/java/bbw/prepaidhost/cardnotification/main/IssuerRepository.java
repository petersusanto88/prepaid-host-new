package bbw.prepaidhost.cardnotification.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bbw.prepaidhost.cardnotification.Service;
import bbw.prepaidhost.cardnotification.model.master.Issuer;

public class IssuerRepository {

	public IssuerRepository(){}
	
	public List<Issuer> getIssuerDBInfo(){
		
		Service.setSessionHibernateMaster();
		Session session 					= Service.masterSessionFactory.getCurrentSession();
		List<Issuer> lIssuer				= null;
		
		Transaction tx 						= session.beginTransaction();
		Query q 							= session.createQuery("from Issuer where status = 1 and approvalStatus = 1 ");
		
		try{
			
			lIssuer							= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			tx.rollback();
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return lIssuer;
	}
	
}
