package bbw.prepaidhost.cardnotification;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.dbconfig.SetSessionFactory;
import bbw.prepaidhost.cardnotification.main.IssuerServiceImpl;
import bbw.prepaidhost.cardnotification.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * @version 1.0
 * <br><br>
 * <strong>Description</strong>:<br>
 * Class ini merupakan main class. Saat aplikasi dijalankan, class ini yang akan dipanggil terlebih dahulu.
 */
public class Service {

	private static String xmlconfig 					= "";
	private static Properties props 					= null;
	public static int timeout 							= 0;
    public static int interval 							= 1;
    private static boolean debug 						= false;
    private static FileOutputStream fos 				= null;
    private static String fileDate 						= "";
    private static String debugFilename 				= "";
    public static OutputStream out;
    public static boolean ReceiveResponse 				= true;
    private static boolean hasKWP 						= false;
    public static MezzoUtil util 						= new MezzoUtil();
    public static IssuerServiceImpl isi 				= new IssuerServiceImpl(); 
    
    public static String hostDBMaster 					= "";
    public static int portDBMaster						= 0;
    public static String nameDBMaster 					= "";
    public static String userDBMaster 					= "";
    public static String passwordDBMaster 				= "";
    
    private static String debugLabel 					= "Service"; 
    
    public static SetSessionFactory sf 				 	= null;    
    public static CardNotificationThread cnt;    
    public static NotificationThread nt;
    
    public static String aesKey 						= "";
    
//    Localhost : 
      String config 									= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_card_notification.ini";
      
//    Development : 
//    String config 									= "config_service_card_notification.ini";
    
    public static SessionFactory masterSessionFactory	= null;
    
    public static String mailHost 						= "";
    public static String mailUsername					= "";
    public static String mailPassword 					= "";
    public static int mailPort 							= 0;

	public static void main( String[] args ) throws IOException, InterruptedException
    {
		Service service = new Service(args);
    }
	
	public Service(String as[]) throws IOException, InterruptedException{
		
		try
        {		
			
			getConfiguration(config);			
			fileDate				= util.currentDate();
			debug 					= true;			
			debugFilename			= props.getProperty("debugFilename");
			interval				= Integer.parseInt( props.getProperty("interval") );
			
			hostDBMaster 			= props.getProperty("hostDBMaster");
			portDBMaster 			= Integer.parseInt( props.getProperty("portDBMaster") );
			nameDBMaster 			= props.getProperty("nameDBMaster");
			userDBMaster 			= props.getProperty("userDBMaster");
			passwordDBMaster 		= props.getProperty("passwordDBMaster");
			
			mailHost 				= props.getProperty("mailHost");
			mailUsername	 		= props.getProperty("mailUsername");
			mailPassword 			= props.getProperty("mailPassword");
			mailPort 				= Integer.parseInt( props.getProperty("mailPort") );
			
			aesKey 					= props.getProperty("dbAESKey");
			
			sf						= new SetSessionFactory();
			
			openFileDebug();
			writeDebug("----------------------------------------------------------------",debugLabel);
            writeDebug((new StringBuilder()).append("CARD NOTIFICATION SERVICE, started at : ").append(util.currentDateTime()).toString(), debugLabel);
            writeDebug("----------------------------------------------------------------",debugLabel);	

            cnt			= new CardNotificationThread(1);
            cnt.run();
            
            /*nt 			= new NotificationThread(1);
            nt.run();*/
            
            Service.masterSessionFactory.close();
            
        }catch(Exception exception)
        {
        	Service.writeDebug("Exception :  " + exception.toString(), debugLabel);   	
        }
		
	}
	
	
	public static void writeDebug(String s, String label){
		try{
			
			System.out.println("[" + label + "] " + s);
			if(out != null)
				out.write((new StringBuilder()).append("[" + label + "] ").append(s).append("\r\n").toString().getBytes());
		
		
			if(fileDate.compareTo(util.currentDate()) != 0){
				try{
					if(fos != null)
					  fos.close();
				}catch(Exception exception){
					System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
				}
				fileDate = util.currentDate();
				openFileDebug();
			}
			s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
			try{
				fos.write(s.getBytes());
			}
			catch(IOException ioexception){
				System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
			}
		
		
		}catch(Exception exception1) { }
	}
	
	public static void writeDebug(String s, int i)
    {
        try
        {
        	
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
	
	private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            System.out.println("debugfilename  " + s);
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
	
	private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                Service.writeDebug("Exception getConfiguration :  " + filenotfoundexception, debugLabel);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                Service.writeDebug("Exception getConfiguration :  " + ioexception, debugLabel);
                System.exit(1);
            }
        }
    }
	
	/**
	 * <strong>Description</strong>:<br>
	 * function ini digunakan untuk melakukan koneksi ke database master dengan menggunakan konfigurasi database di file. 
	 */
	public static void setSessionHibernateMaster(){
		Service.masterSessionFactory 	= Service.sf.setSession(hostDBMaster, 
																userDBMaster, 
																passwordDBMaster, 
																portDBMaster, 
																nameDBMaster,
																(short)1);
	}
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk koneksi ke database client, dengan informasi koneksi database didapat dari data issuer
	 * di database master table MSIssuers.
	 * @param hostDBClient
	 * @param usernameDBClient
	 * @param passwordDBClient
	 * @param portDB
	 * @param nameDB
	 * @return
	 */
	public static SessionFactory setSessionHibernateClient( String hostDBClient,
															String usernameDBClient,
															String passwordDBClient,
															int portDB,
															String nameDB) {
		
		SessionFactory clientSessionFactory;
		
		clientSessionFactory 			= Service.sf.setSession(hostDBClient, 
																usernameDBClient, 
																passwordDBClient, 
																portDB, 
																nameDB, 
																(short)2);
		
		return clientSessionFactory;
		
	}
}
