package bbw.prepaidhost.cardnotification.main;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;

import bbw.prepaidhost.cardnotification.model.client.ViewCardWillExpire;
import bbw.prepaidhost.cardnotification.model.master.CardNotificationLog;

public interface CardNotificationService {

	public List<Object[]> getCardWillExpire( SessionFactory sessionFactory,
			   								 String dbAESKey );
	public boolean isExistCardNotificationLog( SessionFactory session, 
											   Date logDate,
											   String cardNumber,
											   short notificationType);		
	public void addCardNotificationLog( SessionFactory session,
										CardNotificationLog data);		
	
}
