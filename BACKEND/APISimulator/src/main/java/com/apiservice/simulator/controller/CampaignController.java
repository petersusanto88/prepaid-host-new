package com.apiservice.simulator.controller;


import java.text.ParseException;

import javax.servlet.annotation.MultipartConfig;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.apiservice.simulator.utility.JSON;
import com.apiservice.simulator.utility.Utility;

@RestController
@RequestMapping("/api.tms")
@MultipartConfig(fileSizeThreshold = 20971520)

public class CampaignController {
	
	@Autowired
	private Utility util;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/handshake", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody String handshake( @RequestBody String jsonParam ) throws Exception{
		
		JSONObject joResult 		= new JSONObject();		
		JSONObject joResultLogin 	= new JSONObject();		
		JSONObject joParam 			= JSON.newJSONObject( jsonParam );
		
		if( joParam.containsKey("session_id") ){	
			
			joResult.put("errCode", "00");
			joResult.put("errMsg", "OK");
			joResult.put("session_id", JSON.get(joParam, "session_id"));
			joResult.put("token", util.generateRandom(32));
			
		}else{
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Param not valid");
			
		}
		
		return joResult.toString();
		
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/campaignPromo", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody String campaignPromo( @RequestBody String jsonParam ) throws Exception{
		
		JSONObject joResult 		= new JSONObject();		
		JSONObject joResultLogin 	= new JSONObject();		
		JSONObject joParam 			= JSON.newJSONObject( jsonParam );
		
		joResult.put("response_status","00");
		joResult.put("session_id",JSON.get(joParam, "session_id"));
		joResult.put("issuer_id",JSON.getInt(joParam, "issuer_id"));
		joResult.put("merchant_id", JSON.get(joParam, "merchant_id"));
		joResult.put("terminal_id", JSON.get(joParam, "terminal_id"));
		joResult.put("card_type", JSON.get(joParam, "card_type"));
		
		return joResult.toString();
		
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/blacklist", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody String blacklist( @RequestBody String jsonParam ) throws Exception{
		
		JSONObject joResult 		= new JSONObject();		
		JSONObject joResultLogin 	= new JSONObject();		
		JSONObject joParam 			= JSON.newJSONObject( jsonParam );
		
		joResult.put("response_status","00");
		joResult.put("session_id",JSON.get(joParam, "session_id"));
		joResult.put("issuer_id",JSON.getInt(joParam, "issuer_id"));
		joResult.put("merchant_id", JSON.get(joParam, "merchant_id"));
		joResult.put("terminal_id", JSON.get(joParam, "terminal_id"));
		joResult.put("card_id", JSON.get(joParam, "card_id"));
		joResult.put("response_status", "00");
		joResult.put("card_type", JSON.get(joParam, "card_type"));
		
		return joResult.toString();
		
	}
	
}
