package com.apiservice.simulator.utility;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.*;

import org.springframework.stereotype.Service;

@Service
public class Utility {

	public static String sendRequest(String url_api, String body_content, String content_type)
    {
    	String msg="";
		String s3 = "";
		
		String request = url_api;
		
		try
        {
			URL url = new URL(request);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false); 
			connection.setRequestMethod("POST"); 
			connection.setRequestProperty("Content-Type", content_type); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(body_content.getBytes().length));
			connection.setUseCaches (false);
			OutputStream outputstream = connection.getOutputStream();
            outputstream.write(body_content.getBytes());
            outputstream.close();
            int i = connection.getResponseCode();
            if(i == 200)
            {
                InputStream inputstream = connection.getInputStream();
                int j = connection.getContentLength();
                if(j > 0)
                {
                    byte abyte0[] = new byte[j];
                    int k;
                    for(int j1 = 0; (k = inputstream.read()) != -1; j1++)
                        abyte0[j1] = (byte)k;

                    s3 = new String(abyte0);
                } else
                {
                    char c = '\u0800';
                    byte abyte1[] = new byte[c];
                    int k1 = 0;
                    do
                    {
                        int i1;
                        if((i1 = inputstream.read()) == -1)
                            break;
                        abyte1[k1] = (byte)i1;
                        if(++k1 == c)
                        {
                            s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
                            abyte1 = new byte[c];
                            k1 = 0;
                        }
                    } while(true);
                    if(k1 < c)
                        s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
                }
            }
            s3 = s3.trim();
            msg=s3;
            //billing.writeDebug((new StringBuilder()).append("Request response : ").append(s3).toString());
            
            
        }catch(Exception ex){
        	System.out.println("Exception <sendRequest> : " + ex.getMessage());
        }
		
		return msg;
	}
	
	public String localTransactionTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        return simpledateformat.format(date);
    }
	
	public String simpleLocalTransactionTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyMMddHms");
        return simpledateformat.format(date);
    }
	
	public String getBindParameter( int count )
    {
   	 String tanda = "?";
   	 for( int v=1; v<count; v++ ) tanda += ",?";
   	 return tanda;
    }
	
	public String getTransactionNumber( long logId, String branchCode, int issuerId ){
		
		String transNumber = logId + 
				 	  	     branchCode + 
				 	  	     issuerId + 
				 	  	     this.simpleLocalTransactionTime();
		
		return transNumber;
		
	}
	
	 public String generateRemittanceTraceNumber(){
	        Calendar calendar = Calendar.getInstance();
	        Date date = calendar.getTime();
	        SimpleDateFormat simpledateformat = new SimpleDateFormat("MMddkkmmss");
	        return simpledateformat.format(date);
	 }
	 
	 public static Date addingMinutes( int minute ) throws ParseException{
	        
        Date newTime 							= new Date();
        newTime 								= DateUtils.addMinutes(newTime, minute);
        SimpleDateFormat simpledateformat 		= new SimpleDateFormat("yyyy-MM-dd H:m:s");
        
        return newTime;
		
	}
	 
	public String generateRandom( int len ){
		
	   String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   SecureRandom rnd = new SecureRandom();
		
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
		
	}
	
	public Date getDateTime() throws ParseException{
		
		Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        return simpledateformat.parse(simpledateformat.format(date));
				
	}
	
	public static byte[] hexToByte(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	
	public String stringTransactionTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMddHms");
        return simpledateformat.format(date);
    }
	
	public String generateFileName(){
		
		return generateRandom(7) + stringTransactionTime();
		
	}
	
}
