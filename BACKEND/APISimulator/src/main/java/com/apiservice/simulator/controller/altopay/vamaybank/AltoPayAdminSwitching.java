package com.apiservice.simulator.controller.altopay.vamaybank;

import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.apiservice.simulator.utility.JSON;

@RestController
@RequestMapping("/api")
public class AltoPayAdminSwitching {

	@CrossOrigin(origins="*")
	@RequestMapping(value="/inquiryRequest", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody String inquiryRequest( @RequestBody String jsonParam ) throws Exception{
		
		JSONObject joResult 		= new JSONObject();		
		JSONObject joResultLogin 	= new JSONObject();		
		JSONObject joParam 			= JSON.newJSONObject( jsonParam );
		
		if( JSON.get(joParam, "account_number").compareTo("11223344") == 0 ){
			
			joResult.put("result_code", "00");
			joResult.put("result_message", "Approved");
			joResult.put("result_ammount", 10000);			
			joResult.put("result_currency", "360");
			//joResult.put("result_reference_code", "7222810203");
			joResult.put("result_name", "PETER SUSANTO");
			
		}else if( JSON.get(joParam, "account_number").compareTo("44332211") == 0 ){
			
			joResult.put("result_code", "25");
			joResult.put("result_message", "Account number not found");
			
		}
		
		return joResult.toString();
		
	}
	
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/paymentRequest", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody String paymentRequest( @RequestBody String jsonParam ) throws Exception{
		
		JSONObject joResult 		= new JSONObject();		
		JSONObject joResultLogin 	= new JSONObject();		
		JSONObject joParam 			= JSON.newJSONObject( jsonParam );
		
		if( JSON.get(joParam, "account_number").compareTo("11223344") == 0 ){
			
			joResult.put("result_code", "00");
			joResult.put("result_message", "Approved");
			
		}else if( JSON.get(joParam, "account_number").compareTo("44332211") == 0 ){
			
			joResult.put("result_code", "88");
			joResult.put("result_message", "Bill already paid");
			
		}
		
		return joResult.toString();
		
	}
	
}
