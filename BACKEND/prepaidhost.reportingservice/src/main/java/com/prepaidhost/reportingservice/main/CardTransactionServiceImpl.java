package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.json.simple.JSONObject;

import com.prepaidhost.reportingservice.Service;
import com.prepaidhost.reportingservice.model.master.CardTransaction;

public class CardTransactionServiceImpl implements CardTransactionService  {
	
	private static CardTransactionServiceRepository ctr = new CardTransactionServiceRepository();

	public CardTransactionServiceImpl(){}
	
	public List<CardTransaction> getCardTransaction(){
		
		List<CardTransaction> lCardTransaction		= ctr.getCardTransaction();
		
		return lCardTransaction;
		
	}
	
	public JSONObject saveIssuerTransactionReport( long transactionId ){
		
		JSONObject joResult 		= new JSONObject();
		
		String spResult 			= ctr.callSPGenerateIssuerFeeReport(transactionId);
		
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Generate report issuer failed.");	
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMsg", arrSPResult[3]);
			
		}
		
		return joResult;
		
	}
	
	public JSONObject saveCardTransactionReport( long transactionId ){
		
		JSONObject joResult 		= new JSONObject();
		
		String spResult 			= ctr.callSPGenerateCardTransactionReport(transactionId);
		
		if( spResult.compareTo("") == 0 ){
			
			joResult.put("errCode", "-99");
			joResult.put("errMsg", "Generate card transaction report failed.");	
			
		}else{
			
			String[] arrSPResult	= spResult.split("\\|");
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMsg", arrSPResult[3]);
			
		}
		
		return joResult;
		
	}
	
}
