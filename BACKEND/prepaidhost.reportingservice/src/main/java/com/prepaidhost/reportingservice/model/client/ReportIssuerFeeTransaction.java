package com.prepaidhost.reportingservice.model.client;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"RPTIssuerFeeTransaction\"")
public class ReportIssuerFeeTransaction {

	@Id
	@Column(name="\"reportId\"")
	private long reportId;
	
	@Column(name="\"transactionTime\"")
	private Date transactionTime;
	
	@Column(name="\"merchantCode\"")
	private String merchantCode;
	
	@Column(name="\"merchantName\"")
	private String merchantName;
	
	@Column(name="\"terminalCode\"")
	private String terminalCode;
	
	private double amount;
	private double fee;
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public Date getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
}
