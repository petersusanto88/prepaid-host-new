package com.prepaidhost.reportingservice.main;

import com.prepaidhost.reportingservice.model.master.Issuer;

public interface IssuerService {

	public Issuer getIssuerDBInfo( long issuerId );
	
}
