package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.hibernate.SessionFactory;

import com.prepaidhost.reportingservice.model.client.ReportIssuerFeeTransaction;

public class ReportIssuerServiceImpl implements ReportIssuerService {
	
	private static ReportIssuerServiceRepository reportRepo		= new ReportIssuerServiceRepository();

	public ReportIssuerServiceImpl(){
		
	}
	
	public List<ReportIssuerFeeTransaction> getReportIssuerFee( SessionFactory sessionFactClient ){
		return reportRepo.getReportIssuerFee(sessionFactClient);
	}
	
}
