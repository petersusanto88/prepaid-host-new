package com.prepaidhost.reportingservice.dbconfig;

import javax.imageio.spi.ServiceRegistry;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.prepaidhost.reportingservice.model.client.ReportIssuerFeeTransaction;
import com.prepaidhost.reportingservice.model.master.CardTransaction;
import com.prepaidhost.reportingservice.model.master.Issuer;

public class SetSessionFactory {

	private static String hostDB;
	private static String usernameDB;
	private static String passwordDB;
	private static int portDB;
	private static String nameDB;
	
	public SetSessionFactory(){}
	
	public static SessionFactory setSession( String host, String username, String password, int portDB, String nameDB, short type){
		
		String connectionURL		= "jdbc:postgresql://" + host + ":" + portDB + "/" + nameDB;
		SessionFactory sessionFact 	= null;
		
		try{
			
			Configuration conf = new Configuration();
			
			conf.configure("hibernate-annotation.cfg.xml");
			conf.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
			conf.setProperty("hibernate.current_session_context_class", "thread");
			conf.setProperty("hibernate.connection.url", connectionURL);
			conf.setProperty("hibernate.connection.username", username);
			conf.setProperty("hibernate.connection.password", password);
			conf.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
			
			
						
			if( type == 1 ){ // master				
				conf.addAnnotatedClass(CardTransaction.class);			
				conf.addAnnotatedClass(Issuer.class);
			}else if( type == 2 ){ // client				
				conf.addAnnotatedClass(ReportIssuerFeeTransaction.class);
			}
			
			
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder()
																		.applySettings(conf.getProperties());
			
			sessionFact							  				  = conf.buildSessionFactory(serviceRegistryBuilder.build());
			
			
			
		}catch( Throwable ex ){
			throw new ExceptionInInitializerError(ex);
		}
		
		return sessionFact;
		
	}
	
}
