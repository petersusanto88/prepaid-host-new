package com.prepaidhost.reportingservice.main;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.prepaidhost.reportingservice.Service;
import com.prepaidhost.reportingservice.model.master.Issuer;

public class IssuerServiceRepository {

	public IssuerServiceRepository(){}
	
	public Issuer getIssuerDBInfo( long issuerId ){
		
		Service.setSessionHibernateMaster();
		Session session 					= Service.masterSessionFactory.getCurrentSession();
		Issuer issuer 						= null;
		
		Transaction tx 						= session.beginTransaction();
		Query q 							= session.createQuery("from Issuer where issuerId = :issuerId");
		q.setLong("issuerId", issuerId);
		
		try{
			
			issuer 							= (Issuer)q.uniqueResult();
			
		}catch( Exception ex ){
			//tx.rollback();
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return issuer;
	}
	
}
