package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.prepaidhost.reportingservice.Service;
import com.prepaidhost.reportingservice.model.master.CardTransaction;
import com.prepaidhost.reportingservice.util.HibernateUtil;

public class CardTransactionServiceRepository {

	public CardTransactionServiceRepository(){}
	
	public List<CardTransaction> getCardTransaction(){
		
		Service.setSessionHibernateMaster();
		Session session 					= Service.masterSessionFactory.getCurrentSession();
		List<CardTransaction> transList 	= null;
		
		Transaction tx 						= session.beginTransaction();
		Query q 							= session.createQuery(" from CardTransaction where isReported = 0 ");
		
		try{
			
			transList						= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return transList;
		
	}
	
	public String callSPGenerateIssuerFeeReport( long transactionId ){
		
		Service.setSessionHibernateMaster();
		Session session 					= Service.masterSessionFactory.getCurrentSession();
		Transaction tx 						= session.beginTransaction();
		String spResult 					= "";
		String sql 							= " SELECT sp_generate_issuer_fee_report(:transactionId, :aeskey)";
		
		try{
			
			SQLQuery q						= (SQLQuery)session.createSQLQuery(sql)
												.setParameter("transactionId", transactionId)
												.setParameter("aeskey", Service.aesKey);
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[ReportingServiceThread] Exception<callSPGenerateIssuerFeeReport> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			tx.commit();
			
		}
		
		Service.writeDebug("[ReportingServiceThread] callSPGenerateIssuerFeeReport Result : " + spResult + " ###");
		
		return spResult;
		
	}
	
	public String callSPGenerateCardTransactionReport( long transactionId ){
		
		Service.setSessionHibernateMaster();
		Session session 					= Service.masterSessionFactory.getCurrentSession();
		Transaction tx 						= session.beginTransaction();
		String spResult 					= "";
		String sql 							= " SELECT sp_generate_card_transaction_report(:transactionId, :aeskey)";
		
		try{
			
			SQLQuery q						= (SQLQuery)session.createSQLQuery(sql)
												.setParameter("transactionId", transactionId)
												.setParameter("aeskey", Service.aesKey);
			
			List lQResult 					= q.list();
			
			spResult 						= lQResult.get(0).toString();
			
			tx.commit();
			
		}catch( Exception ex ){
			
			Service.writeDebug("[ReportingServiceThread] Exception<callSPGenerateCardTransactionReport> : " + ex.getMessage());
			tx.rollback();
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }
			
			
			
		}
		
		Service.writeDebug("[ReportingServiceThread] callSPGenerateCardTransactionReport Result : " + spResult + " ###");
		
		return spResult;
		
	}
	
}
