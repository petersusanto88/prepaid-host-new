package com.prepaidhost.reportingservice.main;

import com.prepaidhost.reportingservice.model.master.Issuer;

public class IssuerServiceImpl implements IssuerService {

	private static IssuerServiceRepository issuerRepo		= new IssuerServiceRepository();
	
	public IssuerServiceImpl(){}
	
	public Issuer getIssuerDBInfo( long issuerId ){
		
		return issuerRepo.getIssuerDBInfo(issuerId);
		
	}
	
}
