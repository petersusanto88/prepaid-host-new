package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.prepaidhost.reportingservice.model.client.ReportIssuerFeeTransaction;

public class ReportIssuerServiceRepository {

	public ReportIssuerServiceRepository(){}
	
	public List<ReportIssuerFeeTransaction> getReportIssuerFee( SessionFactory sessionFactClient ){
		
		Session session 							= sessionFactClient.getCurrentSession();
		List<ReportIssuerFeeTransaction> lReport	= null;
		Transaction tx 							 	= session.beginTransaction();
		Query q 									= session.createQuery( "from ReportIssuerFeeTransaction" );
		
		try{
			
			lReport 								= q.list();
			tx.commit();
			
		}catch( Exception ex ){
			//tx.rollback();
			System.out.println("Exception : " + ex.getMessage());
		}finally{
			if( session != null && session.isOpen() ){
				session.close();
			}
		}
		
		return lReport;
		
	}
	
}
