package com.prepaidhost.reportingservice;

import java.util.List;

import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;

import com.prepaidhost.reportingservice.dbconfig.SetSessionFactory;
import com.prepaidhost.reportingservice.main.ReportIssuerServiceImpl;
import com.prepaidhost.reportingservice.main.ReportIssuerServiceRepository;
import com.prepaidhost.reportingservice.model.client.ReportIssuerFeeTransaction;
import com.prepaidhost.reportingservice.model.master.CardTransaction;
import com.prepaidhost.reportingservice.model.master.Issuer;
import com.prepaidhost.reportingservice.util.JSON;


/**
 * @author PETER SUSANTO
 * @version 1.0
 * Class thread ini berfungsi untuk memproses data menjadi reporting yang diinginkan. Ada beberapa report yang dibuat : <br>
 * <ul>
 * 	<li>Card Transaction</li>
 *  <li>Issue Fee</li>
 *  <li>Acceptance Fee </li>
 * </ul>
 */
public class ServicePrepaidhostReportingThread{
	
	/*private static SessionFactory sessionFactClient 		= null;
	private static ReportIssuerServiceImpl serviceImpl 		= new ReportIssuerServiceImpl();*/

	public void run(){
		
		try{
			
			//if( !this.stop ){
				
				/*Set connection to Master DB*/
				List<CardTransaction> lCardTransaction		= Service.cts.getCardTransaction();
				
				if( lCardTransaction.size() > 0 ){
					/*1. Generate Issuer & Acceptance Report Transaction*/
					Service.writeDebug("************************************************");
					Service.writeDebug("*       Generate Issuer & Acceptance Report    *");
					Service.writeDebug("************************************************");
					
					for( CardTransaction ct : lCardTransaction ){
						
						Service.writeDebug("================================================");
						Service.writeDebug("Transaction ID : " + ct.getTransactionId());
						
						JSONObject joIssuerReport		= Service.cts.saveIssuerTransactionReport(ct.getTransactionId());
						
						
						
						Service.writeDebug("SP Result Code : " + JSON.get(joIssuerReport, "errCode"));
						Service.writeDebug("SP Result Msg : " + JSON.get(joIssuerReport, "errMsg"));
						Service.writeDebug("================================================");
						
						/*Example for multiple connection database
						 * Jika akan digunakan untuk connection ke db client di level aplikasi*/
						/*
						 * Issuer issuer 		= Service.isi.getIssuerDBInfo(ct.getIssuerId());
						 * sessionFactClient	= Service.sf.setSession(issuer.getDbHost(), 
																	issuer.getDbUsername(), 
																	issuer.getDbPassword(), 
																	issuer.getDbPort(), 
																	issuer.getDbReportName(),
																	(short)2 );
						
						List<ReportIssuerFeeTransaction> lReport		= serviceImpl.getReportIssuerFee(sessionFactClient);*/						
						
						
					}
					
					/*2. Generate Card Transaction Report*/
					Service.writeDebug("************************************************");
					Service.writeDebug("*       Generate Card Report Transaction       *");
					Service.writeDebug("************************************************");
					
					for( CardTransaction ct : lCardTransaction ){
						
						JSONObject joIssuerReport		= Service.cts.saveCardTransactionReport(ct.getTransactionId());
						
						
						Service.writeDebug("================================================");
						Service.writeDebug("Transaction ID : " + ct.getTransactionId());
						Service.writeDebug("SP Result Code : " + JSON.get(joIssuerReport, "errCode"));
						Service.writeDebug("SP Result Msg : " + JSON.get(joIssuerReport, "errMsg"));
						Service.writeDebug("================================================");
						
					}
				
				}
					
				//Service.smrt.interrupt();
				
			//}
			
		}catch( Exception ex ){
			Service.writeDebug("[Service Mezzo Reporting] Exception : " + ex.getMessage());
		}
		
	}
	
	/*volatile boolean stop;
	final ServiceMezzoReportingThread this$0;
	private int stat;
	
	public ServiceMezzoReportingThread(int status)
	{
		super();
        this$0 = ServiceMezzoReportingThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("[Service Mezzo Reporting] Exception : " + ex.toString());
        }
	    
	}*/
	
}
