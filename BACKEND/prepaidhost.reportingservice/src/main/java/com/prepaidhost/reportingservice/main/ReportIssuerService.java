package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.hibernate.SessionFactory;

import com.prepaidhost.reportingservice.model.client.ReportIssuerFeeTransaction;

public interface ReportIssuerService {

	public List<ReportIssuerFeeTransaction> getReportIssuerFee( SessionFactory sessionFactClient );
	
}
