package com.prepaidhost.reportingservice.model.master;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TRCardTransactions\"")
public class CardTransaction {

	@Id
	@Column(name="\"transactionId\"")
	private long transactionId;
	
	@Column(name="\"cardId\"")
	private long cardId;
	
	@Column(name="\"cardNumber\"")
	private String cardNumber;
	
	@Column(name="\"issuerId\"")
	private long issuerId;
	
	@Column(name="\"acquirerId\"")
	private long acquirerId;
	
	@Column(name="\"merchantId\"")
	private long merchantId;
	
	@Column(name="\"terminalId\"")
	private long terminalId;
	
	@Column(name="\"transactionDate\"")
	private Date transactionDate;
	
	@Column(name="\"transactionCode\"")
	private String transactionCode;
	
	private double amount;
	
	@Column(name="\"needSettlement\"")
	private short needSettlement;
	
	@Column(name="\"isSettled\"")
	private short isSettled;
	
	@Column(name="\"isReported\"")
	private short isReported;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public long getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(long issuerId) {
		this.issuerId = issuerId;
	}

	public long getAcquirerId() {
		return acquirerId;
	}

	public void setAcquirerId(long acquirerId) {
		this.acquirerId = acquirerId;
	}

	public long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(long merchantId) {
		this.merchantId = merchantId;
	}

	public long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public short getNeedSettlement() {
		return needSettlement;
	}

	public void setNeedSettlement(short needSettlement) {
		this.needSettlement = needSettlement;
	}

	public short getIsSettled() {
		return isSettled;
	}

	public void setIsSettled(short isSettled) {
		this.isSettled = isSettled;
	}

	public short getIsReported() {
		return isReported;
	}

	public void setIsReported(short isReported) {
		this.isReported = isReported;
	}
	
	
}
