package com.prepaidhost.reportingservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Properties;

import org.hibernate.SessionFactory;

import com.prepaidhost.reportingservice.dbconfig.SetSessionFactory;
import com.prepaidhost.reportingservice.main.CardTransactionService;
import com.prepaidhost.reportingservice.main.CardTransactionServiceImpl;
import com.prepaidhost.reportingservice.main.IssuerService;
import com.prepaidhost.reportingservice.main.IssuerServiceImpl;
import com.prepaidhost.reportingservice.util.MezzoUtil;


/**
 * @author PETER SUSANTO
 * @version 1.0
 * Class ini merupakan main class. Pada constructor, akan dipanggil thread ServicePrepaidhostReportingThread sebagai
 * thread yang akan menjalankan proses pembuatan data reporting.
 */
public class Service {
	
	private static String xmlconfig 					= "";
	private static Properties props 					= null;
	public static int timeout 							= 0;
    public static int interval 							= 1;
    private static boolean debug 						= false;
    private static FileOutputStream fos 				= null;
    private static String fileDate 						= "";
    private static String debugFilename 				= "";
    public static OutputStream out;
    public static boolean ReceiveResponse 				= true;
    private static boolean hasKWP 						= false;
    public static MezzoUtil util 						= new MezzoUtil();
    public static CardTransactionService cts 			= new CardTransactionServiceImpl();
    public static IssuerService isi 					= new IssuerServiceImpl(); 
    
    public static String hostDBMaster 					= "";
    public static int portDBMaster						= 0;
    public static String nameDBMaster 					= "";
    public static String userDBMaster 					= "";
    public static String passwordDBMaster 				= "";
    
    public static SetSessionFactory sf 				 	= null;    
    public static ServicePrepaidhostReportingThread smrt;    
    
    //Localhost
    //String config 										= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_reporting.ini";
    
    //Development
    String config 										= "config_service_reporting.ini";
    
    public static SessionFactory masterSessionFactory	= null;

    public static String aesKey 						= "";
    
	public static void main( String[] args ) throws IOException, InterruptedException
    {
		Service service = new Service(args);
    }
	
	public Service(String as[]) throws IOException, InterruptedException{
		
		try
        {		
			
			getConfiguration(config);			
			fileDate				= util.currentDate();
			debug 					= true;			
			debugFilename			= props.getProperty("debugFilename");
			interval				= Integer.parseInt( props.getProperty("interval") );
			
			hostDBMaster 			= props.getProperty("hostDBMaster");
			portDBMaster 			= Integer.parseInt( props.getProperty("portDBMaster") );
			nameDBMaster 			= props.getProperty("nameDBMaster");
			userDBMaster 			= props.getProperty("userDBMaster");
			passwordDBMaster 		= props.getProperty("passwordDBMaster");
			
			aesKey 					= props.getProperty("dbAESKey");
			
			sf						= new SetSessionFactory();
			
			openFileDebug();
			writeDebug("----------------------------------------------------------------");
            writeDebug((new StringBuilder()).append("MEZZO REPORT SERVICE, started at : ").append(util.currentDateTime()).toString());
            writeDebug("----------------------------------------------------------------");	
            
            //smrt 			= new ServiceMezzoReportingThread(1);
            //smrt.start();
            
            smrt			= new ServicePrepaidhostReportingThread();
            smrt.run();
            
            Service.masterSessionFactory.close();
            
        }catch(Exception exception)
        {
        	Service.writeDebug("[Mezzo Reporting Thread] Exception :  " + exception.toString());   	
        }
		
	}
	
	public static void writeDebug(String s)
    {
        try
        {
        	System.out.println(s);	
            if(out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
            if(debug)
            {
            	if(fileDate.compareTo(util.currentDate()) != 0)
                {
                    try
                    {
                    	
                        if(fos != null)
                            fos.close();
                    }
                    catch(Exception exception)
                    {
                        System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
                    }
                    fileDate = util.currentDate();
                    openFileDebug();
                }
                s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
                try
                {
                    fos.write(s.getBytes());
                }
                catch(IOException ioexception)
                {
                    System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
                }
            }
        }
        catch(Exception exception1) { }
    }
	public static void writeDebug(String s, int i)
    {
        try
        {
        	
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
	
	private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            System.out.println("debugfilename  " + s);
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
	
	private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                Service.writeDebug("Exception getConfiguration :  " + filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                Service.writeDebug("Exception getConfiguration :  " + ioexception);
                System.exit(1);
            }
        }
    }
	
	public static void setSessionHibernateMaster(){
		Service.masterSessionFactory 	= Service.sf.setSession(hostDBMaster, 
																userDBMaster, 
																passwordDBMaster, 
																portDBMaster, 
																nameDBMaster,
																(short)1);
	}
	
}
