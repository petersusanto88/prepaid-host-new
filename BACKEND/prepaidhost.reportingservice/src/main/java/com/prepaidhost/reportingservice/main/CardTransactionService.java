package com.prepaidhost.reportingservice.main;

import java.util.List;

import org.json.simple.JSONObject;

import com.prepaidhost.reportingservice.model.master.CardTransaction;


/**
 * @author PETER SUSANTO
 * <strong>Description</strong>:<br>
 * Class ini merupakan class interface yang terdiri dari method untuk generate reporting.
 */ 
public interface CardTransactionService {
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengambil data transaksi kartu yang memiliki flag belum diproses reporting
	 * @return
	 */
	public List<CardTransaction> getCardTransaction();
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengenerate laporan mengenai issuer fee dan acceptance fee
	 * @param transactionId
	 * @return
	 */
	public JSONObject saveIssuerTransactionReport( long transactionId );
	
	/**
	 * <strong>Description</strong>:<br>
	 * Method ini digunakan untuk mengenerate laporan mengenai transaksi kartu.
	 * @param transactionId
	 * @return
	 */
	public JSONObject saveCardTransactionReport( long transactionId );

}
