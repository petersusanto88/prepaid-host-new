package bbw.prepaidhost.fraudservice.main;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.simple.JSONObject;

import bbw.prepaidhost.fraudservice.Service;
import bbw.prepaidhost.fraudservice.model.SettlementData;

public class SettlementDataRepository {

	public SettlementDataRepository() {}
	
	public List<SettlementData> getSettlementDetail() {
		
		Session session 				= Service.masterSessionFactory.getCurrentSession();
		List<SettlementData> lData 		= null;
		Transaction tx					= session.beginTransaction();
		
		Query q							= session.createQuery(" from SettlementData where isProceedByFraudDetector = 0 order by logDetailId desc ");
		
		try {
			
			lData 						= q.list();		
			tx.commit();
			
		}catch( Exception ex ) {
			
		}finally {
			if( session != null && session.isOpen() ) {
				Service.writeDebug("### Close Connection DB", "");
				session.close();
				Service.masterSessionFactory.close();
			}
		}
		 
		return lData;
		
	}
	
	public String callSPFraudDetection( String cardId,
										String terminalId,
										String merchantId,
										String transactionCodeOnCard,
										String amount,
										String lastBalance,
										String transDateTime,
										String indexIssuer,
										String cardType,
										String aesKey
										) {
		
		String spResult 				= "";
		
		Session session					= Service.masterSessionFactory.getCurrentSession();
		Transaction tx 					= session.beginTransaction();
		
		String sqlQuery 				= " SELECT sp_fraud_detection( :vCardId,"
										+ " 						   :vTerminalId,"
										+ "							   :vMerchantId,"
										+ "							   :vTranscodeOnCard,"
										+ "							   :vAmount,"
										+ "							   :vLastBalance,"
										+ "							   :vTransDateTime,"
										+ "							   :vIndexIssuer,"
										+ "							   :vCardType,"
										+ "							   :vAESKey) ";
		
		try {
			
			SQLQuery q 					= (SQLQuery)session.createSQLQuery(sqlQuery)
											.setParameter("vCardId", cardId)
											.setParameter("vTerminalId", terminalId)
											.setParameter("vMerchantId", merchantId)
											.setParameter("vTranscodeOnCard", transactionCodeOnCard)
											.setParameter("vAmount", amount)
											.setParameter("vLastBalance", lastBalance)
											.setParameter("vTransDateTime", transDateTime)
											.setParameter("vIndexIssuer", indexIssuer)
											.setParameter("vCardType", cardType)
											.setParameter("vAESKey",aesKey);
			
			List lQResult				= q.list();
			spResult 					= lQResult.get(0).toString();
			
			tx.commit();
			
		}catch( Exception ex ){
			Service.writeDebug("Exception<callSPFraudDetection> : " + ex.getMessage(), "SettlementDataRepository");
		}finally{
			
			if (session != null && session.isOpen()) {
				session.close();
		    }	
			
		}
		
		return spResult;
		
	}
	
	public JSONObject updateStatusFraudDetector( long logDetailId ) {
		
		int resultUpdate 			= 0;
		JSONObject joResult 		= new JSONObject();
		Session session 			= Service.masterSessionFactory.getCurrentSession();
		Transaction tx 				= session.beginTransaction();
		Query q 					= session.createQuery( " update SettlementData "
														 + " set isProceedByFraudDetector = 1 "
														 + " where logDetailId = :id " );
		
		try {
			
			q.setLong("id",logDetailId);
			resultUpdate 			= q.executeUpdate();
			tx.commit();
			
			if( resultUpdate == 1 ){
				
				joResult.put("errCode", "00");
				joResult.put("errMsg", "Update transaction status success");
				
			}else{
				
				joResult.put("errCode", "109");
				joResult.put("errMsg", "Update transaction status failed");
				
			}
			
		}catch( Exception ex ){
			joResult.put("errCode", "109");
			joResult.put("errMsg", "Update transaction status failed. Error : " + ex.getMessage());
			Service.writeDebug("Exception <updateStatusFraudDetector> : " + ex.getMessage(), "SettlementDataRepository");
			tx.rollback();
		}finally{
			if (session != null && session.isOpen()) {
				session.close();
		    }
		}
		
		return joResult;
		
	}
	
}
