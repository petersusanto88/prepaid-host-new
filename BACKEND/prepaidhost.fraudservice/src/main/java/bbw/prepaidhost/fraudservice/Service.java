package bbw.prepaidhost.fraudservice;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.solab.iso8583.IsoMessage;

import bbw.prepaidhost.fraudservice.dbconfig.SetSessionFactory;
import bbw.prepaidhost.fraudservice.util.PrepaidhostUtil;

public class Service {
	
	private static Properties props 					= null;
	public static int timeout 							= 0;
    public static int interval 							= 1;
    private static boolean debug 						= false;
    private static FileOutputStream fos 				= null;
    private static String fileDate 						= "";
    private static String debugFilename 				= "";
    public static OutputStream out;
    
    public static PrepaidhostUtil util 					= new PrepaidhostUtil();
    
    public static String hostDBMaster 					= "";
    public static int portDBMaster						= 0;
    public static String nameDBMaster 					= "";
    public static String userDBMaster 					= "";
    public static String passwordDBMaster 				= "";
    
    public static SetSessionFactory sf 				 	= null;   
    String config 										= "D:\\BBW\\AG Prepaid Host\\JAR\\config_service_fraud.ini";
    public static SessionFactory masterSessionFactory	= null;

    public static String aesKey 						= "";    
    private static String debugLabel 					= "Service";
    
    private static ServicePrepaidhostFraudThread spft	= null;

	public static void main( String[] args )
    {
		Service service = new Service(args);
    }
	
	public Service(String as[]){
		
		try
        {
			
			getConfiguration(config);			
			fileDate				= util.currentDate();
			debug 					= true;			
			debugFilename			= props.getProperty("debugFilename");
			interval				= Integer.parseInt( props.getProperty("interval") );
			
			hostDBMaster 			= props.getProperty("hostDBMaster");
			portDBMaster 			= Integer.parseInt( props.getProperty("portDBMaster") );
			nameDBMaster 			= props.getProperty("nameDBMaster");
			userDBMaster 			= props.getProperty("userDBMaster");
			passwordDBMaster 		= props.getProperty("passwordDBMaster");			
			aesKey 					= props.getProperty("dbAESKey");			
			sf						= new SetSessionFactory();
			
			setSessionHibernateMaster();
			
			openFileDebug();
			writeDebug("----------------------------------------------------------------", debugLabel);
            writeDebug((new StringBuilder()).append("PREPAIDHOST REPORT SERVICE, started at : ").append(util.currentDateTime()).toString(), debugLabel);
            writeDebug("----------------------------------------------------------------", debugLabel);
            
            spft 					= new ServicePrepaidhostFraudThread(1);
            spft.run();
			
        }catch(Exception exception)
        {
        	Service.writeDebug("Exception :  " + exception.toString(), debugLabel);   	
        }
		
	}
	

    
    public static void writeDebug(String s, int i)
    {
        try
        {
            System.out.println(s);
            if(i == 0 && out != null)
                out.write((new StringBuilder()).append(s).append("\r\n").toString().getBytes());
        }
        catch(Exception exception) { }
    }
    
    private static void openFileDebug()
    {
        try
        {
            int i = debugFilename.lastIndexOf(".");
            String s = (new StringBuilder()).append(debugFilename.substring(0, i)).append(fileDate).append(debugFilename.substring(i)).toString();
            System.out.println("debugfilename  " + s);
            fos = new FileOutputStream(s, true);
        }
        catch(IOException ioexception)
        {
            System.out.println((new StringBuilder()).append("Open File for Debug Log error : ").append(ioexception.toString()).toString());
        }
    }
    
    public static void writeDebug(String s, String label){
		try{
			
			System.out.println("[" + label + "] " + s);
			if(out != null)
				out.write((new StringBuilder()).append("[" + label + "] ").append(s).append("\r\n").toString().getBytes());
		
		
			if(fileDate.compareTo(util.currentDate()) != 0){
				try{
					if(fos != null)
					  fos.close();
				}catch(Exception exception){
					System.out.println((new StringBuilder()).append("open debug file error : ").append(exception.toString()).toString());
				}
				fileDate = util.currentDate();
				openFileDebug();
			}
			s = (new StringBuilder()).append(util.currentDateTime()).append(" : ").append(s).append("\r\n").toString();
			try{
				fos.write(s.getBytes());
			}
			catch(IOException ioexception){
				System.out.println((new StringBuilder()).append("Error writing to debug file : ").append(ioexception.toString()).toString());
			}
		
		
		}catch(Exception exception1) { }
	}
    
    private void getConfiguration(String s)
    {
        if(s != null)
        {
            File file = new File(s);
            try
            {
                FileInputStream fileinputstream = new FileInputStream(file);
                props = new Properties();
                props.load(fileinputstream);
                fileinputstream.close();
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
                System.exit(1);
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
                System.exit(1);
            }
        }
    }
    
    public static void sendISO( Socket socket, IsoMessage isomessage, String section )
	{
		try{
			
			sendISONew( new String( isomessage.writeData() ), socket, section );	
			
		}catch (Exception e) {  }
	}
	
	
	 public static String sendISONew( String message,Socket s, String section ){
	     String responseServer="";
	     
	     OutputStream outToServer = null;
	     DataOutputStream out = null;
	     
	     try
           {
      
		      String mti=message.substring(0,4);
		       
		      outToServer = s.getOutputStream();
		      out = new DataOutputStream(outToServer);
		      
		      writeDebug("Send ("+mti+") : " + message, debugLabel);
		      out.writeUTF(message);
          }catch(IOException e)
          {
           writeDebug( "IOException.services.send() "+e.getMessage(), debugLabel);
          }finally {
			
          }
	          
	     return responseServer;
	 }
	
	public static void setSessionHibernateMaster(){
		Service.masterSessionFactory 	= Service.sf.setSession(hostDBMaster, 
																userDBMaster, 
																passwordDBMaster, 
																portDBMaster, 
																nameDBMaster,
																(short)1);
	}
}
