package bbw.prepaidhost.fraudservice.main;

import java.util.List;

import org.json.simple.JSONObject;

import bbw.prepaidhost.fraudservice.Service;
import bbw.prepaidhost.fraudservice.model.SettlementData;

public class SettlementDataServiceImpl implements SettlementDataService{

	public SettlementDataServiceImpl() {}
	
	private SettlementDataRepository settlementRepo = new SettlementDataRepository();
	
	private static String debugLabel = "SettlementDataServiceImpl";
	
	public List<SettlementData> getTransactionFromSettlementData(){
		
		return settlementRepo.getSettlementDetail();
		
	}
	
	public JSONObject updateStatusFraudDetector( long logDetailId ) {
		
		return settlementRepo.updateStatusFraudDetector(logDetailId);
		
	}
	
	public JSONObject fraudDetector( String cardId,
									 String terminalId,
									 String merchantId,
									 String transactionCodeOnCard,
									 String amount,
									 String lastBalance,
									 String transDateTime,
									 String indexIssuer,
									 String cardType,
									 String aesKey ) {
		
		JSONObject joResult 		= new JSONObject();

		String spResult 			= settlementRepo.callSPFraudDetection(cardId, terminalId, merchantId, transactionCodeOnCard, amount, lastBalance, transDateTime, indexIssuer, cardType, aesKey);
		
		Service.writeDebug("JO Result : " + spResult, debugLabel);
		
		String[] arrSPResult 		= spResult.split("\\|");
		
		if( arrSPResult.length == 7 ) {
			
			joResult.put("errCode", arrSPResult[0]);
			joResult.put("errMsg", arrSPResult[1]);
			joResult.put("sqlStatus", arrSPResult[2]);
			joResult.put("sqlMessage", arrSPResult[3]);
			joResult.put("indicatorFraud1", arrSPResult[4]);
			joResult.put("indicatorFraud2", arrSPResult[5]);
			joResult.put("indicatorFraud3", arrSPResult[6]);
			
		}
		
		
		return joResult;
		
	}
	
}
