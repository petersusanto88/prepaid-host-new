package bbw.prepaidhost.fraudservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="public.\"TRUploadSettlementLogsDetail\"")
public class SettlementData {

	@Id
	@Column(name="\"logDetailId\"")
	private long logDetailId;
	
	@Column(name="\"cardId\"")
	private String cardId;
	
	@Column(name="\"samId\"")
	private String samId;
	
	@Column(name="\"terminalId\"")
	private String terminalId;
	
	@Column(name="\"merchantId\"")
	private String merchantId;
	
	@Column(name="\"transCode\"")
	private String transCode;
	
	private String rfu;	
	private String amount;
	
	@Column(name="\"lastBalance\"")
	private String lastBalance;
	
	@Column(name="\"dateTime\"")
	private String dateTime;
	
	@Column(name="\"counterSam\"")
	private String counterSam;
	
	@Column(name="\"counterCard\"")
	private String counterCard;
	
	@Column(name="\"indexIssuer\"")
	private String indexIssuer;
	
	@Column(name="\"cardType\"")
	private String cardType;
	
	@Column(name="\"statusWord\"")
	private String statusWord;
	
	private String uid;
	
	@Column(name="\"transactionId\"")
	private String transactionId;
	
	@Column(name="\"isProceedByFraudDetector\"")
	private Short isProceedByFraudDetector;

	public long getLogDetailId() {
		return logDetailId;
	}

	public void setLogDetailId(long logDetailId) {
		this.logDetailId = logDetailId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getSamId() {
		return samId;
	}

	public void setSamId(String samId) {
		this.samId = samId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public String getRfu() {
		return rfu;
	}

	public void setRfu(String rfu) {
		this.rfu = rfu;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getLastBalance() {
		return lastBalance;
	}

	public void setLastBalance(String lastBalance) {
		this.lastBalance = lastBalance;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCounterSam() {
		return counterSam;
	}

	public void setCounterSam(String counterSam) {
		this.counterSam = counterSam;
	}

	public String getCounterCard() {
		return counterCard;
	}

	public void setCounterCard(String counterCard) {
		this.counterCard = counterCard;
	}

	public String getIndexIssuer() {
		return indexIssuer;
	}

	public void setIndexIssuer(String indexIssuer) {
		this.indexIssuer = indexIssuer;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getStatusWord() {
		return statusWord;
	}

	public void setStatusWord(String statusWord) {
		this.statusWord = statusWord;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Short getIsProceedByFraudDetector() {
		return isProceedByFraudDetector;
	}

	public void setIsProceedByFraudDetector(Short isProceedByFraudDetector) {
		this.isProceedByFraudDetector = isProceedByFraudDetector;
	}
	
	
	
}
