package bbw.prepaidhost.fraudservice;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.json.simple.JSONObject;

import bbw.prepaidhost.fraudservice.main.SettlementDataService;
import bbw.prepaidhost.fraudservice.main.SettlementDataServiceImpl;
import bbw.prepaidhost.fraudservice.model.SettlementData;

public class ServicePrepaidhostFraudThread extends Thread {
	
	private static String debugLabel = "ServicePrepaidhostFraudThread";
	private static SettlementDataService settlementDataService = new SettlementDataServiceImpl();
	private static int loop = 0;
	
	static {
		DOMConfigurator.configure("log4j.xml");
	}
	
	private static Logger logger = Logger.getLogger(ServicePrepaidhostFraudThread.class);

	
	public void run() {

		try{
		
			if( !this.stop ) {
				
				Service.writeDebug("Starting Fraud Service Thread...", debugLabel);
				logger.info("Starting Fraud Service Thread...");
				
				while(true){				
					
					Thread.sleep(Service.interval * 1000);
					
					List<SettlementData> lSettlement	= settlementDataService.getTransactionFromSettlementData();
					
					for( SettlementData settlementData : lSettlement ) {
						
						/*Service.writeDebug("-----------------------------------------------", debugLabel);
						Service.writeDebug("Get Settlement Offline Data", debugLabel);
						Service.writeDebug("-----------------------------------------------", debugLabel);*/
						Service.writeDebug(">>> Start Fraud Detecting...", debugLabel);
						
						Service.writeDebug("Log Id : " + settlementData.getLogDetailId(), debugLabel);
						Service.writeDebug("Card Id : " + settlementData.getCardId(), debugLabel);
						Service.writeDebug("Terminal Id : " + settlementData.getTerminalId(), debugLabel);
						Service.writeDebug("Merchant Id : " + settlementData.getMerchantId(), debugLabel);
						Service.writeDebug("Trans date time : " + settlementData.getDateTime(), debugLabel);
						
						
						JSONObject joFraudDetector		= settlementDataService.fraudDetector(settlementData.getCardId(), 
																							  settlementData.getTerminalId(), 
																							  settlementData.getMerchantId(), 
																							  settlementData.getTransCode(), 
																							  settlementData.getAmount(), 
																							  settlementData.getLastBalance(), 
																							  settlementData.getDateTime(), 
																							  settlementData.getIndexIssuer(), 
																							  settlementData.getCardType(), 
																							  Service.aesKey);
						
						Service.writeDebug("Fraud Detector Result : " + joFraudDetector.toString(), debugLabel);
						
						settlementDataService.updateStatusFraudDetector(settlementData.getLogDetailId());
						
					}
					
				}
				
			}
			
		}catch( Exception ex ){
			Service.writeDebug("Exception : " + ex.getMessage(), debugLabel);
		}
		
	}
	
	volatile boolean stop;
	final ServicePrepaidhostFraudThread this$0;
	private int stat;
	
	public ServicePrepaidhostFraudThread(int status)
	{
		super();
        this$0 = ServicePrepaidhostFraudThread.this;
        stop = false;
        try
        {
        	stat=status;
        	
        }catch(Exception ex){
        	Service.writeDebug("Exception : " + ex.toString(), debugLabel);
        }
	    
	}
	
}
