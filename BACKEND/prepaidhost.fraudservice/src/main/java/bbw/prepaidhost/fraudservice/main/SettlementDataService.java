package bbw.prepaidhost.fraudservice.main;

import java.util.List;

import org.json.simple.JSONObject;

import bbw.prepaidhost.fraudservice.model.SettlementData;

public interface SettlementDataService {

	public List<SettlementData> getTransactionFromSettlementData();
	public JSONObject updateStatusFraudDetector( long logDetailId );
	public JSONObject fraudDetector( String cardId,
									 String terminalId,
									 String merchantId,
									 String transactionCodeOnCard,
									 String amount,
									 String lastBalance,
									 String transDateTime,
									 String indexIssuer,
									 String cardType,
									 String aesKey );
	
}
