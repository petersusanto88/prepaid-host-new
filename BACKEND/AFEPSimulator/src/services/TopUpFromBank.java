package services;

import java.net.Socket;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;

import utility.Utility;

public class TopUpFromBank implements Constants {

	private Socket socket;
	private MessageFactory mfact;
	private Utility util = new Utility();
	
	public TopUpFromBank( Socket socket, MessageFactory messagefactory ){
		
		this.socket = socket;
		this.mfact  = messagefactory;
		
	}
	
	private String generateField48( String fromAccountNumber,
									String toAccountNumber,
									String topupAmount,
									String lastBalance,
									String topUpPendingBalance,
									String cardBalance){
		
		String result			= "";
		String refNumber 		= multiSocket.util.randomAlphaNumeric(18);
		
		for( int i = fromAccountNumber.length() + 1; i <= 19; i++ ){
			fromAccountNumber 			= ( new StringBuilder() ).append(fromAccountNumber).append(" ").toString();
		}
		
		for( int i = toAccountNumber.length() + 1; i <= 19; i++ ){
			toAccountNumber 			= ( new StringBuilder() ).append(toAccountNumber).append(" ").toString();
		}
		
		for( int i = topupAmount.length() + 1; i <= 12; i++ ){
			topupAmount 			= ( new StringBuilder() ).append("0").append(topupAmount).toString();
		}
		
		for( int i = lastBalance.length() + 1; i <= 12; i++ ){
			lastBalance 			= ( new StringBuilder() ).append("0").append(lastBalance).toString();
		}
		
		for( int i = topUpPendingBalance.length() + 1; i <= 12; i++ ){
			topUpPendingBalance 			= ( new StringBuilder() ).append("0").append(topUpPendingBalance).toString();
		}
		
		for( int i = cardBalance.length() + 1; i <= 12; i++ ){
			cardBalance 			= ( new StringBuilder() ).append("0").append(cardBalance).toString();
		}
		
		result 						= ( new StringBuilder() ).append(refNumber)
											.append(fromAccountNumber)
											.append(toAccountNumber)
											.append(topupAmount)
											.append(lastBalance)
											.append(topUpPendingBalance)
											.append(cardBalance).toString();
		
		return result;
		
	}
	
	public String generateField57TopupFromATM( String cardNumber,
											   String amount, 
											   String currentBalance,
											   String cardUID,
											   String cardType){
    	
		//Format : 
		//TXN_ID;TXN_TOKEN;TXN_TYPE;TXN_CHANNEL;TID;MID;CARD_ID;KEY_INDEX;CARD_TYPE;AMOUNT TRANSACTION;CARD_BALANCE
		
		String field57Topup	= "";		
		
		field57Topup 		= "100317113047012345671122334401;" + //TXN_ID
							  "064500C30300409AC2667171FAFA39FA39921E9087A910208828903A1165A8AED5E132229342D51E2137BEF179EF89F5A7BBD78DB9B430BC3C6E36E8FCB80834B21958C7D4D0599000" + cardUID + ";" + //TXN_TOKEN
							  "TOPU;" + //TXN_TYPE
							  "ATM0;" + // TXN_CHANNEL
							  "01234567;" + // TERMINAL ID
							  "11223344;" + // MERCHANT ID
							  cardNumber + ";" +   // CARD ID
							  cardType + ";" + // CARD TYPE
							  amount + ";" + // AMOUNT
							  currentBalance; // LAST BALANCE
    			
    	return field57Topup;
    	
    }
	
	public String generateField90( String f7,
								   String f11,
								   String f32,
								   String f33,
								   String f37){
		
		String field90 			 = "";
		
		field90 				 = "0200";
		field90 				 = (new StringBuilder()).append(field90).append(f7).toString();
		field90 				 = (new StringBuilder()).append(field90).append(f11).toString();
		for( int i = f32.length() + 1; i <= 11; i++ ){
			f32 			= ( new StringBuilder() ).append("0").append(f32).toString();
		}
		for( int i = f33.length() + 1; i <= 11; i++ ){
			f33 			= ( new StringBuilder() ).append("0").append(f33).toString();
		}
		field90 				 = (new StringBuilder()).append(field90).append(f32).append(f33).append(f37).toString();
		
		return field90;
		
	}
	
	public void send200TopupPendingFromBank( String cardNumber,
											 String amount,
											 String channelType,
											 String currentBalance,
											 String cardUID,
											 String cardType){
		
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48(FROM_ACCOUNT_NUMBER, 
													  cardNumber, 
													  amount, 
													  "", 
													  "", 
													  currentBalance);
		
		String f57 					= generateField57TopupFromATM(cardNumber, amount, currentBalance, cardUID, cardType);
		
		msg.setValue(2, cardNumber, IsoType.LLVAR, 0);
		msg.setValue(3, "860000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, channelType, IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "01111111", IsoType.ALPHA, 8);
		msg.setValue(42, "11223344", IsoType.ALPHA, 15);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(48, f48, IsoType.LLLVAR, 0);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}
	
	public void send200TopupOnlineFromBank( String cardNumber,
											 String amount,
											 String channelType,
											 String currentBalance,
											 String cardUID,
											 String cardType){
								
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48(FROM_ACCOUNT_NUMBER, 
							  cardNumber, 
							  amount, 
							  "", 
							  "", 
							  currentBalance);
		
		String f57 					= generateField57TopupFromATM(cardNumber, amount, currentBalance, cardUID, cardType);
		
		msg.setValue(2, cardNumber, IsoType.LLVAR, 0);
		msg.setValue(3, "850000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, channelType, IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "01111111", IsoType.ALPHA, 8);
		msg.setValue(42, "11223344", IsoType.ALPHA, 15);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(48, f48, IsoType.LLLVAR, 0);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);

	}
	
	public void send200TopupWalletFromBank( String walletAccount,
											 String amount,
											 String channelType){
								
		IsoMessage msg				= mfact.newMessage(512);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		
		msg.setValue(2, "1234567890123456", IsoType.LLVAR, 0);
		msg.setValue(3, "870000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, channelType, IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(41, "00000123", IsoType.ALPHA, 8);
		msg.setValue(42, "1234567", IsoType.ALPHA, 15);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(104, "0000166281315414954", IsoType.LLVAR, 0);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
	
	}
	
	public void send420( String cardNumber,
						 String amount,
						 String channelType,
						 String currentBalance,
						 String f7_original,
						 String f11_original,
						 String f32_original,
						 String f33_original,
						 String f37_original){
								
		IsoMessage msg				= mfact.newMessage(1056);
		String s9 					= multiSocket.util.localTransactionTime();
		String f37 					= multiSocket.util.randomNumeric(12);
		String f48 					= generateField48(FROM_ACCOUNT_NUMBER, 
													  cardNumber, 
													  amount, 
													  "", 
													  "", 
													  currentBalance);
		
		String f57 					= generateField57TopupFromATM(cardNumber, amount, currentBalance, "", "");
		String f90					= generateField90(f7_original, f11_original, f32_original, f33_original, f37_original);
		
		msg.setValue(2, cardNumber, IsoType.LLVAR, 0);
		msg.setValue(3, "850000", IsoType.NUMERIC, 6);
		msg.setValue(4, amount, IsoType.NUMERIC, 12);
		msg.setValue(7, s9, IsoType.NUMERIC, 10);
		msg.setValue(12, s9.substring(4), IsoType.TIME, 6);
		msg.setValue(13, s9.substring(0,4), IsoType.DATE4, 4);
		msg.setValue(18, channelType, IsoType.NUMERIC, 4);
		msg.setValue(22, "021", IsoType.NUMERIC, 3);
		msg.setValue(32, "016", IsoType.LLVAR, 0);
		msg.setValue(33, "016", IsoType.LLVAR, 0);
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(39, "68", IsoType.ALPHA, 2);
		msg.setValue(41, "01234567", IsoType.ALPHA, 8);
		msg.setValue(42, "11223344", IsoType.ALPHA, 15);
		msg.setValue(43, CARD_ACCEPTOR_LOCATION, IsoType.ALPHA, 40);
		msg.setValue(48, f48, IsoType.LLLVAR, 0);
		msg.setValue(57, f57, IsoType.LLLVAR, 0);
		msg.setValue(90, f90, IsoType.NUMERIC, 54);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
	
	}
	
	public void send9200Acknowledge( IsoMessage isoMsg,
									 String f4,
									 String f11,
									 String f37,
									 String f39){

		IsoMessage msg				= mfact.newMessage(37376);
		String s9 					= multiSocket.util.localTransactionTime();
		
		msg.setField(2, isoMsg.getField(2));
		msg.setField(3, isoMsg.getField(3));
		//msg.setField(4, isoMsg.getField(4));
		msg.setValue(4, f4, IsoType.NUMERIC, 12);
		msg.setField(7, isoMsg.getField(7));
		msg.setValue(11, f11, IsoType.NUMERIC, 6);
		msg.setField(12, isoMsg.getField(12));
		msg.setField(13, isoMsg.getField(13));
		msg.setField(18, isoMsg.getField(18));
		msg.setField(22, isoMsg.getField(22));
		msg.setField(32, isoMsg.getField(32));
		msg.setField(33, isoMsg.getField(33));
		msg.setValue(37, f37, IsoType.ALPHA, 12);
		msg.setValue(39, f39, IsoType.ALPHA, 2);
		msg.setField(41, isoMsg.getField(41));
		msg.setField(42, isoMsg.getField(42));
		msg.setField(43, isoMsg.getField(43));
		msg.setField(57, isoMsg.getField(57));
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}
	
}
