package services;

import java.net.Socket;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;

import utility.Utility;

public class SendKeyExchange {
	
	private Socket socket;
	private MessageFactory mfact;
	private Utility util = new Utility();
	
	private String f48DL = "1138PK00001111B7EFECC0ACC727E04CAA19C67F17FBB1436F81";
	
	public SendKeyExchange( Socket socket, MessageFactory messagefactory ){
		
		this.socket = socket;
		this.mfact  = messagefactory;
		
	}

	public void sendKeyExchange( String cbc ){
		
		IsoMessage msg			= mfact.newMessage(2048);
		String f7 				= multiSocket.util.localTransactionTime();
		
		msg.setValue(2, cbc, IsoType.LLVAR, 19);
		msg.setValue(7, f7, IsoType.NUMERIC, 10);
		msg.setValue(33, cbc, IsoType.LLVAR, 11);
		msg.setValue(48, f48DL, IsoType.LLLVAR, 0);
		msg.setValue(70, "101", IsoType.NUMERIC, 3);
		
		multiSocket.parsingField(msg);
		multiSocket.sendISO(socket, msg);
		
	}
	
}
