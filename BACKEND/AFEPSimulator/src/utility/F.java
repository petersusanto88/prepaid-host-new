package utility;

import java.util.Hashtable;

import services.multiSocket;



import com.solab.iso8583.IsoMessage;

public class F 
{

    public static String FIELD_3_CEK_SALDO			= "30";
    public static String FIELD_3_TARIK_TUNAI		= "01";
    public static String FIELD_3_INQUIRY_TRANSFER	= "34";
    public static String FIELD_3_TRANSFER			= "41";
    public static String FIELD_3_BILLER				= "50";
    
    public static String FIELD_63_INQUIRY			= "00";
    public static String FIELD_63_DEBIT				= "01";
    public static String FIELD_63_CREDIT			= "02";
    
    
    
    public static String parsingMessage( IsoMessage isomessage )
    {
    	String s4 = "";
    	
        if(isomessage.hasField(2))
            s4 = (new StringBuilder()).append(s4).append("Primary Account Number=").append(isomessage.getField(2).toString()).append("|").toString();
        if(isomessage.hasField(3))
            s4 = (new StringBuilder()).append(s4).append("Processing Code=").append(isomessage.getField(3).toString()).append("|").toString();
        if(isomessage.hasField(4))
            s4 = (new StringBuilder()).append(s4).append("Amount Transaction=").append(isomessage.getField(4).toString()).append("|").toString();
        if(isomessage.hasField(7))
            s4 = (new StringBuilder()).append(s4).append("Transaction Date and Time=").append(isomessage.getField(7).toString()).append("|").toString();
        if(isomessage.hasField(11))
            s4 = (new StringBuilder()).append(s4).append("System Trace Audit Number=").append(isomessage.getField(11).toString()).append("|").toString();
        if(isomessage.hasField(12))
            s4 = (new StringBuilder()).append(s4).append("Time, Local Transaction=").append(isomessage.getField(12).toString()).append("|").toString();
        if(isomessage.hasField(13))
            s4 = (new StringBuilder()).append(s4).append("Date, Local Transaction=").append(isomessage.getField(13).toString()).append("|").toString();
        if(isomessage.hasField(15))
            s4 = (new StringBuilder()).append(s4).append("Date, Settlement=").append(isomessage.getField(15).toString()).append("|").toString();
        if(isomessage.hasField(18))
            s4 = (new StringBuilder()).append(s4).append("Merchant Type=").append(isomessage.getField(18).toString()).append("|").toString();
        if(isomessage.hasField(22))
            s4 = (new StringBuilder()).append(s4).append("POS Entry Mode=").append(isomessage.getField(22).toString()).append("|").toString();
        if(isomessage.hasField(26))
            s4 = (new StringBuilder()).append(s4).append("POS Pin Capture Code=").append(isomessage.getField(26).toString()).append("|").toString();
        if(isomessage.hasField(29))
            s4 = (new StringBuilder()).append(s4).append("Amount, Settlement Fee=").append(isomessage.getField(29).toString()).append("|").toString();
        if(isomessage.hasField(31))
            s4 = (new StringBuilder()).append(s4).append("Amount, Settlement Processing Fee=").append(isomessage.getField(31).toString()).append("|").toString();
        if(isomessage.hasField(32))
            s4 = (new StringBuilder()).append(s4).append("Acquiring Institution ID Code=").append(isomessage.getField(32).toString()).append("|").toString();
        if(isomessage.hasField(33))
            s4 = (new StringBuilder()).append(s4).append("Forwarding Institution ID Code=").append(isomessage.getField(33).toString()).append("|").toString();
        if(isomessage.hasField(34))
            s4 = (new StringBuilder()).append(s4).append("Token Data=").append(isomessage.getField(34).toString()).append("|").toString();
        if(isomessage.hasField(35))
            s4 = (new StringBuilder()).append(s4).append("Track 2 Data=").append(isomessage.getField(35).toString()).append("|").toString();
        if(isomessage.hasField(37))
            s4 = (new StringBuilder()).append(s4).append("Retrieval Reference Number=").append(isomessage.getField(37).toString()).append("|").toString();
        if(isomessage.hasField(41))
            s4 = (new StringBuilder()).append(s4).append("Card Acceptor Terminal ID=").append(isomessage.getField(41).toString()).append("|").toString();
        if(isomessage.hasField(43))
            s4 = (new StringBuilder()).append(s4).append("Card Acceptor Name Location=").append(isomessage.getField(43).toString()).append("|").toString();
        if(isomessage.hasField(48))
            s4 = (new StringBuilder()).append(s4).append("Private Field=").append(isomessage.getField(48).toString()).append("|").toString();
        if(isomessage.hasField(49))
            s4 = (new StringBuilder()).append(s4).append("Currency Code, Transaction=").append(isomessage.getField(49).toString()).append("|").toString();
        if(isomessage.hasField(50))
            s4 = (new StringBuilder()).append(s4).append("Currency Code, Settlement=").append(isomessage.getField(50).toString()).append("|").toString();
        if(isomessage.hasField(52))
            s4 = (new StringBuilder()).append(s4).append("PIN Data=").append(isomessage.getField(52).toString()).append("|").toString();
        if(isomessage.hasField(54))
            s4 = (new StringBuilder()).append(s4).append("Additional Amounts=").append(isomessage.getField(54).toString()).append("|").toString();
        if(isomessage.hasField(60))
            s4 = (new StringBuilder()).append(s4).append("Advice Reason Code=").append(isomessage.getField(60).toString()).append("|").toString();
        if(isomessage.hasField(61))
            s4 = (new StringBuilder()).append(s4).append("POS Data=").append(isomessage.getField(61).toString()).append("|").toString();
        if(isomessage.hasField(63))
            s4 = (new StringBuilder()).append(s4).append("Private Field=").append(isomessage.getField(63).toString()).append("|").toString();
        if(isomessage.hasField(76))
            s4 = (new StringBuilder()).append(s4).append("Debits, Number=").append(isomessage.getField(76).toString()).append("|").toString();
        if(isomessage.hasField(77))
            s4 = (new StringBuilder()).append(s4).append("Debits, Reversal Number=").append(isomessage.getField(77).toString()).append("|").toString();
        if(isomessage.hasField(80))
            s4 = (new StringBuilder()).append(s4).append("Inquiries, Number=").append(isomessage.getField(80).toString()).append("|").toString();
        if(isomessage.hasField(84))
            s4 = (new StringBuilder()).append(s4).append("Debits, Processing Fee Amount=").append(isomessage.getField(84).toString()).append("|").toString();
        if(isomessage.hasField(85))
            s4 = (new StringBuilder()).append(s4).append("Debits, Transaction Fee Amount=").append(isomessage.getField(85).toString()).append("|").toString();
        if(isomessage.hasField(89))
            s4 = (new StringBuilder()).append(s4).append("Debits, Reversal Amount=").append(isomessage.getField(89).toString()).append("|").toString();
        if(isomessage.hasField(90))
            s4 = (new StringBuilder()).append(s4).append("Original Data=").append(isomessage.getField(90).toString()).append("|").toString();
        if(isomessage.hasField(95))
            s4 = (new StringBuilder()).append(s4).append("Replacement Amounts=").append(isomessage.getField(95).toString()).append("|").toString();
        if(isomessage.hasField(97))
            s4 = (new StringBuilder()).append(s4).append("Amount, Net Settlement=").append(isomessage.getField(97).toString()).append("|").toString();
        if(isomessage.hasField(99))
            s4 = (new StringBuilder()).append(s4).append("Settlement Institution Identification Code=").append(isomessage.getField(99).toString()).append("|").toString();
        if(isomessage.hasField(100))
            s4 = (new StringBuilder()).append(s4).append("Receiving Institution Identification Code=").append(isomessage.getField(100).toString()).append("|").toString();
        if(isomessage.hasField(102))
            s4 = (new StringBuilder()).append(s4).append("Account Identification-1=").append(isomessage.getField(102).toString()).append("|").toString();
        if(isomessage.hasField(104))
            s4 = (new StringBuilder()).append(s4).append("Transaction Description=").append(isomessage.getField(104).toString()).append("|").toString();
        
        return s4;
    }
    
    private Hashtable parsingPLN(String s)
    {
        if(s.trim().compareTo("") == 0)
            return null;
        Hashtable hashtable = new Hashtable();
        String s1 = "";
        if(s.length() > 20)
        {
            String s2 = s.substring(0, 20);
            s = s.substring(20);
            hashtable.put("Billing Account Number", s2);
        } else
        {
            hashtable.put("Billing Account Number", s);
            return hashtable;
        }
        if(s.length() > 25)
        {
            String s3 = s.substring(0, 25);
            s = s.substring(25);
            hashtable.put("Customer Name", s3);
        } else
        {
            hashtable.put("Customer Name", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s4 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("Segmentation", s4);
        } else
        {
            hashtable.put("Segmentation", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s5 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Category", s5);
        } else
        {
            hashtable.put("Category", s);
            return hashtable;
        }
        if(s.length() > 1)
        {
            String s6 = s.substring(0, 1);
            s = s.substring(1);
            hashtable.put("Bill Status", s6);
        } else
        {
            hashtable.put("Bill Status", s);
            return hashtable;
        }
        if(s.length() > 2)
        {
            String s7 = s.substring(0, 2);
            s = s.substring(2);
            hashtable.put("Total Outstanding", s7);
        } else
        {
            hashtable.put("Total Outstanding", s);
            return hashtable;
        }
        if(s.length() > 32)
        {
            String s8 = s.substring(0, 32);
            s = s.substring(32);
            hashtable.put("Bill Reference", s8);
        } else
        {
            hashtable.put("Bill Reference", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s9 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("Start Bill Period", s9);
        } else
        {
            hashtable.put("Start Bill Period", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s10 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("End Bill Period", s10);
        } else
        {
            hashtable.put("End Bill Period", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s11 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Amount", s11);
        } else
        {
            hashtable.put("Total Bill Amount", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s12 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Penalty", s12);
        } else
        {
            hashtable.put("Total Penalty", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s13 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Other Amount", s13);
        } else
        {
            hashtable.put("Other Amount", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s14 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (A)", s14);
        } else
        {
            hashtable.put("Bill Period (A)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s15 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (A)", s15);
        } else
        {
            hashtable.put("Due Date (A)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s16 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (A)", s16);
        } else
        {
            hashtable.put("Total Bill Period (A)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s17 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (A)", s17);
        } else
        {
            hashtable.put("Incentive / Disincentive (A)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s18 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax (A)", s18);
        } else
        {
            hashtable.put("Value Added Tax (A)", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s19 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (A)", s19);
        } else
        {
            hashtable.put("Penalti Fee (A)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s20 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 1 (A)", s20);
        } else
        {
            hashtable.put("Bill Misc 1 (A)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s21 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 2 (A)", s21);
        } else
        {
            hashtable.put("Bill Misc 2 (A)", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s22 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (B)", s22);
        } else
        {
            hashtable.put("Bill Period (B)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s23 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (B)", s23);
        } else
        {
            hashtable.put("Due Date (B)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s24 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (B)", s24);
        } else
        {
            hashtable.put("Total Bill Period (B)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s25 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (B)", s25);
        } else
        {
            hashtable.put("Incentive / Disincentive (B)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s26 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax (B)", s26);
        } else
        {
            hashtable.put("Value Added Tax (B)", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s27 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (B)", s27);
        } else
        {
            hashtable.put("Penalti Fee (B)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s28 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 1 (B)", s28);
        } else
        {
            hashtable.put("Bill Misc 1 (B)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s29 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 2 (B)", s29);
        } else
        {
            hashtable.put("Bill Misc 2 (B)", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s30 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (C)", s30);
        } else
        {
            hashtable.put("Bill Period (C)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s31 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (C)", s31);
        } else
        {
            hashtable.put("Due Date (C)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s32 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (C)", s32);
        } else
        {
            hashtable.put("Total Bill Period (C)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s33 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (C)", s33);
        } else
        {
            hashtable.put("Incentive / Disincentive (C)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s34 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax (C)", s34);
        } else
        {
            hashtable.put("Value Added Tax (C)", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s35 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (C)", s35);
        } else
        {
            hashtable.put("Penalti Fee (C)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s36 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 1 (C)", s36);
        } else
        {
            hashtable.put("Bill Misc 1 (C)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s37 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 2 (C)", s37);
        } else
        {
            hashtable.put("Bill Misc 2 (C)", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s38 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (D)", s38);
        } else
        {
            hashtable.put("Bill Period (D)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s39 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (D)", s39);
        } else
        {
            hashtable.put("Due Date (D)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s40 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (D)", s40);
        } else
        {
            hashtable.put("Total Bill Period (D)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s41 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (D)", s41);
        } else
        {
            hashtable.put("Incentive / Disincentive (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s42 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax (D)", s42);
        } else
        {
            hashtable.put("Value Added Tax (D)", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s43 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (D)", s43);
        } else
        {
            hashtable.put("Penalti Fee (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s44 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 1 (D)", s44);
        } else
        {
            hashtable.put("Bill Misc 1 (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s45 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 2 (D)", s45);
        } else
        {
            hashtable.put("Bill Misc 2 (D)", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s46 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #1", s46);
        } else
        {
            hashtable.put("Ads Line #1", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s47 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #2", s47);
        } else
        {
            hashtable.put("Ads Line #2", s);
            return hashtable;
        }
        if(s.length() > 15)
        {
            String s48 = s.substring(0, 15);
            s = s.substring(15);
            hashtable.put("Phone Number", s48);
        } else
        {
            hashtable.put("Phone Number", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s49 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Reserved #1", s49);
        } else
        {
            hashtable.put("Reserved #1", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s50 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Reserved #2", s50);
        } else
        {
            hashtable.put("Reserved #2", s);
            return hashtable;
        }
        if(s.compareTo("") != 0)
            hashtable.put("Reserved #3", s);
        return hashtable;
    }

    private Hashtable parsingTransfer(String s)
    {
        if(s.trim().compareTo("") == 0)
            return null;
        Hashtable hashtable = new Hashtable();
        String s1 = "";
        if(s.length() > 18)
        {
            String s2 = s.substring(0, 18);
            s = s.substring(18);
            hashtable.put("Bill Id", s2);
        } else
        {
            hashtable.put("Bill Id", s);
            return hashtable;
        }
        if(s.length() > 18)
        {
            String s3 = s.substring(0, 18);
            s = s.substring(18);
            hashtable.put("Account Id", s3);
        } else
        {
            hashtable.put("Account Id", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s4 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Bill Amount", s4);
        } else
        {
            hashtable.put("Bill Amount", s);
            return hashtable;
        }
        if(s.compareTo("") != 0)
            hashtable.put("Account Owner", s);
        return hashtable;
    }

    private Hashtable parsingTelkomSpeedy(String s)
    {
        if(s.trim().compareTo("") == 0)
            return null;
        Hashtable hashtable = new Hashtable();
        String s1 = "";
        if(s.length() > 20)
        {
            String s2 = s.substring(0, 20);
            s = s.substring(20);
            hashtable.put("Billing Account Number", s2);
        } else
        {
            hashtable.put("Billing Account Number", s);
            return hashtable;
        }
        if(s.length() > 30)
        {
            String s3 = s.substring(0, 30);
            s = s.substring(30);
            hashtable.put("Customer Name", s3);
        } else
        {
            hashtable.put("Customer Name", s);
            return hashtable;
        }
        if(s.length() > 1)
        {
            String s4 = s.substring(0, 1);
            s = s.substring(1);
            hashtable.put("Bill Status", s4);
        } else
        {
            hashtable.put("Bill Status", s);
            return hashtable;
        }
        if(s.length() > 20)
        {
            String s5 = s.substring(0, 20);
            s = s.substring(20);
            hashtable.put("Bill Reference", s5);
        } else
        {
            hashtable.put("Bill Reference", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s6 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("Start Bill Period", s6);
        } else
        {
            hashtable.put("Start Bill Period", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s7 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("End Bill Period", s7);
        } else
        {
            hashtable.put("End Bill Period", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s8 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Bill Amount", s8);
        } else
        {
            hashtable.put("Bill Amount", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s9 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Other Amount", s9);
        } else
        {
            hashtable.put("Other Amount", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s10 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Bill Misc 1", s10);
        } else
        {
            hashtable.put("Bill Misc 1", s);
            return hashtable;
        }
        if(s.compareTo("") != 0)
            hashtable.put("Bill Misc 2", s);
        return hashtable;
    }

    private Hashtable parsingPLNPrepaid(String s)
    {
        if(s.trim().compareTo("") == 0)
            return null;
        Hashtable hashtable = new Hashtable();
        String s1 = "";
        if(s.length() > 20)
        {
            String s2 = s.substring(0, 20);
            s = s.substring(20);
            hashtable.put("Billing Account Number", s2);
        } else
        {
            hashtable.put("Billing Account Number", s);
            return hashtable;
        }
        if(s.length() > 25)
        {
            String s3 = s.substring(0, 25);
            s = s.substring(25);
            hashtable.put("Customer Name", s3);
        } else
        {
            hashtable.put("Customer Name", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s4 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("Segmentation", s4);
        } else
        {
            hashtable.put("Segmentation", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s5 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Category", s5);
        } else
        {
            hashtable.put("Category", s);
            return hashtable;
        }
        if(s.length() > 1)
        {
            String s6 = s.substring(0, 1);
            s = s.substring(1);
            hashtable.put("Bill Status", s6);
        } else
        {
            hashtable.put("Bill Status", s);
            return hashtable;
        }
        if(s.length() > 2)
        {
            String s7 = s.substring(0, 2);
            s = s.substring(2);
            hashtable.put("Total Outstanding Bill", s7);
        } else
        {
            hashtable.put("Total Outstanding Bill", s);
            return hashtable;
        }
        if(s.length() > 32)
        {
            String s8 = s.substring(0, 32);
            s = s.substring(32);
            hashtable.put("Bill Reference", s8);
        } else
        {
            hashtable.put("Bill Reference", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s9 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("Start Bill Period", s9);
        } else
        {
            hashtable.put("Start Bill Period", s);
            return hashtable;
        }
        if(s.length() > 4)
        {
            String s10 = s.substring(0, 4);
            s = s.substring(4);
            hashtable.put("End Bill Period", s10);
        } else
        {
            hashtable.put("End Bill Period", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s11 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Amount", s11);
        } else
        {
            hashtable.put("Total Bill Amount", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s12 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Purchased KWH Unit", s12);
        } else
        {
            hashtable.put("Purchased KWH Unit", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s13 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Admin Charge", s13);
        } else
        {
            hashtable.put("Admin Charge", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s14 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (A)", s14);
        } else
        {
            hashtable.put("Bill Period (A)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s15 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (A)", s15);
        } else
        {
            hashtable.put("Due Date (A)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s16 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (A)", s16);
        } else
        {
            hashtable.put("Total Bill Period (A)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s17 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (A)", s17);
        } else
        {
            hashtable.put("Incentive / Disincentive (A)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s18 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax", s18);
        } else
        {
            hashtable.put("Value Added Tax", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s19 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (A)", s19);
        } else
        {
            hashtable.put("Penalti Fee (A)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s20 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Public Lightning Tax", s20);
        } else
        {
            hashtable.put("Public Lightning Tax", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s21 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Customer Payable Installmet", s21);
        } else
        {
            hashtable.put("Customer Payable Installmet", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s22 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (B)", s22);
        } else
        {
            hashtable.put("Bill Period (B)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s23 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (B)", s23);
        } else
        {
            hashtable.put("Due Date (B)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s24 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Power Purchase", s24);
        } else
        {
            hashtable.put("Power Purchase", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s25 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (B)", s25);
        } else
        {
            hashtable.put("Incentive / Disincentive (B)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s26 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Stamp Duty", s26);
        } else
        {
            hashtable.put("Stamp Duty", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s27 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (B)", s27);
        } else
        {
            hashtable.put("Penalti Fee (B)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s28 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Subscriber ID #1", s28);
        } else
        {
            hashtable.put("Subscriber ID #1", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s29 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Subscriber ID #2", s29);
        } else
        {
            hashtable.put("Subscriber ID #2", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s30 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (C)", s30);
        } else
        {
            hashtable.put("Bill Period (C)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s31 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (C)", s31);
        } else
        {
            hashtable.put("Due Date (C)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s32 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("PLN Reference Number #1", s32);
        } else
        {
            hashtable.put("PLN Reference Number #1", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s33 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("PLN Reference Number #2", s33);
        } else
        {
            hashtable.put("PLN Reference Number #2", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s34 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("PLN Reference Number #3", s34);
        } else
        {
            hashtable.put("PLN Reference Number #3", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s35 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (C)", s35);
        } else
        {
            hashtable.put("Penalti Fee (C)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s36 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Token #1", s36);
        } else
        {
            hashtable.put("Token #1", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s37 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Token #2", s37);
        } else
        {
            hashtable.put("Token #2", s);
            return hashtable;
        }
        if(s.length() > 6)
        {
            String s38 = s.substring(0, 6);
            s = s.substring(6);
            hashtable.put("Bill Period (D)", s38);
        } else
        {
            hashtable.put("Bill Period (D)", s);
            return hashtable;
        }
        if(s.length() > 8)
        {
            String s39 = s.substring(0, 8);
            s = s.substring(8);
            hashtable.put("Due Date (D)", s39);
        } else
        {
            hashtable.put("Due Date (D)", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s40 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Total Bill Period (D)", s40);
        } else
        {
            hashtable.put("Total Bill Period (D)", s);
            return hashtable;
        }
        if(s.length() > 11)
        {
            String s41 = s.substring(0, 11);
            s = s.substring(11);
            hashtable.put("Incentive / Disincentive (D)", s41);
        } else
        {
            hashtable.put("Incentive / Disincentive (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s42 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Value Added Tax (D)", s42);
        } else
        {
            hashtable.put("Value Added Tax (D)", s);
            return hashtable;
        }
        if(s.length() > 9)
        {
            String s43 = s.substring(0, 9);
            s = s.substring(9);
            hashtable.put("Penalti Fee (D)", s43);
        } else
        {
            hashtable.put("Penalti Fee (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s44 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 1 (D)", s44);
        } else
        {
            hashtable.put("Bill Misc 1 (D)", s);
            return hashtable;
        }
        if(s.length() > 10)
        {
            String s45 = s.substring(0, 10);
            s = s.substring(10);
            hashtable.put("Bill Misc 2 (D)", s45);
        } else
        {
            hashtable.put("Bill Misc 2 (D)", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s46 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #1", s46);
        } else
        {
            hashtable.put("Ads Line #1", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s47 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #2", s47);
        } else
        {
            hashtable.put("Ads Line #2", s);
            return hashtable;
        }
        if(s.length() > 15)
        {
            String s48 = s.substring(0, 15);
            s = s.substring(15);
            hashtable.put("Phone Number", s48);
        } else
        {
            hashtable.put("Phone Number", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s49 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #3", s49);
        } else
        {
            hashtable.put("Ads Line #3", s);
            return hashtable;
        }
        if(s.length() > 40)
        {
            String s50 = s.substring(0, 40);
            s = s.substring(40);
            hashtable.put("Ads Line #4", s50);
        } else
        {
            hashtable.put("Ads Line #4", s);
            return hashtable;
        }
        if(s.compareTo("") != 0)
            hashtable.put("Ads Line #5", s);
        return hashtable;
    }
    
    private Hashtable parsingToppulsa(String s)
    {
        if(s.trim().compareTo("") == 0)
            return null;
        Hashtable hashtable = new Hashtable();
        String s1 = "";
        if(s.length() > 36)
        {
            String s2 = s.substring(0, 36);
            s = s.substring(36);
            hashtable.put("No HP Dest", s2);
        } else
        {
            hashtable.put("No HP Dest", s);
            return hashtable;
        }
        if(s.length() > 12)
        {
            String s3 = s.substring(0, 12);
            s = s.substring(12);
            hashtable.put("Voucher", s3);
        } else
        {
            hashtable.put("Voucher", s);
            return hashtable;
        }
        if(s.length() > 52)
        {
            String s4 = s.substring(0);
            s = s.substring(0);
            hashtable.put("Operator", s4);
        } else
        {
            hashtable.put("Operator", s);
            return hashtable;
        }
        
        return hashtable;
    }
        
  
    public static String getTraceID( IsoMessage isomessage )
	{
		String traceid = "-1";
        if( isomessage.hasField(7) && isomessage.hasField(11) )
        	traceid = isomessage.getField(7).toString() + isomessage.getField(11).toString();
//        if( isomessage.hasField(11) )
//        	traceid = isomessage.getField(11).toString();
        return traceid;
	}
    
    public static String getTraceNumber( IsoMessage isomessage )
	{
		String tracenumber = "-1";
        if( isomessage.hasField(37) ) tracenumber = isomessage.getField(37).toString();
        return tracenumber;
	}

    
    public static String getField( IsoMessage isomessage, int index )
	{
		String field = "";
		if( isomessage.hasField( index ) ) field = isomessage.getField( index ).toString();
		return field;
	}

    public static int getFieldInt( IsoMessage isomessage, int index )
	{
		String field = "-1";
		try{
			if( isomessage.hasField( index ) ) field = isomessage.getField( index ).toString();
		}catch (Exception e) {}
		return (int) Integer.parseInt( field );
	}
    
    public static void switchISO( int index, IsoMessage isoSource , IsoMessage isoDestination )
    {
    	if( isoSource.hasField( index ) )
    	{
    		isoDestination.setField( index, isoSource.getField( index ) );
    	}
    }
    
    public static void switchISO( 
    		int indexSource, IsoMessage isoSource, 
    		int indexDestination, IsoMessage isoDestination )
    {
    	if( isoSource.hasField( indexSource ) )
    	{
    		isoDestination.setField( indexDestination, isoSource.getField( indexSource ) );
    	}
    }



}
