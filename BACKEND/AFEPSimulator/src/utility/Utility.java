// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 04/10/2012 7:41:37
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Utility.java

package utility;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Utility
{
	
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static final String N = "0123456789";
	static SecureRandom rnd = new SecureRandom();

    public Utility()
    {
    }

    public static String deleteWhiteSpace(String s)
    {
        s = s.replaceAll("\r", "");
        s = s.replaceAll("\n", "");
        s = s.replaceAll("\r\n", "");
        s = s.replaceAll("\t", "");
        s = s.trim();
        return s;
    }

    public static boolean isTime(String s)
    {
        String as[] = s.split("\\:");
        Calendar calendar = Calendar.getInstance();
        calendar.set(9, 0);
        if(as[0] != null)
            calendar.set(10, Integer.parseInt(as[0]));
        if(as[1] != null)
            calendar.set(12, Integer.parseInt(as[1]));
        if(as.length > 2 && as[2] != null)
            calendar.set(13, Integer.parseInt(as[2]));
        Calendar calendar1 = Calendar.getInstance();
        return calendar.before(calendar1);
    }

    public static String nextTimeInterval(int i)
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        long l = date.getTime();
        l += i * 1000;
        date.setTime(l);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kk:mm");
        return simpledateformat.format(date);
    }

    public static String currentDateTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
        return simpledateformat.format(date);
    }

    public static String currentDate()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
        return simpledateformat.format(date);
    }
    
    public static String formatDate( String format )
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
//        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat simpledateformat = new SimpleDateFormat( format );
        return simpledateformat.format(date);
    }
    

    public static String localTransactionTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MMddkkmmss");
        return simpledateformat.format(date);
    }

    public static String localDate()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MMdd");
        return simpledateformat.format(date);
    }
    
    public static String localTime()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("kkmmss");
        return simpledateformat.format(date);
    }
    
    public static String stripMSISDN(String s)
    {
        String s1 = "";
        if(s.substring(0, 1).compareTo("+") == 0)
            s1 = s.substring(3);
        else
        if(s.substring(0, 1).compareTo("0") == 0)
            s1 = s.substring(1);
        else
        if(s.substring(0, 2).compareTo("62") == 0)
            s1 = s.substring(2);
        else
            s1 = s;
        return s1;
    }

    public static String formatMSISDN(String s)
    {
        String s1 = "";
        if(s.substring(0, 1).compareTo("+") == 0)
            s1 = (new StringBuilder()).append("0").append(s.substring(3)).toString();
        else
        if(s.substring(0, 2).compareTo("62") == 0)
            s1 = (new StringBuilder()).append("0").append(s.substring(2)).toString();
        else
            s1 = s;
        return s1;
    }
    
	 public static String sendRequestPOST( String address, String urlParameters )
	 {
	    	String msg="";
			String s3 = "";
//			String request = address;
			if(!address.startsWith( "http" ) ) address = "http://"+address;
			
			System.out.println(address+"?"+urlParameters);
			try
	        {
				URL url = new URL(address);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();           
				connection.setDoOutput(true);
				connection.setDoInput(true);
				connection.setInstanceFollowRedirects(false); 
				connection.setRequestMethod("POST"); 
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
				connection.setRequestProperty("charset", "utf-8");
				connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
				connection.setUseCaches (false);
				OutputStream outputstream = connection.getOutputStream();
	            outputstream.write(urlParameters.getBytes());
	            outputstream.close();
	            int i = connection.getResponseCode();
	            if(i == 200)
	            {
	                InputStream inputstream = connection.getInputStream();
	                int j = connection.getContentLength();
	                if(j > 0)
	                {
	                    byte abyte0[] = new byte[j];
	                    int k;
	                    for(int j1 = 0; (k = inputstream.read()) != -1; j1++)
	                        abyte0[j1] = (byte)k;

	                    s3 = new String(abyte0);
	                } else
	                {
	                    char c = '\u0800';
	                    byte abyte1[] = new byte[c];
	                    int k1 = 0;
	                    do
	                    {
	                        int i1;
	                        if((i1 = inputstream.read()) == -1)
	                            break;
	                        abyte1[k1] = (byte)i1;
	                        if(++k1 == c)
	                        {
	                            s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
	                            abyte1 = new byte[c];
	                            k1 = 0;
	                        }
	                    } while(true);
	                    if(k1 < c)
	                        s3 = (new StringBuilder()).append(s3).append(new String(abyte1)).toString();
	                }
	            }
	            
	            s3 = s3.trim();
	            msg=s3;
	            
	        }catch(Exception ex){
	        	
	        }
			return msg;
		}

	    public static int random(int min, int max) {
	        Random rand = new Random();
	        int randomNum = rand.nextInt((max - min) + 1) + min;
	        return randomNum;
	    }
	    
	    public String leftPad(String value, int number_of_padding, String padding){	  
	    	
	    	int difference = 0;
	    	
	    	String result = "";

	    	if( value.length() < number_of_padding ){
	    		
	    		difference = number_of_padding - value.length();
	    		
	    		for( int i = 0; i < difference; i++ ){  
	    			
	    			result += padding;
	    			
	    		}
	    		
	    		result += value;
	    		
	    	}
	    	
	    	return result;
	    	
	    }
	    
	    public String randomAlphaNumeric( int len ){
    	   StringBuilder sb = new StringBuilder( len );
    	   for( int i = 0; i < len; i++ ) 
    	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
    	   return sb.toString();
    	}
	    
	    public String randomNumeric( int len ){
	    	   StringBuilder sb = new StringBuilder( len );
	    	   for( int i = 0; i < len; i++ ) 
	    	      sb.append( N.charAt( rnd.nextInt(N.length()) ) );
	    	   return sb.toString();
	    }    
	    
	    	

}
